-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 02:11 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lecture_monitoring`
--

-- --------------------------------------------------------

--
-- Table structure for table `abedailysummary`
--

CREATE TABLE `abedailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abedailysummary`
--

INSERT INTO `abedailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 4),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_table`
--

CREATE TABLE `admin_table` (
  `admin_id` int(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin_img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_table`
--

INSERT INTO `admin_table` (`admin_id`, `email`, `password`, `admin_img`) VALUES
(1, 'admin@admin.com', '$2y$10$O0ya.lBK35tl0r2Ok3The.Ce8pL96yaIdQ2FJARfV98Sx6m3Qdki2', ''),
(2, 'adminlogin@gmail.com', '$2y$10$jxH4IG3IbVG/Jbei7iiKp.l94XSEycn4AqrmFC4fE5CcAiCgc5hWO', '');

-- --------------------------------------------------------

--
-- Table structure for table `aetdailysummary`
--

CREATE TABLE `aetdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aetdailysummary`
--

INSERT INTO `aetdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 11),
(3, 'Wednesday', 11),
(4, 'Thursday', 34),
(5, 'Friday', 23),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `animalproddailysummary`
--

CREATE TABLE `animalproddailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animalproddailysummary`
--

INSERT INTO `animalproddailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 15),
(3, 'Wednesday', 12),
(4, 'Thursday', 13),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `archidailysummary`
--

CREATE TABLE `archidailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `archidailysummary`
--

INSERT INTO `archidailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 12),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `biochemdailysummary`
--

CREATE TABLE `biochemdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biochemdailysummary`
--

INSERT INTO `biochemdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 12),
(4, 'Thursday', 13),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bioscidailysummary`
--

CREATE TABLE `bioscidailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bioscidailysummary`
--

INSERT INTO `bioscidailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 12),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blddailysummary`
--

CREATE TABLE `blddailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blddailysummary`
--

INSERT INTO `blddailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 12),
(5, 'Friday', 11),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chemdailysummary`
--

CREATE TABLE `chemdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chemdailysummary`
--

INSERT INTO `chemdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 15),
(2, 'Tuesday', 14),
(3, 'Wednesday', 13),
(4, 'Thursday', 12),
(5, 'Friday', 11),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chemisdailysummary`
--

CREATE TABLE `chemisdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chemisdailysummary`
--

INSERT INTO `chemisdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 10),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `civildailysummary`
--

CREATE TABLE `civildailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `civildailysummary`
--

INSERT INTO `civildailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 13),
(3, 'Wednesday', 11),
(4, 'Thursday', 14),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comdailysummary`
--

CREATE TABLE `comdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comdailysummary`
--

INSERT INTO `comdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 13),
(3, 'Wednesday', 14),
(4, 'Thursday', 14),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comedududailysummary`
--

CREATE TABLE `comedududailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comedududailysummary`
--

INSERT INTO `comedududailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses_table`
--

CREATE TABLE `courses_table` (
  `course_id` int(8) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `dept_id` int(8) NOT NULL,
  `venue_id` int(8) NOT NULL,
  `level_id` int(1) NOT NULL,
  `credit_unit` int(2) NOT NULL,
  `credit_hour` int(2) NOT NULL,
  `faculty_id` int(3) NOT NULL,
  `lecturer_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses_table`
--

INSERT INTO `courses_table` (`course_id`, `course_title`, `course_code`, `dept_id`, `venue_id`, `level_id`, `credit_unit`, `credit_hour`, `faculty_id`, `lecturer_id`) VALUES
(9, 'Web based Technologies and Multimedia Application', 'IMT 1223', 1, 1, 1, 3, 3, 1, 20),
(10, 'Computer Asisted Reporting', 'IMT 224', 1, 2, 1, 2, 2, 1, 22),
(11, 'Media Studies', 'IMT 224', 1, 2, 2, 2, 2, 1, 23),
(12, 'Web Design with ASP', 'IMT 221', 1, 4, 2, 2, 2, 1, 1),
(13, 'Discrete Mathematics', 'CIT 221', 1, 4, 3, 2, 3, 1, 24),
(14, 'Photography for Multimedia Journalism', 'IMT 3341', 1, 2, 3, 3, 3, 1, 25),
(15, 'Multimedia Video Production', 'IMT 342', 1, 2, 3, 3, 3, 1, 26),
(16, 'Media Theory and Application', 'IMT 343', 1, 4, 3, 2, 2, 1, 27),
(17, 'Practical Computer Networks', 'CIT 322', 1, 2, 3, 3, 3, 1, 29),
(18, 'Structural Query Language', 'CIT 322', 1, 1, 3, 3, 3, 1, 30),
(19, 'Java Desktop Programming', 'CIT 323', 1, 4, 3, 3, 3, 1, 31),
(20, 'System Programming', 'IMT 321', 1, 2, 3, 3, 3, 1, 31),
(21, 'Electronic Commerce', 'IMT 322', 1, 2, 3, 2, 2, 1, 1),
(22, 'Mobile Application Development', 'IMT 323', 1, 2, 3, 2, 2, 1, 32),
(23, 'Computer Security Techniques', 'IMT 526', 1, 1, 5, 2, 2, 1, 32),
(24, 'Multimedia Technology and Programming and Programming', 'IMT 521', 1, 5, 5, 2, 2, 1, 30),
(25, 'Information Retrieval System', 'IMT 524', 1, 1, 5, 2, 2, 1, 33),
(26, 'I.T Laws and Patent', 'CIT 523', 1, 2, 5, 2, 2, 1, 34),
(27, 'Seminar in New Media and Communication Technology', 'MCT 525', 1, 4, 5, 3, 3, 1, 28);

-- --------------------------------------------------------

--
-- Table structure for table `course_reps_table`
--

CREATE TABLE `course_reps_table` (
  `rep_id` int(11) NOT NULL,
  `rep_matric_no` varchar(20) NOT NULL,
  `rep_fname` varchar(255) NOT NULL,
  `rep_sname` varchar(255) NOT NULL,
  `level_id` int(1) NOT NULL,
  `dept_id` int(4) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rep_img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_reps_table`
--

INSERT INTO `course_reps_table` (`rep_id`, `rep_matric_no`, `rep_fname`, `rep_sname`, `level_id`, `dept_id`, `phone`, `email`, `password`, `rep_img`) VALUES
(68, '2014/5474/FT', '2014/5474/FT', 'classrepthree', 3, 1, '12356746534', 'classrepthree@yahoo.com', '$2y$10$O0ya.lBK35tl0r2Ok3The.Ce8pL96yaIdQ2FJARfV98Sx6m3Qdki2', 'Dear Heart.png'),
(66, '2016/1/464CI', '2016/1/464CI', 'classrepone', 1, 1, '09876543213', 'classrepone@yahoo.com', '$2y$10$YCv4XPeAby42AXQVOwkmj.KvpI2ycCinsGN9THkdsNkS1kzLxTIC.', 'microsoft-certified-information-technology-professional-500x500.png'),
(67, '2017/65858', '2017/65858', 'classreptwo', 2, 1, '97653456723', 'classreptwo@yahoo.com', '$2y$10$SGQcGYCwGa2SFKGy12m6Ie3uESirmxyU5.pIKuAnMb9nNDH2RWolC', 'EXAMZ.jpg'),
(69, '20174/5758', '20174/5758', 'classrepfive', 5, 1, '87656453215', 'classrepfive@yahoo.com', '$2y$10$dA0gx5x.POAbFPNc9awduOXch8PJlyF4eXqWQ.mC/kA1f92OF4Y.a', 'CPA-Web-CITP_center_1c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cptdailysummary`
--

CREATE TABLE `cptdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cptdailysummary`
--

INSERT INTO `cptdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 12),
(3, 'Wednesday', 14),
(4, 'Thursday', 11),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cropproddailysummary`
--

CREATE TABLE `cropproddailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cropproddailysummary`
--

INSERT INTO `cropproddailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 15),
(2, 'Tuesday', 13),
(3, 'Wednesday', 13),
(4, 'Thursday', 16),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cssdailysummary`
--

CREATE TABLE `cssdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cssdailysummary`
--

INSERT INTO `cssdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 12),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `department_table`
--

CREATE TABLE `department_table` (
  `dept_id` int(4) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `faculty_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_table`
--

INSERT INTO `department_table` (`dept_id`, `dept_name`, `faculty_id`) VALUES
(1, 'Information and Media Technology', 1),
(2, 'Computer Science.', 1),
(3, 'cyber Security Science.', 1),
(4, 'Mechanical Engineering', 2),
(5, 'Mechatronics Engineering', 2),
(6, 'Agricultural and Bio-resource Engineering', 2),
(7, 'Library and Information Science', 1),
(8, 'Electrical  Electronics Engineering', 2),
(9, 'Computer Engineering', 2),
(10, 'Civil Engineering', 2),
(11, 'Materials and Metallurgical Engineering', 2),
(12, 'Telecommunication Engineering', 2),
(13, 'Biological Science', 8),
(14, 'Building Technology', 3),
(15, 'Architectural Technology', 3),
(17, 'Estate Management and Valuation', 3),
(18, 'Agricultural Extension and Economics', 5),
(20, 'Crop Production Technology', 5),
(21, 'Soil Science and Land Management and Water Resources', 5),
(22, 'Food Science and Nutrition Technology', 5),
(25, 'Aquaculture and Fisheries Technology', 5),
(26, 'Animal Production Technology', 5),
(27, 'Chemical Engineering', 2),
(28, 'Entrepreneurship and Business Studies', 4),
(29, 'Transport Management Technology', 4),
(30, 'Project Management Technology', 4),
(31, 'Quantity Survey', 3),
(32, 'Survey and Geo-informatics', 3),
(33, 'Urban and Regional Planning', 3),
(34, 'Biochemistry', 8),
(35, 'Microbiology', 8),
(36, 'Chemistry', 6),
(37, 'Geography', 6),
(38, 'Geology', 6),
(39, 'Mathematics', 6),
(40, 'Physics', 6),
(41, 'Statistics', 6),
(42, 'Industrial and Technology Education', 7),
(43, 'Science Education', 7),
(44, 'Educational Technology', 7),
(45, 'Communication Education', 7);

-- --------------------------------------------------------

--
-- Table structure for table `ebsdailysummary`
--

CREATE TABLE `ebsdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ebsdailysummary`
--

INSERT INTO `ebsdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `edutechdudailysummary`
--

CREATE TABLE `edutechdudailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edutechdudailysummary`
--

INSERT INTO `edutechdudailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 11),
(3, 'Wednesday', 11),
(4, 'Thursday', 11),
(5, 'Friday', 11),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `electdailysummary`
--

CREATE TABLE `electdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `electdailysummary`
--

INSERT INTO `electdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 13),
(3, 'Wednesday', 4),
(4, 'Thursday', 4),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `estdailysummary`
--

CREATE TABLE `estdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estdailysummary`
--

INSERT INTO `estdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 12),
(4, 'Thursday', 11),
(5, 'Friday', 10),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `faculty_table`
--

CREATE TABLE `faculty_table` (
  `faculty_id` int(3) NOT NULL,
  `faculty_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_table`
--

INSERT INTO `faculty_table` (`faculty_id`, `faculty_name`) VALUES
(1, 'SICT'),
(2, 'SEET'),
(3, 'SET'),
(4, 'SEMT'),
(5, 'SAAT'),
(6, 'SPS'),
(7, 'STE'),
(8, 'SLS');

-- --------------------------------------------------------

--
-- Table structure for table `fisheriesdailysummary`
--

CREATE TABLE `fisheriesdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fisheriesdailysummary`
--

INSERT INTO `fisheriesdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 14),
(2, 'Tuesday', 12),
(3, 'Wednesday', 5),
(4, 'Thursday', 12),
(5, 'Friday', 13),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fstdailysummary`
--

CREATE TABLE `fstdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fstdailysummary`
--

INSERT INTO `fstdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 14),
(3, 'Wednesday', 12),
(4, 'Thursday', 7),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `geodailysummary`
--

CREATE TABLE `geodailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `geodailysummary`
--

INSERT INTO `geodailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 14),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 12),
(5, 'Friday', 13),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `geoldailysummary`
--

CREATE TABLE `geoldailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `geoldailysummary`
--

INSERT INTO `geoldailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 14),
(5, 'Friday', 10),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imtdailysummary`
--

CREATE TABLE `imtdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imtdailysummary`
--

INSERT INTO `imtdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', '12'),
(2, 'Tuesday', '12'),
(3, 'Wednesday', '12'),
(4, 'Thursday', '12'),
(5, 'Friday', '12'),
(6, 'Saturday', '0'),
(7, 'Sunday', '0');

-- --------------------------------------------------------

--
-- Table structure for table `itedailysummary`
--

CREATE TABLE `itedailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itedailysummary`
--

INSERT INTO `itedailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 11),
(4, 'Thursday', 8),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lecturer_table`
--

CREATE TABLE `lecturer_table` (
  `lecturer_id` int(8) NOT NULL,
  `lecturer_fname` varchar(255) NOT NULL,
  `lecturer_sname` varchar(255) NOT NULL,
  `service_no` varchar(10) NOT NULL,
  `dept_id` int(4) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lecturer_img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecturer_table`
--

INSERT INTO `lecturer_table` (`lecturer_id`, `lecturer_fname`, `lecturer_sname`, `service_no`, `dept_id`, `phone`, `email_address`, `password`, `lecturer_img`) VALUES
(1, 'Faiza Jada', 'Babakano', '234DF', 1, '12345678901', 'faiza@yahoo.com', '$2y$10$e8UjqOyX/tQBevxrE7RKyukB42yXKkyNmjn9EkgvxOhelwK5VXP3i', 'infotec2.jpg'),
(20, 'Z. A', 'Hussaini', '6854646', 1, '12673456712', 'zubairu@yahoo.com', '$2y$10$c0oGbQwR.urN7A6FDF6bDuCxFmmtJtse0HQfaVOmd9d8nZdQKm8pK', ''),
(21, 'Sani', 'Yahaya', '8746647', 1, '87553426782', 'sani@yahoo.com', '$2y$10$aUsi/PnJc2Z1ZhCZf5FsTOiKuQidp0Ek4TlJDInrfK1PECOe4Luxm', ''),
(22, 'Mr. Coleman', 'Essien', '65378925', 1, '23567834525', 'coleman@yahoo.com', '$2y$10$A1E.c94BdfbFJUYeKukAYO/32Va76C1.8.aBxECpFq/aX3gVABAKe', ''),
(23, 'Gloria', 'Eneh Omale', '8756598', 1, '78654312345', 'gloria@yahoo.com', '$2y$10$ZYwX3IVg9hq43GdJuW9AM.W7gd4qOxt1gy7pi4ZfS7ClWL4m4GKDi', ''),
(24, 'Mrs. Stella', 'Etuk', '6454747', 1, '98764532567', 'stella@yahoo.com', '$2y$10$.GBlCDuJmxX8PDtgs6JjpuddE1W50Bz7hYflRvFDf2gx/fDsVx/fC', ''),
(25, 'Dr. J.T', 'Kur', '674647', 1, '08156342516', 'kur@yahoo.com', '$2y$10$Jix27pSrRH8nJcjInonvROzLo6SuauJxtELd8tj2H2MMH5v3sbW9q', ''),
(26, 'Dr. A.K', 'Agboola', '76456', 1, '09863452341', 'aboola@yahoo.com', '$2y$10$lFC3CDN8OTtxUi.DpFC3Penbw2PTcyM7OUFlMM0J7K1VeHqIUD2eW', ''),
(27, 'Prof. Uduakah', 'Udoakah', '8767', 1, '09875243124', 'udoaka@yahoo.com', '$2y$10$CUbd9ah.R5fJqNe8IJ661eN5c77w98kvvVHyfcesWnxtf5gEIhn7q', ''),
(28, 'Prof', 'Iwokwagh', '797666', 1, '09876543214', 'iwokwagh@yahoo.com', '$2y$10$Y65TJFlcAJhFnFxd2vqWoOfzvCErWeraUEfo9Fh/qEcFEhWizmTPe', ''),
(29, 'Mohammed', 'Bima', '674546', 1, '09875367235', 'bima@yahoo.com', '$2y$10$QMVatY8YsOPwqAZXgMZj6OpZGrNVv8gP7qDaj8372ij.wrsUxPlaW', ''),
(30, 'I.O', 'Alabi', '7677766', 1, '76586452435', 'alabi@yahoo.com', '$2y$10$AYSJk6lJLQ/KN97tNmOHdOth41QTnpU279BEGG47M2ARFFNg8b5xe', ''),
(31, 'Dr. H,O Aliyu', 'Aliyu', '87655', 1, '98765432341', 'Aliyu@yahoo.com', '$2y$10$txeydDrs6kT2.GNVW9gMxeZ58MiPMJg.fpR1.FPvRltP4SuhILA5m', ''),
(32, 'Dr. S.O', 'Ganiyu', '865857', 1, '09875653452', 'ganiyu@yahoo.com', '$2y$10$ZwJvoNYoHQHGQipgIjSI7.IR89QaPfvdnMK6nbZQV/9YzvCqq0kO6', ''),
(33, 'Dr. Isiaq', 'Oyefolahan', '7686868', 1, '87978756873', 'isiaq@yahoo.com', '$2y$10$ujfVOmvzTDRb/tQz.cn5gO/vV/2xEJcDUwTN0ziiUOTsRqI2NI6UO', ''),
(34, 'Barr. Kasim', 'Agbonika', '86969', 1, '98765674567', 'agbonika@yahoo.com', '$2y$10$F7fTq4vISyYUJ9Kz3lCwJ.bq0e2CqvyjZpptOFcsDbxRUZh7TO7Bm', '');

-- --------------------------------------------------------

--
-- Table structure for table `lecturestatus_table`
--

CREATE TABLE `lecturestatus_table` (
  `status_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecturestatus_table`
--

INSERT INTO `lecturestatus_table` (`status_id`, `status`) VALUES
(1, 'Held'),
(2, 'Postponed'),
(3, 'Absent'),
(4, 'others');

-- --------------------------------------------------------

--
-- Table structure for table `level_table`
--

CREATE TABLE `level_table` (
  `level_id` int(1) NOT NULL,
  `level_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_table`
--

INSERT INTO `level_table` (`level_id`, `level_name`) VALUES
(1, '100'),
(2, '200'),
(3, '300'),
(4, '400'),
(5, '500');

-- --------------------------------------------------------

--
-- Table structure for table `litdailysummary`
--

CREATE TABLE `litdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `litdailysummary`
--

INSERT INTO `litdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 12),
(4, 'Thursday', 8),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mathdailysummary`
--

CREATE TABLE `mathdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mathdailysummary`
--

INSERT INTO `mathdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 13),
(5, 'Friday', 14),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mechadailysummary`
--

CREATE TABLE `mechadailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mechadailysummary`
--

INSERT INTO `mechadailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 14),
(4, 'Thursday', 10),
(5, 'Friday', 1),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mechdailysummary`
--

CREATE TABLE `mechdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mechdailysummary`
--

INSERT INTO `mechdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 10),
(4, 'Thursday', 9),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `metadailysummary`
--

CREATE TABLE `metadailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metadailysummary`
--

INSERT INTO `metadailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 2),
(3, 'Wednesday', 5),
(4, 'Thursday', 8),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `microbiodailysummary`
--

CREATE TABLE `microbiodailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `microbiodailysummary`
--

INSERT INTO `microbiodailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 12),
(5, 'Friday', 11),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `phydailysummary`
--

CREATE TABLE `phydailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phydailysummary`
--

INSERT INTO `phydailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 10),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pmtdailysummary`
--

CREATE TABLE `pmtdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pmtdailysummary`
--

INSERT INTO `pmtdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 11),
(3, 'Wednesday', 15),
(4, 'Thursday', 11),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `qsdailysummary`
--

CREATE TABLE `qsdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qsdailysummary`
--

INSERT INTO `qsdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 14),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `saatdailysummary`
--

CREATE TABLE `saatdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saatdailysummary`
--

INSERT INTO `saatdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 15),
(2, 'Tuesday', 14),
(3, 'Wednesday', 14),
(4, 'Thursday', 11),
(5, 'Friday', 12),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sciedudailysummary`
--

CREATE TABLE `sciedudailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sciedudailysummary`
--

INSERT INTO `sciedudailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 14),
(3, 'Wednesday', 12),
(4, 'Thursday', 11),
(5, 'Friday', 13),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seetdailysummary`
--

CREATE TABLE `seetdailysummary` (
  `id` int(11) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seetdailysummary`
--

INSERT INTO `seetdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 11),
(4, 'Thursday', 14),
(5, 'Friday', 14),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `semtdailysummary`
--

CREATE TABLE `semtdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semtdailysummary`
--

INSERT INTO `semtdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 14),
(3, 'Wednesday', 11),
(4, 'Thursday', 14),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `session_table`
--

CREATE TABLE `session_table` (
  `id` int(11) NOT NULL,
  `session_name` varchar(250) NOT NULL,
  `semester_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_table`
--

INSERT INTO `session_table` (`id`, `session_name`, `semester_name`) VALUES
(1, '2016/2017', 'SECOND SEMESTER');

-- --------------------------------------------------------

--
-- Table structure for table `setdailysummary`
--

CREATE TABLE `setdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setdailysummary`
--

INSERT INTO `setdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 14),
(3, 'Wednesday', 12),
(4, 'Thursday', 14),
(5, 'Friday', 15),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sictdailysummary`
--

CREATE TABLE `sictdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(255) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sictdailysummary`
--

INSERT INTO `sictdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 12),
(3, 'Wednesday', 12),
(4, 'Thursday', 12),
(5, 'Friday', 12),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `slsdailysummary`
--

CREATE TABLE `slsdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slsdailysummary`
--

INSERT INTO `slsdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 1),
(5, 'Friday', 14),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `spsdailysummary`
--

CREATE TABLE `spsdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spsdailysummary`
--

INSERT INTO `spsdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 15),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ssddailysummary`
--

CREATE TABLE `ssddailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ssddailysummary`
--

INSERT INTO `ssddailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 16),
(2, 'Tuesday', 12),
(3, 'Wednesday', 5),
(4, 'Thursday', 8),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `statdailysummary`
--

CREATE TABLE `statdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statdailysummary`
--

INSERT INTO `statdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 9),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stedailysummary`
--

CREATE TABLE `stedailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stedailysummary`
--

INSERT INTO `stedailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 15),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 7),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `svgdailysummary`
--

CREATE TABLE `svgdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `svgdailysummary`
--

INSERT INTO `svgdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 13),
(3, 'Wednesday', 11),
(4, 'Thursday', 12),
(5, 'Friday', 8),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teldailysummary`
--

CREATE TABLE `teldailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teldailysummary`
--

INSERT INTO `teldailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 13),
(2, 'Tuesday', 13),
(3, 'Wednesday', 14),
(4, 'Thursday', 10),
(5, 'Friday', 10),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tmtdailysummary`
--

CREATE TABLE `tmtdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmtdailysummary`
--

INSERT INTO `tmtdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 11),
(2, 'Tuesday', 12),
(3, 'Wednesday', 13),
(4, 'Thursday', 7),
(5, 'Friday', 6),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `update_table`
--

CREATE TABLE `update_table` (
  `update_id` int(10) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `lecture_venue` varchar(255) NOT NULL,
  `new_venue` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time NOT NULL,
  `lecture_status` varchar(50) NOT NULL,
  `faculty_name` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `lecturer_id` int(8) NOT NULL,
  `service_no` varchar(11) NOT NULL,
  `rep_matric_no` varchar(20) NOT NULL,
  `level_id` int(1) NOT NULL,
  `session_name` varchar(255) NOT NULL,
  `semester_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `update_table`
--

INSERT INTO `update_table` (`update_id`, `course_title`, `course_code`, `lecture_venue`, `new_venue`, `date`, `start_time`, `end_time`, `lecture_status`, `faculty_name`, `dept_name`, `lecturer_id`, `service_no`, `rep_matric_no`, `level_id`, `session_name`, `semester_name`) VALUES
(6, '26', '26', 'LR 1 Codel', 'LR 1 Codel', '2017-11-01', '11:25:00', '01:25:00', 'Held', '1', '1', 34, '86969', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(7, '9', '9', 'ADVANCED NETWORKING LABORATORY', 'LR 1 Codel', '2017-11-01', '11:25:00', '02:25:00', 'Postponed', '1', '1', 20, '6854646', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(8, '12', '12', 'AGRIC EXTENSION 4', 'AGRIC EXTENSION 4', '2017-11-01', '11:25:00', '12:25:00', 'Held', '1', '1', 1, '234DF', '2017/65858', 2, '2016/2017', 'SECOND SEMESTER'),
(9, '18', '18', 'ADVANCED NETWORKING LABORATORY', 'LR 1 Codel', '2017-11-01', '11:25:00', '01:25:00', 'Held', '1', '1', 30, '7677766', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(10, '9', '9', 'ADVANCED NETWORKING LABORATORY', 'AGRIC EXTENSION 4', '2017-11-01', '11:25:00', '01:25:00', 'Held', '1', '1', 20, '6854646', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(11, '12', '12', 'AGRIC EXTENSION 4', 'Entreprenueurship centre', '2017-11-01', '09:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2017/65858', 2, '2016/2017', 'SECOND SEMESTER'),
(12, '11', '11', 'LR 1 Codel', '', '2017-11-01', '09:25:00', '11:25:00', 'Held', '1', '1', 23, '8756598', '2017/65858', 2, '2016/2017', 'SECOND SEMESTER'),
(13, '11', '11', 'LR 1 Codel', '', '2017-11-01', '03:25:00', '04:25:00', 'Held', '1', '1', 23, '8756598', '2017/65858', 2, '2016/2017', 'SECOND SEMESTER'),
(14, '13', '13', 'AGRIC EXTENSION 4', 'ADVANCED NETWORKING LABORATORY', '2017-11-01', '11:25:00', '11:25:00', 'Held', '1', '1', 24, '6454747', '2017/65858', 2, '2016/2017', 'SECOND SEMESTER'),
(15, '19', '19', 'AGRIC EXTENSION 4', 'ADVANCED NETWORKING LABORATORY', '2017-11-01', '11:25:00', '03:25:00', 'Held', '1', '1', 31, '87655', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(16, '21', '21', 'LR 1 Codel', '', '2017-11-01', '10:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(17, '20', '20', 'LR 1 Codel', '', '2017-11-01', '01:25:00', '03:25:00', 'Held', '1', '1', 31, '87655', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(18, '16', '16', 'AGRIC EXTENSION 4', '', '2017-11-01', '12:25:00', '02:25:00', 'Absent', '1', '1', 27, '8767', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(19, '22', '22', 'LR 1 Codel', 'AGRIC EXTENSION 4', '2017-11-01', '03:25:00', '05:25:00', 'Held', '1', '1', 32, '865857', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(20, '18', '18', 'ADVANCED NETWORKING LABORATORY', 'ADVANCED NETWORKING LABORATORY', '2017-11-01', '12:25:00', '01:25:00', 'Absent', '1', '1', 30, '7677766', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(21, '25', '25', 'ADVANCED NETWORKING LABORATORY', 'ADVANCED NETWORKING LABORATORY', '2017-11-01', '09:25:00', '11:25:00', 'Held', '1', '1', 33, '7686868', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(22, '24', '24', 'Entreprenueurship centre', 'ADVANCED NETWORKING LABORATORY', '2017-10-30', '12:05:00', '03:25:00', 'Postponed', '1', '1', 30, '7677766', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(23, '26', '26', 'LR 1 Codel', 'Entreprenueurship centre', '2017-10-30', '09:25:00', '11:25:00', 'Held', '1', '1', 34, '86969', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(24, '23', '23', 'ADVANCED NETWORKING LABORATORY', 'ADVANCED NETWORKING LABORATORY', '2017-10-30', '03:25:00', '06:25:00', 'others', '1', '1', 32, '865857', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(25, '27', '27', 'AGRIC EXTENSION 4', '', '2017-10-30', '12:05:00', '02:25:00', 'Held', '1', '1', 28, '797666', '20174/5758', 5, '2016/2017', 'SECOND SEMESTER'),
(26, '19', '19', 'AGRIC EXTENSION 4', 'ADVANCED NETWORKING LABORATORY', '2017-11-16', '01:37:00', '03:00:00', 'Held', '1', '1', 31, '87655', '2016/1/464CI', 3, '2016/2017', 'SECOND SEMESTER'),
(27, '12', '12', 'AGRIC EXTENSION 4', 'LR 1 Codel', '2017-12-04', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(28, '12', '12', 'AGRIC EXTENSION 4', 'AGRIC EXTENSION 4', '2017-11-29', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(29, '12', '12', 'AGRIC EXTENSION 4', '', '2017-11-01', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(30, '12', '12', 'AGRIC EXTENSION 4', 'LR 1 Codel', '2017-11-02', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(31, '12', '12', 'AGRIC EXTENSION 4', '', '2017-10-04', '11:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(32, '12', '12', 'AGRIC EXTENSION 4', 'LR 1 Codel', '2017-09-05', '11:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2016/1/464CI', 1, '2016/2017', 'SECOND SEMESTER'),
(33, '21', '21', 'LR 1 Codel', 'LR 1 Codel', '2017-12-04', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(34, '21', '21', 'LR 1 Codel', 'LR 1 Codel', '2017-11-28', '11:25:00', '11:25:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(35, '21', '21', 'LR 1 Codel', '', '2017-11-08', '11:25:00', '02:25:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(36, '21', '21', 'LR 1 Codel', 'LR 1 Codel', '2017-10-04', '11:25:00', '03:25:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(37, '21', '21', 'LR 1 Codel', 'LR 1 Codel', '2017-11-14', '11:25:00', '01:35:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(38, '21', '21', 'LR 1 Codel', '', '2017-09-27', '11:25:00', '03:25:00', 'Held', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(39, '21', '21', 'LR 1 Codel', '', '2017-08-02', '11:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(40, '21', '21', 'LR 1 Codel', 'AGRIC EXTENSION 4', '2017-09-26', '11:25:00', '11:25:00', 'Postponed', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(41, '21', '21', 'LR 1 Codel', 'LR 1 Codel', '2017-09-26', '11:25:00', '01:25:00', 'Absent', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(42, '21', '21', 'LR 1 Codel', '', '2017-08-29', '11:25:00', '01:25:00', 'Absent', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(43, '21', '21', 'LR 1 Codel', 'AGRIC EXTENSION 4', '2017-08-31', '11:25:00', '01:25:00', 'others', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(44, '21', '21', 'LR 1 Codel', 'Entreprenueurship centre', '2017-08-31', '11:25:00', '11:25:00', 'others', '1', '1', 1, '234DF', '2014/5474/FT', 3, '2016/2017', 'SECOND SEMESTER'),
(45, '21', '21', 'LR 1 Codel', 'Entreprenueurship centre', '2017-12-06', '11:00:00', '01:00:00', 'Held', '1', '1', 1, '234DF', '2016/1/464CI', 3, '2016/2017', 'SECOND SEMESTER'),
(46, '26', '26', 'LR 1 Codel', 'AGRIC EXTENSION 4', '2018-03-03', '11:30:00', '01:25:00', 'others', '1', '1', 34, '86969', '2014/5474/FT', 5, '2016/2017', 'SECOND SEMESTER');

-- --------------------------------------------------------

--
-- Table structure for table `update_table_lecturer`
--

CREATE TABLE `update_table_lecturer` (
  `update_id` int(10) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `lecture_venue` varchar(255) NOT NULL,
  `new_venue` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `lecture_status` varchar(255) NOT NULL,
  `faculty_name` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `lecturer_id` int(8) NOT NULL,
  `service_no` varchar(255) NOT NULL,
  `level_id` int(11) NOT NULL,
  `session_name` varchar(255) NOT NULL,
  `semester_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `update_table_lecturer`
--

INSERT INTO `update_table_lecturer` (`update_id`, `course_title`, `course_code`, `lecture_venue`, `new_venue`, `date`, `start_time`, `end_time`, `lecture_status`, `faculty_name`, `dept_name`, `lecturer_id`, `service_no`, `level_id`, `session_name`, `semester_name`) VALUES
(1, '12', '12', 'AGRIC EXTENSION 4', 'LR 1 Codel', '2017-12-12', '11:25:00', '02:25:00', 'Held', '1', '1', 1, '234DF', 2, '2016/2017', 'SECOND SEMESTER');

-- --------------------------------------------------------

--
-- Table structure for table `urpdailysummary`
--

CREATE TABLE `urpdailysummary` (
  `id` int(2) NOT NULL,
  `week_day` varchar(50) NOT NULL,
  `num_lecture` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urpdailysummary`
--

INSERT INTO `urpdailysummary` (`id`, `week_day`, `num_lecture`) VALUES
(1, 'Monday', 12),
(2, 'Tuesday', 11),
(3, 'Wednesday', 13),
(4, 'Thursday', 11),
(5, 'Friday', 7),
(6, 'Saturday', 0),
(7, 'Sunday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `venue_table`
--

CREATE TABLE `venue_table` (
  `venue_id` int(8) NOT NULL,
  `venue_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue_table`
--

INSERT INTO `venue_table` (`venue_id`, `venue_name`) VALUES
(1, 'ADVANCED NETWORKING LABORATORY'),
(2, 'LR 1 Codel'),
(4, 'AGRIC EXTENSION 4'),
(5, 'Entreprenueurship centre');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abedailysummary`
--
ALTER TABLE `abedailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_table`
--
ALTER TABLE `admin_table`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `aetdailysummary`
--
ALTER TABLE `aetdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animalproddailysummary`
--
ALTER TABLE `animalproddailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archidailysummary`
--
ALTER TABLE `archidailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biochemdailysummary`
--
ALTER TABLE `biochemdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bioscidailysummary`
--
ALTER TABLE `bioscidailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blddailysummary`
--
ALTER TABLE `blddailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chemdailysummary`
--
ALTER TABLE `chemdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chemisdailysummary`
--
ALTER TABLE `chemisdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `civildailysummary`
--
ALTER TABLE `civildailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comdailysummary`
--
ALTER TABLE `comdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comedududailysummary`
--
ALTER TABLE `comedududailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses_table`
--
ALTER TABLE `courses_table`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `dept_id` (`dept_id`),
  ADD KEY `venue_id` (`venue_id`),
  ADD KEY `level_id` (`level_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `lecturer_id` (`lecturer_id`);

--
-- Indexes for table `course_reps_table`
--
ALTER TABLE `course_reps_table`
  ADD PRIMARY KEY (`rep_matric_no`),
  ADD KEY `level_id` (`level_id`),
  ADD KEY `dept_id` (`dept_id`),
  ADD KEY `rep_id` (`rep_id`);

--
-- Indexes for table `cptdailysummary`
--
ALTER TABLE `cptdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cropproddailysummary`
--
ALTER TABLE `cropproddailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cssdailysummary`
--
ALTER TABLE `cssdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_table`
--
ALTER TABLE `department_table`
  ADD PRIMARY KEY (`dept_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `ebsdailysummary`
--
ALTER TABLE `ebsdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edutechdudailysummary`
--
ALTER TABLE `edutechdudailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `electdailysummary`
--
ALTER TABLE `electdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estdailysummary`
--
ALTER TABLE `estdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_table`
--
ALTER TABLE `faculty_table`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `fisheriesdailysummary`
--
ALTER TABLE `fisheriesdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fstdailysummary`
--
ALTER TABLE `fstdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geodailysummary`
--
ALTER TABLE `geodailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geoldailysummary`
--
ALTER TABLE `geoldailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imtdailysummary`
--
ALTER TABLE `imtdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itedailysummary`
--
ALTER TABLE `itedailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecturer_table`
--
ALTER TABLE `lecturer_table`
  ADD PRIMARY KEY (`lecturer_id`),
  ADD UNIQUE KEY `service_no` (`service_no`),
  ADD KEY `dept_id` (`dept_id`);

--
-- Indexes for table `lecturestatus_table`
--
ALTER TABLE `lecturestatus_table`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `level_table`
--
ALTER TABLE `level_table`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `litdailysummary`
--
ALTER TABLE `litdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mathdailysummary`
--
ALTER TABLE `mathdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mechadailysummary`
--
ALTER TABLE `mechadailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mechdailysummary`
--
ALTER TABLE `mechdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadailysummary`
--
ALTER TABLE `metadailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `microbiodailysummary`
--
ALTER TABLE `microbiodailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phydailysummary`
--
ALTER TABLE `phydailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pmtdailysummary`
--
ALTER TABLE `pmtdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qsdailysummary`
--
ALTER TABLE `qsdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saatdailysummary`
--
ALTER TABLE `saatdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sciedudailysummary`
--
ALTER TABLE `sciedudailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seetdailysummary`
--
ALTER TABLE `seetdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semtdailysummary`
--
ALTER TABLE `semtdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_table`
--
ALTER TABLE `session_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setdailysummary`
--
ALTER TABLE `setdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sictdailysummary`
--
ALTER TABLE `sictdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slsdailysummary`
--
ALTER TABLE `slsdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spsdailysummary`
--
ALTER TABLE `spsdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ssddailysummary`
--
ALTER TABLE `ssddailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statdailysummary`
--
ALTER TABLE `statdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stedailysummary`
--
ALTER TABLE `stedailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `svgdailysummary`
--
ALTER TABLE `svgdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teldailysummary`
--
ALTER TABLE `teldailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmtdailysummary`
--
ALTER TABLE `tmtdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `update_table`
--
ALTER TABLE `update_table`
  ADD PRIMARY KEY (`update_id`),
  ADD KEY `lecturer_id` (`lecturer_id`),
  ADD KEY `matric_no` (`rep_matric_no`),
  ADD KEY `level_id` (`level_id`);

--
-- Indexes for table `update_table_lecturer`
--
ALTER TABLE `update_table_lecturer`
  ADD PRIMARY KEY (`update_id`),
  ADD KEY `level_id` (`level_id`),
  ADD KEY `lecturer_id` (`lecturer_id`);

--
-- Indexes for table `urpdailysummary`
--
ALTER TABLE `urpdailysummary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_table`
--
ALTER TABLE `venue_table`
  ADD PRIMARY KEY (`venue_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abedailysummary`
--
ALTER TABLE `abedailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `admin_table`
--
ALTER TABLE `admin_table`
  MODIFY `admin_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `aetdailysummary`
--
ALTER TABLE `aetdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `animalproddailysummary`
--
ALTER TABLE `animalproddailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `archidailysummary`
--
ALTER TABLE `archidailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `biochemdailysummary`
--
ALTER TABLE `biochemdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bioscidailysummary`
--
ALTER TABLE `bioscidailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `blddailysummary`
--
ALTER TABLE `blddailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `chemdailysummary`
--
ALTER TABLE `chemdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `chemisdailysummary`
--
ALTER TABLE `chemisdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `civildailysummary`
--
ALTER TABLE `civildailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `comdailysummary`
--
ALTER TABLE `comdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `comedududailysummary`
--
ALTER TABLE `comedududailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `courses_table`
--
ALTER TABLE `courses_table`
  MODIFY `course_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `course_reps_table`
--
ALTER TABLE `course_reps_table`
  MODIFY `rep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `cptdailysummary`
--
ALTER TABLE `cptdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cropproddailysummary`
--
ALTER TABLE `cropproddailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cssdailysummary`
--
ALTER TABLE `cssdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `department_table`
--
ALTER TABLE `department_table`
  MODIFY `dept_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `ebsdailysummary`
--
ALTER TABLE `ebsdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `edutechdudailysummary`
--
ALTER TABLE `edutechdudailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `electdailysummary`
--
ALTER TABLE `electdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `estdailysummary`
--
ALTER TABLE `estdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `faculty_table`
--
ALTER TABLE `faculty_table`
  MODIFY `faculty_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `fisheriesdailysummary`
--
ALTER TABLE `fisheriesdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `fstdailysummary`
--
ALTER TABLE `fstdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `geodailysummary`
--
ALTER TABLE `geodailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `geoldailysummary`
--
ALTER TABLE `geoldailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `imtdailysummary`
--
ALTER TABLE `imtdailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `itedailysummary`
--
ALTER TABLE `itedailysummary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lecturer_table`
--
ALTER TABLE `lecturer_table`
  MODIFY `lecturer_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `lecturestatus_table`
--
ALTER TABLE `lecturestatus_table`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `level_table`
--
ALTER TABLE `level_table`
  MODIFY `level_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `update_table`
--
ALTER TABLE `update_table`
  MODIFY `update_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `update_table_lecturer`
--
ALTER TABLE `update_table_lecturer`
  MODIFY `update_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
