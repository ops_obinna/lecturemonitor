<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Template 
    {
        var $ci;
         
        function __construct() 
        {
            $this->ci =& get_instance();
        }



        /*
        * @name:				load 
        * @desc:				Loads the template and view specified 
        * @param: loc:			Location (admin or public)
        * @param: tpl_name:		Name of the template
        * @param: view:			Name of the view to load
        * @param: data:			Optional data array
        *
        */

        /*
        *It says look into the admin folder if it is default
        *load either admin template or public template
        *NOTE: $loc = is the template names in the Template folder
        *admin and public as used here is the templates created 
        *in the template folder
        *the variable main (i.e $main) will be required while loading the view inside the template
        *This is because the view has been passed into a data data viariable called main
        *so while loading view in the library we represent the view name with $main
        *
        */
        function load($loc, $tpl_name, $view, $data = null) {
            if($loc == 'admin' && $tpl_name == 'default'){
                $tpl_name = 'admin';
            }

            if($loc == 'public' && $tpl_name == 'default'){
                $tpl_name = 'login';
            }

            if($loc == 'classreps' && $tpl_name == 'default'){
                $tpl_name = 'classreps';
            }
            
             if($loc == 'lecturers' && $tpl_name == 'default'){
                $tpl_name = 'lecturers';
            }

            $data['main'] = $loc. '/' .$view;
            $this->ci->load->view('/Templates/' .$tpl_name, $data);
        }
    }