<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manageprofile extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    
    //updates the lecturer profile
	public function lecturer_profile()
	{  
          /*Form Validation Rules 4 PROFILE UPDATE */
          $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[lecturer_table.email_address]');
            
 
            
            if ($this->form_validation->run() == FALSE) {   
                   
            $data['lecturer'] = $this->db->get_where('lecturer_table', array('lecturer_id' => $this->session->userdata('lecturer_id')))->result();
            //load template if validation is false              
			 $this->template->load('lecturers', 'default', 'manageprofile', $data);
                
            }
        
            
             else {
                
                $data['email_address'] = $this->input->post('email');

                $this->db->where('lecturer_id', $this->session->userdata('lecturer_id'));
                $this->db->update('lecturer_table', $data);
                
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'upload/lecturer/' . $this->session->userdata('lecturer_id') . '.jpg');
                
                $this->session->set_flashdata('success', 'Account Successfully updated');
                redirect(base_url() . 'lecturers/Manageprofile/lecturer_profile');
            }
            
	}
    
    //Change the lecturer password
    public function update_lecturer_password(){
        
        /*Form Validation Rules 4 PROFILE UPDATE */

        $this->form_validation->set_rules('password', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'trim|required|matches[new_password]');
        
        
         if ($this->form_validation->run() == FALSE) {
                
                
                   
            $data['lecturer'] = $this->db->get_where('lecturer_table', array(
                        'lecturer_id' => $this->session->userdata('lecturer_id')
                    ))->result_array();
            //load template if validation is false 
                
             
			 $this->template->load('lecturers', 'default', 'manage_lecturer_password');
                
            }
        else{
            
        $data['password'] = $this->input->post('password');
        $data['new_password'] = $this->input->post('new_password');
        $data['confirm_new_password'] = $this->input->post('confirm_new_password');
            
        //Fetch Current Password from database lecturer_table
        $current_password = $this->db->get_where('lecturer_table', array('lecturer_id' => $this->session->userdata('lecturer_id')
                ))->row()->password;
            
            
        //compare the current password with the password entered current password  if it matches forward to table in database  
        if (password_verify($data['password'], $current_password)) {
            $this->db->where('lecturer_id', $this->session->userdata('lecturer_id'));
            $this->db->update('lecturer_table', array(
            'password' => password_hash($data['new_password'], PASSWORD_DEFAULT)));
            $this->session->set_flashdata('success', 'password_updated');
            } else {
                    // Your Current paasword is incorrect
            echo '<script>alert(\'You have entered a wrong account passwod\');</script>';
                }
            
            redirect(base_url() . 'lecturers/Manageprofile/update_lecturer_password');
            
        }
            
             

    }
    
}