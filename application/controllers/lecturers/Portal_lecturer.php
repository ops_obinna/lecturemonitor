<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_lecturer extends CI_Controller {

function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
	

        
    public function index()	{
      $pf_no = $this->session->userdata('service_no');
      $data['portal'] = $this->Portal_lecturer_model->today_activities($pf_no);	
    
        //starts by running the query for the faculty dropdown  
      $data['facultyDrp'] = $this->Portal_lecturer_model->getfaculty(); 
	
      //load the lecturers template
      $this->template->load('lecturers', 'default', 'portal_lecturer', $data);   
	}
    
    public function semesterial_summary(){
        //fetching lectures held, postponed, absent or others for the day 
        $pf_no = $this->session->userdata('service_no');
        $data['held'] = $this->Portal_lecturer_model->lecture_held($pf_no);	
        $data['absent'] = $this->Portal_lecturer_model->lecture_absent($pf_no);
        $data['postponed'] = $this->Portal_lecturer_model->lecture_postponed($pf_no);
        $data['others'] = $this->Portal_lecturer_model->lecture_others($pf_no);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Portal_lecturer_model->held_summary_five($pf_no);
        $data['absentfive'] = $this->Portal_lecturer_model->absent_summary_five($pf_no);
        $data['postponedfive'] = $this->Portal_lecturer_model->postponed_summary_five($pf_no);
        $data['othersfive'] = $this->Portal_lecturer_model->others_summary_five($pf_no);
        
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Portal_lecturer_model->held_summary_four($pf_no);
        $data['absentfour'] = $this->Portal_lecturer_model->absent_summary_four($pf_no);
        $data['postponedfour'] = $this->Portal_lecturer_model->postponed_summary_four($pf_no);
        $data['othersfour'] = $this->Portal_lecturer_model->others_summary_four($pf_no);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Portal_lecturer_model->held_summary_three($pf_no);
        $data['absentthree'] = $this->Portal_lecturer_model->absent_summary_three($pf_no);
        $data['postponedthree'] = $this->Portal_lecturer_model->postponed_summary_three($pf_no);
        $data['othersthree'] = $this->Portal_lecturer_model->others_summary_three($pf_no);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Portal_lecturer_model->held_summary_two($pf_no);
        $data['absenttwo'] = $this->Portal_lecturer_model->absent_summary_two($pf_no);
        $data['postponedtwo'] = $this->Portal_lecturer_model->postponed_summary_two($pf_no);
        $data['otherstwo'] = $this->Portal_lecturer_model->others_summary_two($pf_no);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Portal_lecturer_model->held_summary_one($pf_no);
        $data['absentone'] = $this->Portal_lecturer_model->absent_summary_one($pf_no);
        $data['postponedone'] = $this->Portal_lecturer_model->postponed_summary_one($pf_no);
        $data['othersone'] = $this->Portal_lecturer_model->others_summary_one($pf_no);
        
        //load the lecturers template
      $this->template->load('lecturers', 'default', 'lecture_summary', $data);   
        
    }
    
    public function update_activity(){
        
      //starts by running the query for the faculty dropdown  
      $data['facultyDrp'] = $this->Portal_lecturer_model->getfaculty(); 
      //load the lecturers template
      $this->template->load('lecturers', 'default', 'updateactivity', $data);
    }
    
    
    
    //call to fill the second dropdown with the departments  
   public function buildDrpDepts()  
   {  
      //set selected faculty id from POST  
      echo $id_faculty = $this->input->post('id',TRUE);  
      //run the query for the departments we specified earlier  
      $deptData['deptDrp']=$this->Portal_lecturer_model->getDeptByFaculty($id_faculty);  
      $output = null;  
      foreach ($deptData['deptDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
         $output .= "<option value='".$row->dept_id."'>".$row->dept_name."</option>";  
      }  
      echo $output; 
       
   }  
    
    
    //call to fill the course title dropdown with the CourseTitle  
   public function buildDrpCourseTitle()  
   {  
      //set selected faculty id from POST  
      echo $id_dept = $this->input->post('id',TRUE);  
      
      //run the query for the CourseTitle we specified earlier 
       //getting the course title based on the dept_id passed to the model 
      $courseTitleData['courseTitleDrp']=$this->Portal_lecturer_model->getCourseTitleByDept($id_dept);  
      $output = null;  
      foreach ($courseTitleData['courseTitleDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
         $output .= "<option value='".$row->course_id."'>".$row->course_title."</option>";  
      }  
      echo $output;  
   }  
    
     //call to fill the course code dropdown with the Course Code  
   public function buildDrpCourseCode()  
   {  
      //set selected course id from POST  
      echo $id_course = $this->input->post('id',TRUE);  
      
      //run the query for the CourseCode we specified earlier  
       //here we get the course code based on the course_id passed to the model
      $courseCodeData['courseCodeDrp']=$this->Portal_lecturer_model->getCourseCodeByDept($id_course);  
      $output = null;  
      foreach ($courseCodeData['courseCodeDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->course_id."'>".$row->course_code. " ". "---" . ">". $row->course_title. "</option>";  
      }  
      echo $output;  
   } 
    
    
    //function to get lecture venue for a particular course
    public function buildVenueDrp(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture venue as specified in the model based on the course_id passed to the model
      $venueData['venueDrp']=$this->Portal_lecturer_model->getVenueByCourse($id_course);  
      $output = null;  
      foreach ($venueData['venueDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->venue_name."'>".$row->venue_name."</option>";  
      }  
      echo $output;  
                                    
    }
    
    
    //function to get lecturer teaching a particular course
    public function buildDrpLecturer(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture as specified in the model based on the course_id passed to the model
      $lecturerData['lecturerDrp']=$this->Portal_lecturer_model->getLecturerByCourse($id_course);  
      $output = null;  
      foreach ($lecturerData['lecturerDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->service_no."'>".$row->lecturer_sname. " ". " ". $row->lecturer_fname. "</option>";  
      }  
      echo $output;  
                                    
    }
    
    
    //function to get lecturer ID for a course to be submitted to the database
    //this function displays in a hidden field in the portal page
    public function buildDrpLecturerEmail(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture as specified in the model based on the course_id passed to the model
      $lecturerEmailData['lecturerEmailDrp']=$this->Portal_lecturer_model->getLecturerByCourse($id_course);  
      $output = null;  
      foreach ($lecturerEmailData['lecturerEmailDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result to send Lecturer ID to database and display email  
          $output .= "<option value='".$row->lecturer_id."'>". $row->email_address. "</option>";  
          
      }  
      echo $output;  
                                    
    }
    
    
     //start course registration 
	public function updateLecture()
	{
      //setting validation rules
		$this->form_validation->set_rules('session_name', 'SESSION','trim|required');
		$this->form_validation->set_rules('semester_name', 'SESSION','trim|required');
		$this->form_validation->set_rules('faculty_name', 'FACULTY','trim|required');
		$this->form_validation->set_rules('dept_name', 'DEPARTMENT', 'trim|required');
		$this->form_validation->set_rules('course_title', 'COURSE TITLE', 'required');
        $this->form_validation->set_rules('course_code', 'COURSE CODE', 'required');
        $this->form_validation->set_rules('service_no', 'LECTURER', 'required');
        $this->form_validation->set_rules('lecturer_email', 'LECTURER EMAIL', 'required');
        $this->form_validation->set_rules('level_id', 'LEVEL', 'required');
		$this->form_validation->set_rules('lecture_venue', 'LECTURE VENUE', 'required');
        $this->form_validation->set_rules('date', 'DATE', 'required');
		$this->form_validation->set_rules('start_time', 'START TIME', 'required');
		$this->form_validation->set_rules('end_time', 'END TIME', 'required');
		$this->form_validation->set_rules('lecture_status', 'STATUS', 'required');
        
				
		if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
            $this->update_activity();

		  }else{



			//else if validation is true then create post array
				$data['session_name'] 	       = $this->input->post('session_name');
				$data['semester_name'] 	       = $this->input->post('semester_name');
				$data['faculty_name'] 	       = $this->input->post('faculty_name');
                $data['dept_name'] 	           = $this->input->post('dept_name');
                $data['course_title'] 	       = $this->input->post('course_title');
                $data['course_code'] 	       = $this->input->post('course_code');
                $data['service_no'] 	       = $this->input->post('service_no');
                $data['lecturer_id'] 	       = $this->input->post('lecturer_email');
                $data['level_id'] 	           = $this->input->post('level_id');
                $data['lecture_venue'] 	       = $this->input->post('lecture_venue');
                $data['new_venue'] 	           = $this->input->post('new_venue');
                $data['date'] 	               = $this->input->post('date');
                $data['start_time'] 	       = $this->input->post('start_time');
                $data['end_time'] 	           = $this->input->post('end_time');
                $data['lecture_status'] 	   = $this->input->post('lecture_status');
                

                
				
					

		//Update lecture status by calling the function in the model class 
		$this->Portal_lecturer_model->updateLecture($data);
		$this->session->set_flashdata('success', 'Lecture Updated Successfully');
		
				
		//redirect to the Portal page and refreshes all form input
		redirect('lecturers/Portal_lecturer');

		}

	} //end course registration
    
    public function myupdates(){
        //fetching lectures held, postponed, absent or others for the day 
        $pf_no = $this->session->userdata('service_no');
        $data['held'] = $this->Portal_lecturer_model->lecture_held_myupdate($pf_no);	
        $data['absent'] = $this->Portal_lecturer_model->lecture_absent_myupdate($pf_no);
        $data['postponed'] = $this->Portal_lecturer_model->lecture_postponed_myupdate($pf_no);
        $data['others'] = $this->Portal_lecturer_model->lecture_others_myupdate($pf_no);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Portal_lecturer_model->held_summary_five_myupdate($pf_no);
        $data['absentfive'] = $this->Portal_lecturer_model->absent_summary_five_myupdate($pf_no);
        $data['postponedfive'] = $this->Portal_lecturer_model->postponed_summary_five_myupdate($pf_no);
        $data['othersfive'] = $this->Portal_lecturer_model->others_summary_five_myupdate($pf_no);
        
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Portal_lecturer_model->held_summary_four_myupdate($pf_no);
        $data['absentfour'] = $this->Portal_lecturer_model->absent_summary_four_myupdate($pf_no);
        $data['postponedfour'] = $this->Portal_lecturer_model->postponed_summary_four_myupdate($pf_no);
        $data['othersfour'] = $this->Portal_lecturer_model->others_summary_four_myupdate($pf_no);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Portal_lecturer_model->held_summary_three_myupdate($pf_no);
        $data['absentthree'] = $this->Portal_lecturer_model->absent_summary_three_myupdate($pf_no);
        $data['postponedthree'] = $this->Portal_lecturer_model->postponed_summary_three_myupdate($pf_no);
        $data['othersthree'] = $this->Portal_lecturer_model->others_summary_three_myupdate($pf_no);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Portal_lecturer_model->held_summary_two_myupdate($pf_no);
        $data['absenttwo'] = $this->Portal_lecturer_model->absent_summary_two_myupdate($pf_no);
        $data['postponedtwo'] = $this->Portal_lecturer_model->postponed_summary_two_myupdate($pf_no);
        $data['otherstwo'] = $this->Portal_lecturer_model->others_summary_two_myupdate($pf_no);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Portal_lecturer_model->held_summary_one_myupdate($pf_no);
        $data['absentone'] = $this->Portal_lecturer_model->absent_summary_one_myupdate($pf_no);
        $data['postponedone'] = $this->Portal_lecturer_model->postponed_summary_one_myupdate($pf_no);
        $data['othersone'] = $this->Portal_lecturer_model->others_summary_one_myupdate($pf_no);
        $this->template->load('lecturers', 'default', 'myupdates', $data);   
        
    }
    
    //this function load AOR FORM 
    public function AORform(){
      $this->template->load('lecturers', 'default', 'aorform');     
    }
	
	    
    
}