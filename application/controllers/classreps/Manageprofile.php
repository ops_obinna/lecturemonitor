<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manageprofile extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

	

    
    //updates the classreps profile
	public function rep_profile()
	{  
//        
    



            /*Form Validation Rules 4 PROFILE UPDATE */


           $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[course_reps_table.email]');
            
 
            
            if ($this->form_validation->run() == FALSE) {
                
                
                   
            $data['rep'] = $this->db->get_where('course_reps_table', array('rep_id' => $this->session->userdata('rep_id')))->result();
            //load template if validation is false 
                
             $this->template->load('classreps', 'default', 'manageprofile', $data);
			 
                
            }
        
            
             else {
                 
                 $data['email'] = $this->input->post('email');

                $this->db->where('rep_id', $this->session->userdata('rep_id'));
                $this->db->update('course_reps_table', $data);
                
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'upload/classreps/' . $this->session->userdata('rep_id') . '.jpg');
                
                $this->session->set_flashdata('success', 'Account Successfully updated');
                redirect(base_url() . 'classreps/Manageprofile/rep_profile');

            }     
	}
    
    //Change the admin password
    public function update_rep_password(){
        
        /*Form Validation Rules 4 PROFILE UPDATE */

        
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]');
        
        $this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'trim|required|matches[new_password]');
        
        
         if ($this->form_validation->run() == FALSE) {
                
                
                   
            $data['rep'] = $this->db->get_where('course_reps_table', array(
                        'rep_id' => $this->session->userdata('rep_id')
                    ))->result_array();
            //load template if validation is false 
                
             $this->template->load('classreps', 'default', 'manage_rep_password');
			 
                
            }
        else{
            
        $data['password'] = $this->input->post('password');
        $data['new_password'] = $this->input->post('new_password');
        $data['confirm_new_password'] = $this->input->post('confirm_new_password');
            
        //Fetch Current Password from database course_reps_table
        $current_password = $this->db->get_where('course_reps_table', array('rep_id' => $this->session->userdata('rep_id')
                ))->row()->password;
            
            
        //compare the current password with the password entered current password  if it matches forward to table in database  
        if (password_verify($data['password'], $current_password)) {
            $this->db->where('rep_id', $this->session->userdata('rep_id'));
            $this->db->update('course_reps_table', array(
            'password' => password_hash($data['new_password'], PASSWORD_DEFAULT)));
            $this->session->set_flashdata('success', 'password_updated');
            } else {
                    // Your Current paasword is incorrect
            echo '<script>alert(\'You have entered a wrong account passwod\');</script>';
                }
            
            redirect(base_url() . 'classreps/Manageprofile/update_rep_password');
            
        }
            
             

    }
    
}