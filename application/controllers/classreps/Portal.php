<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
	
	
	
	public function index()
	{
        //starts by running the query for the faculty dropdown  
      $data['facultyDrp'] = $this->Portal_classrep_model->getfaculty();  
    
      //load the classreps template
      $this->template->load('classreps', 'default', 'portal', $data);
	  
        
	}
    
    //function to extract the course rep matric number session variable (email)
    public function getRepmatric(){
        
        echo $repemail = $this->session->userdata('email');
        $rep['rep_matric_no']=$this->Portal_classrep_model->getRepmatric($repemail); 
        $output = null;  
      foreach ($rep['rep_matric_no'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
         $output .= "<option value='".$row->rep_matric_no."'>".$row->rep_matric_no."</option>";  
      }  
      echo $output; 
    }
    
     //call to fill the second dropdown with the departments  
   public function buildDrpDepts()  
   {  
      //set selected faculty id from POST  
      echo $id_faculty = $this->input->post('id',TRUE);  
      //run the query for the departments we specified earlier  
      $deptData['deptDrp']=$this->Portal_classrep_model->getDeptByFaculty($id_faculty);  
      $output = null;  
      foreach ($deptData['deptDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
         $output .= "<option value='".$row->dept_id."'>".$row->dept_name."</option>";  
      }  
      echo $output; 
       
   }  
    
    
    //call to fill the course title dropdown with the CourseTitle  
   public function buildDrpCourseTitle()  
   {  
      //set selected faculty id from POST  
      echo $id_dept = $this->input->post('id',TRUE);  
      
      //run the query for the CourseTitle we specified earlier 
       //getting the course title based on the dept_id passed to the model 
      $courseTitleData['courseTitleDrp']=$this->Portal_classrep_model->getCourseTitleByDept($id_dept);  
      $output = null;  
      foreach ($courseTitleData['courseTitleDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
         $output .= "<option value='".$row->course_id."'>".$row->course_title."</option>";  
      }  
      echo $output;  
   }  
    
     //call to fill the course code dropdown with the Course Code  
   public function buildDrpCourseCode()  
   {  
      //set selected course id from POST  
      echo $id_course = $this->input->post('id',TRUE);  
      
      //run the query for the CourseCode we specified earlier  
       //here we get the course code based on the course_id passed to the model
      $courseCodeData['courseCodeDrp']=$this->Portal_classrep_model->getCourseCodeByDept($id_course);  
      $output = null;  
      foreach ($courseCodeData['courseCodeDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->course_id."'>".$row->course_code. " ". "---" . ">". $row->course_title. "</option>";  
      }  
      echo $output;  
   } 
    
    
    //function to get lecture venue for a particular course
    public function buildVenueDrp(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture venue as specified in the model based on the course_id passed to the model
      $venueData['venueDrp']=$this->Portal_classrep_model->getVenueByCourse($id_course);  
      $output = null;  
      foreach ($venueData['venueDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->venue_name."'>".$row->venue_name."</option>";  
      }  
      echo $output;  
                                    
    }
    
    
    //function to get lecturer teaching a particular course
    public function buildDrpLecturer(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture as specified in the model based on the course_id passed to the model
      $lecturerData['lecturerDrp']=$this->Portal_classrep_model->getLecturerByCourse($id_course);  
      $output = null;  
      foreach ($lecturerData['lecturerDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->service_no."'>".$row->lecturer_sname. " ". " ". $row->lecturer_fname. "</option>";  
      }  
      echo $output;  
                                    
    }
    
    
    //function to get lecturer ID for a course to be submitted to the database
    //this function displays in a hidden field in the portal page
    public function buildDrpLecturerEmail(){
        //set selected course_id from POST  
      echo $id_course = $this->input->post('id',TRUE);
        //run the query for the Course Title we specified earlier
        //to get the lecture as specified in the model based on the course_id passed to the model
      $lecturerEmailData['lecturerEmailDrp']=$this->Portal_classrep_model->getLecturerByCourse($id_course);  
      $output = null;  
      foreach ($lecturerEmailData['lecturerEmailDrp'] as $row)  
      {  
         //here we build a dropdown item line for each  query result to send Lecturer ID to database and display email  
          $output .= "<option value='".$row->lecturer_id."'>". $row->email_address. "</option>";  
          
      }  
      echo $output;  
                                    
    }
    
    //start course registration 
	public function updateLecture()
	{
      //setting validation rules
		$this->form_validation->set_rules('session_name', 'SESSION','trim|required');
		$this->form_validation->set_rules('semester_name', 'SESSION','trim|required');
		$this->form_validation->set_rules('faculty_name', 'FACULTY','trim|required');
		$this->form_validation->set_rules('dept_name', 'DEPARTMENT', 'trim|required');
		$this->form_validation->set_rules('course_title', 'COURSE TITLE', 'required');
        $this->form_validation->set_rules('course_code', 'COURSE CODE', 'required');
        $this->form_validation->set_rules('service_no', 'LECTURER', 'required');
        $this->form_validation->set_rules('lecturer_email', 'LECTURER EMAIL', 'required');
        $this->form_validation->set_rules('level_id', 'LEVEL', 'required');
		$this->form_validation->set_rules('lecture_venue', 'LECTURE VENUE', 'required');
        $this->form_validation->set_rules('date', 'DATE', 'required');
		$this->form_validation->set_rules('start_time', 'START TIME', 'required');
		$this->form_validation->set_rules('end_time', 'END TIME', 'required');
		$this->form_validation->set_rules('lecture_status', 'STATUS', 'required');
        $this->form_validation->set_rules('rep_matric_no', 'MATRIC', 'required');
				
		if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
            $this->index();

		  }else{



			//else if validation is true then create post array
				$data['session_name'] 	       = $this->input->post('session_name');
				$data['semester_name'] 	       = $this->input->post('semester_name');
				$data['faculty_name'] 	       = $this->input->post('faculty_name');
                $data['dept_name'] 	           = $this->input->post('dept_name');
                $data['course_title'] 	       = $this->input->post('course_title');
                $data['course_code'] 	       = $this->input->post('course_code');
                $data['service_no'] 	       = $this->input->post('service_no');
                $data['lecturer_id'] 	       = $this->input->post('lecturer_email');
                $data['level_id'] 	           = $this->input->post('level_id');
                $data['lecture_venue'] 	       = $this->input->post('lecture_venue');
                $data['new_venue'] 	           = $this->input->post('new_venue');
                $data['date'] 	               = $this->input->post('date');
                $data['start_time'] 	       = $this->input->post('start_time');
                $data['end_time'] 	           = $this->input->post('end_time');
                $data['lecture_status'] 	   = $this->input->post('lecture_status');
                $data['rep_matric_no'] 	       = $this->input->post('rep_matric_no');

                
				
					

		//Update lecture status by calling the function in the model class 
		$this->Portal_classrep_model->updateLecture($data);
		$this->session->set_flashdata('success', 'Lecture Updated Successfully');
		
				
		//redirect to the Portal page and refreshes all form input
		redirect('classreps/portal');

		}

	} //end course registration
	
	
		public function today_activities(){
			$rep_id = $this->session->userdata('rep_matric_no');
			$data['portal'] = $this->Portal_classrep_model->today_activities_model($rep_id);
			$this->template->load('classreps', 'default', 'today_activities', $data);   
		}
		
		public function previous_activities(){
			$rep_id = $this->session->userdata('rep_matric_no');
			$data['portal'] = $this->Portal_classrep_model->previous_activities_model($rep_id);
			$this->template->load('classreps', 'default', 'previous_activities', $data); 
	}
}