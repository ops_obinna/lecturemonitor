<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stedept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    
    public function ite_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->iteheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->iteabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->itepostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->iteotherslecturers();
        $this->template->load('admin', 'default', 'statistics/itestat', $data);
    }
    public function sciedu_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->scieduheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->scieduabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->sciedupostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->scieduotherslecturers();
        $this->template->load('admin', 'default', 'statistics/sciedustat', $data);
    }
    public function edutech_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->edutechheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->edutechabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->edutechpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->edutechotherslecturers();
        $this->template->load('admin', 'default', 'statistics/edutechstat', $data);
    }
    public function commedu_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->commeduheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->commeduabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->commedupostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->commeduotherslecturers();
        $this->template->load('admin', 'default', 'statistics/commedustat', $data);
    }

    
     // MICRO LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function ite_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->itelecturerssummary();

        $this->template->load('admin', 'default', 'statistics/itestatsummary', $data);
    }
    //controller to get individual lecturer in ITE   department
    public function ite_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_ite($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_ite($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_ite($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_ite($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_ite($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_ite($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_ite($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_ite($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_ite($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_ite($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_ite($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_ite($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_ite($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_ite($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_ite($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_ite($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_ite($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_ite($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_ite($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_ite($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_ite($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_ite($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_ite($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_ite($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/ite_lecturerstatistics', $data); 
    }   

     // SCIENCE EDU. LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function sciedu_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->sciedulecturerssummary();

        $this->template->load('admin', 'default', 'statistics/sciedustatsummary', $data);
    }
    //controller to get individual lecturer scideu in sciedu department
    public function sciedu_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_sciedu($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_sciedu($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_sciedu($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_sciedu($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_sciedu($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_sciedu($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_sciedu($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_sciedu($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_sciedu($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_sciedu($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_sciedu($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_sciedu($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_sciedu($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_sciedu($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_sciedu($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_sciedu($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_sciedu($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_sciedu($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_sciedu($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_sciedu($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_sciedu($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_sciedu($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_sciedu($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_sciedu($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/sciedu_lecturerstatistics', $data); 
    }   
        
     // EDUTECH LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function edutech_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->edutechlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/edutechstatsummary', $data);
    }
    //controller to get individual lecturer edutech in edutech department
    public function edutech_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_edutech($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_edutech($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_edutech($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_edutech($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_edutech($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_edutech($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_edutech($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_edutech($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_edutech($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_edutech($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_edutech($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_edutech($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_edutech($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_edutech($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_edutech($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_edutech($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_edutech($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_edutech($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_edutech($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_edutech($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_edutech($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_edutech($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_edutech($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_edutech($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/edutech_lecturerstatistics', $data); 
    }   
       
     // COMMEDU LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function commedu_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->commedulecturerssummary();

        $this->template->load('admin', 'default', 'statistics/commedustatsummary', $data);
    }
    //controller to get individual lecturer commedu in commedu department
    public function commedu_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_commedu($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_commedu($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_commedu($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_commedu($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_commedu($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_commedu($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_commedu($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_commedu($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_commedu($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_commedu($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_commedu($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_commedu($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_commedu($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_commedu($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_commedu($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_commedu($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_commedu($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_commedu($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_commedu($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_commedu($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_commedu($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_commedu($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_commedu($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_commedu($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/commedu_lecturerstatistics', $data); 
    }   

    
}



    
    
