<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spsdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }    
    public function chem_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->chemisheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->chemisabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->chemispostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->chemisotherslecturers();
        $this->template->load('admin', 'default', 'statistics/chemisstat', $data);
    }  
    public function geo_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->geoheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->geoabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->geolpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->geolotherslecturers();
        $this->template->load('admin', 'default', 'statistics/geostat', $data);
    }
    public function geol_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->geolheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->geolabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->geolpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->geolotherslecturers();
        $this->template->load('admin', 'default', 'statistics/geolstat', $data);
    }
    
    public function math_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->mathheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->mathabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->mathpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->mathotherslecturers();
        $this->template->load('admin', 'default', 'statistics/mathstat', $data);
    }
    
    public function phy_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->phyheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->phyabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->phypostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->phyotherslecturers();
        $this->template->load('admin', 'default', 'statistics/phystat', $data);
    }
    
    public function stat_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->statheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->statabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->statpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->statotherslecturers();
        $this->template->load('admin', 'default', 'statistics/statstat', $data);
    }

    
    //  CHEMISTRY LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function chemis_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->chemislecturerssummary();

        $this->template->load('admin', 'default', 'statistics/chemisstatsummary', $data);
    }
    //controller to get individual lecturer statistics in chemis department
    public function chemis_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_chemis($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_chemis($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_chemis($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_chemis($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_chemis($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_chemis($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_chemis($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_chemis($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_chemis($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_chemis($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_chemis($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_chemis($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_chemis($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_chemis($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_chemis($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_chemis($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_chemis($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_chemis($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_chemis($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_chemis($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_chemis($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_chemis($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_chemis($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_chemis($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/chemis_lecturerstatistics', $data); 
    }  
    
    
    
    //GEO LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function geo_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->geolecturerssummary();

        $this->template->load('admin', 'default', 'statistics/geostatsummary', $data);
    }
    //controller to get individual lecturer statistics in geo department
    public function geo_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_geo($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_geo($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_geo($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_geo($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_geo($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_geo($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_geo($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_geo($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_geo($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_geo($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_geo($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_geo($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_geo($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_geo($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_geo($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_geo($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_geo($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_geo($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_geo($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_geo($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_geo($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_geo($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_geo($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_geo($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/geo_lecturerstatistics', $data); 
    }   

    
    
   // GEOL LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function geol_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->geollecturerssummary();

        $this->template->load('admin', 'default', 'statistics/geolstatsummary', $data);
    }
    //controller to get individual lecturer statistics in geol department
    public function geol_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_geol($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_geol($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_geol($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_geol($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_geol($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_geol($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_geol($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_geol($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_geol($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_geol($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_geol($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_geol($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_geol($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_geol($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_geol($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_geol($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_geol($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_geol($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_geol($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_geol($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_geol($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_geol($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_geol($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_geol($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/geol_lecturerstatistics', $data); 
    }  
    
    
    
    
     // MATH LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function math_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->mathlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/mathstatsummary', $data);
    }
    //controller to get individual lecturer statistics in math department
    public function math_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_math($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_math($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_math($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_math($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_math($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_math($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_math($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_math($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_math($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_math($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_math($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_math($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_math($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_math($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_math($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_math($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_math($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_math($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_math($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_math($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_math($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_math($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_math($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_math($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/math_lecturerstatistics', $data); 
    } 
    
    
    
    
     // PHY LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function phy_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->phylecturerssummary();

        $this->template->load('admin', 'default', 'statistics/phystatsummary', $data);
    }
    //controller to get individual lecturer statistics in PHY department
    public function phy_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_phy($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_phy($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_phy($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_phy($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_phy($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_phy($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_phy($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_phy($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_phy($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_phy($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_phy($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_phy($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_phy($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_phy($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_phy($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_phy($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_phy($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_phy($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_phy($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_phy($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_phy($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_phy($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_phy($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_phy($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/phy_lecturerstatistics', $data); 
    }   
    
    
    
    
     // GEOL LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function stat_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->statlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/statstatsummary', $data);
    }
    //controller to get individual lecturer statistics in stat department
    public function stat_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_stat($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_stat($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_stat($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_stat($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_stat($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_stat($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_stat($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_stat($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_stat($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_stat($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_stat($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_stat($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_stat($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_stat($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_stat($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_stat($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_stat($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_stat($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_stat($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_stat($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_stat($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_stat($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_stat($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_stat($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/stat_lecturerstatistics', $data); 
    }   
}



    
    
