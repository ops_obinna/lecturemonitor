<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sictdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    public function imt_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->imtheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->imtabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->imtpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->imtotherslecturers();
         //data for held lectures today passed to view
        $data['imttoday']= $this->Statistics_model->imt_heldstattoday();
        $data['cpttoday']= $this->Statistics_model->cpt_heldstattoday();
        $data['csstoday']= $this->Statistics_model->css_heldstattoday();
        $data['littoday']= $this->Statistics_model->lit_heldstattoday();
        
        
        //data for postponed lectures passed to view
        
        $data['imtpostponedtoday'] = $this->Statistics_model->imt_postponedstattoday();
        $data['cptpostponedtoday'] = $this->Statistics_model->cpt_postponedstattoday();
        $data['csspostponedtoday'] = $this->Statistics_model->css_postponedstattoday();
        $data['litpostponedtoday'] = $this->Statistics_model->lit_postponedstattoday();
        
       
        
        //data for absent lectures passed to view
        
        $data['imtabsenttoday'] = $this->Statistics_model->imt_absentstattoday();
        $data['cptabsenttoday'] = $this->Statistics_model->cpt_absentstattoday();
        $data['cssabsenttoday'] = $this->Statistics_model->css_absentstattoday();
        $data['litabsenttoday'] = $this->Statistics_model->lit_absentstattoday();
        
        //data for others lectures passed to view
        
        $data['imtotherstoday'] = $this->Statistics_model->imt_othersstattoday();
        $data['cptotherstoday'] = $this->Statistics_model->cpt_othersstattoday();
        $data['cssotherstoday'] = $this->Statistics_model->css_othersstattoday();
        $data['litotherstoday'] = $this->Statistics_model->lit_othersstattoday();
        $this->template->load('admin', 'default', 'statistics/imtstat', $data);
    }
    
    public function cpt_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->cptheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->cptabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->cptpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->cptotherslecturers();
        $this->template->load('admin', 'default', 'statistics/cptstat', $data);
    }
    
    public function css_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->cssheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->cssabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->csspostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->cssotherslecturers();
        $this->template->load('admin', 'default', 'statistics/cssstat', $data);
    }
    public function lit_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->litheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->litabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->litpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->litotherslecturers();
        $this->template->load('admin', 'default', 'statistics/litstat', $data);
    }
    
    
    
    //IMT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function imt_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->imtlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/imtstatsummary', $data);
    }
    //controller to get individual lecturer statistics in IMT department
    public function imt_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_imt($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_imt($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_imt($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_imt($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_imt($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_imt($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_imt($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_imt($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_imt($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_imt($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_imt($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_imt($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_imt($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_imt($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_imt($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_imt($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_imt($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_imt($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_imt($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_imt($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_imt($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_imt($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_imt($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_imt($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/imt_lecturerstatistics', $data); 
    }
    
     //CPT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function cpt_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->cptlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/cptstatsummary', $data);
    }
    //controller to get individual lecturer statistics in CPT department
    public function cpt_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_cpt($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_cpt($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_cpt($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_cpt($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_cpt($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_cpt($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_cpt($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_cpt($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_cpt($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_cpt($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_cpt($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_cpt($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_cpt($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_cpt($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_cpt($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_cpt($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_cpt($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_cpt($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_cpt($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_cpt($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_cpt($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_cpt($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_cpt($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_cpt($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/cpt_lecturerstatistics', $data); 
    }
    
    
    
    //CSS LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function css_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->csslecturerssummary();

        $this->template->load('admin', 'default', 'statistics/cssstatsummary', $data);
    }
    //controller to get individual lecturer statistics in CSS department
    public function css_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_css($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_css($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_css($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_css($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_css($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_css($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_css($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_css($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_css($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_css($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_css($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_css($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_css($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_css($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_css($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_css($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_css($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_css($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_css($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_css($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_css($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_css($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_css($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_css($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/css_lecturerstatistics', $data); 
    }
    
   
    
     //LIT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function lit_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->litlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/litstatsummary', $data);
    }
    //controller to get individual lecturer statistics in LIT department
    public function lit_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_lit($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_lit($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_lit($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_lit($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_lit($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_lit($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_lit($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_lit($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_lit($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_lit($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_lit($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_lit($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_lit($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_lit($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_lit($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_lit($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_lit($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_lit($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_lit($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_lit($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_lit($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_lit($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_lit($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_lit($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/lit_lecturerstatistics', $data); 
    }
    
}