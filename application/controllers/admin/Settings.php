<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

/* This is the Controller for the Settings that allow the admin to reset the currrent session and semester*/
	
	public function index()
	{
      
      $this->template->load('admin', 'default', 'settings');
	 //$this->load->view('admin/settings');

			
	}
	
	
	public function session_semester(){
        //setting validation rules
		$this->form_validation->set_rules('session_name', 'sesion name','trim|required');
		$this->form_validation->set_rules('semester_name', 'semester name', 'trim|required');
        
        if ($this->form_validation->run() == FALSE)
          {
	 		//load index controllaer containing the template if validation is false 
			$this->index();

		  }else{



			//else if validation is true then create post array
		
				$data['session_name'] 	= $this->input->post('session_name');
				$data['semester_name']	= $this->input->post('semester_name');
            
            
            
            //Register Session by calling the function in the model class 
		$this->settings_model->session_semester($data);
		$this->session->set_flashdata('success', 'Session and Semester set Successfully');
		
				
		//redirect to the Settings page and refrshes all form input
		redirect('admin/Settings', 'refresh');

		}
	}
}
