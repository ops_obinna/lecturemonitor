<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    public function arc_statistics(){
       
        $data['lecture_held']= $this->Statistics_dept_model->archeldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->arcabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->arcpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->arcotherslecturers();
        $this->template->load('admin', 'default', 'statistics/arcstat', $data);
    }
    
    public function bld_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->bldheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->bldabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->bldpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->bldotherslecturers();
        $this->template->load('admin', 'default', 'statistics/bldstat', $data);
    }
    
    public function est_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->estheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->estabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->estpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->estotherslecturers();
        $this->template->load('admin', 'default', 'statistics/eststat', $data);
    }
    public function qts_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->qtsheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->qtsabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->qtspostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->qtsotherslecturers();
        $this->template->load('admin', 'default', 'statistics/qtsstat', $data);
    }
    
    public function svg_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->svgheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->svgabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->svgpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->svgotherslecturers();
        $this->template->load('admin', 'default', 'statistics/svgstat', $data);
    }
    
    public function urp_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->urpheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->urpabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->urppostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->urpotherslecturers();
        $this->template->load('admin', 'default', 'statistics/urpstat', $data);
    }
    
    
    //ARC LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function arc_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->arclecturerssummary();

        $this->template->load('admin', 'default', 'statistics/arcstatsummary', $data);
    }
    //controller to get individual lecturer statistics in arc department
    public function arc_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_arc($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_arc($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_arc($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_arc($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_arc($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_arc($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_arc($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_arc($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_arc($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_arc($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_arc($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_arc($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_arc($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_arc($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_arc($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_arc($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_arc($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_arc($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_arc($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_arc($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_arc($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_arc($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_arc($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_arc($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/arc_lecturerstatistics', $data); 
    }   
    
    

    
    //BLD LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function bld_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->bldlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/bldstatsummary', $data);
    }
    //controller to get individual lecturer statistics in bld department
    public function bld_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_bld($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_bld($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_bld($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_bld($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_bld($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_bld($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_bld($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_bld($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_bld($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_bld($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_bld($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_bld($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_bld($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_bld($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_bld($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_bld($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_bld($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_bld($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_bld($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_bld($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_bld($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_bld($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_bld($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_bld($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/bld_lecturerstatistics', $data); 
    }   
    
    
    
    //EST LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function est_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->estlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/eststatsummary', $data);
    }
    //controller to get individual lecturer statistics in est department
    public function est_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_est($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_est($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_est($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_est($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_est($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_est($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_est($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_est($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_est($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_est($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_est($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_est($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_est($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_est($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_est($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_est($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_est($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_est($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_est($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_est($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_est($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_est($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_est($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_est($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/est_lecturerstatistics', $data); 
    }   
    
    

    
    //QTS LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function qts_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->qtslecturerssummary();

        $this->template->load('admin', 'default', 'statistics/qtsstatsummary', $data);
    }
    //controller to get individual lecturer statistics in qts department
    public function qts_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_qts($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_qts($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_qts($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_qts($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_qts($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_qts($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_qts($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_qts($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_qts($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_qts($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_qts($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_qts($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_qts($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_qts($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_qts($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_qts($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_qts($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_qts($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_qts($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_qts($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_qts($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_qts($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_qts($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_qts($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/qts_lecturerstatistics', $data); 
    }   
    
    
    //SVG LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function svg_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->svglecturerssummary();

        $this->template->load('admin', 'default', 'statistics/svgstatsummary', $data);
    }
    //controller to get individual lecturer statistics in svg department
    public function svg_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_svg($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_svg($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_svg($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_svg($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_svg($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_svg($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_svg($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_svg($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_svg($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_svg($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_svg($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_svg($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_svg($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_svg($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_svg($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_svg($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_svg($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_svg($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_svg($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_svg($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_svg($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_svg($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_svg($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_svg($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/svg_lecturerstatistics', $data); 
    }   
    
    
  
    //URP LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function urp_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->urplecturerssummary();

        $this->template->load('admin', 'default', 'statistics/urpstatsummary', $data);
    }
    //controller to get individual lecturer statistics in urp department
    public function urp_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_urp($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_urp($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_urp($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_urp($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_urp($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_urp($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_urp($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_urp($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_urp($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_urp($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_urp($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_urp($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_urp($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_urp($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_urp($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_urp($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_urp($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_urp($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_urp($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_urp($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_urp($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_urp($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_urp($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_urp($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/urp_lecturerstatistics', $data); 
    }   
    
    
    
    
    
}



    
    
