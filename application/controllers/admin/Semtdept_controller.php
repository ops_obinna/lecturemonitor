<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semtdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    public function ebs_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->ebsheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->ebsabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->ebspostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->ebsotherslecturers();
        $this->template->load('admin', 'default', 'statistics/ebsstat', $data);
       
        
    }
    
    public function pmt_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->pmtheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->pmtabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->pmtpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->pmtotherslecturers();
        $this->template->load('admin', 'default', 'statistics/pmtstat', $data);
    }
    
    public function tmt_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->tmtheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->tmtabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->tmtpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->tmtotherslecturers();
        $this->template->load('admin', 'default', 'statistics/tmtstat', $data);
    }
   
    
     //EBS LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function ebs_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->ebslecturerssummary();

        $this->template->load('admin', 'default', 'statistics/ebsstatsummary', $data);
    }
    //controller to get individual lecturer statistics in ebs department
    public function ebs_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_ebs($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_ebs($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_ebs($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_ebs($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_ebs($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_ebs($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_ebs($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_ebs($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_ebs($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_ebs($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_ebs($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_ebs($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_ebs($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_ebs($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_ebs($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_ebs($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_ebs($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_ebs($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_ebs($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_ebs($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_ebs($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_ebs($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_ebs($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_ebs($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/ebs_lecturerstatistics', $data); 
    }   
        
    
     //PMT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function pmt_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->pmtlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/pmtstatsummary', $data);
    }
    //controller to get individual lecturer statistics in pmt department
    public function pmt_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_pmt($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_pmt($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_pmt($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_pmt($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_pmt($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_pmt($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_pmt($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_pmt($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_pmt($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_pmt($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_pmt($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_pmt($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_pmt($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_pmt($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_pmt($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_pmt($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_pmt($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_pmt($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_pmt($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_pmt($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_pmt($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_pmt($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_pmt($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_pmt($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/pmt_lecturerstatistics', $data); 
    }   
    
        
    
     //TMT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function tmt_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->tmtlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/tmtstatsummary', $data);
    }
    //controller to get individual lecturer statistics in tmt department
    public function tmt_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_tmt($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_tmt($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_tmt($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_tmt($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_tmt($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_tmt($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_tmt($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_tmt($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_tmt($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_tmt($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_tmt($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_tmt($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_tmt($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_tmt($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_tmt($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_tmt($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_tmt($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_tmt($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_tmt($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_tmt($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_tmt($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_tmt($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_tmt($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_tmt($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/tmt_lecturerstatistics', $data); 
    }   
    
    


}



    
    
