<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}	
	}

	

	//loading of view/template for registration portal views

	//start course registration 
	public function registercourse()
	{
      //setting validation rules
		$this->form_validation->set_rules('course_title', 'course title','trim|required');
		$this->form_validation->set_rules('course_code', 'course code', 'trim|required');
        $this->form_validation->set_rules('faculty_id', 'FACULTY ID', 'required');
		$this->form_validation->set_rules('dept_id', 'DEPARTMENT ID', 'required');
        $this->form_validation->set_rules('lecturer_id', 'LECTURER ID', 'required');
		$this->form_validation->set_rules('venue_id', 'VENUE ID', 'required');
		$this->form_validation->set_rules('level_id', 'Level ID', 'required');
		$this->form_validation->set_rules('credit_unit', 'CREDIT UNIT', 'required');
		$this->form_validation->set_rules('credit_hour', 'CREDIT HOUR', 'required');
				
		if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registercourse');

		  }else{



			//else if validation is true then create post array
		
				$data['course_title'] 	= $this->input->post('course_title');
				$data['course_code']	= $this->input->post('course_code');
                $data['faculty_id']		= $this->input->post('faculty_id');
				$data['dept_id']		= $this->input->post('dept_id');
                $data['lecturer_id']	= $this->input->post('lecturer_id');
				$data['venue_id']		= $this->input->post('venue_id');
				$data['level_id']		= $this->input->post('level_id');
				$data['credit_unit']	= $this->input->post('credit_unit');
				$data['credit_hour']	= $this->input->post('credit_hour');
				
					

		//Register course by calling the function in the model class 
		$this->Registration_model->registercourse($data);
		$this->session->set_flashdata('success', 'Course Registered Successfully');
		
				
		//redirect to the registration page and refrshes all form input
		redirect('admin/Registration_controller/registercourse', 'refresh');

		}

	} //end course registration

	//** the function to register class representative **//
	public function registercourserep()
	{
		//setting validation rules
		$this->form_validation->set_rules('rep_matric_no', 'registration Number','trim|required|is_unique[course_reps_table.rep_matric_no]');
		$this->form_validation->set_rules('rep_fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('rep_sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('level_id', 'Level ID', 'required');
		$this->form_validation->set_rules('dept_id', 'Department ID', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]|is_unique[course_reps_table.phone]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[course_reps_table.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		

		if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registercourserep');

		  }else{

             

			//else is validation is true then create post array
            $matric         = $this->input->post('rep_matric_no');
            $fname          = $this->input->post('rep_matric_no');
            $sname          = $this->input->post('rep_sname');
            $level          = $this->input->post('level_id');
            $dept           = $this->input->post('dept_id');
            $phone          = $this->input->post('phone');
            $email          = $this->input->post('email');
            $pass           = $this->input->post('password');
            $passhashed     = password_hash($pass, PASSWORD_DEFAULT);
            $img            = $_FILES["rep_img"]["name"];       
		
//				$data['rep_matric_no'] 	= $this->input->post('rep_matric_no');
//				$data['rep_fname']		= $this->input->post('rep_fname');
//				$data['rep_sname']		= $this->input->post('rep_sname');
//				$data['level_id']		= $this->input->post('level_id');
//				$data['dept_id']		= $this->input->post('dept_id');
//				$data['phone']			= $this->input->post('phone');
//				$data['email']			= $this->input->post('email');
//				$data['password']		= $this->input->post('password');
//				$data['rep_img'] 		= $_FILES["rep_img"]["name"];
            
                $data['rep_matric_no'] 	= $matric;
				$data['rep_fname']		= $fname;
				$data['rep_sname']		= $sname;
				$data['level_id']		= $level;
				$data['dept_id']		= $dept;
				$data['phone']			= $phone;
				$data['email']			= $email;
				$data['password']		= $passhashed;
				$data['rep_img'] 		= $img;
				

		//Register Student (i.e Class Reps) by calling the function in the model class 
		$this->Registration_model->registerclassrep($data);
		$this->session->set_flashdata('success', 'Course Rep registered successfully');

		$rep_id = $this->db->insert_id();
		move_uploaded_file($_FILES["rep_img"]["tmp_name"], "upload/classreps/" . $rep_id . '.jpg');

			
		//redirect to the registration page and refrshes all form input
		redirect('admin/Registration_controller/registercourserep', 'refresh');

		}	
}//close function registercourserep


//** the function to register Lecturer **//
	public function registerlecturer()
	{
       //setting validation rules
		$this->form_validation->set_rules('lecturer_fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lecturer_sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('service_no', 'Service Number','trim|required|is_unique[lecturer_table.service_no]');
		$this->form_validation->set_rules('dept_id', 'Department ID', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]|is_unique[lecturer_table.phone]');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|is_unique[lecturer_table.email_address]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registerlecturer');

		  }else{



			//else is validation is true then create post array
		       $fname         = $this->input->post('lecturer_fname');
               $sname         = $this->input->post('lecturer_sname');
               $pfNo          = $this->input->post('service_no');
               $dept          = $this->input->post('dept_id');
               $phone         = $this->input->post('phone');
               $email         = $this->input->post('email_address');
               $pass          = $this->input->post('password');
               $passhashed    = password_hash($pass, PASSWORD_DEFAULT);
               $img           = $_FILES["lecturer_img"]["name"];
				
//				$data['lecturer_fname']		= $this->input->post('lecturer_fname');
//				$data['lecturer_sname']		= $this->input->post('lecturer_sname');
//				$data['service_no']			= $this->input->post('service_no');
//				$data['dept_id']			= $this->input->post('dept_id');
//				$data['phone']				= $this->input->post('phone');
//				$data['email_address']		= $this->input->post('email_address');
//				$data['password']			= $this->input->post('password');
//				$data['lecturer_img'] 		= $_FILES["lecturer_img"]["name"];
//				
                $data['lecturer_fname']		= $fname;
				$data['lecturer_sname']		= $sname;
				$data['service_no']			= $pfNo;
				$data['dept_id']			= $dept;
				$data['phone']				= $phone;
				$data['email_address']		= $email;
				$data['password']			= $passhashed;
				$data['lecturer_img'] 		= $img;
				

		//Register lecturer by calling the function in the model class 
		$this->Registration_model->registerlecturer($data);
		$this->session->set_flashdata('success', 'Lecturer Registered Successfully');
		$lecturer_id = $this->db->insert_id();
		move_uploaded_file($_FILES["lecturer_img"]["tmp_name"], "upload/lecturer/" . $lecturer_id . '.jpg');

			
		//redirect to the registration page for Lecturers and refreshes all form input
		redirect('admin/Registration_controller/registerlecturer', 'refresh');

		}
	} //close function registerlecturer


	//** the function to register Faculty **//
	public function registerfaculty()
	{
		//setting validation rules
	   $this->form_validation->set_rules('faculty_name', 'Faculty Name', 'trim|required|is_unique[faculty_table.faculty_name]');

       if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registerfaculty');

		  }else{
		  	$data['faculty_name']		= $this->input->post('faculty_name');

		  	//Register faculty by calling the function in the model class 
			$this->Registration_model->registerfaculty($data);
			$this->session->set_flashdata('success', 'Faculty Registered Successfully');

		//redirect to the registration page for faculty and refrshes all form input
		redirect('admin/Registration_controller/registerfaculty', 'refresh');
		  }
	}//close function registerfaculty  



	//** the function to register department **//
	public function registerdept()
	{
       //setting validation rules
	   $this->form_validation->set_rules('dept_name', 'Department Name', 'trim|required|is_unique[department_table.dept_name]');
	   $this->form_validation->set_rules('faculty_id', 'Faculty', 'trim|required');

       if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registerdept');

		  }else{
		  	$data['dept_name']		= $this->input->post('dept_name');
		  	$data['faculty_id']		= $this->input->post('faculty_id');

		  	//Register dept by calling the function in the model class 
			$this->Registration_model->registerdept($data);
			$this->session->set_flashdata('success', 'Department Registered Successfully');

		//redirect to the registration page and refrshes alll form input
		redirect('admin/Registration_controller/registerdept', 'refresh');
		  }
	}//close function registerdept
		

	public function registervenue()
	{
      //setting validation rules
	   $this->form_validation->set_rules('venue_name', 'Venue Name', 'trim|required|is_unique[venue_table.venue_name]');
	   

       if ($this->form_validation->run() == FALSE)
          {
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'register/registervenue');

		  }else{
		  	$data['venue_name']		= $this->input->post('venue_name');
		  	

		  	//Register dept by calling the function in the model class 
			$this->Registration_model->registervenue($data);
			$this->session->set_flashdata('success', 'Venue Registered Successfully');

		//redirect to the registration page and refrshes alll form input
		redirect('admin/Registration_controller/registervenue', 'refresh');
		  }
	}//close function register venue

}//end Controller
?>
