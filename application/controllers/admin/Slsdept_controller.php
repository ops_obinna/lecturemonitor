<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slsdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    
    public function bio_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->bioheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->bioabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->biopostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->biootherslecturers();
        $this->template->load('admin', 'default', 'statistics/biostat', $data);
    }
    
    public function biochem_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->biochemheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->biochemabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->biochempostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->biochemotherslecturers();
        $this->template->load('admin', 'default', 'statistics/biochemstat', $data);
    }
    public function micro_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->microheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->microabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->micropostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->microotherslecturers();
        $this->template->load('admin', 'default', 'statistics/microstat', $data);
    }
    

    
    
    
      // BIO LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function bio_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->biolecturerssummary();

        $this->template->load('admin', 'default', 'statistics/biostatsummary', $data);
    }
    //controller to get individual lecturer statistics in bio department
    public function bio_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_bio($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_bio($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_bio($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_bio($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_bio($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_bio($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_bio($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_bio($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_bio($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_bio($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_bio($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_bio($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_bio($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_bio($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_bio($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_bio($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_bio($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_bio($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_bio($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_bio($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_bio($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_bio($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_bio($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_bio($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/bio_lecturerstatistics', $data); 
    }   

    
    
      // BIOCHEM LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function biochem_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->biochemlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/biochemstatsummary', $data);
    }
    //controller to get individual lecturer biochem in stat department
    public function biochem_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_biochem($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_biochem($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_biochem($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_biochem($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_biochem($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_biochem($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_biochem($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_biochem($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_biochem($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_biochem($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_biochem($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_biochem($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_biochem($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_biochem($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_biochem($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_biochem($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_biochem($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_biochem($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_biochem($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_biochem($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_biochem($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_biochem($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_biochem($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_biochem($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/biochem_lecturerstatistics', $data); 
    }   

    
    
    
      // MICRO LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function micro_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->microlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/microstatsummary', $data);
    }
    //controller to get individual lecturer microbio in micro department
    public function micro_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_micro($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_micro($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_micro($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_micro($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_micro($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_micro($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_micro($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_micro($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_micro($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_micro($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_micro($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_micro($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_micro($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_micro($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_micro($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_micro($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_micro($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_micro($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_micro($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_micro($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_micro($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_micro($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_micro($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_micro($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/micro_lecturerstatistics', $data); 
    }   
}



    
    
