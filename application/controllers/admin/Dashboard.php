<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

/* This is the Controller for the Dashboard that display daily Ongoing Lectures around the University*/
	
	public function index()
	{
      $data['dashboard'] = $this->ongoinglecture_model->index();
      $this->template->load('admin', 'default', 'dashboard', $data);
	 //$this->load->view('admin/dashboard');

			
	}
}
