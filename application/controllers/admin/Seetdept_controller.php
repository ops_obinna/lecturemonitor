<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seetdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    public function abe_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->abeheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->abeabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->abepostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->abeotherslecturers();
        $this->template->load('admin', 'default', 'statistics/abestat', $data);
       
        
    }
    
    public function elect_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->electheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->electabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->electpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->electotherslecturers();
        $this->template->load('admin', 'default', 'statistics/electstat', $data);
    }
    
    public function mech_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->mechheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->mechabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->mechpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->mechotherslecturers();
        $this->template->load('admin', 'default', 'statistics/mechstat', $data);
    }
    public function mecha_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->mechaheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->mechaabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->mechapostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->mechaotherslecturers();
        $this->template->load('admin', 'default', 'statistics/mechastat', $data);
    }
    
    public function comp_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->compheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->compabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->comppostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->compotherslecturers();
        $this->template->load('admin', 'default', 'statistics/compstat', $data);
    }
    
    public function telecom_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->telecomheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->telecomabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->telecompostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->telecomotherslecturers();
        $this->template->load('admin', 'default', 'statistics/telecomstat', $data);
    }
    
    public function metallurg_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->metallurgheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->metallurgabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->metallurgpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->metallurgotherslecturers();
        $this->template->load('admin', 'default', 'statistics/metallurgstat', $data);
    }
    
    public function civil_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->civilheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->civilabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->civilpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->civilotherslecturers();
        $this->template->load('admin', 'default', 'statistics/civilstat', $data);
    }
    
    
    public function chem_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->chemheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->chemabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->chempostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->chemotherslecturers();
        $this->template->load('admin', 'default', 'statistics/chemstat', $data);
    }

    
    
     //ABE LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function abe_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->abelecturerssummary();

        $this->template->load('admin', 'default', 'statistics/abestatsummary', $data);
    }
    //controller to get individual lecturer statistics in ABE department
    public function abe_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_abe($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_abe($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_abe($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_abe($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_abe($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_abe($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_abe($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_abe($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_abe($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_abe($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_abe($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_abe($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_abe($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_abe($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_abe($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_abe($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_abe($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_abe($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_abe($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_abe($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_abe($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_abe($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_abe($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_abe($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/abe_lecturerstatistics', $data); 
    }
    
    
     
     //ELECT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function elect_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->electlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/electstatsummary', $data);
    }
    //controller to get individual lecturer statistics in ELECT department
    public function elect_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_elect($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_elect($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_elect($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_elect($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_elect($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_elect($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_elect($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_elect($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_elect($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_elect($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_elect($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_elect($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_elect($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_elect($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_elect($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_elect($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_elect($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_elect($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_elect($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_elect($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_elect($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_elect($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_elect($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_elect($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/elect_lecturerstatistics', $data); 
    }
    
    
     //MECH LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function mech_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->mechlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/mechstatsummary', $data);
    }
    //controller to get individual lecturer statistics in MECH department
    public function mech_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_mech($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_mech($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_mech($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_mech($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_mech($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_mech($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_mech($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_mech($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_mech($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_mech($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_mech($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_mech($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_mech($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_mech($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_mech($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_mech($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_mech($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_mech($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_mech($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_mech($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_mech($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_mech($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_mech($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_mech($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/mech_lecturerstatistics', $data); 
    }
    
    
    
    
     //MECHA LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function mecha_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->mechalecturerssummary();

        $this->template->load('admin', 'default', 'statistics/mechastatsummary', $data);
    }
    //controller to get individual lecturer statistics in MECHA department
    public function mecha_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_mecha($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_mecha($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_mecha($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_mecha($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_mecha($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_mecha($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_mecha($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_mecha($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_mecha($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_mecha($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_mecha($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_mecha($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_mecha($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_mecha($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_mecha($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_mecha($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_mecha($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_mecha($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_mecha($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_mecha($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_mecha($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_mecha($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_mecha($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_mecha($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/mecha_lecturerstatistics', $data); 
    }
    
    

    
    
     //CPE LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function cpe_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->cpelecturerssummary();

        $this->template->load('admin', 'default', 'statistics/cpestatsummary', $data);
    }
    //controller to get individual lecturer statistics in CPE department
    public function cpe_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_cpe($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_cpe($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_cpe($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_cpe($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_cpe($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_cpe($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_cpe($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_cpe($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_cpe($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_cpe($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_cpe($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_cpe($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_cpe($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_cpe($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_cpe($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_cpe($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_cpe($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_cpe($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_cpe($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_cpe($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_cpe($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_cpe($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_cpe($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_cpe($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/cpe_lecturerstatistics', $data); 
    }
    
    
    
    
     //TELECOM LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function telecom_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->telecomlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/telecomstatsummary', $data);
    }
    //controller to get individual lecturer statistics in Telecom department
    public function telecom_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_telecom($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_telecom($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_telecom($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_telecom($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_telecom($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_telecom($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_telecom($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_telecom($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_telecom($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_telecom($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_telecom($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_telecom($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_telecom($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_telecom($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_telecom($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_telecom($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_telecom($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_telecom($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_telecom($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_telecom($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_telecom($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_telecom($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_telecom($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_telecom($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/telecom_lecturerstatistics', $data); 
    }
    

  //METALLURG LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function metallurg_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->metallurglecturerssummary();

        $this->template->load('admin', 'default', 'statistics/metallurgstatsummary', $data);
    }
    //controller to get individual lecturer statistics in metallurg department
    public function metallurg_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_metallurg($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_metallurg($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_metallurg($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_metallurg($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_metallurg($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_metallurg($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_metallurg($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_metallurg($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_metallurg($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_metallurg($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_metallurg($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_metallurg($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_metallurg($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_metallurg($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_metallurg($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_metallurg($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_metallurg($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_metallurg($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_metallurg($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_metallurg($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_metallurg($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_metallurg($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_metallurg($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_metallurg($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/metallurg_lecturerstatistics', $data); 
    }   
    
    
    
    //CIVIL LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function civil_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->civillecturerssummary();

        $this->template->load('admin', 'default', 'statistics/civilstatsummary', $data);
    }
    //controller to get individual lecturer statistics in civil department
    public function civil_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_civil($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_civil($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_civil($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_civil($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_civil($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_civil($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_civil($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_civil($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_civil($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_civil($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_civil($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_civil($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_civil($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_civil($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_civil($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_civil($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_civil($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_civil($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_civil($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_civil($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_civil($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_civil($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_civil($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_civil($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/civil_lecturerstatistics', $data); 
    }   
    
    
    
    //CHEM LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function chem_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->chemlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/chemstatsummary', $data);
    }
    //controller to get individual lecturer statistics in chem department
    public function chem_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_chem($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_chem($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_chem($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_chem($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_chem($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_chem($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_chem($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_chem($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_chem($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_chem($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_chem($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_chem($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_chem($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_chem($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_chem($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_chem($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_chem($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_chem($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_chem($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_chem($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_chem($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_chem($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_chem($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_chem($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/chem_lecturerstatistics', $data); 
    }   
    
    
    
}



    
    
