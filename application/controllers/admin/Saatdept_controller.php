<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saatdept_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        
    }
    
    
    public function aet_statistics(){
        
        $data['lecture_held']= $this->Statistics_dept_model->aetheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->aetabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->aetpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->aetotherslecturers();
        $this->template->load('admin', 'default', 'statistics/aetstat', $data);
    }
    
    public function apt_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->aptheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->aptabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->aptpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->aptotherslecturers();
        $this->template->load('admin', 'default', 'statistics/aptstat', $data);
    }
    public function crp_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->crpheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->crpabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->crppostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->crpotherslecturers();
        $this->template->load('admin', 'default', 'statistics/crpstat', $data);
    }
    
    public function fst_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->fstheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->fstabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->fstpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->fstotherslecturers();
        $this->template->load('admin', 'default', 'statistics/fststat', $data);
    }
    
    public function ssd_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->ssdheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->ssdabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->ssdpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->ssdotherslecturers();
        $this->template->load('admin', 'default', 'statistics/ssdstat', $data);
    }
    
    public function aft_statistics(){
        $data['lecture_held']= $this->Statistics_dept_model->aftheldlecturers();
        $data['lecture_absent']= $this->Statistics_dept_model->aftabsentlecturers();
        $data['lecture_postponed']= $this->Statistics_dept_model->aftpostponedlecturers();
        $data['lecture_others']= $this->Statistics_dept_model->aftotherslecturers();
        $this->template->load('admin', 'default', 'statistics/aftstat', $data);
    }
    
    
    
     //AET LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function aet_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->aetlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/aetstatsummary', $data);
    }
    //controller to get individual lecturer statistics in aet department
    public function aet_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_aet($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_aet($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_aet($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_aet($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_aet($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_aet($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_aet($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_aet($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_aet($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_aet($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_aet($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_aet($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_aet($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_aet($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_aet($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_aet($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_aet($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_aet($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_aet($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_aet($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_aet($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_aet($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_aet($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_aet($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/aet_lecturerstatistics', $data); 
    }   
    

    
    
     //APT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function apt_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->aptlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/aptstatsummary', $data);
    }
    //controller to get individual lecturer statistics in apt department
    public function apt_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_apt($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_apt($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_apt($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_apt($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_apt($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_apt($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_apt($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_apt($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_apt($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_apt($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_apt($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_apt($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_apt($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_apt($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_apt($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_apt($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_apt($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_apt($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_apt($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_apt($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_apt($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_apt($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_apt($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_apt($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/apt_lecturerstatistics', $data); 
    }   
    
    
    
     //CRP LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function crp_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->crplecturerssummary();

        $this->template->load('admin', 'default', 'statistics/crpstatsummary', $data);
    }
    //controller to get individual lecturer statistics in crp department
    public function crp_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_crp($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_crp($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_crp($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_crp($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_crp($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_crp($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_crp($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_crp($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_crp($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_crp($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_crp($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_crp($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_crp($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_crp($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_crp($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_crp($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_crp($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_crp($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_crp($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_crp($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_crp($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_crp($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_crp($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_crp($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/crp_lecturerstatistics', $data); 
    }   
    
    
 //FST LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function fst_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->fstlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/fststatsummary', $data);
    }
    //controller to get individual lecturer statistics in fst department
    public function fst_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_fst($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_fst($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_fst($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_fst($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_fst($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_fst($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_fst($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_fst($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_fst($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_fst($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_fst($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_fst($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_fst($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_fst($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_fst($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_fst($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_fst($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_fst($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_fst($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_fst($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_fst($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_fst($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_fst($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_fst($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/fst_lecturerstatistics', $data); 
    }   
    

    
     //SSD LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function ssd_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->ssdlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/ssdstatsummary', $data);
    }
    //controller to get individual lecturer statistics in ssd department
    public function ssd_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_ssd($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_ssd($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_ssd($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_ssd($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_ssd($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_ssd($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_ssd($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_ssd($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_ssd($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_ssd($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_ssd($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_ssd($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_ssd($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_ssd($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_ssd($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_ssd($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_ssd($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_ssd($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_ssd($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_ssd($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_ssd($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_ssd($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_ssd($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_ssd($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/ssd_lecturerstatistics', $data); 
    }   
    
    
    
     //AFT LIST OF LECTURERS TO GET EACH LECTURER PERFORMANCE
     public function aft_statisticssummary(){
        $data['lecturersummary']= $this->Statistics_dept_model->aftlecturerssummary();

        $this->template->load('admin', 'default', 'statistics/aftstatsummary', $data);
    }
    //controller to get individual lecturer statistics in aft department
    public function aft_lecturersstat($lecturer_id){
        $data['held'] = $this->Statistics_dept_model->lecture_held_aft($lecturer_id);	
        $data['absent'] = $this->Statistics_dept_model->lecture_absent_aft($lecturer_id);
        $data['postponed'] = $this->Statistics_dept_model->lecture_postponed_aft($lecturer_id);
        $data['others'] = $this->Statistics_dept_model->lecture_others_aft($lecturer_id);
        
        //semesterial summary for 500Level hundred
        $data['heldfive'] = $this->Statistics_dept_model->held_summary_five_aft($lecturer_id);
        $data['absentfive'] = $this->Statistics_dept_model->absent_summary_five_aft($lecturer_id);
        $data['postponedfive'] = $this->Statistics_dept_model->postponed_summary_five_aft($lecturer_id);
        $data['othersfive'] = $this->Statistics_dept_model->others_summary_five_aft($lecturer_id);
      
         //semesterial summary for 400Level hundred
        $data['heldfour'] = $this->Statistics_dept_model->held_summary_four_aft($lecturer_id);
        $data['absentfour'] = $this->Statistics_dept_model->absent_summary_four_aft($lecturer_id);
        $data['postponedfour'] = $this->Statistics_dept_model->postponed_summary_four_aft($lecturer_id);
        $data['othersfour'] = $this->Statistics_dept_model->others_summary_four_aft($lecturer_id);
        
         //semesterial summary for 300Level hundred
        $data['heldthree'] = $this->Statistics_dept_model->held_summary_three_aft($lecturer_id);
        $data['absentthree'] = $this->Statistics_dept_model->absent_summary_three_aft($lecturer_id);
        $data['postponedthree'] = $this->Statistics_dept_model->postponed_summary_three_aft($lecturer_id);
        $data['othersthree'] = $this->Statistics_dept_model->others_summary_three_aft($lecturer_id);
        
         //semesterial summary for 200Level hundred
        $data['heldtwo'] = $this->Statistics_dept_model->held_summary_two_aft($lecturer_id);
        $data['absenttwo'] = $this->Statistics_dept_model->absent_summary_two_aft($lecturer_id);
        $data['postponedtwo'] = $this->Statistics_dept_model->postponed_summary_two_aft($lecturer_id);
        $data['otherstwo'] = $this->Statistics_dept_model->others_summary_two_aft($lecturer_id);
        
         //semesterial summary for 100Level hundred
        $data['heldone'] = $this->Statistics_dept_model->held_summary_one_aft($lecturer_id);
        $data['absentone'] = $this->Statistics_dept_model->absent_summary_one_aft($lecturer_id);
        $data['postponedone'] = $this->Statistics_dept_model->postponed_summary_one_aft($lecturer_id);
        $data['othersone'] = $this->Statistics_dept_model->others_summary_one_aft($lecturer_id);
        
        $this->template->load('admin', 'default', 'statistics/aft_lecturerstatistics', $data); 
    }   
    
    
    

}



    
    
