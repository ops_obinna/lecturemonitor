<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ongoinlecture_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

	
	//functions to load SICT views/Template
	public function infotech()
	{  
	   $data['infotech'] = $this->ongoinglecture_model->getlist_infotech();
       $this->template->load('admin', 'default', 'ongoinlectures/sict/infotech', $data);
	}

	public function compsci()
	{
		 $data['compsci'] = $this->ongoinglecture_model->getlist_compsci();
		 $this->template->load('admin', 'default', 'ongoinlectures/sict/compsci', $data); 
	}

	public function cybersec()
	{   
		$data['cybersec'] = $this->ongoinglecture_model->getlist_cybersec();
		 $this->template->load('admin', 'default', 'ongoinlectures/sict/cybersec', $data); 
	}
	public function libinfotech()
	{   
		$data['libinfotech'] = $this->ongoinglecture_model->getlist_libinfotech();
		 $this->template->load('admin', 'default', 'ongoinlectures/sict/libinfotech', $data); 
	}


	//functions to load SEET views/Template
	public function agricbiores()
	{   
		$data['agricbiores'] = $this->ongoinglecture_model->getlist_agricbiores();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/agricbiores', $data); 
	}
	public function chemeng()
	{   
		$data['chemeng'] = $this->ongoinglecture_model->getlist_chemeng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/chemeng', $data); 
	}
	public function civileng()
	{   
		 $data['civileng'] = $this->ongoinglecture_model->getlist_civileng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/civileng', $data); 
	}
	public function compeng()
	{   
		$data['compeng'] = $this->ongoinglecture_model->getlist_compeng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/compeng', $data); 
	}
	public function electeng()
	{   
		$data['electeng'] = $this->ongoinglecture_model->getlist_electeng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/electeng', $data); 
	}
	public function mechatroeng()
	{   
		$data['mechatroeng'] = $this->ongoinglecture_model->getlist_mechatroeng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/mechatroeng', $data); 
	}
	public function mecheng()
	{   
		 $data['mecheng'] = $this->ongoinglecture_model->getlist_mecheng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/mecheng', $data); 
	}
	public function metallureng()
	{   
		$data['metallureng'] = $this->ongoinglecture_model->getlist_metallureng();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/metallureng', $data); 
	}
	public function telecom()
	{   
		$data['telecom'] = $this->ongoinglecture_model->getlist_telecom();
		 $this->template->load('admin', 'default', 'ongoinlectures/seet/telecom', $data); 
	}




	//functions to load SAAT views/Template
	public function agricext()
	{   
		 $data['agricext'] = $this->ongoinglecture_model->getlist_agricext();
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/agricext', $data); 
	}
	public function animalprod()
	{   
		 $data['animalprod'] = $this->ongoinglecture_model->getlist_animalprod();
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/animalprod',$data); 
	}
	public function aquafishtech()
	{	
		 $data['aquafishtech'] = $this->ongoinglecture_model->getlist_aquafishtech();	   
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/aquafishtech', $data); 
	}
	public function cropprod()
	{   
		 $data['cropprod'] = $this->ongoinglecture_model->getlist_cropprod();
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/cropprod', $data); 
	}
	public function foodtech()
	{ 		
		 $data['foodtech'] = $this->ongoinglecture_model->getlist_foodtech();  
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/foodtech', $data); 
	}
	public function soilsci()
	{   
		 $data['soilsci'] = $this->ongoinglecture_model->getlist_soilsci();
		 $this->template->load('admin', 'default', 'ongoinlectures/saat/soilsci', $data); 
	}



	//functions to load SET views/Template
	public function archi()
	{   
		$data['archi'] = $this->ongoinglecture_model->getlist_archi();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/archi', $data); 
	}
	public function building()
	{   
		$data['building'] = $this->ongoinglecture_model->getlist_building();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/building', $data); 
	}
	public function estatemgt()
	{   
		$data['estatemgt'] = $this->ongoinglecture_model->getlist_estatemgt();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/estatemgt', $data); 
	}
	public function quantsurv()
	{   
		$data['quantsurv'] = $this->ongoinglecture_model->getlist_quantsurv();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/quantsurv', $data); 
	}
	public function svg()
	{   
		$data['svg'] = $this->ongoinglecture_model->getlist_svg();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/svg', $data); 
	}
	public function urp()
	{   
		$data['urp'] = $this->ongoinglecture_model->getlist_urp();
		 $this->template->load('admin', 'default', 'ongoinlectures/set/urp', $data); 
	}


	//functions to load SEMT views/Template
	public function ebs()
	{   
		 $data['ebs'] = $this->ongoinglecture_model->getlist_ebs();
		 $this->template->load('admin', 'default', 'ongoinlectures/semt/ebs', $data); 
	}
	public function projmgt()
	{   
		$data['projmgt'] = $this->ongoinglecture_model->getlist_projmgt();
		 $this->template->load('admin', 'default', 'ongoinlectures/semt/projmgt', $data); 
	}
	public function tranmgt()
	{   
		$data['tranmgt'] = $this->ongoinglecture_model->getlist_tranmgt();
		 $this->template->load('admin', 'default', 'ongoinlectures/semt/tranmgt', $data); 
	}


	//functions to load SPS views/Template
	public function chem()
	{   
		$data['chem'] = $this->ongoinglecture_model->getlist_chem();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/chem',  $data); 
	}
	public function geography()
	{   
		$data['geography'] = $this->ongoinglecture_model->getlist_geography();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/geography', $data); 
	}
	public function geology()
	{   
		 $data['geology'] = $this->ongoinglecture_model->getlist_geology();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/geology', $data); 
	}
	public function math()
	{   
		$data['math'] = $this->ongoinglecture_model->getlist_math();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/math', $data); 
	}
	public function physics()
	{   
		$data['physics'] = $this->ongoinglecture_model->getlist_physics();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/physics', $data); 
	}
	public function stat()
	{   
		$data['stat'] = $this->ongoinglecture_model->getlist_stat();
		 $this->template->load('admin', 'default', 'ongoinlectures/sps/stat', $data); 
	}




	//functions to load SLS views/Template
	public function biochem()
	{   
		$data['biochem'] = $this->ongoinglecture_model->getlist_biochem();
		 $this->template->load('admin', 'default', 'ongoinlectures/sls/biochem', $data); 
	}
	
	public function biosci()
	{   
		$data['biosci'] = $this->ongoinglecture_model->getlist_biosci();
		 $this->template->load('admin', 'default', 'ongoinlectures/sls/biosci', $data); 
	}
	public function microbio()
	{   
		$data['microbio'] = $this->ongoinglecture_model->getlist_microbio();
		 $this->template->load('admin', 'default', 'ongoinlectures/sls/microbio', $data); 
	}


	//functions to load SSTE views/Template
	public function commedu()
	{   
		$data['commedu'] = $this->ongoinglecture_model->getlist_commedu();
		 $this->template->load('admin', 'default', 'ongoinlectures/sste/commedu', $data); 
	}
	public function edutech()
	{   
		$data['edutech'] = $this->ongoinglecture_model->getlist_edutech();
		 $this->template->load('admin', 'default', 'ongoinlectures/sste/edutech', $data); 
	}
	public function indtechedu()
	{   
		$data['indtechedu'] = $this->ongoinglecture_model->getlist_indtechedu();
		 $this->template->load('admin', 'default', 'ongoinlectures/sste/indtechedu', $data); 
	}
	public function sciedu()
	{   
		$data['sciedu'] = $this->ongoinglecture_model->getlist_sciedu();
		 $this->template->load('admin', 'default', 'ongoinlectures/sste/sciedu', $data); 
	}
	
	
	
	
	





	
}
