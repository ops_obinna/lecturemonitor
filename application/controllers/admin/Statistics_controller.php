<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    //index function to display lectures for faculty for the present date
    public function index(){
        /*Statistics data for held lectures passed to the view*/
        $data['sict']= $this->Statistics_model->sict_stat();
        $data['seet']= $this->Statistics_model->seet_stat();
        $data['set']= $this->Statistics_model->set_stat();
        $data['semt']= $this->Statistics_model->semt_stat();
        $data['saat']= $this->Statistics_model->saat_stat();
        $data['sps']= $this->Statistics_model->sps_stat();
        $data['ste']= $this->Statistics_model->ste_stat();
        $data['sls']= $this->Statistics_model->sls_stat();
        
         /*Statistics data for postponed lectures passed to the view*/
        $data['sictpostponed']= $this->Statistics_model->sict_postponedstat();
        $data['seetpostponed']= $this->Statistics_model->seet_postponedstat();
        $data['setpostponed']= $this->Statistics_model->set_postponedstat();
        $data['semtpostponed']= $this->Statistics_model->semt_postponedstat();
        $data['saatpostponed']= $this->Statistics_model->saat_postponedstat();
        $data['spspostponed']= $this->Statistics_model->sps_postponedstat();
        $data['stepostponed']= $this->Statistics_model->ste_postponedstat();
        $data['slspostponed']= $this->Statistics_model->sls_postponedstat();
        
        
        
        /*Statistics data for Absent lectures passed to the view*/
        $data['sictabsent']= $this->Statistics_model->sict_absentstat();
        $data['seetabsent']= $this->Statistics_model->seet_absentstat();
        $data['setabsent']= $this->Statistics_model->set_absentstat();
        $data['semtabsent']= $this->Statistics_model->semt_absentstat();
        $data['saatabsent']= $this->Statistics_model->saat_absentstat();
        $data['spsabsent']= $this->Statistics_model->sps_absentstat();
        $data['steabsent']= $this->Statistics_model->ste_absentstat();
        $data['slsabsent']= $this->Statistics_model->sls_absentstat();
        
         /*Statistics data for Absent lecturers for other reasons passed to the view*/
        $data['sictothers']= $this->Statistics_model->sict_othersstat();
        $data['seetothers']= $this->Statistics_model->seet_othersstat();
        $data['setothers']= $this->Statistics_model->set_othersstat();
        $data['semtothers']= $this->Statistics_model->semt_othersstat();
        $data['saatothers']= $this->Statistics_model->saat_othersstat();
        $data['spsothers']= $this->Statistics_model->sps_othersstat();
        $data['steothers']= $this->Statistics_model->ste_othersstat();
        $data['slsothers']= $this->Statistics_model->sls_othersstat();
        
        //summary of supposed lecture too be held for sict
        //This data array outputs daily lecture summary for faculties 
        $data['sictsummary']= $this->Statistics_model->sictdailysummary();
        
        //summary of supposed lecture to be held for seet
        $data['seetsummary']= $this->Statistics_model->seetdailysummary();
        
        //summary of supposed lecture to be held for saat
        $data['saatsummary']= $this->Statistics_model->saatdailysummary();
        
        //summary of supposed lecture to be held for semt
        $data['semtsummary']= $this->Statistics_model->semtdailysummary();
        
        //summary of supposed lecture to be held for set
        $data['setsummary']= $this->Statistics_model->setdailysummary();
        
        //summary of supposed lecture to be held for sls
        $data['slssummary']= $this->Statistics_model->slsdailysummary();
        
        //summary of supposed lecture too be held for sps
        $data['spssummary']= $this->Statistics_model->spsdailysummary();
        
        //summary of supposed lecture too be held for ste
        $data['stesummary']= $this->Statistics_model->stedailysummary();
        
        
        $this->template->load('admin', 'default', 'statistics/faculty', $data);
    }
    
    //SICT LEctures for the current date
    public function sict_statistics(){
        //data for held lectures today passed to view
        $data['imttoday']= $this->Statistics_model->imt_heldstattoday();
        $data['cpttoday']= $this->Statistics_model->cpt_heldstattoday();
        $data['csstoday']= $this->Statistics_model->css_heldstattoday();
        $data['littoday']= $this->Statistics_model->lit_heldstattoday();
        
        
        //data for postponed lectures passed to view
        
        $data['imtpostponedtoday'] = $this->Statistics_model->imt_postponedstattoday();
        $data['cptpostponedtoday'] = $this->Statistics_model->cpt_postponedstattoday();
        $data['csspostponedtoday'] = $this->Statistics_model->css_postponedstattoday();
        $data['litpostponedtoday'] = $this->Statistics_model->lit_postponedstattoday();
        
       
        
        //data for absent lectures passed to view
        
        $data['imtabsenttoday'] = $this->Statistics_model->imt_absentstattoday();
        $data['cptabsenttoday'] = $this->Statistics_model->cpt_absentstattoday();
        $data['cssabsenttoday'] = $this->Statistics_model->css_absentstattoday();
        $data['litabsenttoday'] = $this->Statistics_model->lit_absentstattoday();
        
        //data for others lectures passed to view
        
        $data['imtotherstoday'] = $this->Statistics_model->imt_othersstattoday();
        $data['cptotherstoday'] = $this->Statistics_model->cpt_othersstattoday();
        $data['cssotherstoday'] = $this->Statistics_model->css_othersstattoday();
        $data['litotherstoday'] = $this->Statistics_model->lit_othersstattoday();
        
        
        //summary of supposed lecture to be held for imt
        $data['imtsummary']= $this->Statistics_model->imtdailysummary();
        
        
        //summary of supposed lecture to be held for cpt
        $data['cptsummary']= $this->Statistics_model->cptdailysummary();
        
        //summary of supposed lecture to be held for css
        $data['csssummary']= $this->Statistics_model->cssdailysummary();
        
        
        //summary of supposed lecture to be held for lit
        $data['litsummary']= $this->Statistics_model->litdailysummary();
                
        
        
        $this->template->load('admin', 'default', 'statistics/sictstatistics', $data);
    }
    
     //SEET LEctures for the current date
    public function seet_statistics(){
        
        //data for held lectures today passed to view
        $data['abetoday']= $this->Statistics_model->abe_heldstattoday();
        $data['electtoday']= $this->Statistics_model->elect_heldstattoday();
        $data['mechtoday']= $this->Statistics_model->mech_heldstattoday();
        $data['mechtrotoday']= $this->Statistics_model->mechtro_heldstattoday();
        $data['comptoday']= $this->Statistics_model->comp_heldstattoday();
        $data['telecomtoday']= $this->Statistics_model->telecom_heldstattoday();
        $data['metallurgtoday']= $this->Statistics_model->metallurg_heldstattoday();
        $data['civiltoday']= $this->Statistics_model->civil_heldstattoday();
        $data['chemtoday']= $this->Statistics_model->chem_heldstattoday();
        
        
         //data for postponed lectures today passed to view
        $data['abepostponedtoday']= $this->Statistics_model->abe_postponedstattoday();
        $data['electpostponedtoday']= $this->Statistics_model->elect_postponedstattoday();
        $data['mechpostponedtoday']= $this->Statistics_model->mech_postponedstattoday();
        $data['mechtropostponedtoday']= $this->Statistics_model->mechtro_postponedstattoday();
        $data['comppostponedtoday']= $this->Statistics_model->comp_postponedstattoday();
        $data['telecompostponedtoday']= $this->Statistics_model->telecom_postponedstattoday();
        $data['metallurgpostponedtoday']= $this->Statistics_model->metallurg_postponedstattoday();
        $data['civilpostponedtoday']= $this->Statistics_model->civil_postponedstattoday();
        $data['chempostponedtoday']= $this->Statistics_model->chem_postponedstattoday();
        
        //data for absent lectures today passed to view
        $data['abeabsenttoday']= $this->Statistics_model->abe_absentstattoday();
        $data['electabsenttoday']= $this->Statistics_model->elect_absentstattoday();
        $data['mechabsenttoday']= $this->Statistics_model->mech_absentstattoday();
        $data['mechtroabsenttoday']= $this->Statistics_model->mechtro_absentstattoday();
        $data['compabsenttoday']= $this->Statistics_model->comp_absentstattoday();
        $data['telecomabsenttoday']= $this->Statistics_model->telecom_absentstattoday();
        $data['metallurgabsenttoday']= $this->Statistics_model->metallurg_absentstattoday();
        $data['civilabsenttoday']= $this->Statistics_model->civil_absentstattoday();
        $data['chemabsenttoday']= $this->Statistics_model->chem_absentstattoday();
        
        //data for absent lecturers today for other reasons passed to view
        $data['abeotherstoday']= $this->Statistics_model->abe_othersstattoday();
        $data['electotherstoday']= $this->Statistics_model->elect_othersstattoday();
        $data['mechotherstoday']= $this->Statistics_model->mech_othersstattoday();
        $data['mechtrootherstoday']= $this->Statistics_model->mechtro_othersstattoday();
        $data['compotherstoday']= $this->Statistics_model->comp_othersstattoday();
        $data['telecomotherstoday']= $this->Statistics_model->telecom_othersstattoday();
        $data['metallurgotherstoday']= $this->Statistics_model->metallurg_othersstattoday();
        $data['civilotherstoday']= $this->Statistics_model->civil_othersstattoday();
        $data['chemotherstoday']= $this->Statistics_model->chem_othersstattoday();
        
        
        //summary of supposed lecture to be held for Agric and Bioresource Engineering
        $data['abesummary']= $this->Statistics_model->abedailysummary();
        
        //summary of supposed lecture to be held for Electrical Engineering
        $data['electsummary']= $this->Statistics_model->electdailysummary();
        
        //summary of supposed lecture to be held for Mechanical Engineering
        $data['mechsummary']= $this->Statistics_model->mechdailysummary();
        
        //summary of supposed lecture to be held for Mechatronics Engineering
        $data['mechtrosummary']= $this->Statistics_model->mechtrodailysummary();
        
        //summary of supposed lecture to be held for Agric and Computer Engineering
        $data['compsummary']= $this->Statistics_model->compdailysummary();
        
        //summary of supposed lecture to be held for Telecom Enginering Engineering
        $data['telecomsummary']= $this->Statistics_model->telecomdailysummary();
        
        //summary of supposed lecture to be held for Metallurgical Engineering
        $data['metallurgsummary']= $this->Statistics_model->metallurgdailysummary();
        
        //summary of supposed lecture to be held for Civil Engineering
        $data['civilsummary']= $this->Statistics_model->civildailysummary();
            
        //summary of supposed lecture to be held for Chemical Engineering
        $data['chemsummary']= $this->Statistics_model->chemdailysummary();
        
        $this->template->load('admin', 'default', 'statistics/seetstatistics', $data);
        
    }
    
    public function semt_statistics(){
         //data for held lectures today passed to view
        $data['ebstoday']= $this->Statistics_model->ebs_heldstattoday();
        $data['pmttoday']= $this->Statistics_model->pmt_heldstattoday();
        $data['tmttoday']= $this->Statistics_model->tmt_heldstattoday();
        
        //data for postponed lectures today passed to view
        $data['ebspostponedtoday']= $this->Statistics_model->ebs_postponedstattoday();
        $data['pmtpostponedtoday']= $this->Statistics_model->pmt_postponedstattoday();
        $data['tmtpostponedtoday']= $this->Statistics_model->tmt_postponedstattoday();
        
          //data for absent lectures today passed to view
        $data['ebsabsenttoday']= $this->Statistics_model->ebs_absentstattoday();
        $data['pmtabsenttoday']= $this->Statistics_model->pmt_absentstattoday();
        $data['tmtabsenttoday']= $this->Statistics_model->tmt_absentstattoday();
        
        
        //data for absent lecturers today for other reasons passed to view
        $data['ebsotherstoday']= $this->Statistics_model->ebs_othersstattoday();
        $data['pmtotherstoday']= $this->Statistics_model->pmt_othersstattoday();
        $data['tmtotherstoday']= $this->Statistics_model->tmt_othersstattoday();
        
        
        //summary of supposed lecture to be held for Entrepreneurship and Business Studies
        $data['ebssummary']= $this->Statistics_model->ebsdailysummary();
        
        
        //summary of supposed lecture to be held for Projec Management Technology
        $data['pmtsummary']= $this->Statistics_model->pmtdailysummary();
        
        
        //summary of supposed lecture to be held for Transport Management Technology
        $data['tmtsummary']= $this->Statistics_model->tmtdailysummary();
   
                
        $this->template->load('admin', 'default', 'statistics/semtstatistics', $data);
    }
    
    public function set_statistics(){
         //data for held lectures today passed to view
        $data['arctoday']= $this->Statistics_model->arc_heldstattoday();
        $data['bldtoday']= $this->Statistics_model->bld_heldstattoday();
        $data['esttoday']= $this->Statistics_model->est_heldstattoday();
        $data['qtstoday']= $this->Statistics_model->qts_heldstattoday();
        $data['svgtoday']= $this->Statistics_model->svg_heldstattoday();
        $data['urptoday']= $this->Statistics_model->urp_heldstattoday();
        
        //data for  lectures postponed today passed to view
        $data['arcpostponedtoday']= $this->Statistics_model->arc_postponedstattoday();
        $data['bldpostponedtoday']= $this->Statistics_model->bld_postponedstattoday();
        $data['estpostponedtoday']= $this->Statistics_model->est_postponedstattoday();
        $data['qtspostponedtoday']= $this->Statistics_model->qts_postponedstattoday();
        $data['svgpostponedtoday']= $this->Statistics_model->svg_postponedstattoday();
        $data['urppostponedtoday']= $this->Statistics_model->urp_postponedstattoday();
        
        //data for lecturers Absent today passed to view
        $data['arcabsenttoday']= $this->Statistics_model->arc_absentstattoday();
        $data['bldabsenttoday']= $this->Statistics_model->bld_absentstattoday();
        $data['estabsenttoday']= $this->Statistics_model->est_absentstattoday();
        $data['qtsabsenttoday']= $this->Statistics_model->qts_absentstattoday();
        $data['svgabsenttoday']= $this->Statistics_model->svg_absentstattoday();
        $data['urpabsenttoday']= $this->Statistics_model->urp_absentstattoday();
        
        //data for lecturers absent for other reasons passed to view
        $data['arcotherstoday']= $this->Statistics_model->arc_othersstattoday();
        $data['bldotherstoday']= $this->Statistics_model->bld_othersstattoday();
        $data['estotherstoday']= $this->Statistics_model->est_othersstattoday();
        $data['qtsotherstoday']= $this->Statistics_model->qts_othersstattoday();
        $data['svgotherstoday']= $this->Statistics_model->svg_othersstattoday();
        $data['urpotherstoday']= $this->Statistics_model->urp_othersstattoday();
        
        //summary of supposed lecture to be held for Architecture
        $data['arcsummary']= $this->Statistics_model->arcdailysummary();
        
        //summary of supposed lecture to be held for Building
        $data['bldsummary']= $this->Statistics_model->blddailysummary();
        
        //summary of supposed lecture to be held for Estate Management
        $data['estsummary']= $this->Statistics_model->estdailysummary();
        
        //summary of supposed lecture to be held for Quantity Survey
        $data['qtssummary']= $this->Statistics_model->qtsdailysummary();
        
        //summary of supposed lecture to be held for Survey and Geoinformatics
        $data['svgsummary']= $this->Statistics_model->svgdailysummary();
        
        //summary of supposed lecture to be held for Urban Regional Planning
        $data['urpsummary']= $this->Statistics_model->urpdailysummary();
        
        
                
        $this->template->load('admin', 'default', 'statistics/setstatistics', $data);
    }
    
    public function saat_statistics(){
        
         //data for held lectures today passed to view
        $data['aettoday']= $this->Statistics_model->aet_heldstattoday();
        $data['apttoday']= $this->Statistics_model->apt_heldstattoday();
        $data['crptoday']= $this->Statistics_model->crp_heldstattoday();
        $data['fsttoday']= $this->Statistics_model->fst_heldstattoday();
        $data['ssdtoday']= $this->Statistics_model->ssd_heldstattoday();
        $data['afttoday']= $this->Statistics_model->aft_heldstattoday();
        
        
         //data for postponed lectures today passed to view
        $data['aetpostponedtoday']= $this->Statistics_model->aet_postponedstattoday();
        $data['aptpostponedtoday']= $this->Statistics_model->apt_postponedstattoday();
        $data['crppostponedtoday']= $this->Statistics_model->crp_postponedstattoday();
        $data['fstpostponedtoday']= $this->Statistics_model->fst_postponedstattoday();
        $data['ssdpostponedtoday']= $this->Statistics_model->ssd_postponedstattoday();
        $data['aftpostponedtoday']= $this->Statistics_model->aft_postponedstattoday();
        
          //data for absent lectures today passed to view
        $data['aetabsenttoday']= $this->Statistics_model->aet_absentstattoday();
        $data['aptabsenttoday']= $this->Statistics_model->apt_absentstattoday();
        $data['crpabsenttoday']= $this->Statistics_model->crp_absentstattoday();
        $data['fstabsenttoday']= $this->Statistics_model->fst_absentstattoday();
        $data['ssdabsenttoday']= $this->Statistics_model->ssd_absentstattoday();
        $data['aftabsenttoday']= $this->Statistics_model->aft_absentstattoday();
        
         //data for others lectures today passed to view
        $data['aetotherstoday']= $this->Statistics_model->aet_othersstattoday();
        $data['aptotherstoday']= $this->Statistics_model->apt_othersstattoday();
        $data['crpotherstoday']= $this->Statistics_model->crp_othersstattoday();
        $data['fstotherstoday']= $this->Statistics_model->fst_othersstattoday();
        $data['ssdotherstoday']= $this->Statistics_model->ssd_othersstattoday();
        $data['aftotherstoday']= $this->Statistics_model->aft_othersstattoday();
        
        
        //summary of supposed lecture to be held for Agric Extension
        $data['aetsummary']= $this->Statistics_model->aetdailysummary();
        
        //summary of supposed lecture to be held for Animal Production Tech.
        $data['aptsummary']= $this->Statistics_model->aptdailysummary();
        
        //summary of supposed lecture to be held for Crop Production
        $data['crpsummary']= $this->Statistics_model->crpdailysummary();
        
        //summary of supposed lecture to be held for FST
        $data['fstsummary']= $this->Statistics_model->fstdailysummary();
        
        //summary of supposed lecture to be held for Soil Science
        $data['ssdsummary']= $this->Statistics_model->ssddailysummary();
        
        //summary of supposed lecture to be held for Aquaculture Fishery Tech
        $data['aftsummary']= $this->Statistics_model->aftdailysummary();
        
        
        $this->template->load('admin', 'default', 'statistics/saatstatistics', $data);
    }
    
    public function sps_statistics(){
         //data for held lectures today passed to view
        $data['chemistoday']= $this->Statistics_model->chemis_heldstattoday();
        $data['geotoday']= $this->Statistics_model->geo_heldstattoday();
        $data['geologtoday']= $this->Statistics_model->geolog_heldstattoday();
        $data['mathtoday']= $this->Statistics_model->math_heldstattoday();
        $data['phytoday']= $this->Statistics_model->phy_heldstattoday();
        $data['stattoday']= $this->Statistics_model->stat_heldstattoday();
        
          //data for postponed lectures today passed to view
        $data['chemispostponedtoday']= $this->Statistics_model->chemis_postponedstattoday();
        $data['geopostponedtoday']= $this->Statistics_model->geo_postponedstattoday();
        $data['geologpostponedtoday']= $this->Statistics_model->geolog_postponedstattoday();
        $data['mathpostponedtoday']= $this->Statistics_model->math_heldstattoday();
        $data['phypostponedtoday']= $this->Statistics_model->phy_postponedstattoday();
        $data['statpostponedtoday']= $this->Statistics_model->stat_postponedstattoday();
        
         //data for absent lectures today passed to view
        $data['chemisabsenttoday']= $this->Statistics_model->chemis_absentstattoday();
        $data['geoabsenttoday']= $this->Statistics_model->geo_absentstattoday();
        $data['geologabsenttoday']= $this->Statistics_model->geolog_absentstattoday();
        $data['mathabsenttoday']= $this->Statistics_model->math_absentstattoday();
        $data['phyabsenttoday']= $this->Statistics_model->phy_absentstattoday();
        $data['statabsenttoday']= $this->Statistics_model->stat_absentstattoday();
        
        //data for absent lectures others today passed to view
        $data['chemisotherstoday']= $this->Statistics_model->chemis_othersstattoday();
        $data['geootherstoday']= $this->Statistics_model->geo_othersstattoday();
        $data['geologotherstoday']= $this->Statistics_model->geolog_othersstattoday();
        $data['mathotherstoday']= $this->Statistics_model->math_othersstattoday();
        $data['phyotherstoday']= $this->Statistics_model->phy_othersstattoday();
        $data['statotherstoday']= $this->Statistics_model->stat_othersstattoday();
        
        //summary of supposed lecture to be held for Chemistry
        $data['chemissummary']= $this->Statistics_model->chemisdailysummary();
        
        //summary of supposed lecture to be held for Geography
        $data['geosummary']= $this->Statistics_model->geodailysummary();
        
        //summary of supposed lecture to be held for Geology
        $data['geologsummary']= $this->Statistics_model->geologdailysummary();
        
        //summary of supposed lecture to be held for Mathematics
        $data['mathsummary']= $this->Statistics_model->mathdailysummary();
        
        //summary of supposed lecture to be held for Physics
        $data['physummary']= $this->Statistics_model->phydailysummary();
        
        
        //summary of supposed lecture to be held for statistics
        $data['statsummary']= $this->Statistics_model->statdailysummary();
        
        
        
        
        
        $this->template->load('admin', 'default', 'statistics/spsstatistics', $data);
    }
    public function sls_statistics(){
         //data for held lectures today passed to view
        $data['biotoday']= $this->Statistics_model->bio_heldstattoday();
        $data['biochemtoday']= $this->Statistics_model->biochem_heldstattoday();
        $data['microbiotoday']= $this->Statistics_model->microbio_heldstattoday();
        
         //data for postponed lectures today passed to view
        $data['biopostponedtoday']= $this->Statistics_model->bio_postponedstattoday();
        $data['biochempostponedtoday']= $this->Statistics_model->biochem_postponedstattoday();
        $data['microbiopostponedtoday']= $this->Statistics_model->microbio_postponedstattoday();
        
         //data for absent lectures today passed to view
        $data['bioabsenttoday']= $this->Statistics_model->bio_absentstattoday();
        $data['biochemabsenttoday']= $this->Statistics_model->biochem_absentstattoday();
        $data['microbioabsenttoday']= $this->Statistics_model->microbio_absentstattoday();
       
        
         //data for absent lectures others today passed to view
        $data['biootherstoday']= $this->Statistics_model->bio_othersstattoday();
        $data['biochemotherstoday']= $this->Statistics_model->biochem_othersstattoday();
        $data['microbiootherstoday']= $this->Statistics_model->microbio_othersstattoday();
        
        //summary of supposed lecture to be held for Biological Science
        $data['biosummary']= $this->Statistics_model->biodailysummary();
        
        //summary of supposed lecture to be held for Bio-chemistry
        $data['biochemsummary']= $this->Statistics_model->biochemdailysummary();
        
        
        //summary of supposed lecture to be held for Microbiology
        $data['microsummary']= $this->Statistics_model->microdailysummary();


       
        
        $this->template->load('admin', 'default', 'statistics/slsstatistics', $data);
    }
    
    public function ste_statistics(){
         //data for held lectures today passed to view
        $data['itetoday']= $this->Statistics_model->ite_heldstattoday();
        $data['sciedutoday']= $this->Statistics_model->sciedu_heldstattoday();
        $data['edutechtoday']= $this->Statistics_model->edutech_heldstattoday();
        $data['comedutoday']= $this->Statistics_model->comedu_heldstattoday();
        
        //data for postponed lectures today passed to view
        $data['itepostponedtoday']= $this->Statistics_model->ite_postponedstattoday();
        $data['sciedupostponedtoday']= $this->Statistics_model->sciedu_postponedstattoday();
        $data['edutechpostponedtoday']= $this->Statistics_model->edutech_postponedstattoday();
        $data['comedupostponedtoday']= $this->Statistics_model->comedu_postponedstattoday();
        
        //data for absent lectures today passed to view
        $data['iteabsenttoday']= $this->Statistics_model->ite_absentstattoday();
        $data['scieduabsenttoday']= $this->Statistics_model->sciedu_absentstattoday();
        $data['edutechabsenttoday']= $this->Statistics_model->edutech_absentstattoday();
        $data['comeduabsenttoday']= $this->Statistics_model->comedu_absentstattoday();
        
        //data for absent lectures others today passed to view
        $data['iteotherstoday']= $this->Statistics_model->ite_othersstattoday();
        $data['scieduotherstoday']= $this->Statistics_model->sciedu_othersstattoday();
        $data['edutechotherstoday']= $this->Statistics_model->edutech_othersstattoday();
        $data['comeduotherstoday']= $this->Statistics_model->comedu_othersstattoday();
        
        
        //summary of supposed lecture to be held for Industrial Tech Edu.
        $data['itesummary']= $this->Statistics_model->itedailysummary();
        
        //summary of supposed lecture to be held for Science EDucation.
        $data['sciedusummary']= $this->Statistics_model->sciedudailysummary();
        
        //summary of supposed lecture to be held for Education Techology
        $data['edutechsummary']= $this->Statistics_model->edutechdailysummary();
        
        //summary of supposed lecture to be held for Communication Education.
        $data['comedusummary']= $this->Statistics_model->comedudailysummary();

                  
        $this->template->load('admin', 'default', 'statistics/stestatistics', $data);
    }

    //Summary To View Semesterial Statistics Evaluation 
    public function facultysummary(){
        
                /*Statistics data for held lectures passed to the view*/
        $data['sictheldsummary']= $this->Statistics_model->sict_heldstat_summay();
        $data['seetheldsummary']= $this->Statistics_model->seet_heldstat_summay();
        $data['setheldsummary']= $this->Statistics_model->set_heldstat_summay();
        $data['semtheldsummary']= $this->Statistics_model->semt_heldstat_summay();
        $data['saatheldsummary']= $this->Statistics_model->saat_heldstat_summay();
        $data['spsheldsummary']= $this->Statistics_model->sps_heldstat_summay();
        $data['steheldsummary']= $this->Statistics_model->ste_heldstat_summay();
        $data['slsheldsummary']= $this->Statistics_model->sls_heldstat_summay();
        
         /*Statistics data for postponed lectures passed to the view*/
        $data['sictpostponedsummary']= $this->Statistics_model->sict_postponedstat_summay();
        $data['seetpostponedsummary']= $this->Statistics_model->seet_postponedstat_summay();
        $data['setpostponedsummary']= $this->Statistics_model->set_postponedstat_summay();
        $data['semtpostponedsummary']= $this->Statistics_model->semt_postponedstat_summay();
        $data['saatpostponedsummary']= $this->Statistics_model->saat_postponedstat_summay();
        $data['spspostponedsummary']= $this->Statistics_model->sps_postponedstat_summay();
        $data['stepostponedsummary']= $this->Statistics_model->ste_postponedstat_summay();
        $data['slspostponedsummary']= $this->Statistics_model->sls_postponedstat_summay();
        
        
        
        /*Statistics data for Absent lectures passed to the view*/
        $data['sictabsentsummary']= $this->Statistics_model->sict_absentstat_summay();
        $data['seetabsentsummary']= $this->Statistics_model->seet_absentstat_summay();
        $data['setabsentsummary']= $this->Statistics_model->set_absentstat_summay();
        $data['semtabsentsummary']= $this->Statistics_model->semt_absentstat_summay();
        $data['saatabsentsummary']= $this->Statistics_model->saat_absentstat_summay();
        $data['spsabsentsummary']= $this->Statistics_model->sps_absentstat_summay();
        $data['steabsentsummary']= $this->Statistics_model->ste_absentstat_summay();
        $data['slsabsentsummary']= $this->Statistics_model->sls_absentstat_summay();
        
         /*Statistics data for Absent lecturers for other reasons passed to the view*/
        $data['sictotherssummary']= $this->Statistics_model->sict_othersstat_summay();
        $data['seetotherssummary']= $this->Statistics_model->seet_othersstat_summay();
        $data['setotherssummary']= $this->Statistics_model->set_othersstat_summay();
        $data['semtotherssummary']= $this->Statistics_model->semt_othersstat_summay();
        $data['saatotherssummary']= $this->Statistics_model->saat_othersstat_summay();
        $data['spsotherssummary']= $this->Statistics_model->sps_othersstat_summay();
        $data['steotherssummary']= $this->Statistics_model->ste_othersstat_summay();
        $data['slsotherssummary']= $this->Statistics_model->sls_othersstat_summay();
       $this->template->load('admin', 'default', 'statistics/facultysummary', $data); 
    }
    
    //SICT Held Lectures for the Semester
    public function sict_statisticssummary(){
        //data for held lectures for the semester passed to view
        $data['imtheldsummary']= $this->Statistics_model->imt_heldstatsummary();
        $data['cptheldsummary']= $this->Statistics_model->cpt_heldstatsummary();
        $data['cssheldsummary']= $this->Statistics_model->css_heldstatsummary();
        $data['litheldsummary']= $this->Statistics_model->lit_heldstatsummary();
        
        
        //data for postponed lectures passed to view
        
        $data['imtpostponedsummary'] = $this->Statistics_model->imt_postponedstatsummary();
        $data['cptpostponedsummary'] = $this->Statistics_model->cpt_postponedstatsummary();
        $data['csspostponedsummary'] = $this->Statistics_model->css_postponedstatsummary();
        $data['litpostponedsummary'] = $this->Statistics_model->lit_postponedstatsummary();
        
       
        
        //data for absent lectures passed to view
        
        $data['imtabsentsummary'] = $this->Statistics_model->imt_absentstatsummary();
        $data['cptabsentsummary'] = $this->Statistics_model->cpt_absentstatsummary();
        $data['cssabsentsummary'] = $this->Statistics_model->css_absentstatsummary();
        $data['litabsentsummary'] = $this->Statistics_model->lit_absentstatsummary();
        
        //data for others lectures passed to view
        
        $data['imtotherssummary'] = $this->Statistics_model->imt_othersstatsummary();
        $data['cptotherssummary'] = $this->Statistics_model->cpt_othersstatsummary();
        $data['cssotherssummary'] = $this->Statistics_model->css_othersstatsummary();
        $data['litotherssummary'] = $this->Statistics_model->lit_othersstatsummary();
        
        
       
        
        
        $this->template->load('admin', 'default', 'statistics/sictsummary', $data);
    }
    
    //SEET Held Lectures for the Semester
    public function seet_statisticssummary(){
    
        //data for held lectures today passed to view
        $data['abeheldsummary']= $this->Statistics_model->abe_heldstatsummary();
        $data['electheldsummary']= $this->Statistics_model->elect_heldstatsummary();
        $data['mechheldsummary']= $this->Statistics_model->mech_heldstatsummary();
        $data['mechtroheldsummary']= $this->Statistics_model->mechtro_heldstatsummary();
        $data['compheldsummary']= $this->Statistics_model->comp_heldstatsummary();
        $data['telecomheldsummary']= $this->Statistics_model->telecom_heldstatsummary();
        $data['metallurgheldsummary']= $this->Statistics_model->metallurg_heldstatsummary();
        $data['civilheldsummary']= $this->Statistics_model->civil_heldstatsummary();
        $data['chemheldsummary']= $this->Statistics_model->chem_heldstatsummary();
        
        
         //data for postponed lectures today passed to view
        $data['abepostponedsummary']= $this->Statistics_model->abe_postponedstatsummary();
        $data['electpostponedsummary']= $this->Statistics_model->elect_postponedstatsummary();
        $data['mechpostponedsummary']= $this->Statistics_model->mech_postponedstatsummary();
        $data['mechtropostponedsummary']= $this->Statistics_model->mechtro_postponedstatsummary();
        $data['comppostponedsummary']= $this->Statistics_model->comp_postponedstatsummary();
        $data['telecompostponedsummary']= $this->Statistics_model->telecom_postponedstatsummary();
        $data['metallurgpostponedsummary']= $this->Statistics_model->metallurg_postponedstatsummary();
        $data['civilpostponedsummary']= $this->Statistics_model->civil_postponedstatsummary();
        $data['chempostponedsummary']= $this->Statistics_model->chem_postponedstatsummary();
        
        //data for absent lectures today passed to view
        $data['abeabsentsummary']= $this->Statistics_model->abe_absentstatsummary();
        $data['electabsentsummary']= $this->Statistics_model->elect_absentstatsummary();
        $data['mechabsentsummary']= $this->Statistics_model->mech_absentstatsummary();
        $data['mechtroabsentsummary']= $this->Statistics_model->mechtro_absentstatsummary();
        $data['compabsentsummary']= $this->Statistics_model->comp_absentstatsummary();
        $data['telecomabsentsummary']= $this->Statistics_model->telecom_absentstatsummary();
        $data['metallurgabsentsummary']= $this->Statistics_model->metallurg_absentstatsummary();
        $data['civilabsentsummary']= $this->Statistics_model->civil_absentstatsummary();
        $data['chemabsentsummary']= $this->Statistics_model->chem_absentstatsummary();
        
        //data for absent lecturers today for other reasons passed to view
        $data['abeotherssummary']= $this->Statistics_model->abe_othersstatsummary();
        $data['electotherssummary']= $this->Statistics_model->elect_othersstatsummary();
        $data['mechotherssummary']= $this->Statistics_model->mech_othersstatsummary();
        $data['mechtrootherssummary']= $this->Statistics_model->mechtro_othersstatsummary();
        $data['compotherssummary']= $this->Statistics_model->comp_othersstatsummary();
        $data['telecomotherssummary']= $this->Statistics_model->telecom_othersstatsummary();
        $data['metallurgotherssummary']= $this->Statistics_model->metallurg_othersstatsummary();
        $data['civilotherssummary']= $this->Statistics_model->civil_othersstatsummary();
        $data['chemotherssummary']= $this->Statistics_model->chem_othersstatsummary();
        
        
        
        
        $this->template->load('admin', 'default', 'statistics/seetsummary', $data);
        
    }
    //SEMT Held Lectures for the Semester
    public function semt_statisticssummary(){
         //data for held lectures today passed to view
        $data['ebsheldsummary']= $this->Statistics_model->ebs_heldstatsummary();
        $data['pmtheldsummary']= $this->Statistics_model->pmt_heldstatsummary();
        $data['tmtheldsummary']= $this->Statistics_model->tmt_heldstatsummary();
        
        //data for postponed lectures today passed to view
        $data['ebspostponedsummary']= $this->Statistics_model->ebs_postponedstatsummary();
        $data['pmtpostponedsummary']= $this->Statistics_model->pmt_postponedstatsummary();
        $data['tmtpostponedsummary']= $this->Statistics_model->imt_postponedstatsummary();
        
          //data for absent lectures today passed to view
        $data['ebsabsentsummary']= $this->Statistics_model->ebs_absentstatsummary();
        $data['pmtabsentsummary']= $this->Statistics_model->pmt_absentstatsummary();
        $data['tmtabsentsummary']= $this->Statistics_model->tmt_absentstatsummary();
        
        
        //data for absent lecturers today for other reasons passed to view
        $data['ebsotherssummary']= $this->Statistics_model->ebs_othersstatsummary();
        $data['pmtotherssummary']= $this->Statistics_model->pmt_othersstatsummary();
        $data['tmtotherssummary']= $this->Statistics_model->tmt_othersstatsummary();
          
                
        $this->template->load('admin', 'default', 'statistics/semtsummary', $data);
    }
    //SET Held Lectures for the Semester
    public function set_statisticssummary(){
         //data for held lectures today passed to view
        $data['archeldsummary']= $this->Statistics_model->arc_heldstatsummary();
        $data['bldheldsummary']= $this->Statistics_model->bld_heldstatsummary();
        $data['estheldsummary']= $this->Statistics_model->est_heldstatsummary();
        $data['qtsheldsummary']= $this->Statistics_model->qts_heldstatsummary();
        $data['svgheldsummary']= $this->Statistics_model->svg_heldstatsummary();
        $data['urpheldsummary']= $this->Statistics_model->urp_heldstatsummary();
        
        //data for  lectures postponed today passed to view
        $data['arcpostponedsummary']= $this->Statistics_model->arc_postponedstatsummary();
        $data['bldpostponedsummary']= $this->Statistics_model->bld_postponedstatsummary();
        $data['estpostponedsummary']= $this->Statistics_model->est_postponedstatsummary();
        $data['qtspostponedsummary']= $this->Statistics_model->qts_postponedstatsummary();
        $data['svgpostponedsummary']= $this->Statistics_model->svg_postponedstatsummary();
        $data['urppostponedsummary']= $this->Statistics_model->urp_postponedstatsummary();
        
        //data for lecturers Absent today passed to view
        $data['arcabsentsummary']= $this->Statistics_model->arc_absentstatsummary();
        $data['bldabsentsummary']= $this->Statistics_model->bld_absentstatsummary();
        $data['estabsentsummary']= $this->Statistics_model->est_absentstatsummary();
        $data['qtsabsentsummary']= $this->Statistics_model->qts_absentstatsummary();
        $data['svgabsentsummary']= $this->Statistics_model->svg_absentstatsummary();
        $data['urpabsentsummary']= $this->Statistics_model->urp_absentstatsummary();
        
        //data for lecturers absent for other reasons passed to view
        $data['arcotherssummary']= $this->Statistics_model->arc_othersstatsummary();
        $data['bldotherssummary']= $this->Statistics_model->bld_othersstatsummary();
        $data['estotherssummary']= $this->Statistics_model->est_othersstatsummary();
        $data['qtsotherssummary']= $this->Statistics_model->qts_othersstatsummary();
        $data['svgotherssummary']= $this->Statistics_model->svg_othersstatsummary();
        $data['urpotherssummary']= $this->Statistics_model->urp_othersstatsummary();
        
        
        
                
        $this->template->load('admin', 'default', 'statistics/setsummary', $data);
    }
    //SAAT Held Lectures for the Semester
    public function saat_statisticssummary(){
        
         //data for held lectures today passed to view
        $data['aetheldsummary']= $this->Statistics_model->aet_heldstatsummary();
        $data['aptheldsummary']= $this->Statistics_model->apt_heldstatsummary();
        $data['crpheldsummary']= $this->Statistics_model->crp_heldstatsummary();
        $data['fstheldsummary']= $this->Statistics_model->fst_heldstatsummary();
        $data['ssdheldsummary']= $this->Statistics_model->ssd_heldstatsummary();
        $data['aftheldsummary']= $this->Statistics_model->aft_heldstatsummary();
        
        
         //data for postponed lectures today passed to view
        $data['aetpostponedsummary']= $this->Statistics_model->aet_postponedstatsummary();
        $data['aptpostponedsummary']= $this->Statistics_model->apt_postponedstatsummary();
        $data['crppostponedsummary']= $this->Statistics_model->crp_postponedstatsummary();
        $data['fstpostponedsummary']= $this->Statistics_model->fst_postponedstatsummary();
        $data['ssdpostponedsummary']= $this->Statistics_model->ssd_postponedstatsummary();
        $data['aftpostponedsummary']= $this->Statistics_model->aft_postponedstatsummary();
        
          //data for absent lectures today passed to view
        $data['aetabsentsummary']= $this->Statistics_model->aet_absentstatsummary();
        $data['aptabsentsummary']= $this->Statistics_model->apt_absentstatsummary();
        $data['crpabsentsummary']= $this->Statistics_model->crp_absentstatsummary();
        $data['fstabsentsummary']= $this->Statistics_model->fst_absentstatsummary();
        $data['ssdabsentsummary']= $this->Statistics_model->ssd_absentstatsummary();
        $data['aftabsentsummary']= $this->Statistics_model->aft_absentstatsummary();
        
         //data for others lectures today passed to view
        $data['aetotherssummary']= $this->Statistics_model->aet_othersstatsummary();
        $data['aptotherssummary']= $this->Statistics_model->apt_othersstatsummary();
        $data['crpotherssummary']= $this->Statistics_model->crp_othersstatsummary();
        $data['fstotherssummary']= $this->Statistics_model->fst_othersstatsummary();
        $data['ssdotherssummary']= $this->Statistics_model->ssd_othersstatsummary();
        $data['aftotherssummary']= $this->Statistics_model->aft_othersstatsummary();
         
        $this->template->load('admin', 'default', 'statistics/saatsummary', $data);
    }
    
    //SPS Held Lectures for the Semester
    public function sps_statisticssummary(){
         //data for held lectures today passed to view
        $data['chemisheldsummary']= $this->Statistics_model->chemis_heldstatsummary();
        $data['geoheldsummary']= $this->Statistics_model->geo_heldstatsummary();
        $data['geologheldsummary']= $this->Statistics_model->geolog_heldstatsummary();
        $data['mathheldsummary']= $this->Statistics_model->math_heldstatsummary();
        $data['phyheldsummary']= $this->Statistics_model->phy_heldstatsummary();
        $data['statheldsummary']= $this->Statistics_model->stat_heldstatsummary();
        
          //data for postponed lectures today passed to view
        $data['chemispostponedsummary']= $this->Statistics_model->chemis_postponedstatsummary();
        $data['geopostponedsummary']= $this->Statistics_model->geo_postponedstatsummary();
        $data['geologpostponedsummary']= $this->Statistics_model->geolog_postponedstatsummary();
        $data['mathpostponedsummary']= $this->Statistics_model->math_heldstatsummary();
        $data['phypostponedsummary']= $this->Statistics_model->phy_postponedstatsummary();
        $data['statpostponedsummary']= $this->Statistics_model->stat_postponedstatsummary();
        
         //data for absent lectures today passed to view
        $data['chemisabsentsummary']= $this->Statistics_model->chemis_absentstatsummary();
        $data['geoabsentsummary']= $this->Statistics_model->geo_absentstatsummary();
        $data['geologabsentsummary']= $this->Statistics_model->geolog_absentstatsummary();
        $data['mathabsentsummary']= $this->Statistics_model->math_absentstatsummary();
        $data['phyabsentsummary']= $this->Statistics_model->phy_absentstatsummary();
        $data['statabsentsummary']= $this->Statistics_model->stat_absentstatsummary();
        
        //data for absent lectures others today passed to view
        $data['chemisotherssummary']= $this->Statistics_model->chemis_othersstatsummary();
        $data['geootherssummary']= $this->Statistics_model->geo_othersstatsummary();
        $data['geologotherssummary']= $this->Statistics_model->geolog_othersstatsummary();
        $data['mathotherssummary']= $this->Statistics_model->math_othersstatsummary();
        $data['phyotherssummary']= $this->Statistics_model->phy_othersstatsummary();
        $data['statotherssummary']= $this->Statistics_model->stat_othersstatsummary();
        
           
        
        
        
        $this->template->load('admin', 'default', 'statistics/spssummary', $data);
    }
     //SLS Held Lectures for the Semester
    public function sls_statisticssummary(){
         //data for held lectures today passed to view
        $data['bioheldsummary']= $this->Statistics_model->bio_heldstatsummary();
        $data['biochemheldsummary']= $this->Statistics_model->biochem_heldstatsummary();
        $data['microbioheldsummary']= $this->Statistics_model->microbio_heldstatsummary();
        
         //data for postponed lectures today passed to view
        $data['biopostponedsummary']= $this->Statistics_model->bio_postponedstatsummary();
        $data['biochempostponedsummary']= $this->Statistics_model->biochem_postponedstatsummary();
        $data['microbiopostponedsummary']= $this->Statistics_model->microbio_postponedstatsummary();
        
         //data for absent lectures today passed to view
        $data['bioabsentsummary']= $this->Statistics_model->bio_absentstatsummary();
        $data['biochemabsentsummary']= $this->Statistics_model->biochem_absentstatsummary();
        $data['microbioabsentsummary']= $this->Statistics_model->microbio_absentstatsummary();
       
        
         //data for absent lectures others today passed to view
        $data['biootherssummary']= $this->Statistics_model->bio_othersstatsummary();
        $data['biochemotherssummary']= $this->Statistics_model->biochem_othersstatsummary();
        $data['microbiootherssummary']= $this->Statistics_model->microbio_othersstatsummary();
        
             
        
        $this->template->load('admin', 'default', 'statistics/slssummary', $data);
    }
    //STE Held Lectures for the Semester
    public function ste_statisticssummary(){
         //data for held lectures today passed to view
        $data['iteheldsummary']= $this->Statistics_model->ite_heldstatsummary();
        $data['scieduheldsummary']= $this->Statistics_model->sciedu_heldstatsummary();
        $data['edutechheldsummary']= $this->Statistics_model->edutech_heldstatsummary();
        $data['comeduheldsummary']= $this->Statistics_model->comedu_heldstatsummary();
        
        //data for postponed lectures today passed to view
        $data['itepostponedsummary']= $this->Statistics_model->ite_postponedstatsummary();
        $data['sciedupostponedsummary']= $this->Statistics_model->sciedu_postponedstatsummary();
        $data['edutechpostponedsummary']= $this->Statistics_model->edutech_postponedstatsummary();
        $data['comedupostponedsummary']= $this->Statistics_model->comedu_postponedstatsummary();
        
        //data for absent lectures today passed to view
        $data['iteabsentsummary']= $this->Statistics_model->ite_absentstatsummary();
        $data['scieduabsentsummary']= $this->Statistics_model->sciedu_absentstatsummary();
        $data['edutechabsentsummary']= $this->Statistics_model->edutech_absentstatsummary();
        $data['comeduabsentsummary']= $this->Statistics_model->comedu_absentstatsummary();
        
        //data for absent lectures others today passed to view
        $data['iteotherssummary']= $this->Statistics_model->ite_othersstatsummary();
        $data['scieduotherssummary']= $this->Statistics_model->sciedu_othersstatsummary();
        $data['edutechotherssummary']= $this->Statistics_model->edutech_othersstatsummary();
        $data['comeduotherssummary']= $this->Statistics_model->comedu_othersstatsummary();
        
        
       

                  
        $this->template->load('admin', 'default', 'statistics/stesummary', $data);
    }
}

	