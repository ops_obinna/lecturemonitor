<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

	
	//functions to load update views/Template
	public function updatecourse()
	{
       $this->template->load('admin', 'default', 'view_update/updatecourse');
	}
	public function updatecourserep()
	{
       $this->template->load('admin', 'default', 'view_update/updatecourserep');
	}
	public function updatedepartment()
	{
       $this->template->load('admin', 'default', 'view_update/updatedepartment');
	}
	public function updatefaculty()
	{
       $this->template->load('admin', 'default', 'view_update/updatefaculty');
	}
	public function updatelecturer()
	{
       $this->template->load('admin', 'default', 'view_update/updatelecturer');
	}
	public function updatevenue()
	{
       $this->template->load('admin', 'default', 'view_update/updatevenue');
	}
}
