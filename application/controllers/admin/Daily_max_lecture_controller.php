<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_max_lecture_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}
    
    
    public function index(){
        $this->template->load('admin', 'default', 'statistics/facultyMaxReg');
        
    }
    
    
    //function to load view for registration of daily maximum lectures for faculties
    public function facultyMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/facultyMaxReg');
        
    }
	
	
    //fuction to submit daily maximum lecture for SICT
    public function submitdailymaxsict(){
		//truncate the table before reinserting data
		$this->db->truncate('sictdailysummary');
		
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('sictdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        ///redirect('admin/Daily_max_lecture_controller/submitdailymaxsict', 'refresh');
		
         //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
              //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxseet(){
		//truncate the table before reinserting data
		$this->db->truncate('seetdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('seetdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
         //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxsaat(){
		//truncate the table before reinserting data
		$this->db->truncate('saatdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('saatdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        
		
         //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxsemt(){
		//truncate the table before reinserting data
		$this->db->truncate('semtdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('semtdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
           //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxset(){
		//truncate the table before reinserting data
		$this->db->truncate(' setdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('setdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
            //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxsps(){
		//truncate the table before reinserting data
		$this->db->truncate('spsdailysummary');
		
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('spsdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
            //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxsls(){
		//truncate the table before reinserting data
		$this->db->truncate('slsdailysummary');
		
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('slsdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
         //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
    public function submitdailymaxste(){
		//truncate the table before reinserting data
		$this->db->truncate('stedailysummary');
		
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('stedailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
		
            //loads the template
            $this->facultyMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
             //loads the template
            $this->facultyMax(); 
        }
        
        
        }
    
	
/* THE BELOW FUNCTIONS IS FOR DEPARTMENTS REGISTRATION 
    FOR DAILY LECTURE LIMITS
*/
//function to load view for registration of daily maximum lectures for sict
    public function sictMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/sictMax');
        
    }
    
     //SUBMIT FOR IMT
    public function submitdailymaximt(){
		//truncate the table before reinserting data
		$this->db->truncate('imtdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('imtdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->sictMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->sictMax();
        }
        
        
        }
    
     //SUBMIT FOR CPT
    public function submitdailymaxcpt(){
		//truncate the table before reinserting data
		$this->db->truncate('cptdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('cptdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->sictMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->sictMax();       
        }
        
        
        }
    
     //SUBMIT FOR CSS
    public function submitdailymaxcss(){
		//truncate the table before reinserting data
		$this->db->truncate('cssdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('cssdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->sictMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->sictMax();
        }
        
        
        }
    
     //SUBMIT FOR LIT
    public function submitdailymaxlit(){
		//truncate the table before reinserting data
		$this->db->truncate('litdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('litdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->sictMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->sictMax();
        }
        
        
        }
    
    
    
    
    //function to load view for registration of daily maximum lectures for seet
    public function seetMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/seetMax');
        
    }
    //function to register ABE daily max Lecture
    public function submitdailymaxabe(){
		//truncate the table before reinserting data
		$this->db->truncate('abedailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('abedailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Elect daily max Lecture
    public function submitdailymaxelect(){
		//truncate the table before reinserting data
		$this->db->truncate('electdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('electdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Mechanical daily max Lecture
    public function submitdailymaxmech(){
		//truncate the table before reinserting data
		$this->db->truncate('mechdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('mechdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Mecha daily max Lecture
    public function submitdailymaxmecha(){
		//truncate the table before reinserting data
		$this->db->truncate('mechadailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('mechadailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Metalurgical daily max Lecture
    public function submitdailymaxmeta(){
		//truncate the table before reinserting data
		$this->db->truncate('metadailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('metadailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Computer Engineering daily max Lecture
    public function submitdailymaxcomp(){
		//truncate the table before reinserting data
		$this->db->truncate('comdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('comdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    
    //function to register Chemcal Eng. daily max Lecture
    public function submitdailymaxchem(){
		//truncate the table before reinserting data
		$this->db->truncate('chemdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('chemdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    //function to register Civil Eng. daily max Lecture
    public function submitdailymaxcivil(){
		//truncate the table before reinserting data
		$this->db->truncate('civildailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('civildailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    
    //function to register Telecom Eng. daily max Lecture
    public function submitdailymaxtel(){
		//truncate the table before reinserting data
		$this->db->truncate('teldailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('teldailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->seetMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->seetMax();      
        }
        
        
        }
    
    
    
    //function to load view for registration of daily maximum lectures for saat
    public function saatMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/saatMax');
        
    }
    //SUBMIT FOR AET
    public function submitdailymaxaet(){
		//truncate the table before reinserting data
		$this->db->truncate('aetdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('aetdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax(); 
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();           
        }
        
        
        }
    
    //SUBMIT FOR ANIMAL PRODUCTION
    public function submitdailymaxanimalprod(){
		//truncate the table before reinserting data
		$this->db->truncate('animalproddailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('animalproddailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();            
        }
        
        
        }
    
    //SUBMIT FOR CROP PRODUCTION
    public function submitdailymaxcropprod(){
		//truncate the table before reinserting data
		$this->db->truncate('cropproddailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('cropproddailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();            
        }
        
        
        }
    
    //SUBMIT FOR SOIL SCIENCE
    public function submitdailymaxssd(){
		//truncate the table before reinserting data
		$this->db->truncate('ssddailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('ssddailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();          
        }
        
        
        }
    
    //SUBMIT FOR FST
    public function submitdailymaxfst(){
		//truncate the table before reinserting data
		$this->db->truncate('fstdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('fstdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();      
        }
        
        
        }
    
    //SUBMIT FOR AQUA
    public function submitdailymaxaquatech(){
		//truncate the table before reinserting data
		$this->db->truncate('fisheriesdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('fisheriesdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->saatMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->saatMax();      
        }
        
        
        }
    
    
    
    
    //function to load view for registration of daily maximum lectures for semt
    public function semtMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/semtMax');
        
    }
    
    //SUBMIT FOR EBS
    public function submitdailymaxebs(){
		//truncate the table before reinserting data
		$this->db->truncate('ebsdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('ebsdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->semtMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->semtMax();      
        }
        
        
        }
     //SUBMIT FOR EBS
    public function submitdailymaxpmt(){
		//truncate the table before reinserting data
		$this->db->truncate('pmtdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('pmtdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->semtMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->semtMax();      
        }
        
        
        }
    
     //SUBMIT FOR EBS
    public function submitdailymaxtmt(){
		//truncate the table before reinserting data
		$this->db->truncate('tmtdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('tmtdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->semtMax();
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->semtMax();      
        }
        
        
        }
    
    
    //function to load view for registration of daily maximum lectures for set
    public function setMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/setMax');
        
    }
    
        //SUBMIT FOR ARCHI
    public function submitdailymaxarchi(){
		//truncate the table before reinserting data
		$this->db->truncate('archidailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('archidailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        //SUBMIT FOR URP
    public function submitdailymaxurp(){
		//truncate the table before reinserting data
		$this->db->truncate('urpdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('urpdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        //SUBMIT FOR SVG
    public function submitdailymaxsvg(){
		//truncate the table before reinserting data
		$this->db->truncate('svgdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('svgdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        //SUBMIT FOR ESTATE
    public function submitdailymaxest(){
		//truncate the table before reinserting data
		$this->db->truncate('estdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('estdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        //SUBMIT FOR QS
    public function submitdailymaxqs(){
		//truncate the table before reinserting data
		$this->db->truncate('qsdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('qsdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        //SUBMIT FOR BUILDING
    public function submitdailymaxbld(){
		//truncate the table before reinserting data
		$this->db->truncate('blddailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('blddailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->setMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->setMax();      
        }
        
        
        }
    
        
    //function to load view for registration of daily maximum lectures for sps
    public function spsMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/spsMax');
        
    }
    
    //SUBMIT FOR MATHS
    public function submitdailymaxmath(){
		//truncate the table before reinserting data
		$this->db->truncate('mathdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('mathdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }
    
    //SUBMIT PHYSICS
    public function submitdailymaxphy(){
		//truncate the table before reinserting data
		$this->db->truncate('phydailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('phydailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }
    
    //SUBMIT CHEMISTRY
    public function submitdailymaxchemis(){
		//truncate the table before reinserting data
		$this->db->truncate('chemisdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('chemisdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }
    
    //SUBMIT GEOGRAPHY
    public function submitdailymaxgeo(){
		//truncate the table before reinserting data
		$this->db->truncate('geodailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('geodailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }
    
    //SUBMIT GEOLOGY
    public function submitdailymaxgeol(){
		//truncate the table before reinserting data
		$this->db->truncate('geoldailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('geoldailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }
    
    //SUBMIT STATISTICS
    public function submitdailymaxstat(){
		//truncate the table before reinserting data
		$this->db->truncate('statdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('statdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->spsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->spsMax();     
        }
        
        
        }

    
    //function to load view for registration of daily maximum lectures for sls
    public function slsMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/slsMax');
        
    }
    
     //SUBMIT BIOLOGICAL SCIENCE
    public function submitdailymaxbiosci(){
		//truncate the table before reinserting data
		$this->db->truncate('bioscidailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('bioscidailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->slsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->slsMax();     
        }
        
        
        }
    
    //SUBMIT BIO-Chemistry
    public function submitdailymaxbiochem(){
		//truncate the table before reinserting data
		$this->db->truncate('biochemdailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('biochemdailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->slsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->slsMax();     
        }
        
        
        }
    
    //SUBMIT MICROBIOLOGY
    public function submitdailymaxmicrobio(){
		//truncate the table before reinserting data
		$this->db->truncate('microbiodailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('microbiodailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->slsMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->slsMax();     
        }
        
        
        }
    
    
    
    //function to load view for registration of daily maximum lectures for ste
    public function steMax(){
        
        
        $this->template->load('admin', 'default', 'statistics/steMax');
        
    }
    
    //SUBMIT industrial Technology Education
    public function submitdailymaxite(){
		//truncate the table before reinserting data
		$this->db->truncate('itedailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('itedailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->steMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->steMax();     
        }
        
        
        }
    
    
    //SUBMIT industrial Science Education
    public function submitdailysciedu(){
		//truncate the table before reinserting data
		$this->db->truncate('sciedudailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('sciedudailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->steMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->steMax();     
        }
    
    
}
    
    //SUBMIT industrial Education Technology
    public function submitdailyedutech(){
		//truncate the table before reinserting data
		$this->db->truncate('edutechdudailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('edutechdudailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->steMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->steMax();     
        }
    
    
}
    
    //SUBMIT Communication Education
    public function submitdailymaxcomedu(){
		//truncate the table before reinserting data
		$this->db->truncate('comedududailysummary');
        $validation =  array(
            array('field' => 'week_day[]', 'rules' => 'required'),
            array('field' => 'num_lecture[]', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($validation);
        if($this->form_validation->run() == true){
            
        $week_day = $this->input->post('week_day[]');
        $num_lecture = $this->input->post('num_lecture[]');

        
        $value = array();
        for($i = 0; $i < count($week_day); $i++){   
            $value[$i] = array(
                'week_day' => $week_day[$i],
                'num_lecture' => $num_lecture[$i]
            
            );
            
        }

        $this->db->insert_batch('comedududailysummary', $value);
        $this->session->set_flashdata('success', 'Submitted Successfully');
                    		
        //redirect('admin/Daily_max_lecture_controller/submitdailymaxseet', 'refresh');
            
		//load template
        $this->steMax();      
        }else{
            $this->session->set_flashdata('danger', 'Data Not Submitted Check your input accept only Numbers');
            //load template
            $this->steMax();     
        }
    
    
}
}    
    