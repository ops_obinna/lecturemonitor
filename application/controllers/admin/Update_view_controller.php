<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_view_controller extends CI_Controller {
	function __construct(){
		parent::__construct();

		//check Login
		if(!$this->session->userdata('logged_in')){
			redirect('login/user_login');
		}
	}

	
	//functions to load different database contents
	public function view_course()
	{
	   $data['view_course'] = $this->Update_view_model->get_course();
       $this->template->load('admin', 'default', 'view_update/view_course', $data);
	}
	public function view_courserep()
	{
		$data['view_courserep'] = $this->Update_view_model->get_courserep();
       $this->template->load('admin', 'default', 'view_update/view_courserep', $data);
	}
	public function view_department()
	{
	   $data['view_department'] = $this->Update_view_model->get_department();
       $this->template->load('admin', 'default', 'view_update/view_department', $data);
	}
	public function view_faculty()
	{
	   $data['view_faculty'] = $this->Update_view_model->get_faculty();
       $this->template->load('admin', 'default', 'view_update/view_faculty', $data);
	}
	public function view_lecturer()
	{
	   $data['view_lecturer'] = $this->Update_view_model->get_lecturer();
       $this->template->load('admin', 'default', 'view_update/view_lecturer', $data);
	}

	public function view_venue()
	{
		$data['view_venue'] = $this->Update_view_model->get_venue();
		$this->template->load('admin', 'default', 'view_update/view_venue', $data);
		
	}
	


	//BELOW FUNCTIONS TO EDIT AND UPDATE VARIOUS UPDATES REQUIRED

	public function edit_venue($venue_id)
	{
		//setting validation rules
	   $this->form_validation->set_rules('venue_name', 'Venue Name', 'trim|required');
	   

       if ($this->form_validation->run() == FALSE)
          {

          	//get the current Venue
          	$data['item'] = $this->Update_view_model->getvenuename($venue_id);
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_venue', $data);

		  }else{
		  	$data['venue_name']		= $this->input->post('venue_name');
		  	

		  	//Update Venue by calling the function in the model class 
			$this->Update_view_model->updatevenue($venue_id, $data);

			$this->session->set_flashdata('success', 'Venue Updated Successfully');
 
		//redirect to the registration page and refrshes all form input
		 redirect(base_url() . "admin/Update_view_controller/venue_updated");

		  }
		
	}

	//This Function will display flash message in the view_venue view if venue is updated successfully
	public function venue_updated()
	{
		$this->view_venue();
		$this->session->set_flashdata('success', 'Venue Updated Successfully');
	}

	public function delete_venue($venue_id)
	{
        $venue_name = $this->Update_view_model->delete_venue($venue_id);
        redirect(base_url() . "admin/Update_view_controller/view_venue", 'refresh');

	}




	public function edit_lecturers($lecturer_id)
	{
		//setting validation rules
        $this->form_validation->set_rules('lecturer_id', 'Lecturer ID', 'trim|required');
		$this->form_validation->set_rules('lecturer_fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lecturer_sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('service_no', 'Service Number','trim|required');
		$this->form_validation->set_rules('dept_id', 'Department ID', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		

		if ($this->form_validation->run() == FALSE)
          {
          	//Get the present vales for the currently edited Id
          	$data['item'] = $this->Update_view_model->getlecturerdata($lecturer_id);
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_lecturers', $data);

		  }else{

			//else is validation is true then create post array
		
				$data['lecturer_id']		= $this->input->post('lecturer_id');
				$data['lecturer_fname']		= $this->input->post('lecturer_fname');
				$data['lecturer_sname']		= $this->input->post('lecturer_sname');
				$data['service_no']			= $this->input->post('service_no');
				$data['phone']				= $this->input->post('phone');
				$data['dept_id']			= $this->input->post('dept_id');
				$data['phone']				= $this->input->post('phone');
				$data['email_address']		= $this->input->post('email_address');
				$data['password']			= $this->input->post('password');
				$data['lecturer_img'] 		= $_FILES["lecturer_img"]["name"];
				

		//Register lecturer by calling the function in the model class 
		$this->Update_view_model->updatelecturer($lecturer_id, $data);
		
		$lecturer_id = $this->db->insert_id();
		move_uploaded_file($_FILES["lecturer_img"]["tmp_name"], "upload/lecturer/" . $lecturer_id . '.jpg');

			
		//redirect lecturere_updated function in the the update_view_controller
		redirect(base_url() . 'admin/Update_view_controller/lecturer_updated');

		}

	}
    
    public function delete_lecturers($lecturer_id){
        $lect_delete = $this->Update_view_model->deletelecturer($lecturer_id);
        $this->session->set_flashdata('success', 'Lecturer Deleted');
        redirect(base_url() . "admin/Update_view_controller/view_lecturer", 'refresh');

        
    }

	public function lecturer_updated(){
		$this->session->set_flashdata('success', 'Lecturer Updated Successfully');
		$this->view_lecturer();
	}





	public function edit_faculty($faculty_id)
	{
		//setting validation rules
	   $this->form_validation->set_rules('faculty_name', 'Faculty Name', 'trim|required');

       if ($this->form_validation->run() == FALSE)
          {
          	//get the current Faculty
          	$data['item'] = $this->Update_view_model->getfacultyname($faculty_id);
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_faculty', $data);

		  }else{
		  	$data['faculty_name']	= $this->input->post('faculty_name');

		  	//update faculty by calling the function in the model class 
			$this->Update_view_model->updatefaculty($faculty_id, $data);
			

		//redirect to the registration page and refrshes alll form input
		redirect(base_url(). "admin/Update_view_controller/faculty_updated");
		  }
		
	}
    
    public function delete_faculty($faculty_id){
        $faculty_delete = $this->Update_view_model->deletefaculty($faculty_id);
        $this->session->set_flashdata('success', 'Faculty Deleted');
        redirect(base_url() . "admin/Update_view_controller/view_faculty", 'refresh');

        
    }

	public function faculty_updated(){
		$this->session->set_flashdata('success', 'Faculty Updated Successfully');
		$this->view_faculty();
	}

	public function edit_department($dept_id)
	{
	   //setting validation rules
	   $this->form_validation->set_rules('dept_name', 'Department Name', 'trim|required');
	   $this->form_validation->set_rules('faculty_id', 'Faculty', 'trim|required');

       if ($this->form_validation->run() == FALSE)
          {
          	//get the current Venue
          	$data['item'] = $this->Update_view_model->getdeptname($dept_id);
	 		
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_department', $data);

		  }else{
		  	$data['dept_name']		= $this->input->post('dept_name');
		  	$data['faculty_id']		= $this->input->post('faculty_id');

		  	//Updates dept by calling the function in the model class 
			$this->Update_view_model->updatedept($dept_id, $data);

			
		//redirect to view department page
		 redirect(base_url() . "admin/Update_view_controller/dept_updated");
		  }
	}
    
    public function delete_department($dept_id){
        $dept_delete = $this->Update_view_model->deletedept($dept_id);
        $this->session->set_flashdata('success', 'Department Deleted');
        redirect(base_url() . "admin/Update_view_controller/view_department", 'refresh');
    }


	//This Function will display flash message in the view_department view if venue is updated successfully
	public function dept_updated()
	{
		$this->session->set_flashdata('success', 'department Updated Successfully');
		$this->view_department();
		
	}

	public function delete_dept($dept_id)
	{
		
	}



	public function edit_coursereps($rep_id)
	{
		//setting validation rules
		$this->form_validation->set_rules('rep_matric_no', 'registration Number','trim|required');
		$this->form_validation->set_rules('rep_fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('rep_sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('level_id', 'Level ID', 'required');
		$this->form_validation->set_rules('dept_id', 'Department ID', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE)
          {
          	//get the values from database of the current id (class rep) been updated
          	$data['item'] = $this->Update_view_model->getrepdata($rep_id);
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_coursereps', $data);

		  }else{



			//else is validation is true then create post array
		
				$data['rep_matric_no'] 	= $this->input->post('rep_matric_no');
				$data['rep_fname']		= $this->input->post('rep_fname');
				$data['rep_sname']		= $this->input->post('rep_sname');
				$data['level_id']		= $this->input->post('level_id');
				$data['phone']			= $this->input->post('phone');
				$data['dept_id']		= $this->input->post('dept_id');
				$data['phone']			= $this->input->post('phone');
				$data['email']			= $this->input->post('email');
				$data['password']		= $this->input->post('password');
				$data['rep_img'] 		= $_FILES["rep_img"]["name"];
				

		//Update Student (i.e Class Reps) by calling the function in the model class 
		$this->Update_view_model->updateclassrep($rep_id, $data);
		

		//$rep_id = $this->db->insert_id();
        
            
            move_uploaded_file($_FILES['rep_img']['tmp_name'], 'upload/classreps/' . $rep_id . '.jpg');
        
		

			
		//redirect to the view Reps page
		redirect(base_url() . "admin/Update_view_controller/rep_updated");
		}

	}//end function
    
    //function to delete course_rep
	function delete_coursereps($rep_id){
        $rep_delete = $this->Update_view_model->deleteclassrep($rep_id);
        $this->session->set_flashdata('success', 'Course Representative Deleted');
        redirect(base_url() . "admin/Update_view_controller/view_courserep", 'refresh');
	}
    
    

	public function rep_updated(){
		$this->session->set_flashdata('success', 'Course Representative Updated successfully');
		$this->view_courserep();
	}


	//Update courses function
	public function edit_courses($course_id)
	{   
	   //setting validation rules
		$this->form_validation->set_rules('course_title', 'course title','trim|required');
		$this->form_validation->set_rules('course_code', 'course code', 'trim|required');
        $this->form_validation->set_rules('faculty_id', 'FACULTY ID', 'required');
		$this->form_validation->set_rules('dept_id', 'DEPARTMENT ID', 'required');
        $this->form_validation->set_rules('lecturer_id', 'LECTURER ID', 'required');
		$this->form_validation->set_rules('venue_id', 'VENUE ID', 'required');
		$this->form_validation->set_rules('level_id', 'Level ID', 'required');
		$this->form_validation->set_rules('credit_unit', 'CREDIT UNIT', 'required');
		$this->form_validation->set_rules('credit_hour', 'CREDIT HOUR', 'required');
				
		if ($this->form_validation->run() == FALSE)
          {
          	//get the current course
          	$data['item'] = $this->Update_view_model->getcoursetitle($course_id);
	 		//load template if validation is false 
			$this->template->load('admin', 'default', 'view_update/edit_courses', $data);

		  }else{



			//else if validation is true then create post array
		
				$data['course_title'] 	= $this->input->post('course_title');
				$data['course_code']	= $this->input->post('course_code');
                $data['faculty_id']		= $this->input->post('faculty_id');
				$data['dept_id']		= $this->input->post('dept_id');
                $data['lecturer_id']	= $this->input->post('lecturer_id');
				$data['venue_id']		= $this->input->post('venue_id');
				$data['level_id']		= $this->input->post('level_id');
				$data['credit_unit']	= $this->input->post('credit_unit');
				$data['credit_hour']	= $this->input->post('credit_hour');
				
					

		//Update course by calling the function in the model class 
		$this->Update_view_model->updatecourse($course_id, $data);

		//redirect to the view course page
		redirect(base_url() . "admin/Update_view_controller/course_updated");

		}

	}
    
    public function delete_courses($course_id){
        $course_delete = $this->Update_view_model->deletecourse($course_id);
        $this->session->set_flashdata('success', 'Course Deleted');
        redirect(base_url() . "admin/Update_view_controller/view_course", 'refresh');
    }

	public function course_updated(){
		$this->session->set_flashdata('success', 'Course updated Successfully');
		$this->view_course();
	}


}
