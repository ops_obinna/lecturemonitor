<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct(){
		parent::__construct();		
	}

      
	public function index()
	{

		
		$this->template->load('public', 'default', 'login');
        //$this->load->view('public/login');
	}


//      //THIS FUNCTION TAKES CARE OF THE LOGIN FUNCTIONALITY OF THE SYSTEM USERS 
//      public function user_login(){
//
//            //Check Validation rules 
//            $this->form_validation->set_rules('email', 'Email', 'required');
//            $this->form_validation->set_rules('password', 'Password', 'required');
//            
//
//            if($this->form_validation->run() == FALSE){ 
//            $this->template->load('public', 'default', 'login');
//                //$this->load->view('public/login');
//            }else{
//                  
//                  //get post Data 
//                  $email = $this->input->post('email');
//                  $password = $this->input->post('password');
//                  
//
//                  $admin_id = $this->Login_model->can_login_admin($email, $password);
//                  $rep_id   = $this->Login_model->can_login_rep($email, $password);
//
//                  if($admin_id){
//                        $admin_data  = array(
//                              'admin_id'         => $admin_id,
//                              'email'            => $email,
//                              'admin_img'        => $admin_img,
//                              'logged_in'        => true
//                             
//                               );
//
//                        //set Session Data
//                        $this->session->set_userdata($admin_data);
//                        //set Message 
//                        
//                        $this->session->set_flashdata('success','Admin sucessfully  logged in');
//
//                        //redirect
//                        redirect('admin/dashboard');
//
//                   //Check for class representatives     
//                  }elseif($rep_id){
//                        $rep_data  = array(
//                              'rep_id'           => $rep_id,
//                              'email'            => $email,
//                              'rep_matric_no'    => $rep_matric_no,
//                              'phone'            => $phone,
//                              'rep_sname'        => $rep_sname,
//                              'rep_fname'        => $rep_fname,
//                              'logged_in'        => true
//                               );
//
//                        //set Session Data
//                        $this->session->set_userdata($rep_data);
//                        //set Message 
//
//
//                        $this->session->set_flashdata('success','Class Rep has been logged in');
//
//                        //redirect
//                        redirect('classreps/portal');
//
//                  
//                  } else{
//
//                        //set Message 
//                  $this->session->set_flashdata('error','Invalid Login');
//
//                  //redirect
//                  redirect('login/user_login');
//
//                  }                 
//            
//            }
//      }

    
    
     //THIS FUNCTION TAKES CARE OF THE LOGIN FUNCTIONALITY OF THE SYSTEM USERS 
      public function user_login(){

    
            //Check Validation rules 
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            

            if($this->form_validation->run() == FALSE){ 
			
            $this->template->load('public', 'default', 'login');
                
            }else{
                  
                  //get post Data 
                  $email = $this->input->post('email');
                  $password = $this->input->post('password');
                  

                  $admin_id     = $this->Login_model->can_login_admin($email);
                  $rep_id       = $this->Login_model->can_login_rep($email);
                  $lecturer_id    = $this->Login_model->can_login_lecturer($email);
                 
                if($admin_id){
                  foreach($admin_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $admin_data  = array(
                              'admin_id'         => $row->admin_id,
                              'email'            => $email,
                              'logged_in'        => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($admin_data);
						
                        //set Message 
                         $this->session->set_flashdata('success','Admin sucessfully  logged in');

                        //redirect
                        redirect('admin/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login');
                       }
                  }
            }
                  
                  elseif($rep_id){
                      
                      
                           
                           foreach($rep_id as $row_rep){
                               
                               //Check for class representatives     
                              if(password_verify($password, $row_rep->password)){
                                    $rep_data  = array(
                                          'rep_id'           => $row_rep->rep_id,
                                          'email'            => $email,
										  'rep_fname'		 => $row_rep->rep_fname,
                                          'rep_matric_no'    => $row_rep->rep_matric_no,
                                          'logged_in'        => true
                                           );

                                    //set Session Data
                                    $this->session->set_userdata($rep_data);
                                    //set Message 

                                  
                                    $this->session->set_flashdata('success','Class Rep has been logged in');

                                    //redirect
                                    redirect('classreps/portal');
                         }else{
                                  //displays errors if password is wrong
                            $this->session->set_flashdata('error','Wrong Password');
                            redirect('login');

                             } 
                         }                 
                     }
                elseif($lecturer_id){
                      
                      
                           
                           foreach($lecturer_id as $row_lecturer){
                               
                               //Check for lecturer     
                              if(password_verify($password, $row_lecturer->password)){
                                    $rep_data  = array(
                                          'lecturer_id'         => $row_lecturer->lecturer_id,
                                          'email'               => $email,
										  'lecturer_fname'		=> $row_lecturer->lecturer_fname,
                                          'lecturer_sname'		=> $row_lecturer->lecturer_sname,
                                          'service_no'          => $row_lecturer->service_no,
                                          'logged_in'           => true
                                           );

                                    //set Session Data
                                    $this->session->set_userdata($rep_data);
                                    //set Message 

                                  
                                    $this->session->set_flashdata('success','Lecturer  has been logged in');

                                    //redirect
                                    redirect('lecturers/Portal_lecturer');
                         }else{
                                  //displays errors if password is wrong
                            $this->session->set_flashdata('error','Wrong Password');
                            redirect('login');

                              } 
                          }                 
                     }
                
                else{
                           //displays errors if password or email is wrong
                            $this->session->set_flashdata('error','Invalid Login');
                            redirect('login');
                           
                    }
             
      }
}



      //Function to take care of admin logout 
      public function logout_admin(){

            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('admin_in');
            $this->session->unset_userdata('email');
			$this->session->unset_userdata('admin_id');
            $this->session->sess_destroy();


            //message
            $this->session->set_flashdata('success', 'you are logged out');
            redirect('login/user_login');
            

      }
    
      //Function to take care of Class Rep logout 
      public function logout_rep(){

            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('rep_id');
            $this->session->unset_userdata('rep_fname');
            $this->session->unset_userdata('rep_sname');
			$this->session->unset_userdata('rep_matric_no');
            $this->session->sess_destroy();
          
            
            //message
            $this->session->set_flashdata('success', 'you are logged out');
            redirect('login/user_login');
            

      }
    
    //Function to take care of lecturer logout 
      public function logout_lecturer(){

            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('lecturer_id');
            $this->session->unset_userdata('lecturer_fname');
			$this->session->unset_userdata('lecturer_sname');
            $this->session->unset_userdata('service_no');
            $this->session->sess_destroy();
          
            
            //message
            $this->session->set_flashdata('success', 'you are logged out');
            redirect('login/user_login');
            

      }
    
    

     
	
}//END CLASS LOGIN