<?php
class Manageprofile_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}

	/******* THE MODEL FOR DIFFERENT SELECT *****/
    
    //function to get the specific admin from the database using their admin ID
	function get_admin($admin_id){
        
        $this->db->select('*')->from('admin_table')->where('admin_id', $admin_id);
	  	$query = $this->db->get();
	  	return $query->result();					
	}
    
    //function to update the admin profile 
	public function update_admin($admin_id, $data){
 		$this->table = 'admin_table';
 		$this->db->where('admin_id', $admin_id);
 		$this->db->update($this->table, $data);
 	} 
    //get image url for admin
    function get_image_url($id = '')
    {
        if (file_exists('upload/admin/'  . $id . '.jpg')){
            $image_url = base_url() . 'upload/admin/' . $id . '.jpg'; 
              
        }else
            $image_url = base_url() . 'upload/user.jpg';
            return $image_url;            
    }
	
	//get image url for classreps
	function get_image_url_rep($id = '')
    {
        if (file_exists('upload/classreps/'  . $id . '.jpg')){
            $image_url = base_url() . 'upload/classreps/' . $id . '.jpg'; 
                
        }else
            $image_url = base_url() . 'upload/user.jpg';
            return $image_url;            
    }
	//get image url for lecturer
	function get_image_url_lecturer($id = '')
    {
        if (file_exists('upload/lecturer/'  . $id . '.jpg')){
            $image_url = base_url() . 'upload/lecturer/' . $id . '.jpg';    
        
        }else
            $image_url = base_url() . 'upload/user.jpg';
            return $image_url;            
    }

}