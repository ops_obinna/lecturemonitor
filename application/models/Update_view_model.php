<?php
class Update_view_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}

	/******* THE MODEL FOR DIFFERENT SELECT *****/

	function get_venue(){
		$this->db->select()->from('venue_table');
	  	$query = $this->db->get();
	  	return $query->result();
					
	}

	function get_lecturer(){
		$this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();


	}

	function get_faculty(){
		$this->db->select()->from('faculty_table');
	  	$query = $this->db->get();
	  	return $query->result();
	}

	function get_department(){
        $this->db->select('*');
        $this->db->from('department_table');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = department_table.faculty_id');
        $query = $this->db->get();
        return $query->result();
	}

	function get_courserep(){
        $this->db->select('*');
        $this->db->from('course_reps_table');
        $this->db->join('department_table', 'department_table.dept_id = course_reps_table.dept_id');
        $this->db->join('level_table', 'level_table.level_id = course_reps_table.level_id');
        $query = $this->db->get();
        return $query->result();
	}
	function get_course(){
        $this->db->select('*');
        $this->db->from('courses_table');
        $this->db->join('department_table', 'department_table.dept_id = courses_table.dept_id');
        $this->db->join('level_table', 'level_table.level_id = courses_table.level_id');
        $this->db->join('venue_table', 'venue_table.venue_id = courses_table.venue_id');
        $query = $this->db->get();
        return $query->result();
	}




/******* THE MODEL FOR EDITING AND DELETING DATABASE CONTENTS *****/
	

	/*** THE FUNCTION BELOW HANDLES UPDATING LECTURE VENUES**/
	public function getvenuename($venue_id){
		$this->db->select('*')->from('venue_table')->where('venue_id', $venue_id);
	  	$query = $this->db->get();
	  	return $query->row();

 	}


 	public function updatevenue($venue_id, $data){
 		$this->table = 'venue_table';
 		$this->db->where('venue_id', $venue_id);
 		$this->db->update($this->table, $data);

 	} 

 	public function delete_venue($venue_id){
 		$this->table = 'venue_table';
 		$this->db->where('venue_id', $venue_id);
 		$this->db->delete($this->table);

 	}


 	/*** THE FUNCTION BELOW HANDLES UPDATING DEPARTMENT**/
 	public function getdeptname($dept_id){
 		$this->db->select('*')->from('department_table')->where('dept_id', $dept_id);
	  	$query = $this->db->get();
	  	return $query->row();

 	}

 	public function updatedept($dept_id, $data){
 		$this->table = 'department_table';
 		$this->db->where('dept_id', $dept_id);
 		$this->db->update($this->table, $data);
 	}
    
    public function deletedept($dept_id){
 		$this->table = 'department_table';
 		$this->db->where('dept_id', $dept_id);
 		$this->db->delete($this->table);

 	}



 	/*** THE FUNCTION BELOW HANDLES UPDATING FACULTY**/
 	public function getfacultyname($faculty_id){
 		$this->db->select('*')->from('faculty_table')->where('faculty_id', $faculty_id);
	  	$query = $this->db->get();
	  	return $query->row();

 	}

 	public function updatefaculty($faculty_id, $data){
 		$this->table = 'faculty_table';
 		$this->db->where('faculty_id', $faculty_id);
 		$this->db->update($this->table, $data);
 	}
    
    public function deletefaculty($faculty_id){
 		$this->table = 'faculty_table';
 		$this->db->where('faculty_id', $faculty_id);
 		$this->db->delete($this->table);

 	}


 	/*** THE FUNCTION BELOW HANDLES UPDATING FACULTY**/
 	public function getcoursetitle($course_id){
 		$this->db->select('*')->from('courses_table')->where('course_id', $course_id);
	  	$query = $this->db->get();
	  	return $query->row();

 	}

 	public function updatecourse($course_id, $data){
 		$this->table = 'courses_table';
 		$this->db->where('course_id', $course_id);
 		$this->db->update($this->table, $data);

 	}
    
    public function deletecourse($course_id){
        $this->table = 'courses_table';
 		$this->db->where('course_id', $course_id);
 		$this->db->delete($this->table);
    }

 	/*** THE FUNCTION BELOW HANDLES UPDATING COURSEREPS**/
 	public function getrepdata($rep_id){
 		$this->db->select('*')->from('course_reps_table')->where('rep_id', $rep_id);
	  	$query = $this->db->get();
	  	return $query->row();
 	}

 	public function updateclassrep($rep_id, $data){
 		$this->table = 'course_reps_table';
 		$this->db->where('rep_id', $rep_id);
 		$this->db->update($this->table, $data);
 	}
    
    public function deleteclassrep($rep_id){
        $this->table = 'course_reps_table';
 		$this->db->where('rep_id', $rep_id);
 		$this->db->delete($this->table);
    }

 	/*** THE FUNCTION BELOW HANDLES UPDATING LECTURERS**/
 	public function getlecturerdata($lecturer_id){
 		$this->db->select('*')->from('lecturer_table')->where('lecturer_id', $lecturer_id);
	  	$query = $this->db->get();
	  	return $query->row();

 	}
 	public function updatelecturer($lecturer_id, $data){
 		$this->table = 'lecturer_table';
 		$this->db->where('lecturer_id', $lecturer_id);
 		$this->db->replace($this->table, $data);
 	}
    
    public function deletelecturer($lecturer_id){
        $this->table = 'lecturer_table';
 		$this->db->where('lecturer_id', $lecturer_id);
 		$this->db->delete($this->table);
    }
    
    
    ////////IMAGE URL//////////
    function get_image_url($id = '')
    {
        if (file_exists('upload/classreps/'  . $id . '.jpg')){
            $image_url = base_url() . 'upload/classreps/' . $id . '.jpg';
            
        } elseif (file_exists('upload/lecturer/'  . $id . '.jpg')) {
            
            $image_url = base_url() . 'upload/lecturer/' . $id . '.jpg';
            
        }else
            $image_url = base_url() . 'upload/user.jpg';
        
            return $image_url;
        
            
    }

}