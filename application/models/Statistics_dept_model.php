<?php
class Statistics_dept_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
	}
    
    //Get List of lectures held for IMT statistics today
    public function imtheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 1);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for IMT statistics today
    public function imtabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 1);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for IMT statistics today
    public function imtpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 1);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for IMT statistics today
    public function imtotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 1);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //CPT MODELS
    //Get List of lectures held for CPT statistics today
    public function cptheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 2);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for CPT statistics today
    public function cptabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 2);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for CPT statistics today
    public function cptpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 2);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for CPT statistics today
    public function cptotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 2);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    
    //CSS MODEL
    //Get List of lectures held for CSS statistics today
    public function cssheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 3);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for CSS statistics today
    public function cssabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 3);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for CSS statistics today
    public function csspostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 3);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for CSS statistics today
    public function cssotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 3);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    
    
    //LIT MDEL
    
    //Get List of lectures held for LIT statistics today
    public function litheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 7);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for LIT statistics today
    public function litabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 7);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for LIT statistics today
    public function litpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 7);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for LIT statistics today
    public function litotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 7);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //ENGINEERING MODELS
    //Get List of lectures held for ABE statistics today
    public function abeheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 6);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for ABE statistics today
    public function abeabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 6);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for ABE statistics today
    public function abepostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 6);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for ABE statistics today
    public function abeotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 6);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for MECH statistics today
    public function mechheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 4);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for MECH statistics today
    public function mechabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 4);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for MECH statistics today
    public function mechpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 4);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for mech statistics today
    public function mechotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 4);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for elect statistics today
    public function electheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 8);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for elect statistics today
    public function electabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 8);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for elect statistics today
    public function electpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 8);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for elect statistics today
    public function electotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 8);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for MECHA statistics today
    public function mechaheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 5);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for MECHA statistics today
    public function mechaabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 5);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for MECHA statistics today
    public function mechapostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 5);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for mecha statistics today
    public function mechaotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 5);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
    //Get List of lectures held for COMP statistics today
    public function compheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 9);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for Comp statistics today
    public function compabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 9);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for Comp statistics today
    public function comppostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 9);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for comp statistics today
    public function compotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 9);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for telecom statistics today
    public function telecomheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 12);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for telecom statistics today
    public function telecomabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 12);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for Telecom statistics today
    public function telecompostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 12);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for telecom statistics today
    public function telecomotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 12);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures held for metallurg statistics today
    public function metallurgheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 11);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for metallurg statistics today
    public function metallurgabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 11);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for metallurg statistics today
    public function metallurgpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 11);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for metallurg statistics today
    public function metallurgotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 11);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for civil statistics today
    public function civilheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 10);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for civil statistics today
    public function civilabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 10);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for civil statistics today
    public function civilpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 10);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for civil statistics today
    public function civilotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 10);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }



    //Get List of lectures held for chem statistics today
    public function chemheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 27);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for chem statistics today
    public function chemabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 27);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for chem statistics today
    public function chempostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 27);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for chem statistics today
    public function chemotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 27);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }



//STARTS SEMT MODELS
    //Get List of lectures held for chem statistics today
    public function ebsheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 28);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for EBS statistics today
    public function ebsabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 28);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for EBS statistics today
    public function ebspostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 28);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for EBS statistics today
    public function ebsotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 28);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for PMT statistics today
    public function pmtheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 30);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for PMT statistics today
    public function pmtabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 30);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for PMT statistics today
    public function pmtpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 30);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for PMT statistics today
    public function pmtotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 30);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


//Get List of lectures held for TMT statistics today
    public function tmtheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 29);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for TMT statistics today
    public function tmtabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 29);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for TMT statistics today
    public function tmtpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 29);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for TMT statistics today
    public function tmtotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 29);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


//SET MODELS
    //Get List of lectures held for ARC statistics today
    public function archeldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 15);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for ARC statistics today
    public function arcabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 15);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for ARC statistics today
    public function arcpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 15);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for arc statistics today
    public function arcotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 15);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }







//Get List of lectures held for BLD statistics today
    public function bldheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 14);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for BLD statistics today
    public function bldabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 14);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for BLD statistics today
    public function bldpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 14);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for BLD statistics today
    public function bldotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 14);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


//Get List of lectures held for EST statistics today
    public function estheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 17);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for EST statistics today
    public function estabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 17);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for EST statistics today
    public function estpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 17);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for EST statistics today
    public function estotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 17);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


    //Get List of lectures held for QTS statistics today
    public function qtsheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 31);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for QTS statistics today
    public function qtsabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 31);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for QTS statistics today
    public function qtspostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 31);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for QTS statistics today
    public function qtsotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 31);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    
    //Get List of lectures held for SVG statistics today
    public function svgheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 32);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for SVG statistics today
    public function svgabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 32);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for SVG statistics today
    public function svgpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 32);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for SVG statistics today
    public function svgotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 32);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
    //Get List of lectures held for URP statistics today
    public function urpheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 33);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for URP statistics today
    public function urpabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 33);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for URP statistics today
    public function urppostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 33);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for URP statistics today
    public function urpotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 33);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    
    //SAAT MODELS
        
    //Get List of lectures held for AET statistics today
    public function aetheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 18);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for AET statistics today
    public function aetabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 18);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for AET statistics today
    public function aetpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 18);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for AET statistics today
    public function aetotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 18);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
        
    //Get List of lectures held for APT statistics today
    public function aptheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 26);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for APT statistics today
    public function aptabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 26);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for APT statistics today
    public function aptpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 26);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for APT statistics today
    public function aptotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 26);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
        
    //Get List of lectures held for CRP statistics today
    public function crpheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 20);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for CRP statistics today
    public function crpabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 20);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for CRP statistics today
    public function crppostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 20);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for CRP statistics today
    public function crpotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 20);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
    
    
        
    //Get List of lectures held for FST statistics today
    public function fstheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 22);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for FST statistics today
    public function fstabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 22);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for FST statistics today
    public function fstpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 22);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for FST statistics today
    public function fstotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 22);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
        
    //Get List of lectures held for SSD statistics today
    public function ssdheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 21);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for SSD statistics today
    public function ssdabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 21);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for SSD statistics today
    public function ssdpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 21);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for SSD statistics today
    public function ssdotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 21);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
        
    //Get List of lectures held for AFT statistics today
    public function aftheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 25);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for AFT statistics today
    public function aftabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 25);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for AFT statistics today
    public function aftpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 25);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for AFT statistics today
    public function aftotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 25);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //SPS MODEL 
            
    //Get List of lectures held for CHEMISTRY statistics today
    public function chemisheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 36);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for CHEMISTRY statistics today
    public function chemisabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 36);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for CHEMISTRY statistics today
    public function chemispostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 36);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for CHEMISTRY statistics today
    public function chemisotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 36);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
 

    
    
   
    
    //Get List of lectures held for GEOGRAHY statistics today
    public function geoheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 37);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for GEOGRAHY statistics today
    public function geoabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 37);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for GEOGRAHY statistics today
    public function geopostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 37);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for GEOGRAHY statistics today
    public function geootherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 37);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for GEOLOGY statistics today
    public function geolheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 38);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for GEOLOGY statistics today
    public function geolabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 38);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for GEOLOGY statistics today
    public function geolpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 38);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for GEOLOGY statistics today
    public function geolotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 38);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for MATHEMATICS statistics today
    public function mathheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 39);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for MATHEMATICS statistics today
    public function mathabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 39);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for MATHEMATICS statistics today
    public function mathpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 39);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for MATHEMATICS statistics today
    public function mathotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 39);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures held for PHYSICS statistics today
    public function phyheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 40);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for PHYSICS statistics today
    public function phyabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 40);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for PHYSICS statistics today
    public function phypostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 40);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for PHYSICS statistics today
    public function phyotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 40);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
    //Get List of lectures held for STATISTICS statistics today
    public function statheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 41);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for STATISTICS statistics today
    public function statabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 41);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for STATISTICS statistics today
    public function statpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 41);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for STATISTICS statistics today
    public function statotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 41);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


    
    //Get List of lectures held for BIOLOGICAL SCIENCE statistics today
    public function bioheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 13);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for BIOLOGICAL SCIENCE statistics today
    public function bioabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 13);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for BIOLOGICAL SCIENCE statistics today
    public function biopostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 13);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for BIOLOGICAL SCIENCE statistics today
    public function biootherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 13);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
        
    //Get List of lectures held for BIOCHEM statistics today
    public function biochemheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 34);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for BIOCHEM statistics today
    public function biochemabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 34);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for BIOCHEM statistics today
    public function biochempostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 34);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for BIOCHEM statistics today
    public function biochemotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 34);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
        
    //Get List of lectures held for MICRO statistics today
    public function microheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 35);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for MICRO statistics today
    public function microabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 35);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for MICRO statistics today
    public function micropostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 35);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for MICRO statistics today
    public function microotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 35);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }


    //SSTE MODEL 
        //Get List of lectures held for ITE statistics today
    public function iteheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 42);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for ITE statistics today
    public function iteabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 42);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for ITE statistics today
    public function itepostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 42);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for ITE statistics today
    public function iteotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 42);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    
        //Get List of lectures held for SCIEDU statistics today
    public function scieduheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 43);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for SCIEDU statistics today
    public function scieduabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 43);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for SCIEDU statistics today
    public function sciedupostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 43);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for SCIEDU statistics today
    public function scieduotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 43);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
        //Get List of lectures held for EDUTECH statistics today
    public function edutechheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 44);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for EDUTECH statistics today
    public function edutechabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 44);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for EDUTECH statistics today
    public function edutechpostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 44);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for EDUTECH statistics today
    public function edutechotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 44);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
     //Get List of lectures held for EDUTECH statistics today
    public function commeduheldlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 45);
        $this->db->where('update_table.lecture_status', 'Held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Absent for COMMEDU statistics today
    public function commeduabsentlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 45);
        $this->db->where('update_table.lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //Get List of lectures Postponed for COMMEDU statistics today
    public function commedupostponedlecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 45);
        $this->db->where('update_table.lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get List of lectures Others for COMMEDU statistics today
    public function commeduotherslecturers(){
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->where('update_table.dept_name', 45);
        $this->db->where('update_table.lecture_status', 'others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = update_table.lecturer_id');
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
//DEPARTMENTAL SUMMARY BEGINS HERE
/*THE MODELS BELOW WILL GET SUMMARY OF LECTURES HELD 
*DEPARTMENTALLY FOR THE SEMESTER  LECTURER BY LECTURER
*/
    
//BEGIN FOR IMT LECTURERS
    //Select all the lecturers registered for the semest in IMT
    public function imtlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',1);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for IMT department the semester for a particular lecturer
     public function lecture_held_imt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
      //function to get list of all lectures Absent for IMT department for the semester for a partcular lecturer
     function lecture_absent_imt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
      //function to get list of all lectures postponed  for IMT department for a lecturer for the semester
     function lecture_postponed_imt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
     function lecture_others_imt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Absent 500 level
    function absent_summary_five_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();







}
    //function for absent 400 level
    function absent_summary_four_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_imt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR IMT INDIVIDUAL LECTURER SUMMARY
    
    
    //BEGIN FOR CPT LECTURERS
    //Select all the lecturers registered for the semest in CPT
    public function cptlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',2);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for CPT department the semester for a particular lecturer
    public function lecture_held_cpt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for CPT department for the semester for a partcular lecturer
    function lecture_absent_cpt($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for CPT department for a lecturer for the semester
    function lecture_postponed_cpt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_cpt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();







}
    //function for absent 400 level
    function absent_summary_four_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_cpt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR CPT INDIVIDUAL LECTURER SUMMARY 
    
    
     //BEGIN FOR CSS LECTURERS
    //Select all the lecturers registered for the semest in CSS
    public function csslecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',3);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for CSS department the semester for a particular lecturer
    public function lecture_held_css($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for CSS department for the semester for a partcular lecturer
    function lecture_absent_css($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for CSS department for a lecturer for the semester
    function lecture_postponed_css($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_css($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();







}
    //function for absent 400 level
    function absent_summary_four_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_css($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR CSS INDIVIDUAL LECTURER SUMMARY 
    
    
  
    
    
     //BEGIN FOR LIT LECTURERS
    //Select all the lecturers registered for the semest in LIT
    public function litlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',7);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for LIT department the semester for a particular lecturer
    public function lecture_held_lit($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for LIT department for the semester for a partcular lecturer
    function lecture_absent_lit($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for LIT department for a lecturer for the semester
    function lecture_postponed_lit($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_lit($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_lit($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR LIT INDIVIDUAL LECTURER SUMMARY 
    

    //BEGIN FOR ABE LECTURERS
    //Select all the lecturers registered for the semest in ABE
    public function abelecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',6);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for ABE department the semester for a particular lecturer
    public function lecture_held_abe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for ABE department for the semester for a partcular lecturer
    function lecture_absent_abe($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for ABE department for a lecturer for the semester
    function lecture_postponed_abe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_abe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_abe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR ABE INDIVIDUAL LECTURER SUMMARY 
    
    
    //BEGIN FOR E/E LECTURERS
    //Select all the lecturers registered for the semest in Elect
    public function electlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',8);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for ELECT department the semester for a particular lecturer
    public function lecture_held_elect($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for ELECT department for the semester for a partcular lecturer
    function lecture_absent_elect($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for ELECT department for a lecturer for the semester
    function lecture_postponed_elect($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_elect($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_elect($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR ELECT INDIVIDUAL LECTURER SUMMARY 
    
    
    
     //BEGIN FOR MECH LECTURERS
    //Select all the lecturers registered for the semest in MECHANICAL
    public function mechlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',4);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for MECH department the semester for a particular lecturer
    public function lecture_held_mech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for MECH department for the semester for a partcular lecturer
    function lecture_absent_mech($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for MECH department for a lecturer for the semester
    function lecture_postponed_mech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_mech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_mech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR MECH INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
     //BEGIN FOR MECHATRONICS LECTURERS
    //Select all the lecturers registered for the semest in MECHATRONICS
    public function mechalecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',5);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for MECHA department the semester for a particular lecturer
    public function lecture_held_mecha($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for MECHA department for the semester for a partcular lecturer
    function lecture_absent_mecha($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for MECHA department for a lecturer for the semester
    function lecture_postponed_mecha($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_mecha($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_mecha($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR MECHATRONICS INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
     //BEGIN FOR COMPUTER ENGINEERING LECTURERS
    //Select all the lecturers registered for the semest in COMPUTER ENGINEERING
    public function cpelecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',9);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for CPE department the semester for a particular lecturer
    public function lecture_held_cpe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for CPE department for the semester for a partcular lecturer
    function lecture_absent_cpe($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for CPE department for a lecturer for the semester
    function lecture_postponed_cpe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_cpe($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_cpe($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR CPE INDIVIDUAL LECTURER SUMMARY
    
    
    
    
     //BEGIN FOR TELECOM LECTURERS
    //Select all the lecturers registered for the semest in TELECOM
    public function telecomlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',12);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for TELECOM department the semester for a particular lecturer
    public function lecture_held_telecom($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for TELECOM department for the semester for a partcular lecturer
    function lecture_absent_telecom($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for TELECOM department for a lecturer for the semester
    function lecture_postponed_telecom($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_telecom($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_telecom($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR TELECOM INDIVIDUAL LECTURER SUMMARY 
    
    
     //BEGIN FOR METALLURG LECTURERS
    //Select all the lecturers registered for the semest in METALLURGICAL ENG
    public function metallurglecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',11);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for metallurg department the semester for a particular lecturer
    public function lecture_held_metallurg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for MECH department for the semester for a partcular lecturer
    function lecture_absent_metallurg($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for metallurg department for a lecturer for the semester
    function lecture_postponed_metallurg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_metallurg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_metallurg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR metallurg INDIVIDUAL LECTURER SUMMARY 
    
     //BEGIN FOR CIVIL LECTURERS
    //Select all the lecturers registered for the semest in CIVIL ENG
    public function civillecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',10);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for civil department the semester for a particular lecturer
    public function lecture_held_civil($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for civil department for the semester for a partcular lecturer
    function lecture_absent_civil($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  civil department for a lecturer for the semester
    function lecture_postponed_civil($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_civil($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_civil($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR civil INDIVIDUAL LECTURER SUMMARY 
    
       //BEGIN FOR CHEM LECTURERS
    //Select all the lecturers registered for the semest in CHEM ENG
    public function chemlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',27);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for Chem department the semester for a particular lecturer
    public function lecture_held_chem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for Chem department for the semester for a partcular lecturer
    function lecture_absent_chem($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  chem department for a lecturer for the semester
    function lecture_postponed_chem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_chem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_chem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR chem INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
       //BEGIN FOR EBS LECTURERS
    //Select all the lecturers registered for the semest in EBS ENG
    public function ebslecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',28);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for EBS department the semester for a particular lecturer
    public function lecture_held_ebs($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for ebs department for the semester for a partcular lecturer
    function lecture_absent_ebs($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  ebs department for a lecturer for the semester
    function lecture_postponed_ebs($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_ebs($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_ebs($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR ebs INDIVIDUAL LECTURER SUMMARY 
    
    
    
       //BEGIN FOR PMT LECTURERS
    //Select all the lecturers registered for the semest in PMT
    public function pmtlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',30);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for PMT department the semester for a particular lecturer
    public function lecture_held_pmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for PMT department for the semester for a partcular lecturer
    function lecture_absent_pmt($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  PMT department for a lecturer for the semester
    function lecture_postponed_pmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_pmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_pmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR PMT INDIVIDUAL LECTURER SUMMARY 
    
    
    
       //BEGIN FOR TMT LECTURERS
    //Select all the lecturers registered for the semester in TMT
    public function tmtlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',29);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for TMT department the semester for a particular lecturer
    public function lecture_held_tmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for TMT department for the semester for a partcular lecturer
    function lecture_absent_tmt($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  TMT department for a lecturer for the semester
    function lecture_postponed_tmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_tmt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_tmt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR chem INDIVIDUAL LECTURER SUMMARY 
    
    
         //BEGIN FOR ARC LECTURERS
    //Select all the lecturers registered for the semester in ARC
    public function arclecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',15);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for ARC department the semester for a particular lecturer
    public function lecture_held_arc($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for ARC department for the semester for a partcular lecturer
    function lecture_absent_arc($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  ARC department for a lecturer for the semester
    function lecture_postponed_arc($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_arc($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_arc($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR ARC INDIVIDUAL LECTURER SUMMARY 
    
    
    
         //BEGIN FOR BLD LECTURERS
    //Select all the lecturers registered for the semester in BLD
    public function bldlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',14);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for BLD department the semester for a particular lecturer
    public function lecture_held_bld($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for BLD department for the semester for a partcular lecturer
    function lecture_absent_bld($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  BLD department for a lecturer for the semester
    function lecture_postponed_bld($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_bld($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_bld($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR BLD INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
         //BEGIN FOR EST LECTURERS
    //Select all the lecturers registered for the semester in EST
    public function estlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',17);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for EST department the semester for a particular lecturer
    public function lecture_held_est($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for TMT department for the semester for a partcular lecturer
    function lecture_absent_est($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  EST department for a lecturer for the semester
    function lecture_postponed_est($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_est($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_est($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR EST INDIVIDUAL LECTURER SUMMARY 
    
    
    
         //BEGIN FOR QTS LECTURERS
    //Select all the lecturers registered for the semester in QTS
    public function qtslecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',31);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for QTS department the semester for a particular lecturer
    public function lecture_held_qts($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for QTS department for the semester for a partcular lecturer
    function lecture_absent_qts($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  QTS department for a lecturer for the semester
    function lecture_postponed_qts($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_qts($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_qts($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR QTS INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
         //BEGIN FOR SVG LECTURERS
    //Select all the lecturers registered for the semester in SVG
    public function svglecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',32);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for SVG department the semester for a particular lecturer
    public function lecture_held_svg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for SVG department for the semester for a partcular lecturer
    function lecture_absent_svg($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  SVG department for a lecturer for the semester
    function lecture_postponed_svg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_svg($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_svg($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR SVG INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
         //BEGIN FOR URP LECTURERS
    //Select all the lecturers registered for the semester in URP
    public function urplecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',33);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for URP department the semester for a particular lecturer
    public function lecture_held_urp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for URP department for the semester for a partcular lecturer
    function lecture_absent_urp($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  URP department for a lecturer for the semester
    function lecture_postponed_urp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_urp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_urp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR URP INDIVIDUAL LECTURER SUMMARY 
  
    
    
    
    //BEGIN FOR AET LECTURERS
    //Select all the lecturers registered for the semester in AET
    public function aetlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',18);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for AET department the semester for a particular lecturer
    public function lecture_held_aet($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for AET department for the semester for a partcular lecturer
    function lecture_absent_aet($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  AET department for a lecturer for the semester
    function lecture_postponed_aet($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_aet($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_aet($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR AET INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    //BEGIN FOR APT LECTURERS
    //Select all the lecturers registered for the semester in APT
    public function aptlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',26);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for APT department the semester for a particular lecturer
    public function lecture_held_apt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for APT department for the semester for a partcular lecturer
    function lecture_absent_apt($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  APT department for a lecturer for the semester
    function lecture_postponed_apt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_apt($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_apt($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR APT INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
    
    
    //BEGIN FOR CRP LECTURERS
    //Select all the lecturers registered for the semester in CRP
    public function crplecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',20);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for CRP department the semester for a particular lecturer
    public function lecture_held_crp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for CRP department for the semester for a partcular lecturer
    function lecture_absent_crp($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  CRP department for a lecturer for the semester
    function lecture_postponed_crp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_crp($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_crp($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR CRP INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
    //BEGIN FOR FST LECTURERS
    //Select all the lecturers registered for the semester in URP
    public function fstlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',22);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for FST department the semester for a particular lecturer
    public function lecture_held_fst($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for FST department for the semester for a partcular lecturer
    function lecture_absent_fst($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  FST department for a lecturer for the semester
    function lecture_postponed_fst($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_fst($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_fst($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR FST INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
    //BEGIN FOR SSD LECTURERS
    //Select all the lecturers registered for the semester in SSD
    public function ssdlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',21);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for SSD department the semester for a particular lecturer
    public function lecture_held_ssd($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for SSD department for the semester for a partcular lecturer
    function lecture_absent_ssd($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  SSD department for a lecturer for the semester
    function lecture_postponed_ssd($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_ssd($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_ssd($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR SSD INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
    //BEGIN FOR AFT LECTURERS
    //Select all the lecturers registered for the semester in AFT
    public function aftlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',25);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for AFT department the semester for a particular lecturer
    public function lecture_held_aft($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for AFT department for the semester for a partcular lecturer
    function lecture_absent_aft($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  AFT department for a lecturer for the semester
    function lecture_postponed_aft($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_aft($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_aft($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR AFT INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
     //BEGIN FOR CHEMIS LECTURERS
    //Select all the lecturers registered for the semester in CHEMIS
    public function chemislecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',36);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for CHEMIS department the semester for a particular lecturer
    public function lecture_held_chemis($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for CHEMIS department for the semester for a partcular lecturer
    function lecture_absent_chemis($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  CHEMIS department for a lecturer for the semester
    function lecture_postponed_chemis($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_chemis($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_chemis($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR CHEMIS INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
     //BEGIN FOR GEOGRAHY LECTURERS
    //Select all the lecturers registered for the semester in GEOGRAHY
    public function geolecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',37);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for GEO department the semester for a particular lecturer
    public function lecture_held_geo($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for GEO department for the semester for a partcular lecturer
    function lecture_absent_geo($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  GEO department for a lecturer for the semester
    function lecture_postponed_geo($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_geo($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_geo($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR GEO INDIVIDUAL LECTURER SUMMARY 
    
    
    
    
    
    
    
     //BEGIN FOR GEOLOGY LECTURERS
    //Select all the lecturers registered for the semester in GEOLOGY
    public function geollecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',38);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for GEOLOGY department the semester for a particular lecturer
    public function lecture_held_geol($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for GEO department for the semester for a partcular lecturer
    function lecture_absent_geol($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  GEOL department for a lecturer for the semester
    function lecture_postponed_geol($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_geol($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_geol($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR GEOLOGY INDIVIDUAL LECTURER SUMMARY
    
    
    
     //BEGIN FOR MATHEMATICS LECTURERS
    //Select all the lecturers registered for the semester in mathematics
    public function mathlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',39);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for MATH department the semester for a particular lecturer
    public function lecture_held_math($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for MATH department for the semester for a partcular lecturer
    function lecture_absent_math($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  MATH department for a lecturer for the semester
    function lecture_postponed_math($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_math($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_math($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR MATH INDIVIDUAL LECTURER SUMMARY
    
    
    
    
    
   //BEGIN FOR PHYSICS LECTURERS
    //Select all the lecturers registered for the semester in PHYSICS
    public function phylecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',40);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for PHYSICS department the semester for a particular lecturer
    public function lecture_held_phy($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for PHYSICS department for the semester for a partcular lecturer
    function lecture_absent_phy($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  PHYSICS department for a lecturer for the semester
    function lecture_postponed_phy($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_phy($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_phy($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR PHY INDIVIDUAL LECTURER SUMMARY  
    
    
  
    
    
    
     //BEGIN FOR STATISTICS LECTURERS
    //Select all the lecturers registered for the semester in STATISTICS
    public function statlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',41);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for STAT department the semester for a particular lecturer
    public function lecture_held_stat($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for stat department for the semester for a partcular lecturer
    function lecture_absent_stat($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  STAT department for a lecturer for the semester
    function lecture_postponed_stat($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_stat($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_stat($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR STATISTICS INDIVIDUAL LECTURER SUMMARY
    
    
    
         //BEGIN FOR BIO.SCI. LECTURERS
    //Select all the lecturers registered for the semester in BIO.SCI
    public function biolecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',13);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for BIO SCI. department the semester for a particular lecturer
    public function lecture_held_bio($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for bio.sci. department for the semester for a partcular lecturer
    function lecture_absent_bio($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  BIO SCI. department for a lecturer for the semester
    function lecture_postponed_bio($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_bio($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_bio($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR BIO SCI.  INDIVIDUAL LECTURER SUMMARY
    
    
    

         //BEGIN FOR BIOCHEM LECTURERS
    //Select all the lecturers registered for the semester in BIOCHEM
    public function biochemlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',34);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for BIOChem department the semester for a particular lecturer
    public function lecture_held_biochem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for biochem department for the semester for a partcular lecturer
    function lecture_absent_biochem($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  BIOCHEM department for a lecturer for the semester
    function lecture_postponed_biochem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_biochem($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_biochem($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR BIOCHEM INDIVIDUAL LECTURER SUMMARY
    
 
    
    
         //BEGIN FOR MICROBIO LECTURERS
    //Select all the lecturers registered for the semester in MICROBIO
    public function microlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',35);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for MICRO department the semester for a particular lecturer
    public function lecture_held_micro($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for microbio department for the semester for a partcular lecturer
    function lecture_absent_micro($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  MICROBIO department for a lecturer for the semester
    function lecture_postponed_micro($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_micro($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_micro($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR MICROBIO  INDIVIDUAL LECTURER SUMMARY
  
    
     //BEGIN FOR ITE LECTURERS
    //Select all the lecturers registered for the semester in ITE
    public function itelecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',42);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for ITE department the semester for a particular lecturer
    public function lecture_held_ite($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for ITE department for the semester for a partcular lecturer
    function lecture_absent_ite($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  ITE department for a lecturer for the semester
    function lecture_postponed_ite($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_ite($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_ite($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR ITE  INDIVIDUAL LECTURER SUMMARY
        
    
    
    
         //BEGIN FOR SCIEDU LECTURERS
    //Select all the lecturers registered for the semester in SCIEDU
    public function sciedulecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',43);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for SCIEDU department the semester for a particular lecturer
    public function lecture_held_sciedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for sciedu department for the semester for a partcular lecturer
    function lecture_absent_sciedu($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  SCIEDU department for a lecturer for the semester
    function lecture_postponed_sciedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_sciedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_sciedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR SCIEDU  INDIVIDUAL LECTURER SUMMARY
    
    
    
         //BEGIN FOR EDUTECH LECTURERS
    //Select all the lecturers registered for the semester in EDUTECH
    public function edutechlecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',44);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for EDUTECH department the semester for a particular lecturer
    public function lecture_held_edutech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for edutech department for the semester for a partcular lecturer
    function lecture_absent_edutech($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for  EDUTECH department for a lecturer for the semester
    function lecture_postponed_edutech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_edutech($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_edutech($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR EDUTECH  INDIVIDUAL LECTURER SUMMARY
    
    
         //BEGIN FOR COMMEDU LECTURERS
    //Select all the lecturers registered for the semester in COMMEDU
    public function commedulecturerssummary(){
        $this->db->select('*');
        $this->db->from('lecturer_table');
        $this->db->where('lecturer_table.dept_id',45);
        $this->db->join('department_table', 'department_table.dept_id = lecturer_table.dept_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to select list of all lectures held for COMMEDU department the semester for a particular lecturer
    public function lecture_held_commedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures Absent for commedu department for the semester for a partcular lecturer
    function lecture_absent_commedu($lecturer_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function to get list of all lectures postponed  for COMMEDU department for a lecturer for the semester
    function lecture_postponed_commedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }  
    //function to get list of all lectures Absent for other reasons for the semester for a lecturer
    function lecture_others_commedu($lecturer_id){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    //function for Held 500 level
    function held_summary_five_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 500 level
    function absent_summary_five_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 500 level
    function postponed_summary_five_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 500 level
    function others_summary_five_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for held 400 level
    function held_summary_four_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
}
    //function for absent 400 level
    function absent_summary_four_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for postponed 400 level
    function postponed_summary_four_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 400 level
    function others_summary_four_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for held 300 level
    function held_summary_three_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Absent 300 level
    function absent_summary_three_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Postponed 300 level
    function postponed_summary_three_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Others 300 level
    function others_summary_three_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 200 level
    function held_summary_two_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for absent 200 level
    function absent_summary_two_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for postponed 200 level
    function postponed_summary_two_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for others 200 level
    function others_summary_two_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for Held 100 level
    function held_summary_one_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Held 100 level
    function absent_summary_one_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
     //function for Postponed 100 level
    function postponed_summary_one_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function for others 100 level
    function others_summary_one_commedu($lecturer_id){
        $this->db->where('update_table.lecturer_id', $lecturer_id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
//END FOR COMMEDU  INDIVIDUAL LECTURER SUMMARY
    
    
}
    
    
    
    
    
    
    
