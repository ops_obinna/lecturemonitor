<?php
class Ongoinglecture_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}

	//THIS INDEX FUNCTION IS THE MODEL THAT DISPLAY ON THE DASHBOARD ALL LECTURES  FOR THE DAY FOR ALL DEPARTMENTS
	public function index(){
        
        $today = date("Y-m-d");
        $this->db->select();
        $this->db->from('update_table');
        $this->db->where('date', $today);
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        
        $query = $this->db->get();
        return $query->result();

	}


	/* STARTING IMPLEMENTATION OF THE DEPARTMENTAL LECTURE VIEWS */

	/*  SCHOOL OF AGRICULTURAL TECHNOLOGY */
	//function to  view Agric Extension and Economics department
	public function getlist_agricext(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 18 );
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();		
                    
	}//end function

	public function getlist_animalprod(){
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 26);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();	
		
	}//end function

	public function getlist_aquafishtech(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 25);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();	
	}//end function

	public function getlist_cropprod(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',20);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();	

	}//end function

	public function getlist_foodtech(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',22);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();

	}//end function

	public function getlist_soilsci(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',21);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();		

	}//end function


	/*  SCHOOL OF ENGINEERING AND ENGINEERING TECHNOLOGY */
	public function getlist_agricbiores(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',6);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_chemeng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',27);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_civileng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',10);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'departmesnt_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_compeng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 9);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_electeng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 8);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_mechatroeng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 5);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_mecheng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 4);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_metallureng(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 11);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_telecom(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 12);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function


	/*  SCHOOL OF ENVIRONMENTAL TECHNOLOGY */
	public function getlist_archi(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 15);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_building(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 14);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_estatemgt(){
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 17);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESsC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_quantsurv(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 31);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_svg(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 32);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_urp(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 33);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function



	/*  SCHOOL OF INFORMATION AND COMMUNICATIONS TECHNOLOGY */
	public function getlist_compsci(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',2);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_cybersec(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',3);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_infotech(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',1);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_libinfotech(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',7);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	/*  SCHOOL OF ENTERPRENUERSHIP AND MANAGEMENT TECHNOLOGY */
	public function getlist_ebs(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 28);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_projmgt(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 30);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_tranmgt(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 29);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function


	/*  SCHOOL OF LIFE SCIENCES */
	public function getlist_biochem(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',34);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_biosci(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 13);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_microbio(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 35);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function



	/*  SCHOOL OF SCIENCE AND TECHNOLOGY EDUCATION */
	public function getlist_commedu(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 45);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	
	public function getlist_edutech(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 44);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	
	public function getlist_indtechedu(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 42);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function


	public function getlist_sciedu(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 43);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function


	/*  SCHOOL OF PHYSICAL SCIENCE */
	public function getlist_chem(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name',36);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_geography(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 37);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_geology(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 38);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function


	public function getlist_math(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 39);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_physics(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 40);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function

	public function getlist_stat(){ 
		$today = date("Y-m-d");
		//fetches the data from the database
		$this->db->select()->from('update_table');
		$this->db->where('update_table.dept_name', 41);
		$this->db->where('date', $today);
		$this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
  		$query = $this->db->get();
  		return $query->result_array();
	}//end function





}	 