<?php
class Statistics_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
        /* STRUCTURE
        * 1. WHOLE SCHOOL STATISTICS BY FACULTY
        * 2. SICT STATISTICS departmental
        * 3. SEET Statistics departmental
        * 4. SEMT Statistics departmental
        * 5. SET Statistics departmental
        * 6. SAAT Statistics departmental
        * 7. SPS Statistics departmental
        * 8. SLS Statistics departmental
        * 9. SSTE Statistics departmental
		*/
	}
    
    
    /* MODEL TO FETCH LECTURES HELD FOR THE DAY BY FACULTY*/
    //function to fetch number of lectures in SICT
    function sict_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    //function to fetch number of lectures in SEET
    function seet_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function to fetch number of lectures in SET
    function set_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SEMT
    function semt_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SAAT
    function saat_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SPS
    function sps_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in STE
    function ste_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SLS
    function sls_stat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    
    /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures in SICT
    function sict_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET
    function seet_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function to fetch number of lectures in SET
    function set_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SEMT
    function semt_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SAAT
    function saat_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SPS
    function sps_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in STE
    function ste_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SLS
    function sls_postponedstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    
     
    
     /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lectures in SICT
    function sict_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET
    function seet_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function to fetch number of lectures in SET
    function set_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SEMT
    function semt_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SAAT
    function saat_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SPS
    function sps_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in STE
    function ste_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SLS
    function sls_absentstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    /* MODEL TO FETCH LECTURERS ABSENT FOR OTHER REASONS */
    //function to fetch number of lectures in SICT
    function sict_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET
    function seet_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function to fetch number of lectures in SET
    function set_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SEMT
    function semt_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SAAT
    function saat_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SPS
    function sps_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in STE
    function ste_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    //function to fetch number of lectures in SLS
    function sls_othersstat(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    /* END OF SCHOOL STATISTICS BY FACULTY*/
    
    
    
    /*
    *
    */
     /* MODEL TO FETCH SICT DEPARTMENTAL STATISTICS FOR THE DAY */
    //function to fetch number of lectures in IMT
    function imt_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    /* MODEL TO FETCH SICT DEPARTMENTAL STATISTICS FOR POSTPONED LECTURES FOR THE DAY */
    //function to fetch number of lectures in IMT
    function imt_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    /* MODEL TO FETCH SICT DEPARTMENTAL STATISTICS FOR ABSENT LECTURES FOR THE DAY */
    //function to fetch number of lectures in IMT
    function imt_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    /* MODEL TO FETCH SICT DEPARTMENTAL STATISTICS FOR OTHER EXCUSES LECTURES FOR THE DAY */
    //function to fetch number of lectures in IMT
    function imt_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    
    /*
    *
    *
    *
    *
    */
    /* MEDEL FOR SEET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SEET
     function abe_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function elect_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    /* MODEL TO FETCH SEET DEPARTMENTAL STATISTICS FOR POSTPONED LECTURES FOR THE DAY */
     function abe_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    function elect_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    /* MODEL TO FETCH SEET DEPARTMENTAL STATISTICS FOR ABSENT LECTURES FOR THE DAY */
     function abe_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    function elect_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     /* MODEL TO FETCH SEET DEPARTMENTAL STATISTICS FOR ABSENT LECTURERS DUE TO OTHER REASONS */
     function abe_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    function elect_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    /*MODEL FOR SEMT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SEMT
     function ebs_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
 
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures postponed in SEMT
     function ebs_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     /* MODEL TO FETCH LECTURERS ABSENT FOR THE DAY */
    //function to fetch number of lectures postponed in SEMT
     function ebs_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     /* MODEL TO FETCH LECTURERS ABSENT FOR OTHER REASONS */
    //function to fetch number of lectures postponed in SEMT
     function ebs_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
     /*MODEL FOR SET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SET
     function arc_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    /*MODEL FOR SET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures postponed in SET
     function arc_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    
    
   
    
    /*MODEL FOR SET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lecturers absent in SET
     function arc_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    
     /*MODEL FOR SET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURERS ABSENT FOR OTHER REASONS */
    //function to fetch number of lecturers absent  for other reasons in SET
     function arc_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
     /*MODEL FOR SAAT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SAAT
     function aet_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
    /*MODEL FOR SAAT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures Postponed in SAAT
     function aet_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
    /*MODEL FOR SAAT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lectures Absent in SAAT
     function aet_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
    /*MODEL FOR SAAT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES OTHERS FOR THE DAY */
    //function to fetch number of lectures Others in SAAT
     function aet_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }  
    
    
     /*MODEL FOR SPS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SPS
     function chemis_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
     /*MODEL FOR SPS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures postponed in SPS
     function chemis_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     /*MODEL FOR SPS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lecturers Absent in SPS
     function chemis_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    /*MODEL FOR SPS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURERS ABSENT FOR OTHER REASONS */
    //function to fetch number of lecturers Absent in SPS
     function chemis_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     /*MODEL FOR SLS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SLS
     function bio_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     /*MODEL FOR SLS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures postponed in SLS
     function bio_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
     /*MODEL FOR SLS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lectures absent in SLS
     function bio_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
     /*MODEL FOR SLS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES OTHERS FOR THE DAY */
    //function to fetch number of lecturers absent for others in SLS
     function bio_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     /*MODEL FOR SSTE DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SSTE
     function ite_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_heldstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    /*MODEL FOR SSTE DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES POSTPONED FOR THE DAY */
    //function to fetch number of lectures postponed in SSTE
     function ite_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_postponedstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
     /*MODEL FOR SSTE DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES ABSENT FOR THE DAY */
    //function to fetch number of lectures absent in SSTE
     function ite_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_absentstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
     /*MODEL FOR SSTE DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES OTHERS FOR THE DAY */
    //function to fetch number of lectures Others in SSTE
     function ite_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_othersstattoday(){
        $today = date("Y-m-d");
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('date', $today);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    //BELOW FUNCTIONS ARE TO GET DAILY LECTURE MAX FOR FACULTY
    
    //function to get daily summary of number of supposed lecture for SICT
    public function sictdailysummary(){
        $today = date("l");
        $this->db->select()->from('sictdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SEET
    public function seetdailysummary(){
        $today = date("l");
        $this->db->select()->from('seetdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SAAT
    public function saatdailysummary(){
        $today = date("l");
        $this->db->select()->from('saatdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SEMT
    public function semtdailysummary(){
        $today = date("l");
        $this->db->select()->from('semtdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SET
    public function setdailysummary(){
        $today = date("l");
        $this->db->select()->from('setdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    //function to get daily summary of number of supposed lecture for SPS
    public function spsdailysummary(){
        $today = date("l");
        $this->db->select()->from('spsdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for STE
    public function stedailysummary(){
        $today = date("l");
        $this->db->select()->from('stedailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SLS
    public function slsdailysummary(){
        $today = date("l");
        $this->db->select()->from('slsdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    
    //END FUNCTIONS ARE TO GET DAILY LECTURE MAX FOR FACULTY
    
    //FUNCTION TO GET DAILY SUMMARY LECTURE FOR SICT DEPARTMENTS
    
     //function to get daily summary of number of supposed lecture for IMT
    public function imtdailysummary(){
        $today = date("l");
        $this->db->select()->from('imtdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for CPT
    public function cptdailysummary(){
        $today = date("l");
        $this->db->select()->from('cptdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for CSS
    public function cssdailysummary(){
        $today = date("l");
        $this->db->select()->from('cssdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for LIT
    public function litdailysummary(){
        $today = date("l");
        $this->db->select()->from('litdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //FUNCTION TO GET DAILY SUMMARY LECTURE FOR SEET DEPARTMENTS
    
     //function to get daily summary of number of supposed lecture for ABE
    public function abedailysummary(){
        $today = date("l");
        $this->db->select()->from('abedailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for elect/elect
    public function electdailysummary(){
        $today = date("l");
        $this->db->select()->from('electdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for mechanical Engineering
    public function mechdailysummary(){
        $today = date("l");
        $this->db->select()->from('mechdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Mechatroncs Engineering
    public function mechtrodailysummary(){
        $today = date("l");
        $this->db->select()->from('mechadailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Computer Engieering
    public function compdailysummary(){
        $today = date("l");
        $this->db->select()->from('comdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Telecommunication Engineering
    public function telecomdailysummary(){
        $today = date("l");
        $this->db->select()->from('teldailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for Metallugical Engineering
    public function metallurgdailysummary(){
        $today = date("l");
        $this->db->select()->from('metadailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Chemical Engineerings
    public function chemdailysummary(){
        $today = date("l");
        $this->db->select()->from('chemdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
     //function to get daily summary of number of supposed lecture for Civil Engineerings
    public function civildailysummary(){
        $today = date("l");
        $this->db->select()->from('civildailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    
    //function to get daily summary of number of supposed lecture for EBS
    public function ebsdailysummary(){
        $today = date("l");
        $this->db->select()->from('ebsdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for PMT
    public function pmtdailysummary(){
        $today = date("l");
        $this->db->select()->from('pmtdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for TMT
    public function tmtdailysummary(){  
        $today = date("l");
        $this->db->select()->from('tmtdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for Architecture
    public function arcdailysummary(){  
        $today = date("l");
        $this->db->select()->from('archidailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Building
    public function blddailysummary(){  
        $today = date("l");
        $this->db->select()->from('blddailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Estate Mgt
    public function estdailysummary(){  
        $today = date("l");
        $this->db->select()->from('estdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Quantity Survey
    public function qtsdailysummary(){  
        $today = date("l");
        $this->db->select()->from('qsdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Survey and Geo
    public function svgdailysummary(){  
        $today = date("l");
        $this->db->select()->from('svgdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for URP
    public function urpdailysummary(){  
        $today = date("l");
        $this->db->select()->from('urpdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for AET
    public function aetdailysummary(){  
        $today = date("l");
        $this->db->select()->from('aetdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for APT
    public function aptdailysummary(){  
        $today = date("l");
        $this->db->select()->from('animalproddailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for CRP
    public function crpdailysummary(){  
        $today = date("l");
        $this->db->select()->from('cropproddailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for FST
    public function fstdailysummary(){  
        $today = date("l");
        $this->db->select()->from('fstdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SSD
    public function ssddailysummary(){  
        $today = date("l");
        $this->db->select()->from('ssddailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for AQUACULTURE
    public function aftdailysummary(){  
        $today = date("l");
        $this->db->select()->from('fisheriesdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for CHEMISTRY
    public function chemisdailysummary(){  
        $today = date("l");
        $this->db->select()->from('chemisdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for geogrphy
    public function geodailysummary(){  
        $today = date("l");
        $this->db->select()->from('geodailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for GEOLOGY
    public function geologdailysummary(){  
        $today = date("l");
        $this->db->select()->from('geoldailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Mathematics
    public function mathdailysummary(){  
        $today = date("l");
        $this->db->select()->from('mathdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for PHYSIC
    public function phydailysummary(){  
        $today = date("l");
        $this->db->select()->from('phydailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Statistics
    public function statdailysummary(){  
        $today = date("l");
        $this->db->select()->from('statdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
     //function to get daily summary of number of supposed lecture for Bio-sci.
    public function biodailysummary(){  
        $today = date("l");
        $this->db->select()->from('bioscidailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Bio-chem
    public function biochemdailysummary(){  
        $today = date("l");
        $this->db->select()->from('biochemdailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for Micro
    public function microdailysummary(){  
        $today = date("l");
        $this->db->select()->from('microbiodailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    
    //function to get daily summary of number of supposed lecture for ITE
    public function itedailysummary(){  
        $today = date("l");
        $this->db->select()->from('itedailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result(); 	
        
    }
    
    //function to get daily summary of number of supposed lecture for SCI.EDU
    public function sciedudailysummary(){  
        $today = date("l");
        $this->db->select()->from('sciedudailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result();
    
    
     }
    
    //function to get daily summary of number of supposed lecture for EDU TECH
    public function edutechdailysummary(){  
        $today = date("l");
        $this->db->select()->from('edutechdudailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result();
    
    
     }
    
    
    //function to get daily summary of number of supposed lecture for COM. EDU
    public function comedudailysummary(){  
        $today = date("l");
        $this->db->select()->from('comedududailysummary');
        $this->db->where('week_day', $today);
        $query = $this->db->get();
         return $query->result();
    
    
     }
    
    
     /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER  BY FACULTY*/
    //function to fetch number of lectures in SICT for the Semester
    function sict_heldstat_summay(){
        $this->db->where('faculty_name', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET for the Semester
    function seet_heldstat_summay(){
        $this->db->where('faculty_name', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
     
    }
    //function to fetch number of lectures in SET for the Semester
    function set_heldstat_summay(){
        $this->db->where('faculty_name', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SEMT for the Semester
    function semt_heldstat_summay(){
        $this->db->where('faculty_name', 4);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SAAT for the Semester
    function saat_heldstat_summay(){
        $this->db->where('faculty_name', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SPS for the Semester
    function sps_heldstat_summay(){
        $this->db->where('faculty_name', 6);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in STE for the Semester
    function ste_heldstat_summay(){
        $this->db->where('faculty_name', 7);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    //function to fetch number of lectures in SLS for the Semester
    function sls_heldstat_summay(){
        $this->db->where('faculty_name', 8);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
 
         /* MODEL TO FETCH LECTURES POSTPONEDFOR THE SEMESTER  BY FACULTY*/
    //function to fetch number of lectures in SICT for the Semester
    function sict_postponedstat_summay(){
        $this->db->where('faculty_name', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET for the Semester
    function seet_postponedstat_summay(){
        $this->db->where('faculty_name', 2);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
     
    }
    //function to fetch number of lectures in SET for the Semester
    function set_postponedstat_summay(){
        $this->db->where('faculty_name', 3);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SEMT for the Semester
    function semt_postponedstat_summay(){
        $this->db->where('faculty_name', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SAAT for the Semester
    function saat_postponedstat_summay(){
        $this->db->where('faculty_name', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SPS for the Semester
    function sps_postponedstat_summay(){
        $this->db->where('faculty_name', 6);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in STE for the Semester
    function ste_postponedstat_summay(){
        $this->db->where('faculty_name', 7);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    //function to fetch number of lectures in SLS for the Semester
    function sls_postponedstat_summay(){
        $this->db->where('faculty_name', 8);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    
         /* MODEL TO FETCH LECTURES ABSENT FOR THE SEMESTER  BY FACULTY*/
    //function to fetch number of lectures in SICT for the Semester
    function sict_absentstat_summay(){
        $this->db->where('faculty_name', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET for the Semester
    function seet_absentstat_summay(){
        $this->db->where('faculty_name', 2);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
     
    }
    //function to fetch number of lectures in SET for the Semester
    function set_absentstat_summay(){
        $this->db->where('faculty_name', 3);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SEMT for the Semester
    function semt_absentstat_summay(){
        $this->db->where('faculty_name', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SAAT for the Semester
    function saat_absentstat_summay(){
        $this->db->where('faculty_name', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SPS for the Semester
    function sps_absentstat_summay(){
        $this->db->where('faculty_name', 6);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in STE for the Semester
    function ste_absentstat_summay(){
        $this->db->where('faculty_name', 7);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    //function to fetch number of lectures in SLS for the Semester
    function sls_absentstat_summay(){
        $this->db->where('faculty_name', 8);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    
         /* MODEL TO FETCH LECTURES ABSENT  FOR  OTHER  REASONS FOR THE SEMESTER  BY FACULTY*/
    //function to fetch number of lectures in SICT for the Semester
    function sict_othersstat_summay(){
        $this->db->where('faculty_name', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in SEET for the Semester
    function seet_othersstat_summay(){
        $this->db->where('faculty_name', 2);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
     
    }
    //function to fetch number of lectures in SET for the Semester
    function set_othersstat_summay(){
        $this->db->where('faculty_name', 3);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SEMT for the Semester
    function semt_othersstat_summay(){
        $this->db->where('faculty_name', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SAAT for the Semester
    function saat_othersstat_summay(){
        $this->db->where('faculty_name', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in SPS for the Semester
    function sps_othersstat_summay(){
        $this->db->where('faculty_name', 6);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    //function to fetch number of lectures in STE for the Semester
    function ste_othersstat_summay(){
        $this->db->where('faculty_name', 7);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    //function to fetch number of lectures in SLS for the Semester
    function sls_othersstat_summay(){
        $this->db->where('faculty_name', 8);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    
}
    
    
    
    
    
      /* MODEL TO FETCH SICT DEPARTMENTAL STATISTICS FOR THE SEMESTER */
    //function to fetch number of lectures in IMT
    function imt_heldstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_heldstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_heldstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_heldstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in IMT
    function imt_postponedstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_postponedstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_postponedstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_postponedstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in IMT
    function imt_absentstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_absentstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_absentstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_absentstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    //function to fetch number of lectures in IMT
    function imt_othersstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function cpt_othersstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 2);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function css_othersstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 3);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function lit_othersstatsummary(){
        $this->db->where('faculty_name', 1);
        $this->db->where('dept_name', 7);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    
    /* MODEL FOR SEET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER */
    //function to fetch number of lectures held in SEET
     function abe_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function elect_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_heldstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
     function abe_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function elect_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_postponedstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
    
     function abe_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function elect_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_absentstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    
      function abe_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 6);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function elect_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 8);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mech_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function mechtro_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function comp_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 9);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function telecom_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 12);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function metallurg_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 11);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function civil_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 10);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function chem_othersstatsummary(){
        $this->db->where('faculty_name', 2);
        $this->db->where('dept_name', 27);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     /*MODEL FOR SEMT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER */
    //function to fetch number of lectures held in SEMT
     function ebs_heldstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_heldstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_heldstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function ebs_postponedstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_postponedstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_postponedstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function ebs_absentstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_absentstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_absentstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function ebs_othersstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 28);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function pmt_othersstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 30);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
     function tmt_othersstatsummary(){
        $this->db->where('faculty_name', 4);
        $this->db->where('dept_name', 29);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
  
     /*MODEL FOR SET DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER */
    //function to fetch number of lectures held in SET
     function arc_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_heldstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    } 
    
     function arc_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_postponedstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }  
    
    function arc_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_absentstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }  
    
    function arc_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 15);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
     
    function bld_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 14);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
        
    }
    
    function est_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 17);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function qts_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 31);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function svg_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 32);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
    function urp_othersstatsummary(){
        $this->db->where('faculty_name', 3);
        $this->db->where('dept_name', 33);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();  
    }
    
  /*MODEL FOR SAAT DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER*/
    //function to fetch number of lectures held in SAAT
     function aet_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_heldstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
   function aet_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_postponedstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
  
    function aet_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_absentstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
   
    function aet_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 18);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function apt_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 26);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function crp_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 20);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function fst_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 22);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function ssd_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 21);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function aft_othersstatsummary(){
        $this->db->where('faculty_name', 5);
        $this->db->where('dept_name', 25);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
   
    
     /*MODEL FOR SPS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER */
    //function to fetch number of lectures held in SPS
     function chemis_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_heldstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function chemis_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_postponedstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
    function chemis_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_absentstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
    function chemis_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 36);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
     function geo_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 37);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function geolog_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 38);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function math_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 39);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function phy_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 40);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function stat_othersstatsummary(){
        $this->db->where('faculty_name', 6);
        $this->db->where('dept_name', 41);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
  
     /*MODEL FOR SLS DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE SEMESTER */
    //function to fetch number of lectures held in SLS
     function bio_heldstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_heldstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_heldstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function bio_postponedstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_postponedstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_postponedstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
    
     function bio_absentstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_absentstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_absentstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
    
     function bio_othersstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 13);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function biochem_othersstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 34);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function microbio_othersstatsummary(){
        $this->db->where('faculty_name', 8);
        $this->db->where('dept_name', 35);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     } 
  
     /*MODEL FOR SSTE DEPARTMENTAL STARTS HERE */
      /* MODEL TO FETCH LECTURES HELD FOR THE DAY */
    //function to fetch number of lectures held in SSTE
     function ite_heldstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_heldstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_heldstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_heldstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
      function ite_postponedstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_postponedstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_postponedstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_postponedstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
      function ite_absentstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_absentstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_absentstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_absentstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    
       function ite_othersstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 42);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function sciedu_othersstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 43);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function edutech_othersstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 44);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
    
    function comedu_othersstatsummary(){
        $this->db->where('faculty_name', 7);
        $this->db->where('dept_name', 45);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows(); 
         
     }
} 