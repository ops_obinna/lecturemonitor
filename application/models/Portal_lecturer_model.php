<?php
class Portal_lecturer_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}

	//fetches lecture activities for lecturer today
	public function today_activities($pf_no){ 
        $today = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('service_no', $pf_no);
        $this->db->where('date', $today);
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');        
        $query = $this->db->get();
        return $query->result();
	}
    
    //function to get list of all lectures held for the semester
     function lecture_held($pf_no){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for the semester
     function lecture_absent($pf_no){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for the semester
     function lecture_postponed($pf_no){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for other reasons for the semester
     function lecture_others($pf_no){
       $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
     //function for Held 500 level
    function held_summary_five($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Absent 500 level
    function absent_summary_five($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for postponed 500 level
    function postponed_summary_five($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for others 500 level
    function others_summary_five($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for held 400 level
    function held_summary_four($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for absent 400 level
    function absent_summary_four($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for postponed 400 level
    function postponed_summary_four($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for others 400 level
    function others_summary_four($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for held 300 level
    function held_summary_three($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for Absent 300 level
    function absent_summary_three($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Postponed 300 level
    function postponed_summary_three($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Others 300 level
    function others_summary_three($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Held 200 level
    function held_summary_two($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for absent 200 level
    function absent_summary_two($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for postponed 200 level
    function postponed_summary_two($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for others 200 level
    function others_summary_two($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
     //function for Held 100 level
    function held_summary_one($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Held 100 level
    function absent_summary_one($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for Postponed 100 level
    function postponed_summary_one($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    //function for others 100 level
    function others_summary_one($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table');
        return $query->num_rows();
    }
    
    
    
     //fill your faculty dropdown  
   public function getfaculty()  
   {  
      $this->db->select('faculty_id,faculty_name');  
      $this->db->from('faculty_table');  
      $query = $this->db->get();
      return $query->result_array();
  
   }
    
    //fill your department dropdown depending on the selected faculty  
   public function getDeptByFaculty($faculty_id=string)  
   {  
      $this->db->select('dept_id,dept_name');  
      $this->db->from('department_table');  
      $this->db->where('faculty_id',$faculty_id); 
      $this->db->order_by('dept_name', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  
    
    
    //fill your course title dropdown depending on the selected department  
   public function getCourseTitleByDept($dept_id=string)  
   {  
      $this->db->select('dept_id,course_title,course_id');  
      $this->db->from('courses_table');  
      $this->db->where('dept_id',$dept_id);  
      $this->db->order_by('course_title', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  
    
    //populate Course Code dropdown
    public function getCourseCodeByDept($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->order_by('course_code', 'ASC');
      $query = $this->db->get();  
      return $query->result();   
    }
    
    //populate Lecturer dropdown
    public function getLecturerByCourse($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = courses_table.lecturer_id');
      $query = $this->db->get();  
      return $query->result();        
        
    }
    
    //populate Venue dropdown
    public function getVenueByCourse($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->join('venue_table', 'venue_table.venue_id = courses_table.venue_id');
      $query = $this->db->get();  
      return $query->result();                
    }
    
    //submit update for lectures
    public function updateLecture($data){
        $this->table = 'update_table_lecturer'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 
    } 
    
    
    //****THE BELOW FUNCTIONS WILL LET THE LECTURER VIEW HIS/HER UPDATES MADE BY THEMSELVES AS AGAINST THAT MADE BY COURSEREP*//
    //*    
    //*
    //*
    //* */
            
    //function to get list of all lectures held for the semester
     function lecture_held_myupdate($pf_no){
       $this->db->select('*');
        $this->db->from('update_table_lecturer');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'held');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table_lecturer.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table_lecturer.course_code','courses_table.course_id = update_table_lecturer.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table_lecturer.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table_lecturer.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for the semester
     function lecture_absent_myupdate($pf_no){
       $this->db->select('*');
        $this->db->from('update_table_lecturer');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Absent');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table_lecturer.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table_lecturer.course_code','courses_table.course_id = update_table_lecturer.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table_lecturer.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table_lecturer.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for the semester
     function lecture_postponed_myupdate($pf_no){
       $this->db->select('*');
        $this->db->from('update_table_lecturer');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Postponed');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table_lecturer.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table_lecturer.course_code','courses_table.course_id = update_table_lecturer.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table_lecturer.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table_lecturer.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    //function to get list of all lectures Absent for other reasons for the semester
     function lecture_others_myupdate($pf_no){
       $this->db->select('*');
        $this->db->from('update_table_lecturer');
        $this->db->where('service_no', $pf_no);
        $this->db->where('lecture_status', 'Others');
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table_lecturer.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table_lecturer.course_code','courses_table.course_id = update_table_lecturer.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table_lecturer.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table_lecturer.level_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
     //function for Held 500 level
    function held_summary_five_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Absent 500 level
    function absent_summary_five_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for postponed 500 level
    function postponed_summary_five_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for others 500 level
    function others_summary_five_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 5);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for held 400 level
    function held_summary_four_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for absent 400 level
    function absent_summary_four_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for postponed 400 level
    function postponed_summary_four_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for others 400 level
    function others_summary_four_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 4);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for held 300 level
    function held_summary_three_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for Absent 300 level
    function absent_summary_three_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Postponed 300 level
    function postponed_summary_three_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Others 300 level
    function others_summary_three_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 3);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Held 200 level
    function held_summary_two_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for absent 200 level
    function absent_summary_two_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'absent');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for postponed 200 level
    function postponed_summary_two_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'postponed');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for others 200 level
    function others_summary_two_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 2);
        $this->db->where('lecture_status', 'others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
     //function for Held 100 level
    function held_summary_one_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'held');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Held 100 level
    function absent_summary_one_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Absent');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for Postponed 100 level
    function postponed_summary_one_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Postponed');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
    
    //function for others 100 level
    function others_summary_one_myupdate($id){
        $this->db->where('service_no', $id);
        $this->db->where('level_id', 1);
        $this->db->where('lecture_status', 'Others');
        $query = $this->db->get('update_table_lecturer');
        return $query->num_rows();
    }
        
    /**THE END OF LECTURERS TO VIEW THE COURSE UPDATE THEY MADE FOR THEMSELVES*/
    
    
    
}