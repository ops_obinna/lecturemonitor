<?php
class Portal_classrep_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}
    
    //fill your faculty dropdown  
   public function getfaculty()  
   {  
      $this->db->select('faculty_id,faculty_name');  
      $this->db->from('faculty_table');  
      $query = $this->db->get();
      return $query->result_array();
  
   }  
   //fill your department dropdown depending on the selected faculty  
   public function getDeptByFaculty($faculty_id=string)  
   {  
      $this->db->select('dept_id,dept_name');  
      $this->db->from('department_table');  
      $this->db->where('faculty_id',$faculty_id); 
      $this->db->order_by('dept_name', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  
    
    
    //fill your course title dropdown depending on the selected department  
   public function getCourseTitleByDept($dept_id=string)  
   {  
      
       
      $this->db->select('dept_id,course_title,course_id');  
      $this->db->from('courses_table');  
      $this->db->where('dept_id',$dept_id);  
      $this->db->order_by('course_title', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  
    
    //populate Course Code dropdown
    public function getCourseCodeByDept($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->order_by('course_code', 'ASC');
      $query = $this->db->get();  
      return $query->result();   
    }
    
    //populate Lecturer dropdown
    public function getLecturerByCourse($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->join('lecturer_table', 'lecturer_table.lecturer_id = courses_table.lecturer_id');
      $query = $this->db->get();  
      return $query->result();        
        
    }
    
    //populate Venue dropdown
    public function getVenueByCourse($course_id=string)
    {
      $this->db->select();  
      $this->db->from('courses_table');  
      $this->db->where('course_id',$course_id);  
      $this->db->join('venue_table', 'venue_table.venue_id = courses_table.venue_id');
      $query = $this->db->get();  
      return $query->result();                
    }
    
    //submit update for lectures
    public function updateLecture($data){
        $this->table = 'update_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 
    }    
	
	
	public function today_activities_model($rep_id){
        $today = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('rep_matric_no', $rep_id);
        $this->db->where('date', $today);
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name');        
        $query = $this->db->get();
        return $query->result();

	}
	
	
	public function previous_activities_model($rep_id){
        $this->db->select('*');
        $this->db->from('update_table');
        $this->db->where('rep_matric_no', $rep_id);
        $this->db->order_by('update_id','DESC');
        $this->db->join('faculty_table', 'faculty_table.faculty_id = update_table.faculty_name');
        $this->db->join('courses_table', 'courses_table.course_id = update_table.course_code','courses_table.course_id = update_table.course_title');
        $this->db->join('department_table', 'department_table.dept_id = update_table.dept_name'); 
        $this->db->join('level_table', 'level_table.level_id = update_table.level_id');
        $query = $this->db->get();
        return $query->result();

	}

    
}