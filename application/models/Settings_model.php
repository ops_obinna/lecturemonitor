<?php
class Settings_model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}


	//function to register Session
	public function session_semester($data){

		//truncate the table before reinserting data
		$this->db->truncate('session_table');
        
        
        $this->table = 'session_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}
    
}
    