
<?php

class Login_model extends CI_Model{

//	 public function can_login_admin($email, $password) {
//            $this->table = 'admin_table';
//            $this->db->select('*');
//            $this->db->from($this->table);
//            $this->db->where('email', $email);
//            $this->db->where('password', $password);
//            $this->db->limit(1);
//
//            $query = $this->db->get();
//
//            if($query->num_rows() == 1){
//                  return $query->row()->admin_id;
//            }else{
//                  return false;
//            }
//      }
//
////
//      public function can_login_rep($email, $password){
//
//            $this->table = 'course_reps_table';
//            $this->db->select('*');
//            $this->db->from($this->table);
//            $this->db->where('email', $email);
//            $this->db->where('password', $password);
//            $this->db->limit(1);
//
//            $query = $this->db->get();
//
//            if($query->num_rows() == 1){
//                  return $query->row()->rep_id;
//            }else{
//                  return false;
//            }
//
//      }
    
    
     public function can_login_admin($email) {
           $this->db->select('*');
           $this->db->from('admin_table');
           $this->db->where('email', $email);
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }

//
      public function can_login_rep($email){
            $this->db->select('*');
            $this->db->from('course_reps_table');
            $this->db->where('email', $email);
            $this->db->limit(1);
            $query = $this->db->get();

            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
                  return false;
            }

      }
    
    public function can_login_lecturer($email){
            $this->db->select('*');
            $this->db->from('lecturer_table');
            $this->db->where('email_address', $email);
            $this->db->limit(1);
            $query = $this->db->get();

            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
                  return false;
            }

      }

		
}