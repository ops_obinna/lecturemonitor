<?php
class Registration_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		
		
	}


	//function to register course reps
	public function registerclassrep($data){

		$this->table = 'course_reps_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function registerclassrep

	//function to register lecturer
	public function registerlecturer($data){
		$this->table = 'lecturer_table'; //Defining the table
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function registerlecturer

	//function to register faculty
	public function registerfaculty($data){

		$this->table = 'faculty_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function register faculty

	//function to register department
	public function registerdept($data){

		$this->table = 'department_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function register department

	//function to register venue
	public function registervenue($data){

		$this->table = 'venue_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function register department

	//function to register course
	public function registercourse($data){

		$this->table = 'courses_table'; //defining the table name
		$this->db->insert($this->table, $data); //performing insert operation 

	}//end function register department


	/* STARTING IMPLEMENTATION OF THE DEPARTMENTAL LECTURE VIEWS */

	//function to  view Agric Extension and Economics department
	public function getlist_agricext(){ //This function is not working

		//fetches the data from the database
		$this->db->select()->from('update_table')->where('dept_name','Agricultural Economics and Extention Technology');
  		$query = $this->db->get();
  		return $query->result_array();
  		
		

	}//end function
}	 