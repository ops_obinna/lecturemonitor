<hr />
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Activities List ');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Venue </th>
                            <th>New Venue </th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($portal as $portal_list):?>
                        <tr>
                            
							<td><?php echo $portal_list->course_title; ?></td>
							<td><?php echo $portal_list->course_code; ?></td>
							<td><?php echo $portal_list->date; ?></td>
							<td><?php echo $portal_list->start_time ?></td>
							<td><?php echo $portal_list->end_time; ?></td>
							<td><?php echo $portal_list->lecture_status; ?></td>
                            <td><?php echo $portal_list->lecture_venue; ?></td>
                            <td><?php echo $portal_list->new_venue; ?></td>
							
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>