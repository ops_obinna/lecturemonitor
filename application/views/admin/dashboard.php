
<!--This is the View for  the Dashboard that display daily Ongoing Lectures around the University*/ -->

		<h3>
			<span class="label label-primary">Activities List</span>
			<small>Dashboard is been updated as activities occur</small>
		</h3>
		<br />
            



<?php if($dashboard) : ?>
<!--		<div id="lectures"></div>-->
		<table class="table table-bordered datatable table-hover" id="table-4">
			<thead>
				<tr class="warning">
							<th>Update_id</th>
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Venue </th>
                            <th>New Venue </th>
							<th>Department</th>
							<th>Faculty</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Service_No</th>
							<th>Matric_No</th>
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($dashboard as $dashboard_list) : ?>
				   
				        <tr class="odd gradeX">
							<td class="info"><?php echo $dashboard_list->update_id; ?></td>
							<td><?php echo $dashboard_list->course_title; ?></td>
							<td><?php echo $dashboard_list->course_code; ?></td>
							<td><?php echo $dashboard_list->lecture_venue; ?></td>
                            <td><?php echo $dashboard_list->new_venue; ?></td>
							<td><?php echo $dashboard_list->dept_name; ?></td>
							<td><?php echo $dashboard_list->faculty_name; ?></td>
							<td><?php echo $dashboard_list->date; ?></td>
							<td><?php echo $dashboard_list->start_time ?></td>
							<td><?php echo $dashboard_list->end_time; ?></td>
							<td><?php echo $dashboard_list->lecture_status; ?></td>
							<td><?php echo $dashboard_list->service_no; ?></td>
							<td><?php echo $dashboard_list->rep_matric_no; ?></td>
						</tr>	

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
        <?php else : ?>
	<p> No Lecture Update For Now </p>
	<?php endif; ?>
	
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			}                   
            
            );
				
		</script>


<script type="text/javascript"> 
        $(document).ready(function() {  
          setInterval(function(){
             $('table-4').load("<?php echo base_url();?>/admin/Dashboard/index").fadeIn("slow") 
          }, 1000);          
                   
        });  
    </script>  
  
                                  
                               
		
				
		