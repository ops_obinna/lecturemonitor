<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Agricultural Economics and Extension Technology  Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $aetheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aetheld['service_no']; ?></td>

                    <td>
                        <?php echo $aetheld['lecturer_fname']; ?>
                        <?php echo $aetheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aetheld['faculty_name']; ?></td>
                    <td><?php echo $aetheld['dept_name']; ?></td>
                    <td><?php echo $aetheld['course_code']; ?></td>
                    <td><?php echo $aetheld['level_name']; ?></td>
                    <td><?php echo $aetheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $aetheld['start_time']; ?> -  <?php echo $aetheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $aetabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aetabsent['service_no']; ?></td>

                    <td>
                        <?php echo $aetabsent['lecturer_fname']; ?>
                        <?php echo $aetabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aetabsent['faculty_name']; ?></td>
                    <td><?php echo $aetabsent['dept_name']; ?></td>
                    <td><?php echo $aetabsent['course_code']; ?></td>
                    <td><?php echo $aetabsent['level_name']; ?></td>
                    <td><?php echo $aetabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $aetabsent['start_time']; ?> -  <?php echo $aetabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $aetpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $aetpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $aetpostponed['lecturer_fname']; ?>
                        <?php echo  $aetpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $aetpostponed['faculty_name']; ?></td>
                    <td><?php echo  $aetpostponed['dept_name']; ?></td>
                    <td><?php echo  $aetpostponed['course_code']; ?></td>
                    <td><?php echo  $aetpostponed['level_name']; ?></td>
                    <td><?php echo  $aetpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $aetpostponed['start_time']; ?> -  <?php echo $aetpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $aetothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aetothers['service_no']; ?></td>

                    <td>
                        <?php echo $aetothers['lecturer_fname']; ?>
                        <?php echo $aetothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aetothers['faculty_name']; ?></td>
                    <td><?php echo $aetothers['dept_name']; ?></td>
                    <td><?php echo $aetothers['course_code']; ?></td>
                    <td><?php echo $aetothers['level_name']; ?></td>
                    <td><?php echo $aetothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $aetothers['start_time']; ?> -  <?php echo $aetothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

