<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Semesterial List for Lecture Held ');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th>PF.NO</th>
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Venue </th>
                            <th>New Venue </th>
                            <th>Level </th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($held as $held_list):?>
                        <tr>
                            <td><?php echo $held_list->service_no; ?></td>
							<td><?php echo $held_list->course_title; ?></td>
							<td><?php echo $held_list->course_code; ?></td>
							<td><?php echo $held_list->date; ?></td>
							<td><?php echo $held_list->start_time ?></td>
							<td><?php echo $held_list->end_time; ?></td>
							<td><?php echo $held_list->lecture_status; ?></td>
                            <td><?php echo $held_list->lecture_venue; ?></td>
                            <td><?php echo $held_list->new_venue; ?></td>
                            <td><?php echo $held_list->level_name; ?></td>
							
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>
<hr />
<div class="row">
<div class="col-md-6">


<!--javascript -->
    
<canvas id="myBarChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets: [{
            label: 'Number of  lectures held',
            data: ["<?php echo $heldfive; ?>", 
                   "<?php echo $heldfour; ?>", 
                   "<?php echo $heldthree; ?>", 
                   "<?php echo $heldtwo; ?>", 
                   "<?php echo $heldone; ?>"
                  ],
           backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
<div class="col-md-6">


    <!--javascript -->
    
<canvas id="myPieChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $heldfive; ?>", 
                   "<?php echo $heldfour; ?>", 
                   "<?php echo $heldthree; ?>", 
                   "<?php echo $heldtwo; ?>", 
                   "<?php echo $heldone; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
	
</div>

<hr />
<div class="row">
	<div class="col-md-12">
        
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Semesterial List for Absent Lectures');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th>PF.NO</th>
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Venue </th>
                            <th>New Venue </th>
                            <th>Level </th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($absent as $absent_list):?>
                        <tr>
                            <td><?php echo $absent_list->service_no; ?></td>
							<td><?php echo $absent_list->course_title; ?></td>
							<td><?php echo $absent_list->course_code; ?></td>
							<td><?php echo $absent_list->date; ?></td>
							<td><?php echo $absent_list->start_time ?></td>
							<td><?php echo $absent_list->end_time; ?></td>
							<td><?php echo $absent_list->lecture_status; ?></td>
                            <td><?php echo $absent_list->lecture_venue; ?></td>
                            <td><?php echo $absent_list->new_venue; ?></td>
                            <td><?php echo $absent_list->level_name; ?></td>
							
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>

<hr />
<div class="row">

<div class="col-md-6">

<!--javascript -->
    
<canvas id="myBarChartAbsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartAbsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets: [{
            label: 'Number of  lectures Absent',
            data: ["<?php echo $absentfive; ?>", 
                   "<?php echo $absentfour; ?>", 
                   "<?php echo $absentthree; ?>", 
                   "<?php echo $absenttwo; ?>", 
                   "<?php echo $absentone; ?>"
                   ],
          backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-6">


    <!--javascript -->
    
<canvas id="myPieChartAbsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartAbsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $absentfive; ?>", 
                   "<?php echo $absentfour; ?>", 
                   "<?php echo $absentthree; ?>", 
                   "<?php echo $absenttwo; ?>", 
                   "<?php echo $absentone; ?>"
                  
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>

</div>

<hr />
<div class="row">
	<div class="col-md-12">
        
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Semesterial List for Postponed Lectures');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th>PF.NO</th>
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Venue </th>
                            <th>New Venue </th>
                            <th>Level </th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($postponed as $postponed_list):?>
                        <tr>
                            <td><?php echo $postponed_list->service_no; ?></td>
							<td><?php echo $postponed_list->course_title; ?></td>
							<td><?php echo $postponed_list->course_code; ?></td>
							<td><?php echo $postponed_list->date; ?></td>
							<td><?php echo $postponed_list->start_time ?></td>
							<td><?php echo $postponed_list->end_time; ?></td>
							<td><?php echo $postponed_list->lecture_status; ?></td>
                            <td><?php echo $postponed_list->lecture_venue; ?></td>
                            <td><?php echo $postponed_list->new_venue; ?></td>
                            <td><?php echo $postponed_list->level_name; ?></td>
							
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>
<hr />
<div class="row">

<div class="col-md-6">

<!--javascript -->
    
<canvas id="myBarChartPostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartPostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets: [{
            label: 'Number of  lectures Postponed',
            data: ["<?php echo $postponedfive; ?>", 
                   "<?php echo $postponedfour; ?>", 
                   "<?php echo $postponedthree; ?>", 
                   "<?php echo $postponedtwo; ?>", 
                   "<?php echo $postponedone; ?>"
                   ],
          backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-6">


    <!--javascript -->
    
<canvas id="myPieChartPostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartPostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $postponedfive; ?>", 
                   "<?php echo $postponedfour; ?>", 
                   "<?php echo $postponedthree; ?>", 
                   "<?php echo $postponedtwo; ?>", 
                   "<?php echo $postponedone; ?>"
                  
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>

</div>

<hr />
<div class="row">
	<div class="col-md-12">
        
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Semesterial List for Lectures Absent for Other Reasons');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th>PF.NO</th>
							<th>Course_title</th>
							<th>Course_code</th>
							<th>Date</th>
							<th>Start_time</th>
							<th>End_time</th>
							<th>Status</th>
							<th>Venue </th>
                            <th>New Venue </th>
                            <th>Level </th>
						</tr>
					</thead>
                    <tbody>
                    	<?php foreach($others as $others_list):?>
                        <tr>
                            <td><?php echo $others_list->service_no; ?></td>
							<td><?php echo $others_list->course_title; ?></td>
							<td><?php echo $others_list->course_code; ?></td>
							<td><?php echo $others_list->date; ?></td>
							<td><?php echo $others_list->start_time ?></td>
							<td><?php echo $others_list->end_time; ?></td>
							<td><?php echo $others_list->lecture_status; ?></td>
                            <td><?php echo $others_list->lecture_venue; ?></td>
                            <td><?php echo $others_list->new_venue; ?></td>
                            <td><?php echo $others_list->level_name; ?></td>
							
                    </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>

<hr />
<div class="row">

<div class="col-md-6">

<!--javascript -->
    
<canvas id="myBarChartOthers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartOthers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets: [{
            label: 'Absent for other reasons',
            data: ["<?php echo $othersfive; ?>", 
                   "<?php echo $othersfour; ?>", 
                   "<?php echo $othersthree; ?>", 
                   "<?php echo $otherstwo; ?>", 
                   "<?php echo $othersone; ?>"
                   ],
          backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-6">


    <!--javascript -->
    
<canvas id="myPieChartOthers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartOthers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["500Level", "400Level", "300Level", "200Level", "100Level"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $othersfive; ?>", 
                   "<?php echo $othersfour; ?>", 
                   "<?php echo $othersthree; ?>", 
                   "<?php echo $otherstwo; ?>", 
                   "<?php echo $othersone; ?>"
                  
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>

</div>
<hr />
<div class="row">
	<div class="col-md-12">
        
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo('Summary Review');?>
                </a>
            </li>
			
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
					
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		
							<th>Level</th>
							<th>Held</th>
							<th>Absent</th>
							<th>Postponed</th>
							<th>Others</th>
							
						</tr>
					</thead>
                    <tbody>
                    	
                        <tr>
                            <td><?php echo "500"; ?></td>
							<td><?php echo $heldfive; ?></td>
							<td><?php echo $absentfive; ?></td>
							<td><?php echo $postponedfive; ?></td>
							<td><?php echo $othersfive; ?></td>
							
							
							
                    </tr>
                        
                        
                        
                        <tr>
                            
							<td><?php echo "400"; ?></td>
							<td><?php echo $heldfour; ?></td>
							<td><?php echo $absentfour; ?></td>
							<td><?php echo $postponedfour; ?></td>
							<td><?php echo $othersfour; ?></td>
							
                    </tr>
                        
                        
                        
                        <tr>
                            
							<td><?php echo "300"; ?></td>
							<td><?php echo $heldthree; ?></td>
							<td><?php echo $absentthree; ?></td>
							<td><?php echo $postponedthree; ?></td>
							<td><?php echo $othersthree; ?></td>
							
                    </tr>
                        
                        
                        
                        <tr>
                            
							<td><?php echo "200"; ?></td>
							<td><?php echo $heldtwo; ?></td>
							<td><?php echo $absenttwo; ?></td>
							<td><?php echo $postponedtwo; ?></td>
							<td><?php echo $otherstwo; ?></td>
							
                    </tr>
                        
                        
                        
                        <tr>
                            
							<td><?php echo "100"; ?></td>
							<td><?php echo $heldone; ?></td>
							<td><?php echo $absentone; ?></td>
							<td><?php echo $postponedone; ?></td>
							<td><?php echo $othersone; ?></td>
							
                    </tr>
                        
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
        </div>
    </div>
</div>