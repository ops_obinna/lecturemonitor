<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Crop Production Technology  Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $crpheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $crpheld['service_no']; ?></td>

                    <td>
                        <?php echo $crpheld['lecturer_fname']; ?>
                        <?php echo $crpheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $crpheld['faculty_name']; ?></td>
                    <td><?php echo $crpheld['dept_name']; ?></td>
                    <td><?php echo $crpheld['course_code']; ?></td>
                    <td><?php echo $crpheld['level_name']; ?></td>
                    <td><?php echo $crpheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $crpheld['start_time']; ?> -  <?php echo $crpheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $crpabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $crpabsent['service_no']; ?></td>

                    <td>
                        <?php echo $crpabsent['lecturer_fname']; ?>
                        <?php echo $crpabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $crpabsent['faculty_name']; ?></td>
                    <td><?php echo $crpabsent['dept_name']; ?></td>
                    <td><?php echo $crpabsent['course_code']; ?></td>
                    <td><?php echo $crpabsent['level_name']; ?></td>
                    <td><?php echo $crpabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $crpabsent['start_time']; ?> -  <?php echo $crpabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $crppostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $crppostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $crppostponed['lecturer_fname']; ?>
                        <?php echo  $crppostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $crppostponed['faculty_name']; ?></td>
                    <td><?php echo  $crppostponed['dept_name']; ?></td>
                    <td><?php echo  $crppostponed['course_code']; ?></td>
                    <td><?php echo  $crppostponed['level_name']; ?></td>
                    <td><?php echo  $crppostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $crppostponed['start_time']; ?> -  <?php echo $crppostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $crpothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $crpothers['service_no']; ?></td>

                    <td>
                        <?php echo $crpothers['lecturer_fname']; ?>
                        <?php echo $crpothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $crpothers['faculty_name']; ?></td>
                    <td><?php echo $crpothers['dept_name']; ?></td>
                    <td><?php echo $crpothers['course_code']; ?></td>
                    <td><?php echo $crpothers['level_name']; ?></td>
                    <td><?php echo $crpothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $crpothers['start_time']; ?> -  <?php echo $crpothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

