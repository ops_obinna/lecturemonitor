
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SPS Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $chemistoday; ?>", 
                   "<?php echo $geotoday; ?>", 
                   "<?php echo $geologtoday; ?>", 
                   "<?php echo $mathtoday; ?>", 
                   "<?php echo $phytoday; ?>", 
                   "<?php echo $stattoday; ?>"
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             data: [
                   "<?php echo $chemistoday; ?>", 
                   "<?php echo $geotoday; ?>", 
                   "<?php echo $geologtoday; ?>", 
                   "<?php echo $mathtoday; ?>", 
                   "<?php echo $phytoday; ?>", 
                   "<?php echo $stattoday; ?>"
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>HELD</td>
        <td><?php echo $chemistoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($chemissummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>HELD</td>
        <td><?php echo $geotoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>HELD</td>
        <td><?php echo $geologtoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geologsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>HELD</td>
        <td><?php echo $mathtoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($mathsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>HELD</td>
        <td><?php echo $phytoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($physummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>HELD</td>
        <td><?php echo $stattoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($statsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SPS Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $chemispostponedtoday; ?>", 
                   "<?php echo $geopostponedtoday; ?>", 
                   "<?php echo $geologpostponedtoday; ?>", 
                   "<?php echo $mathpostponedtoday; ?>", 
                   "<?php echo $phypostponedtoday; ?>", 
                   "<?php echo $statpostponedtoday; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            data: [
                   "<?php echo $chemispostponedtoday; ?>", 
                   "<?php echo $geopostponedtoday; ?>", 
                   "<?php echo $geologpostponedtoday; ?>", 
                   "<?php echo $mathpostponedtoday; ?>", 
                   "<?php echo $phypostponedtoday; ?>", 
                   "<?php echo $statpostponedtoday; ?>"
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Postponed Lectures   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>POSTPONED</td>
        <td><?php echo $chemispostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($chemissummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>POSTPONED</td>
        <td><?php echo $geopostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>POSTPONED</td>
        <td><?php echo $geologpostponedtoday . " out of";?>
             <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geologsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>POSTPONED</td>
        <td><?php echo $mathpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($mathsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>POSTPONED</td>
        <td><?php echo $phypostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($physummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>POSTPONED</td>
        <td><?php echo $statpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($statsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SPS Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lecturers absent department',
            data: [
                   "<?php echo $chemisabsenttoday; ?>", 
                   "<?php echo $geoabsenttoday; ?>", 
                   "<?php echo $geologabsenttoday; ?>", 
                   "<?php echo $mathabsenttoday; ?>", 
                   "<?php echo $phyabsenttoday; ?>", 
                   "<?php echo $statabsenttoday; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            data: [
                   "<?php echo $chemisabsenttoday; ?>", 
                   "<?php echo $geoabsenttoday; ?>", 
                   "<?php echo $geologabsenttoday; ?>", 
                   "<?php echo $mathabsenttoday; ?>", 
                   "<?php echo $phyabsenttoday; ?>", 
                   "<?php echo $statabsenttoday; ?>"
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Lecturers   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>ABSENT</td>
        <td><?php echo $chemisabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($chemissummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>ABSENT</td>
        <td><?php echo $geoabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>ABSENT</td>
        <td><?php echo $geologabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geologsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>ABSENT</td>
        <td><?php echo $mathabsenttoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($mathsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>ABSENT</td>
        <td><?php echo $phyabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($physummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>ABSENT</td>
        <td><?php echo $statabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($statsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SPS Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lecturers absent other reasons',
            data: [
                   "<?php echo $chemisotherstoday; ?>", 
                   "<?php echo $geootherstoday; ?>", 
                   "<?php echo $geologotherstoday; ?>", 
                   "<?php echo $mathotherstoday; ?>", 
                   "<?php echo $phyotherstoday; ?>", 
                   "<?php echo $statotherstoday; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            data: [
                   "<?php echo $chemisotherstoday; ?>", 
                   "<?php echo $geootherstoday; ?>", 
                   "<?php echo $geologotherstoday; ?>", 
                   "<?php echo $mathotherstoday; ?>", 
                   "<?php echo $phyotherstoday; ?>", 
                   "<?php echo $statotherstoday; ?>"
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Others   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>OTHERS</td>
        <td><?php echo $chemisotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($chemissummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>OTHERS</td>
        <td><?php echo $geootherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>OTHERS</td>
        <td><?php echo $geologotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($geologsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>OTHERS</td>
        <td><?php echo $mathotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($mathsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>OTHERS</td>
        <td><?php echo $phyotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($physummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>OTHERS</td>
        <td><?php echo $statotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($statsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>


    
    