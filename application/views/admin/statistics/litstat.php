<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Library Information Science Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $litheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $litheld['service_no']; ?></td>

                    <td>
                        <?php echo $litheld['lecturer_fname']; ?>
                        <?php echo $litheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $litheld['faculty_name']; ?></td>
                    <td><?php echo $litheld['dept_name']; ?></td>
                    <td><?php echo $litheld['course_code']; ?></td>
                    <td><?php echo $litheld['level_name']; ?></td>
                    <td><?php echo $litheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $litheld['start_time']; ?> -  <?php echo $litheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $litabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $litabsent['service_no']; ?></td>

                    <td>
                        <?php echo $litabsent['lecturer_fname']; ?>
                        <?php echo $litabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $litabsent['faculty_name']; ?></td>
                    <td><?php echo $litabsent['dept_name']; ?></td>
                    <td><?php echo $litabsent['course_code']; ?></td>
                    <td><?php echo $litabsent['level_name']; ?></td>
                    <td><?php echo $litabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $litabsent['start_time']; ?> -  <?php echo $litabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $litpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $litpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $litpostponed['lecturer_fname']; ?>
                        <?php echo  $litpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $litpostponed['faculty_name']; ?></td>
                    <td><?php echo  $litpostponed['dept_name']; ?></td>
                    <td><?php echo  $litpostponed['course_code']; ?></td>
                    <td><?php echo  $litpostponed['level_name']; ?></td>
                    <td><?php echo  $litpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $litpostponed['start_time']; ?> -  <?php echo $litpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $litothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $litothers['service_no']; ?></td>

                    <td>
                        <?php echo $litothers['lecturer_fname']; ?>
                        <?php echo $litothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $litothers['faculty_name']; ?></td>
                    <td><?php echo $litothers['dept_name']; ?></td>
                    <td><?php echo $litothers['course_code']; ?></td>
                    <td><?php echo $litothers['level_name']; ?></td>
                    <td><?php echo $litothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $litothers['start_time']; ?> -  <?php echo $litothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

