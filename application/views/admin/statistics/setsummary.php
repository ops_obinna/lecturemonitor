
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SET Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $archeldsummary; ?>", 
                   "<?php echo $bldheldsummary; ?>", 
                   "<?php echo $estheldsummary; ?>",
                   "<?php echo $qtsheldsummary; ?>",
                   "<?php echo $svgheldsummary; ?>",
                   "<?php echo $urpheldsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $archeldsummary; ?>", 
                   "<?php echo $bldheldsummary; ?>", 
                   "<?php echo $estheldsummary; ?>",
                   "<?php echo $qtsheldsummary; ?>",
                   "<?php echo $svgheldsummary; ?>",
                   "<?php echo $urpheldsummary; ?>"
                   
                  ],
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures held for the Semester 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>HELD</td>
        <td><?php echo $archeldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>HELD</td>
        <td><?php echo $bldheldsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>HELD</td>
        <td><?php echo $estheldsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>HELD</td>
        <td><?php echo $qtsheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>HELD</td>
        <td><?php echo $svgheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>HELD</td>
        <td><?php echo $urpheldsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SET Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $arcpostponedsummary; ?>", 
                   "<?php echo $bldpostponedsummary; ?>", 
                   "<?php echo $estpostponedsummary; ?>",
                   "<?php echo $qtspostponedsummary; ?>",
                   "<?php echo $svgpostponedsummary; ?>",
                   "<?php echo $urppostponedsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcpostponedsummary; ?>", 
                   "<?php echo $bldpostponedsummary; ?>", 
                   "<?php echo $estpostponedsummary; ?>",
                   "<?php echo $qtspostponedsummary; ?>",
                   "<?php echo $svgpostponedsummary; ?>",
                   "<?php echo $urppostponedsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>POSTPONED</td>
        <td><?php echo $arcpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>POSTPONED</td>
        <td><?php echo $bldpostponedsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>POSTPONED</td>
        <td><?php echo $estpostponedsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>POSTPONED</td>
        <td><?php echo $qtspostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>POSTPONED</td>
        <td><?php echo $svgpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>POSTPONED</td>
        <td><?php echo $urppostponedsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SET Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $arcabsentsummary; ?>", 
                   "<?php echo $bldabsentsummary; ?>", 
                   "<?php echo $estabsentsummary; ?>",
                   "<?php echo $qtsabsentsummary; ?>",
                   "<?php echo $svgabsentsummary; ?>",
                   "<?php echo $urpabsentsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
        
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcabsentsummary; ?>", 
                   "<?php echo $bldabsentsummary; ?>", 
                   "<?php echo $estabsentsummary; ?>",
                   "<?php echo $qtsabsentsummary; ?>",
                   "<?php echo $svgabsentsummary; ?>",
                   "<?php echo $urpabsentsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>ABSENT</td>
        <td><?php echo $arcabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>ABSENT</td>
        <td><?php echo $bldabsentsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>ABSENT</td>
        <td><?php echo $estabsentsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>ABSENT</td>
        <td><?php echo $qtsabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>ABSENT</td>
        <td><?php echo $svgabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>ABSENT</td>
        <td><?php echo $urpabsentsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for other reasons present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SET Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $arcotherssummary; ?>", 
                   "<?php echo $bldotherssummary; ?>", 
                   "<?php echo $estotherssummary; ?>",
                   "<?php echo $qtsotherssummary; ?>",
                   "<?php echo $svgotherssummary; ?>",
                   "<?php echo $urpotherssummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcotherssummary; ?>", 
                   "<?php echo $bldotherssummary; ?>", 
                   "<?php echo $estotherssummary; ?>",
                   "<?php echo $qtsotherssummary; ?>",
                   "<?php echo $svgotherssummary; ?>",
                   "<?php echo $urpotherssummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturer Absent Other for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>OTHERS</td>
        <td><?php echo $arcotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>OTHERS</td>
        <td><?php echo $bldotherssummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>OTHERS</td>
        <td><?php echo $estotherssummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>OTHERS</td>
        <td><?php echo $qtsotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>OTHERS</td>
        <td><?php echo $svgotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>OTHERS</td>
        <td><?php echo $urpotherssummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>