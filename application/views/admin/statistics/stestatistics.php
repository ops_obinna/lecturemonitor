<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SSTE Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $itetoday; ?>", 
                   "<?php echo $sciedutoday; ?>", 
                   "<?php echo $edutechtoday; ?>",
                   "<?php echo $comedutoday; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $itetoday; ?>", 
                   "<?php echo $sciedutoday; ?>", 
                   "<?php echo $edutechtoday; ?>",
                   "<?php echo $comedutoday; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>HELD</td>
        <td><?php  echo $itetoday . " out of"; ?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($itesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>HELD</td>
        <td><?php echo $sciedutoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sciedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>HELD</td>
        <td><?php echo $edutechtoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($edutechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>HELD</td>
        <td><?php echo $comedutoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($comedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SSTE Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $itepostponedtoday; ?>", 
                   "<?php echo $sciedupostponedtoday; ?>", 
                   "<?php echo $edutechpostponedtoday; ?>",
                   "<?php echo $comedupostponedtoday; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $itepostponedtoday; ?>", 
                   "<?php echo $sciedupostponedtoday; ?>", 
                   "<?php echo $edutechpostponedtoday; ?>",
                   "<?php echo $comedupostponedtoday; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures postponed   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>POSTPONED</td>
        <td><?php  echo $itepostponedtoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($itesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>POSTPONED</td>
        <td><?php echo $sciedupostponedtoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sciedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>POSTPONED</td>
        <td><?php echo $edutechpostponedtoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($edutechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>POSTPONED</td>
        <td><?php echo $comedupostponedtoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($comedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>



<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SSTE Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: [
                   "<?php echo $iteabsenttoday; ?>", 
                   "<?php echo $scieduabsenttoday; ?>", 
                   "<?php echo $edutechabsenttoday; ?>",
                   "<?php echo $comeduabsenttoday; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $iteabsenttoday; ?>", 
                   "<?php echo $scieduabsenttoday; ?>", 
                   "<?php echo $edutechabsenttoday; ?>",
                   "<?php echo $comeduabsenttoday; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>ABSENT</td>
        <td><?php  echo $iteabsenttoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($itesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>ABSENT</td>
        <td><?php echo $scieduabsenttoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sciedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>ABSENT</td>
        <td><?php echo $edutechabsenttoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($edutechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>ABSENT</td>
        <td><?php echo $comeduabsenttoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($comedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SSTE Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>


<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lecturers absent for other reasosn',
            data: [
                   "<?php echo $iteotherstoday; ?>", 
                   "<?php echo $scieduotherstoday; ?>", 
                   "<?php echo $edutechotherstoday; ?>",
                   "<?php echo $comeduotherstoday; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $iteotherstoday; ?>", 
                   "<?php echo $scieduotherstoday; ?>", 
                   "<?php echo $edutechotherstoday; ?>",
                   "<?php echo $comeduotherstoday; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent others  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>OTHERS</td>
        <td><?php  echo $iteotherstoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($itesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>OTHERS</td>
        <td><?php echo $scieduotherstoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sciedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>OTHERS</td>
        <td><?php echo $edutechotherstoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($edutechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr> 
    <tr>
        <td>3</td>
        <td>COMMUNICATION EDU.</td>
        <td>OTHERS</td>
        <td><?php echo $comeduotherstoday . " out of"; ?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($comedusummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>


<br><br>


    
    