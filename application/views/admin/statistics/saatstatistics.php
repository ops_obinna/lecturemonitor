
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SAAT Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $aettoday; ?>", 
                   "<?php echo $apttoday; ?>", 
                   "<?php echo $crptoday; ?>",
                   "<?php echo $fsttoday; ?>",
                   "<?php echo $ssdtoday; ?>",
                   "<?php echo $afttoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
           data: ["<?php echo $aettoday; ?>", 
                   "<?php echo $apttoday; ?>", 
                   "<?php echo $crptoday; ?>",
                   "<?php echo $fsttoday; ?>",
                   "<?php echo $ssdtoday; ?>",
                   "<?php echo $afttoday; ?>"
                   
                  ],
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>HELD</td>
        <td><?php echo $aettoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>HELD</td>
        <td><?php echo $apttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>HELD</td>
        <td><?php echo $crptoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($crpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>HELD</td>
        <td><?php echo $fsttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($fstsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>HELD</td>
        <td><?php echo $ssdtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ssdsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>HELD</td>
        <td><?php echo $afttoday . " out of";?>
             <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aftsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SAAT Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
      data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $aetpostponedtoday; ?>", 
                   "<?php echo $aptpostponedtoday; ?>", 
                   "<?php echo $crppostponedtoday; ?>",
                   "<?php echo $fstpostponedtoday; ?>",
                   "<?php echo $ssdpostponedtoday; ?>",
                   "<?php echo $aftpostponedtoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $aetpostponedtoday; ?>", 
                   "<?php echo $aptpostponedtoday; ?>", 
                   "<?php echo $crppostponedtoday; ?>",
                   "<?php echo $fstpostponedtoday; ?>",
                   "<?php echo $ssdpostponedtoday; ?>",
                   "<?php echo $aftpostponedtoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>POSTPONED</td>
        <td><?php echo $aetpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>POSTPONED</td>
        <td><?php echo $aptpostponedtoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>POSTPONED</td>
        <td><?php echo $crppostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($crpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>POSTPONED</td>
        <td><?php echo $fstpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($fstsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>POSTPONED</td>
        <td><?php echo $ssdpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ssdsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>POSTPONED</td>
        <td><?php echo $aftpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aftsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SAAT Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lecturers absent',
             data: ["<?php echo $aetabsenttoday; ?>", 
                   "<?php echo $aptabsenttoday; ?>", 
                   "<?php echo $crpabsenttoday; ?>",
                   "<?php echo $fstabsenttoday; ?>",
                   "<?php echo $ssdabsenttoday; ?>",
                   "<?php echo $aftabsenttoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $aetabsenttoday; ?>", 
                   "<?php echo $aptabsenttoday; ?>", 
                   "<?php echo $crpabsenttoday; ?>",
                   "<?php echo $fstabsenttoday; ?>",
                   "<?php echo $ssdabsenttoday; ?>",
                   "<?php echo $aftabsenttoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>ABSENT</td>
        <td><?php echo $aetabsenttoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>ABSENT</td>
        <td><?php echo $aptabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>ABSENT</td>
        <td><?php echo $crpabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($crpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>ABSENT</td>
        <td><?php echo $fstabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($fstsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>ABSENT</td>
        <td><?php echo $ssdabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ssdsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>ABSENT</td>
        <td><?php echo $aftabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aftsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for other reasons present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SAAT Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
             data: [
                   "<?php echo $aetotherstoday; ?>", 
                   "<?php echo $aptotherstoday; ?>", 
                   "<?php echo $crpotherstoday; ?>",
                   "<?php echo $fstotherstoday; ?>",
                   "<?php echo $ssdotherstoday; ?>",
                   "<?php echo $aftotherstoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $aetotherstoday; ?>", 
                   "<?php echo $aptotherstoday; ?>", 
                   "<?php echo $crpotherstoday; ?>",
                   "<?php echo $fstotherstoday; ?>",
                   "<?php echo $ssdotherstoday; ?>",
                   "<?php echo $aftotherstoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Absent other  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>OTHERS</td>
        <td><?php echo $aetotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>OTHERS</td>
        <td><?php echo $aptotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>OTHERS</td>
        <td><?php echo $crpotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($crpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>OTHERS</td>
        <td><?php echo $fstotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($fstsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>OTHERS</td>
        <td><?php echo $ssdotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ssdsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>OTHERS</td>
        <td><?php echo $aftotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($aftsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br> 