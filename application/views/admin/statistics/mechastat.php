<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Mechatronics Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $mechaheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechaheld['service_no']; ?></td>

                    <td>
                        <?php echo $mechaheld['lecturer_fname']; ?>
                        <?php echo $mechaheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechaheld['faculty_name']; ?></td>
                    <td><?php echo $mechaheld['dept_name']; ?></td>
                    <td><?php echo $mechaheld['course_code']; ?></td>
                    <td><?php echo $mechaheld['level_name']; ?></td>
                    <td><?php echo $mechaheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechaheld['start_time']; ?> -  <?php echo $mechheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $mechaabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechaabsent['service_no']; ?></td>

                    <td>
                        <?php echo $mechaabsent['lecturer_fname']; ?>
                        <?php echo $mechaabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechaabsent['faculty_name']; ?></td>
                    <td><?php echo $mechaabsent['dept_name']; ?></td>
                    <td><?php echo $mechaabsent['course_code']; ?></td>
                    <td><?php echo $mechaabsent['level_name']; ?></td>
                    <td><?php echo $mechaabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechaabsent['start_time']; ?> -  <?php echo $mechaabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $mechapostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $mechapostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $mechapostponed['lecturer_fname']; ?>
                        <?php echo  $mechapostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $mechapostponed['faculty_name']; ?></td>
                    <td><?php echo  $mechapostponed['dept_name']; ?></td>
                    <td><?php echo  $mechapostponed['course_code']; ?></td>
                    <td><?php echo  $mechapostponed['level_name']; ?></td>
                    <td><?php echo  $mechapostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $mechapostponed['start_time']; ?> -  <?php echo $mechapostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $mechaothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechaothers['service_no']; ?></td>

                    <td>
                        <?php echo $mechaothers['lecturer_fname']; ?>
                        <?php echo $mechaothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechaothers['faculty_name']; ?></td>
                    <td><?php echo $mechaothers['dept_name']; ?></td>
                    <td><?php echo $mechaothers['course_code']; ?></td>
                    <td><?php echo $mechaothers['level_name']; ?></td>
                    <td><?php echo $mechaothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechaothers['start_time']; ?> -  <?php echo $mechaothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

