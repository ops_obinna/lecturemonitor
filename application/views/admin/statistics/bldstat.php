<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Building Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $bldheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bldheld['service_no']; ?></td>

                    <td>
                        <?php echo $bldheld['lecturer_fname']; ?>
                        <?php echo $bldheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bldheld['faculty_name']; ?></td>
                    <td><?php echo $bldheld['dept_name']; ?></td>
                    <td><?php echo $bldheld['course_code']; ?></td>
                    <td><?php echo $bldheld['level_name']; ?></td>
                    <td><?php echo $bldheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $bldheld['start_time']; ?> -  <?php echo $bldheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $bldabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bldabsent['service_no']; ?></td>

                    <td>
                        <?php echo $bldabsent['lecturer_fname']; ?>
                        <?php echo $bldabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bldabsent['faculty_name']; ?></td>
                    <td><?php echo $bldabsent['dept_name']; ?></td>
                    <td><?php echo $bldabsent['course_code']; ?></td>
                    <td><?php echo $bldabsent['level_name']; ?></td>
                    <td><?php echo $bldabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $bldabsent['start_time']; ?> -  <?php echo $bldabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $bldpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $bldpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $bldpostponed['lecturer_fname']; ?>
                        <?php echo  $bldpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $bldpostponed['faculty_name']; ?></td>
                    <td><?php echo  $bldpostponed['dept_name']; ?></td>
                    <td><?php echo  $bldpostponed['course_code']; ?></td>
                    <td><?php echo  $bldpostponed['level_name']; ?></td>
                    <td><?php echo  $bldpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $bldpostponed['start_time']; ?> -  <?php echo $bldpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $bldothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bldothers['service_no']; ?></td>

                    <td>
                        <?php echo $bldothers['lecturer_fname']; ?>
                        <?php echo $bldothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bldothers['faculty_name']; ?></td>
                    <td><?php echo $bldothers['dept_name']; ?></td>
                    <td><?php echo $bldothers['course_code']; ?></td>
                    <td><?php echo $bldothers['level_name']; ?></td>
                    <td><?php echo $bldothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $bldothers['start_time']; ?> -  <?php echo $bldothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

