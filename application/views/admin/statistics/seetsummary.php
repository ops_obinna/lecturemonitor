<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SEET Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $abeheldsummary; ?>", 
                   "<?php echo $electheldsummary; ?>", 
                   "<?php echo $mechheldsummary; ?>", 
                   "<?php echo $mechtroheldsummary; ?>", 
                   "<?php echo $compheldsummary; ?>", 
                   "<?php echo $telecomheldsummary; ?>", 
                   "<?php echo $metallurgheldsummary; ?>",
                   "<?php echo $civilheldsummary; ?>", 
                   "<?php echo $chemheldsummary; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.",
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            data: [
                   "<?php echo $abeheldsummary; ?>", 
                   "<?php echo $electheldsummary; ?>", 
                   "<?php echo $mechheldsummary; ?>", 
                   "<?php echo $mechtroheldsummary; ?>", 
                   "<?php echo $compheldsummary; ?>", 
                   "<?php echo $telecomheldsummary; ?>", 
                   "<?php echo $metallurgheldsummary; ?>",
                   "<?php echo $civilheldsummary; ?>", 
                   "<?php echo $chemheldsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Summary Lectures Held For the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>HELD</td>
        <td>
            <?php echo $abeheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>HELD</td>
        <td><?php echo $electheldsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>HELD</td>
        <td><?php echo $mechheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>HELD</td>
        <td><?php echo $mechtroheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>HELD</td>
        <td><?php echo $compheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>HELD</td>
        <td><?php echo $telecomheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>HELD</td>
        <td><?php echo $metallurgheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>HELD</td>
        <td><?php echo $civilheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>HELD</td>
        <td><?php echo $chemheldsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SEET Departments and Total Number of lectures Postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $abepostponedsummary; ?>", 
                   "<?php echo $electpostponedsummary; ?>", 
                   "<?php echo $mechpostponedsummary; ?>", 
                   "<?php echo $mechtropostponedsummary; ?>", 
                   "<?php echo $comppostponedsummary; ?>", 
                   "<?php echo $telecompostponedsummary; ?>", 
                   "<?php echo $metallurgpostponedsummary; ?>",
                   "<?php echo $civilpostponedsummary; ?>", 
                   "<?php echo $chempostponedsummary; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
           data: [
                   "<?php echo $abepostponedsummary; ?>", 
                   "<?php echo $electpostponedsummary; ?>", 
                   "<?php echo $mechpostponedsummary; ?>", 
                   "<?php echo $mechtropostponedsummary; ?>", 
                   "<?php echo $comppostponedsummary; ?>", 
                   "<?php echo $telecompostponedsummary; ?>", 
                   "<?php echo $metallurgpostponedsummary; ?>",
                   "<?php echo $civilpostponedsummary; ?>", 
                   "<?php echo $chempostponedsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed For The Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $abepostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>HELD</td>
        <td><?php echo $electpostponedsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $mechpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>POSTPONED</td>
        <td><?php echo $mechtropostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $comppostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $telecompostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $metallurgpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $civilpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $chempostponedsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers absent for today -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SEET Departments and lecturers Absent for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: [
                   "<?php echo $abeabsentsummary; ?>", 
                   "<?php echo $electabsentsummary; ?>", 
                   "<?php echo $mechabsentsummary; ?>", 
                   "<?php echo $mechtroabsentsummary; ?>", 
                   "<?php echo $compabsentsummary; ?>", 
                   "<?php echo $telecomabsentsummary; ?>", 
                   "<?php echo $metallurgabsentsummary; ?>",
                   "<?php echo $civilabsentsummary; ?>", 
                   "<?php echo $chemabsentsummary; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETcanceled" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETcanceled");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
          data: [
                   "<?php echo $abeabsentsummary; ?>", 
                   "<?php echo $electabsentsummary; ?>", 
                   "<?php echo $mechabsentsummary; ?>", 
                   "<?php echo $mechtroabsentsummary; ?>", 
                   "<?php echo $compabsentsummary; ?>", 
                   "<?php echo $telecomabsentsummary; ?>", 
                   "<?php echo $metallurgabsentsummary; ?>",
                   "<?php echo $civilabsentsummary; ?>", 
                   "<?php echo $chemabsentsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent For the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $abeabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>ABSENT</td>
        <td><?php echo $electabsentsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $mechabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>ABSENT</td>
        <td><?php echo $mechtroabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $compabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $telecomabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $metallurgabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $civilabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $chemabsentsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<!--Script displaying statistices for lecturers absent for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SEET Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $abeotherssummary; ?>", 
                   "<?php echo $electotherssummary; ?>", 
                   "<?php echo $mechotherssummary; ?>", 
                   "<?php echo $mechtrootherssummary; ?>", 
                   "<?php echo $compotherssummary; ?>", 
                   "<?php echo $telecomotherssummary; ?>", 
                   "<?php echo $metallurgotherssummary; ?>",
                   "<?php echo $civilotherssummary; ?>", 
                   "<?php echo $chemotherssummary; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],

            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.",
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            data: ["<?php echo $abeotherssummary; ?>", 
                   "<?php echo $electotherssummary; ?>", 
                   "<?php echo $mechotherssummary; ?>", 
                   "<?php echo $mechtrootherssummary; ?>", 
                   "<?php echo $compotherssummary; ?>", 
                   "<?php echo $telecomotherssummary; ?>", 
                   "<?php echo $metallurgotherssummary; ?>",
                   "<?php echo $civilotherssummary; ?>", 
                   "<?php echo $chemotherssummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Absent for Other Reasons for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $abeotherssummary;?>

        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>OTHERS</td>
        <td><?php echo $electotherssummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $mechotherssummary;?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>OTHERS</td>
        <td><?php echo $mechtrootherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $compotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $telecomotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $metallurgotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $civilotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $chemotherssummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>
