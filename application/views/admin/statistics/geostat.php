
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Geography Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $geoheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geoheld['service_no']; ?></td>

                    <td>
                        <?php echo $geoheld['lecturer_fname']; ?>
                        <?php echo $geoheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geoheld['faculty_name']; ?></td>
                    <td><?php echo $geoheld['dept_name']; ?></td>
                    <td><?php echo $geoheld['course_code']; ?></td>
                    <td><?php echo $geoheld['level_name']; ?></td>
                    <td><?php echo $geoheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $geoheld['start_time']; ?> -  
                        <?php echo $geoheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $geoabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geoabsent['service_no']; ?></td>

                    <td>
                        <?php echo $geoabsent['lecturer_fname']; ?>
                        <?php echo $geoabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geoabsent['faculty_name']; ?></td>
                    <td><?php echo $geoabsent['dept_name']; ?></td>
                    <td><?php echo $geoabsent['course_code']; ?></td>
                    <td><?php echo $geoabsent['level_name']; ?></td>
                    <td><?php echo $geoabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $geoabsent['start_time']; ?> - 
                        <?php echo $geoabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $geopostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $geopostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $geopostponed['lecturer_fname']; ?>
                        <?php echo  $geopostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $geopostponed['faculty_name']; ?></td>
                    <td><?php echo  $geopostponed['dept_name']; ?></td>
                    <td><?php echo  $geopostponed['course_code']; ?></td>
                    <td><?php echo  $geopostponed['level_name']; ?></td>
                    <td><?php echo  $geopostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $geopostponed['start_time']; ?> -  
                        <?php echo  $geopostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $geoothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geoothers['service_no']; ?></td>

                    <td>
                        <?php echo $geoothers['lecturer_fname']; ?>
                        <?php echo $geoothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geoothers['faculty_name']; ?></td>
                    <td><?php echo $geoothers['dept_name']; ?></td>
                    <td><?php echo $geoothers['course_code']; ?></td>
                    <td><?php echo $geoothers['level_name']; ?></td>
                    <td><?php echo $geoothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $geoothers['start_time']; ?> - 
                        <?php echo $geoothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

