<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Architectural Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $archeld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $archeld['service_no']; ?></td>

                    <td>
                        <?php echo $archeld['lecturer_fname']; ?>
                        <?php echo $archeld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $archeld['faculty_name']; ?></td>
                    <td><?php echo $archeld['dept_name']; ?></td>
                    <td><?php echo $archeld['course_code']; ?></td>
                    <td><?php echo $archeld['level_name']; ?></td>
                    <td><?php echo $archeld['lecture_status']; ?></td>
                    <td>
                        <?php echo $archeld['start_time']; ?> -  
                        <?php echo $archeld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $arcabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $arcabsent['service_no']; ?></td>

                    <td>
                        <?php echo $arcabsent['lecturer_fname']; ?>
                        <?php echo $arcabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $arcabsent['faculty_name']; ?></td>
                    <td><?php echo $arcabsent['dept_name']; ?></td>
                    <td><?php echo $arcabsent['course_code']; ?></td>
                    <td><?php echo $arcabsent['level_name']; ?></td>
                    <td><?php echo $arcabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $arcabsent['start_time']; ?> - 
                        <?php echo $arcabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $arcpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $arcpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $arcpostponed['lecturer_fname']; ?>
                        <?php echo  $arcpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $arcpostponed['faculty_name']; ?></td>
                    <td><?php echo  $arcpostponed['dept_name']; ?></td>
                    <td><?php echo  $arcpostponed['course_code']; ?></td>
                    <td><?php echo  $arcpostponed['level_name']; ?></td>
                    <td><?php echo  $arcpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $arcpostponed['start_time']; ?> - 
                        <?php echo $arcpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $arcothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $arcothers['service_no']; ?></td>

                    <td>
                        <?php echo $arcothers['lecturer_fname']; ?>
                        <?php echo $arcothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $arcothers['faculty_name']; ?></td>
                    <td><?php echo $arcothers['dept_name']; ?></td>
                    <td><?php echo $arcothers['course_code']; ?></td>
                    <td><?php echo $arcothers['level_name']; ?></td>
                    <td><?php echo $arcothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $arcothers['start_time']; ?> -  
                        <?php echo $arcothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

