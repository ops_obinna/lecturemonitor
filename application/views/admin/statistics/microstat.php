<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Microbiology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $microbioheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $microbioheld['service_no']; ?></td>

                    <td>
                        <?php echo $microbioheld['lecturer_fname']; ?>
                        <?php echo $microbioheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $microbioheld['faculty_name']; ?></td>
                    <td><?php echo $microbioheld['dept_name']; ?></td>
                    <td><?php echo $microbioheld['course_code']; ?></td>
                    <td><?php echo $microbioheld['level_name']; ?></td>
                    <td><?php echo $microbioheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $microbioheld['start_time']; ?> -  <?php echo $microbioheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $microbioabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $microbioabsent['service_no']; ?></td>

                    <td>
                        <?php echo $microbioabsent['lecturer_fname']; ?>
                        <?php echo $microbioabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $microbioabsent['faculty_name']; ?></td>
                    <td><?php echo $microbioabsent['dept_name']; ?></td>
                    <td><?php echo $microbioabsent['course_code']; ?></td>
                    <td><?php echo $microbioabsent['level_name']; ?></td>
                    <td><?php echo $microbioabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $microbioabsent['start_time']; ?> -  <?php echo $microbioabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $microbiopostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $microbiopostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $microbiopostponed['lecturer_fname']; ?>
                        <?php echo  $microbiopostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $microbiopostponed['faculty_name']; ?></td>
                    <td><?php echo  $microbiopostponed['dept_name']; ?></td>
                    <td><?php echo  $microbiopostponed['course_code']; ?></td>
                    <td><?php echo  $microbiopostponed['level_name']; ?></td>
                    <td><?php echo  $microbiopostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $microbiopostponed['start_time']; ?> -  <?php echo $microbiopostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $microbioothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $microbioothers['service_no']; ?></td>

                    <td>
                        <?php echo $microbioothers['lecturer_fname']; ?>
                        <?php echo $microbioothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $microbioothers['faculty_name']; ?></td>
                    <td><?php echo $microbioothers['dept_name']; ?></td>
                    <td><?php echo $microbioothers['course_code']; ?></td>
                    <td><?php echo $microbioothers['level_name']; ?></td>
                    <td><?php echo $microbioothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $microbioothers['start_time']; ?> -  <?php echo $microbioothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

