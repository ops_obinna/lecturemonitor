
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Geology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $geologheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geologheld['service_no']; ?></td>

                    <td>
                        <?php echo $geologheld['lecturer_fname']; ?>
                        <?php echo $geologheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geologheld['faculty_name']; ?></td>
                    <td><?php echo $geologheld['dept_name']; ?></td>
                    <td><?php echo $geologheld['course_code']; ?></td>
                    <td><?php echo $geologheld['level_name']; ?></td>
                    <td><?php echo $geologheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $geologheld['start_time']; ?> -  
                        <?php echo $geologheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $geologabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geologabsent['service_no']; ?></td>

                    <td>
                        <?php echo $geologabsent['lecturer_fname']; ?>
                        <?php echo $geologabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geologabsent['faculty_name']; ?></td>
                    <td><?php echo $geologabsent['dept_name']; ?></td>
                    <td><?php echo $geologabsent['course_code']; ?></td>
                    <td><?php echo $geologabsent['level_name']; ?></td>
                    <td><?php echo $geologabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $geologabsent['start_time']; ?> - 
                        <?php echo $geologabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $geologpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $geologpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $geologpostponed['lecturer_fname']; ?>
                        <?php echo  $geologpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $geologpostponed['faculty_name']; ?></td>
                    <td><?php echo  $geologpostponed['dept_name']; ?></td>
                    <td><?php echo  $geologpostponed['course_code']; ?></td>
                    <td><?php echo  $geologpostponed['level_name']; ?></td>
                    <td><?php echo  $geologpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $geologpostponed['start_time']; ?> -  
                        <?php echo  $geologpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $geologothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $geologothers['service_no']; ?></td>

                    <td>
                        <?php echo $geologothers['lecturer_fname']; ?>
                        <?php echo $geologothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $geologothers['faculty_name']; ?></td>
                    <td><?php echo $geologothers['dept_name']; ?></td>
                    <td><?php echo $geologothers['course_code']; ?></td>
                    <td><?php echo $geologothers['level_name']; ?></td>
                    <td><?php echo $geologothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $geologothers['start_time']; ?> - 
                        <?php echo $geologothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

