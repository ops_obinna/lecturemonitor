
<!--This is the View for  the Dashboard that display daily Ongoing Lectures around the University*/ -->

		<h3>
			<span class="label label-primary">Active Lecturers This Semester</span>
			
		</h3>
		<br />
<?php if($lecturersummary) : ?>
<!--		<div id="lectures"></div>-->
		<table class="table table-bordered datatable table-hover" id="table-4">
			<thead>
				<tr class="warning">
			        <th>ID</th>
					<th>First Name</th>
					<th>Surname</th>
					<th>Service No</th>
					<th>Dept_ID</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Actions</th>           
                                      		
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecturersummary as $lecturer_list) : ?>
				   
				<tr class="odd gradeX">
				    <td><?php echo $lecturer_list->lecturer_id; ?></td>
					
					<td><?php echo $lecturer_list->lecturer_fname; ?></td>
					<td><?php echo $lecturer_list->lecturer_sname; ?></td>
					<td class="center"><?php echo $lecturer_list->service_no; ?></td>
					<td class="center"><?php echo $lecturer_list->dept_name; ?></td>
					<td class="center"><?php echo $lecturer_list->phone; ?></td>
					<td class="center"><?php echo $lecturer_list->email_address; ?></td>
					<td>
                        
						<div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT LECTURERS LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/spsdept_controller/geo_lecturersstat/' .$lecturer_list->lecturer_id.'','Semesterial Lecture Statistics','class="entypo-chart-bar"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE PROFILE LINK -->
                                       
                                        
                                    </ul>
                                </div>                   
                        
					</td>
                    
                    
                </tr>	

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
        <?php else : ?>
	<p> No Lecturer Registered For Now </p>
	<?php endif; ?>
	
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			}                   
            
            );
				
		</script>