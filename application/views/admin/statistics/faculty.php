
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing Faculties and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">

<!--javascript -->
    
<canvas id="myBarChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of  lectures held by faculty',
            data: ["<?php echo $sict; ?>", 
                   "<?php echo $seet; ?>", 
                   "<?php echo $set; ?>", 
                   "<?php echo $semt; ?>", 
                   "<?php echo $saat; ?>", 
                   "<?php echo $sps; ?>",
                   "<?php echo $ste; ?>",
                   "<?php echo $sls; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">


    <!--javascript -->
    
<canvas id="myPieChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sict; ?>", 
                   "<?php echo $seet; ?>", 
                   "<?php echo $set; ?>", 
                   "<?php echo $semt; ?>", 
                   "<?php echo $saat; ?>", 
                   "<?php echo $sps; ?>",
                   "<?php echo $ste; ?>",
                   "<?php echo $sls; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
 
 
 <div class="col-md-4">
    
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary HELD Lectures  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>HELD</td>
        <td>
            <?php echo $sict . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sictsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>HELD</td>
        <td>
            <?php echo $seet . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($seetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>HELD</td>
        <td>
            
            <?php echo $semt . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($semtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>HELD</td>
        <td>
            <?php echo $saat . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($saatsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>HELD</td>
        <td>
        
            <?php echo $set . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($setsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        <td>
    </tr>
     <tr>
        <td>6</td>
        <td>SPS</td>
        <td>HELD</td>
        <td>
         
            <?php echo $sps . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($spssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
     <tr>
        <td>7</td>
        <td>SLS</td>
        <td>HELD</td>
        <td><?php echo $sls . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($slssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?></td>
    </tr>
     <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>HELD</td>
        <td>
         
         <?php echo $ste . " out of";?> 
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($stesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
         </td>
    </tr>
  
</table>
    
    
</div>
</div>

<br><br>
<!--Script Displaying Statistices for postponed Lectures for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing Faculties  and Total Number of postponed lectures - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartPostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartPostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lectures postponed by faculty',
            data: ["<?php echo $sictpostponed; ?>", 
                   "<?php echo $seetpostponed; ?>", 
                   "<?php echo $setpostponed; ?>", 
                   "<?php echo $semtpostponed; ?>", 
                   "<?php echo $saatpostponed; ?>", 
                   "<?php echo $spspostponed; ?>",
                   "<?php echo $stepostponed; ?>",
                   "<?php echo $slspostponed; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictpostponed; ?>", 
                   "<?php echo $seetpostponed; ?>", 
                   "<?php echo $setpostponed; ?>", 
                   "<?php echo $semtpostponed; ?>", 
                   "<?php echo $saatpostponed; ?>", 
                   "<?php echo $spspostponed; ?>",
                   "<?php echo $stepostponed; ?>",
                   "<?php echo $slspostponed; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary  Postponed Lectures  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $sictpostponed . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sictsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $seetpostponed . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($seetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $semtpostponed . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($semtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
            
        
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $saatpostponed  . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($saatsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $setpostponed . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($setsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $spspostponed . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($spssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $slspostponed . " out of";;?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($slssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $stepostponed . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($stesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
  
</table>
    </div>
</div>


<br><br>
<!--Script Displaying Statistices for Absent Lecturers for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (3) Showing Faculties and Total Number of Absent lecturers for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lecturers Absent by faculty',
            data: ["<?php echo $sictabsent; ?>", 
                   "<?php echo $seetabsent; ?>", 
                   "<?php echo $setabsent; ?>", 
                   "<?php echo $semtabsent; ?>", 
                   "<?php echo $saatabsent; ?>", 
                   "<?php echo $spsabsent; ?>",
                   "<?php echo $steabsent; ?>",
                   "<?php echo $slsabsent; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictabsent; ?>", 
                   "<?php echo $seetabsent; ?>", 
                   "<?php echo $setabsent; ?>", 
                   "<?php echo $semtabsent; ?>", 
                   "<?php echo $saatabsent; ?>", 
                   "<?php echo $spsabsent; ?>",
                   "<?php echo $steabsent; ?>",
                   "<?php echo $slsabsent; ?>"
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary for Absent Lecturers  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $sictabsent . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sictsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>ABSENT</td>
        <td>
            
            <?php echo $seetabsent . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($seetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $semtabsent . " out of";?>
             <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($semtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $saatabsent . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($saatsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>ABSENT</td>
        <td>
            <?php echo $setabsent . " out of";;?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($setsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>ABSENT</td>
        <td>
            <?php echo $spsabsent . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($spssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>ABSENT</td>
        <td>
            <?php echo $slsabsent . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($slssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>ABSENT</td>
        <td>
            <?php echo $steabsent . " out of";;?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($stesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
  
</table>
    </div>
</div>

<br><br>
<!--Script Displaying Statistices for Absent Lecturers for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing Faculties and Total Number of Lecturers Absent for Other reasons  - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lecturers Absent for other reasons',
            data: ["<?php echo $sictothers; ?>", 
                   "<?php echo $seetothers; ?>", 
                   "<?php echo $setothers; ?>", 
                   "<?php echo $semtothers; ?>", 
                   "<?php echo $saatothers; ?>", 
                   "<?php echo $spsothers; ?>",
                   "<?php echo $steothers; ?>",
                   "<?php echo $slsothers; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictothers; ?>", 
                   "<?php echo $seetothers; ?>", 
                   "<?php echo $setothers; ?>", 
                   "<?php echo $semtothers; ?>", 
                   "<?php echo $saatothers; ?>", 
                   "<?php echo $spsothers; ?>",
                   "<?php echo $steothers; ?>",
                   "<?php echo $slsothers; ?>"
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
        <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary for Other Reasons  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>OTHERS</td>
        <td>
            <?php echo $sictothers . " out of";?>
             <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($sictsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>OTHERS</td>
        <td><?php echo $seetothers . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($seetsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>OTHERS</td>
        <td><?php echo $semtothers . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($semtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>OTHERS</td>
        <td><?php echo $saatothers . " out of";?>
             <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($saatsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>OTHERS</td>
        <td><?php echo $setothers . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($setsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>OTHERS</td>
        <td>
            <?php echo $spsothers . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($spssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 

        
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>OTHERS</td>
        <td><?php echo $slsothers . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($slssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>OTHERS</td>
        <td><?php echo $steothers . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($stesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
    </div>
</div>
<br><br>


    

    




