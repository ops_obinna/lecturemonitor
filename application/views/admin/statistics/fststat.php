<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Food Science and Nutrition Technology  Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $fstheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $fstheld['service_no']; ?></td>

                    <td>
                        <?php echo $fstheld['lecturer_fname']; ?>
                        <?php echo $fstheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $fstheld['faculty_name']; ?></td>
                    <td><?php echo $fstheld['dept_name']; ?></td>
                    <td><?php echo $fstheld['course_code']; ?></td>
                    <td><?php echo $fstheld['level_name']; ?></td>
                    <td><?php echo $fstheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $fstheld['start_time']; ?> -  <?php echo $fstheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $fstabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $fstabsent['service_no']; ?></td>

                    <td>
                        <?php echo $fstabsent['lecturer_fname']; ?>
                        <?php echo $fstabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $fstabsent['faculty_name']; ?></td>
                    <td><?php echo $fstabsent['dept_name']; ?></td>
                    <td><?php echo $fstabsent['course_code']; ?></td>
                    <td><?php echo $fstabsent['level_name']; ?></td>
                    <td><?php echo $fstabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $fstabsent['start_time']; ?> -  <?php echo $fstabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $fstpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $fstpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $fstpostponed['lecturer_fname']; ?>
                        <?php echo  $fstpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $fstpostponed['faculty_name']; ?></td>
                    <td><?php echo  $fstpostponed['dept_name']; ?></td>
                    <td><?php echo  $fstpostponed['course_code']; ?></td>
                    <td><?php echo  $fstpostponed['level_name']; ?></td>
                    <td><?php echo  $fstpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $fstpostponed['start_time']; ?> -  <?php echo $fstpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $fstothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $fstothers['service_no']; ?></td>

                    <td>
                        <?php echo $fstothers['lecturer_fname']; ?>
                        <?php echo $fstothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $fstothers['faculty_name']; ?></td>
                    <td><?php echo $fstothers['dept_name']; ?></td>
                    <td><?php echo $fstothers['course_code']; ?></td>
                    <td><?php echo $fstothers['level_name']; ?></td>
                    <td><?php echo $fstothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $fstothers['start_time']; ?> -  <?php echo $fstothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

