<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Transport Management Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $tmtheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $tmtheld['service_no']; ?></td>

                    <td>
                        <?php echo $tmtheld['lecturer_fname']; ?>
                        <?php echo $tmtheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $tmtheld['faculty_name']; ?></td>
                    <td><?php echo $tmtheld['dept_name']; ?></td>
                    <td><?php echo $tmtheld['course_code']; ?></td>
                    <td><?php echo $tmtheld['level_name']; ?></td>
                    <td><?php echo $tmtheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $tmtheld['start_time']; ?> -  <?php echo $tmtheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $tmtabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $tmtabsent['service_no']; ?></td>

                    <td>
                        <?php echo $tmtabsent['lecturer_fname']; ?>
                        <?php echo $tmtabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $tmtabsent['faculty_name']; ?></td>
                    <td><?php echo $tmtabsent['dept_name']; ?></td>
                    <td><?php echo $tmtabsent['course_code']; ?></td>
                    <td><?php echo $tmtabsent['level_name']; ?></td>
                    <td><?php echo $tmtabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $tmtabsent['start_time']; ?> -  <?php echo $tmtabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $tmtpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $tmtpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $tmtpostponed['lecturer_fname']; ?>
                        <?php echo  $tmtpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $tmtpostponed['faculty_name']; ?></td>
                    <td><?php echo  $tmtpostponed['dept_name']; ?></td>
                    <td><?php echo  $tmtpostponed['course_code']; ?></td>
                    <td><?php echo  $tmtpostponed['level_name']; ?></td>
                    <td><?php echo  $tmtpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $tmtpostponed['start_time']; ?> -  <?php echo $tmtpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $tmtothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $tmtothers['service_no']; ?></td>

                    <td>
                        <?php echo $tmtothers['lecturer_fname']; ?>
                        <?php echo $tmtothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $tmtothers['faculty_name']; ?></td>
                    <td><?php echo $tmtothers['dept_name']; ?></td>
                    <td><?php echo $tmtothers['course_code']; ?></td>
                    <td><?php echo $tmtothers['level_name']; ?></td>
                    <td><?php echo $tmtothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $tmtothers['start_time']; ?> -  <?php echo $tmtothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

