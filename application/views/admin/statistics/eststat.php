<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em">Estate Management and valuation Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $estheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $estheld['service_no']; ?></td>

                    <td>
                        <?php echo $estheld['lecturer_fname']; ?>
                        <?php echo $estheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $estheld['faculty_name']; ?></td>
                    <td><?php echo $estheld['dept_name']; ?></td>
                    <td><?php echo $estheld['course_code']; ?></td>
                    <td><?php echo $estheld['level_name']; ?></td>
                    <td><?php echo $estheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $estheld['start_time']; ?> -  <?php echo $estheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $estabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $estabsent['service_no']; ?></td>

                    <td>
                        <?php echo $estabsent['lecturer_fname']; ?>
                        <?php echo $estabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $estabsent['faculty_name']; ?></td>
                    <td><?php echo $estabsent['dept_name']; ?></td>
                    <td><?php echo $estabsent['course_code']; ?></td>
                    <td><?php echo $estabsent['level_name']; ?></td>
                    <td><?php echo $estabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $estabsent['start_time']; ?> -  <?php echo $estabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $estpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $estpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $estpostponed['lecturer_fname']; ?>
                        <?php echo  $estpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $estpostponed['faculty_name']; ?></td>
                    <td><?php echo  $estpostponed['dept_name']; ?></td>
                    <td><?php echo  $estpostponed['course_code']; ?></td>
                    <td><?php echo  $estpostponed['level_name']; ?></td>
                    <td><?php echo  $estpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $estpostponed['start_time']; ?> -  <?php echo $estpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $estothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $estothers['service_no']; ?></td>

                    <td>
                        <?php echo $estothers['lecturer_fname']; ?>
                        <?php echo $estothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $estothers['faculty_name']; ?></td>
                    <td><?php echo $estothers['dept_name']; ?></td>
                    <td><?php echo $estothers['course_code']; ?></td>
                    <td><?php echo $estothers['level_name']; ?></td>
                    <td><?php echo $estothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $estothers['start_time']; ?> -  <?php echo $estothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

