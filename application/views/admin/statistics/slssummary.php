
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SLS Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $bioheldsummary; ?>", 
                   "<?php echo $biochemheldsummary; ?>", 
                   "<?php echo $microbioheldsummary; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $bioheldsummary; ?>", 
                   "<?php echo $biochemheldsummary; ?>", 
                   "<?php echo $microbioheldsummary; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Summary
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>HELD</td>
        <td><?php echo $bioheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>HELD</td>
        <td><?php echo $biochemheldsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>HELD</td>
        <td><?php echo $microbioheldsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SLS Departments and Total Number of lectures postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $biopostponedsummary; ?>", 
                   "<?php echo $biochempostponedsummary; ?>", 
                   "<?php echo $microbiopostponedsummary; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $biopostponedsummary; ?>", 
                   "<?php echo $biochempostponedsummary; ?>", 
                   "<?php echo $microbiopostponedsummary; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures postponed for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>POSTPONED</td>
        <td><?php echo $biopostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>POSTPONED</td>
        <td><?php echo $biochempostponedsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>POSTPONED</td>
        <td><?php echo $microbiopostponedsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>



<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SLS Departments and Total Number of lecturers Absent for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lecturers Absent by department',
            data: [
                   "<?php echo $bioabsentsummary; ?>", 
                   "<?php echo $biochemabsentsummary; ?>", 
                   "<?php echo $microbioabsentsummary; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
               data: [
                   "<?php echo $bioabsentsummary; ?>", 
                   "<?php echo $biochemabsentsummary; ?>", 
                   "<?php echo $microbioabsentsummary; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures absent for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>ABSENT</td>
        <td><?php echo $bioabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>ABSENT</td>
        <td><?php echo $biochemabsentsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>ABSENT</td>
        <td><?php echo $microbioabsentsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SLS Departments and Total Number of lecturers Absent for other reasons for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>


<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lecturers Absent for other reasons',
            data: [
                   "<?php echo $biootherssummary; ?>", 
                   "<?php echo $biochemotherssummary; ?>", 
                   "<?php echo $microbiootherssummary; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $biootherssummary; ?>", 
                   "<?php echo $biochemotherssummary; ?>", 
                   "<?php echo $microbiootherssummary; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent others for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>OTHERS</td>
        <td><?php echo $biootherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>OTHERS</td>
        <td><?php echo $biochemotherssummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>OTHERS</td>
        <td><?php echo $microbiootherssummary;?>
        </td>
    </tr>  
</table>
</div>
</div>


<br><br>


    
    