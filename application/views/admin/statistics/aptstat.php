<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Animal Production Technology  Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $aptheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aptheld['service_no']; ?></td>

                    <td>
                        <?php echo $aptheld['lecturer_fname']; ?>
                        <?php echo $aptheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aptheld['faculty_name']; ?></td>
                    <td><?php echo $aptheld['dept_name']; ?></td>
                    <td><?php echo $aptheld['course_code']; ?></td>
                    <td><?php echo $aptheld['level_name']; ?></td>
                    <td><?php echo $aptheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $aptheld['start_time']; ?> -  <?php echo $aptheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $aptabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aptabsent['service_no']; ?></td>

                    <td>
                        <?php echo $aptabsent['lecturer_fname']; ?>
                        <?php echo $aptabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aptabsent['faculty_name']; ?></td>
                    <td><?php echo $aptabsent['dept_name']; ?></td>
                    <td><?php echo $aptabsent['course_code']; ?></td>
                    <td><?php echo $aptabsent['level_name']; ?></td>
                    <td><?php echo $aptabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $aptabsent['start_time']; ?> -  <?php echo $aptabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $aptpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $aptpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $aptpostponed['lecturer_fname']; ?>
                        <?php echo  $aptpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $aptpostponed['faculty_name']; ?></td>
                    <td><?php echo  $aptpostponed['dept_name']; ?></td>
                    <td><?php echo  $aptpostponed['course_code']; ?></td>
                    <td><?php echo  $aptpostponed['level_name']; ?></td>
                    <td><?php echo  $aptpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $aptpostponed['start_time']; ?> -  <?php echo $aptpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $aptothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aptothers['service_no']; ?></td>

                    <td>
                        <?php echo $aptothers['lecturer_fname']; ?>
                        <?php echo $aptothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aptothers['faculty_name']; ?></td>
                    <td><?php echo $aptothers['dept_name']; ?></td>
                    <td><?php echo $aptothers['course_code']; ?></td>
                    <td><?php echo $aptothers['level_name']; ?></td>
                    <td><?php echo $aptothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $aptothers['start_time']; ?> -  <?php echo $aptothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

