
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SAAT Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $aetheldsummary; ?>", 
                   "<?php echo $aptheldsummary; ?>", 
                   "<?php echo $crpheldsummary; ?>",
                   "<?php echo $fstheldsummary; ?>",
                   "<?php echo $ssdheldsummary; ?>",
                   "<?php echo $aftheldsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
          data: ["<?php echo $aetheldsummary; ?>", 
                   "<?php echo $aptheldsummary; ?>", 
                   "<?php echo $crpheldsummary; ?>",
                   "<?php echo $fstheldsummary; ?>",
                   "<?php echo $ssdheldsummary; ?>",
                   "<?php echo $aftheldsummary; ?>"
                   
                  ],
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures held for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>HELD</td>
        <td><?php echo $aetheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>HELD</td>
        <td><?php echo $aptheldsummary;?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>HELD</td>
        <td><?php echo $crpheldsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>HELD</td>
        <td><?php echo $fstheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>HELD</td>
        <td><?php echo $ssdheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>HELD</td>
        <td><?php echo $aftheldsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SAAT Departments and Total Number of lectures postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
      data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $aetpostponedsummary; ?>", 
                   "<?php echo $aptpostponedsummary; ?>", 
                   "<?php echo $crppostponedsummary; ?>",
                   "<?php echo $fstpostponedsummary; ?>",
                   "<?php echo $ssdpostponedsummary; ?>",
                   "<?php echo $aftpostponedsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: [
                   "<?php echo $aetpostponedsummary; ?>", 
                   "<?php echo $aptpostponedsummary; ?>", 
                   "<?php echo $crppostponedsummary; ?>",
                   "<?php echo $fstpostponedsummary; ?>",
                   "<?php echo $ssdpostponedsummary; ?>",
                   "<?php echo $aftpostponedsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>POSTPONED</td>
        <td><?php echo $aetpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>POSTPONED</td>
        <td><?php echo $aptpostponedsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>POSTPONED</td>
        <td><?php echo $crppostponedsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>POSTPONED</td>
        <td><?php echo $fstpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>POSTPONED</td>
        <td><?php echo $ssdpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>POSTPONED</td>
        <td><?php echo $aftpostponedsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SAAT Departments and Total Number of lecturers Absent for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lecturers absent',
             data: ["<?php echo $aetabsentsummary; ?>", 
                   "<?php echo $aptabsentsummary; ?>", 
                   "<?php echo $crpabsentsummary; ?>",
                   "<?php echo $fstabsentsummary; ?>",
                   "<?php echo $ssdabsentsummary; ?>",
                   "<?php echo $aftabsentsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $aetabsentsummary; ?>", 
                   "<?php echo $aptabsentsummary; ?>", 
                   "<?php echo $crpabsentsummary; ?>",
                   "<?php echo $fstabsentsummary; ?>",
                   "<?php echo $ssdabsentsummary; ?>",
                   "<?php echo $aftabsentsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent for the Semester
         </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>ABSENT</td>
        <td><?php echo $aetabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>ABSENT</td>
        <td><?php echo $aptabsentsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>ABSENT</td>
        <td><?php echo $crpabsentsummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>ABSENT</td>
        <td><?php echo $fstabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>ABSENT</td>
        <td><?php echo $ssdabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>ABSENT</td>
        <td><?php echo $aftabsentsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for other reasons present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SAAT Departments and Total Number of lecturers Absent for other reasons for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSAATothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSAATothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
             data: [
                   "<?php echo $aetotherssummary; ?>", 
                   "<?php echo $aptotherssummary; ?>", 
                   "<?php echo $crpotherssummary; ?>",
                   "<?php echo $fstotherssummary; ?>",
                   "<?php echo $ssdotherssummary; ?>",
                   "<?php echo $aftotherssummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSAATothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSAATothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["AET", "APT", "CRP", "FST", "SSD", "AFT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: [
                   "<?php echo $aetotherssummary; ?>", 
                   "<?php echo $aptotherssummary; ?>", 
                   "<?php echo $crpotherssummary; ?>",
                   "<?php echo $fstotherssummary; ?>",
                   "<?php echo $ssdotherssummary; ?>",
                   "<?php echo $aftotherssummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Absent other for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>AET</td>
        <td>OTHERS</td>
        <td><?php echo $aetotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>APT</td>
        <td>OTHERS</td>
        <td><?php echo $aptotherssummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>CRP</td>
        <td>OTHERS</td>
        <td><?php echo $crpotherssummary;?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>FST</td>
        <td>OTHERS</td>
        <td><?php echo $fstotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SSD</td>
        <td>OTHERS</td>
        <td><?php echo $ssdotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>AFT</td>
        <td>OTHERS</td>
        <td><?php echo $aftotherssummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br> 