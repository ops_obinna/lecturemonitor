
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Soil Science and Land Management and Water Resources  Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $ssdheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ssdheld['service_no']; ?></td>

                    <td>
                        <?php echo $ssdheld['lecturer_fname']; ?>
                        <?php echo $ssdheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ssdheld['faculty_name']; ?></td>
                    <td><?php echo $ssdheld['dept_name']; ?></td>
                    <td><?php echo $ssdheld['course_code']; ?></td>
                    <td><?php echo $ssdheld['level_name']; ?></td>
                    <td><?php echo $ssdheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $ssdheld['start_time']; ?> -  
                        <?php echo $ssdheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $ssdabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ssdabsent['service_no']; ?></td>

                    <td>
                        <?php echo $ssdabsent['lecturer_fname']; ?>
                        <?php echo $ssdabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ssdabsent['faculty_name']; ?></td>
                    <td><?php echo $ssdabsent['dept_name']; ?></td>
                    <td><?php echo $ssdabsent['course_code']; ?></td>
                    <td><?php echo $ssdabsent['level_name']; ?></td>
                    <td><?php echo $ssdabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $ssdabsent['start_time']; ?> -  <?php echo $ssdabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $ssdpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $ssdpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $ssdpostponed['lecturer_fname']; ?>
                        <?php echo  $ssdpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $ssdpostponed['faculty_name']; ?></td>
                    <td><?php echo  $ssdpostponed['dept_name']; ?></td>
                    <td><?php echo  $ssdpostponed['course_code']; ?></td>
                    <td><?php echo  $ssdpostponed['level_name']; ?></td>
                    <td><?php echo  $ssdpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $ssdpostponed['start_time']; ?> -  <?php echo $ssdpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $ssdothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ssdothers['service_no']; ?></td>

                    <td>
                        <?php echo $ssdothers['lecturer_fname']; ?>
                        <?php echo $ssdothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ssdothers['faculty_name']; ?></td>
                    <td><?php echo $ssdothers['dept_name']; ?></td>
                    <td><?php echo $ssdothers['course_code']; ?></td>
                    <td><?php echo $ssdothers['level_name']; ?></td>
                    <td><?php echo $ssdothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $ssdothers['start_time']; ?> -  <?php echo $ssdothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

