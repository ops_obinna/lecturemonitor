
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SICT Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $imtheldsummary; ?>", 
                   "<?php echo $cptheldsummary; ?>", 
                   "<?php echo $cssheldsummary; ?>", 
                   "<?php echo $litheldsummary; ?>", 
                   
                  ],
            
             backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php  echo $imtheldsummary; ?>", 
                   "<?php echo $cptheldsummary; ?>", 
                   "<?php echo $cssheldsummary; ?>", 
                   "<?php echo $litheldsummary; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
Summary Lectures held 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>HELD</td>
        <td><?php echo $imtheldsummary;?>        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>HELD</td>
        <td><?php echo $cptheldsummary;?>        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>HELD</td>
        <td><?php echo $cssheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>HELD</td>
        <td><?php echo $litheldsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SICT Departments and Total Number of lectures postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: ["<?php echo $imtpostponedsummary; ?>", 
                   "<?php echo $cptpostponedsummary; ?>", 
                   "<?php echo $csspostponedsummary; ?>", 
                   "<?php echo $litpostponedsummary; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtpostponedsummary; ?>", 
                   "<?php echo $cptpostponedsummary; ?>", 
                   "<?php echo $csspostponedsummary; ?>", 
                   "<?php echo $litpostponedsummary; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Summary Postponed Lectures 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $imtpostponedsummary;?>        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>POSTPONED</td>
        <td><?php echo $cptpostponedsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>POSTPONED</td>
        <td><?php echo $csspostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>POSTPONED</td>
        <td><?php echo $litpostponedsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SICT Departments and Total Number of lecturers Absent for the Semester  - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: ["<?php echo $imtabsentsummary; ?>", 
                   "<?php echo $cptabsentsummary; ?>", 
                   "<?php echo $cssabsentsummary; ?>", 
                   "<?php echo $litabsentsummary; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtabsentsummary; ?>", 
                   "<?php echo $cptabsentsummary; ?>", 
                   "<?php echo $cssabsentsummary; ?>", 
                   "<?php echo $litabsentsummary; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
<div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
Summary Absent Lecturers 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>ABSENT</td>
        <td><?php echo $imtabsentsummary;?>
            
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>ABSENT</td>
        <td><?php echo $cptabsentsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>ABSENT</td>
        <td><?php echo $cssabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>ABSENT</td>
        <td><?php echo $litabsentsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>
<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SICT Departments and Total Number of lecturers Absent for other reasons For the Semester- <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $imtotherssummary; ?>", 
                   "<?php echo $cptotherssummary; ?>", 
                   "<?php echo $cssotherssummary; ?>", 
                   "<?php echo $litotherssummary; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtotherssummary; ?>", 
                   "<?php echo $cptotherssummary; ?>", 
                   "<?php echo $cssotherssummary; ?>", 
                   "<?php echo $litotherssummary; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Summary Absent Others Reasons 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>OTHERS</td>
        <td><?php echo $imtotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>OTHERS</td>
        <td><?php echo $cptotherssummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>OTHERS</td>
        <td><?php echo $cssotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>LIT</td>
        <td>OTHERS</td>
        <td><?php echo $litotherssummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>
<br><br>