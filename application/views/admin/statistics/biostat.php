<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Biological Science Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $bioheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bioheld['service_no']; ?></td>

                    <td>
                        <?php echo $bioheld['lecturer_fname']; ?>
                        <?php echo $bioheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bioheld['faculty_name']; ?></td>
                    <td><?php echo $bioheld['dept_name']; ?></td>
                    <td><?php echo $bioheld['course_code']; ?></td>
                    <td><?php echo $bioheld['level_name']; ?></td>
                    <td><?php echo $bioheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $bioheld['start_time']; ?> -  <?php echo $bioheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $bioabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bioabsent['service_no']; ?></td>

                    <td>
                        <?php echo $bioabsent['lecturer_fname']; ?>
                        <?php echo $bioabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bioabsent['faculty_name']; ?></td>
                    <td><?php echo $bioabsent['dept_name']; ?></td>
                    <td><?php echo $bioabsent['course_code']; ?></td>
                    <td><?php echo $bioabsent['level_name']; ?></td>
                    <td><?php echo $bioabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $bioabsent['start_time']; ?> -  <?php echo $bioabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $biopostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $biopostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $biopostponed['lecturer_fname']; ?>
                        <?php echo  $biopostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $biopostponed['faculty_name']; ?></td>
                    <td><?php echo  $biopostponed['dept_name']; ?></td>
                    <td><?php echo  $biopostponed['course_code']; ?></td>
                    <td><?php echo  $biopostponed['level_name']; ?></td>
                    <td><?php echo  $biopostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $biopostponed['start_time']; ?> -  <?php echo $biopostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $bioothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $bioothers['service_no']; ?></td>

                    <td>
                        <?php echo $bioothers['lecturer_fname']; ?>
                        <?php echo $bioothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $bioothers['faculty_name']; ?></td>
                    <td><?php echo $bioothers['dept_name']; ?></td>
                    <td><?php echo $bioothers['course_code']; ?></td>
                    <td><?php echo $bioothers['level_name']; ?></td>
                    <td><?php echo $bioothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $bioothers['start_time']; ?> -  <?php echo $bioothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

