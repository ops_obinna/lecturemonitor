
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Chemistry Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $chemisheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemisheld['service_no']; ?></td>

                    <td>
                        <?php echo $chemisheld['lecturer_fname']; ?>
                        <?php echo $chemisheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemisheld['faculty_name']; ?></td>
                    <td><?php echo $chemisheld['dept_name']; ?></td>
                    <td><?php echo $chemisheld['course_code']; ?></td>
                    <td><?php echo $chemisheld['level_name']; ?></td>
                    <td><?php echo $chemisheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemisheld['start_time']; ?> -  
                        <?php echo $chemisheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $chemisabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemisabsent['service_no']; ?></td>

                    <td>
                        <?php echo $chemisabsent['lecturer_fname']; ?>
                        <?php echo $chemisabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemisabsent['faculty_name']; ?></td>
                    <td><?php echo $chemisabsent['dept_name']; ?></td>
                    <td><?php echo $chemisabsent['course_code']; ?></td>
                    <td><?php echo $chemisabsent['level_name']; ?></td>
                    <td><?php echo $chemisabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemisabsent['start_time']; ?> - 
                        <?php echo $chemisabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $chemispostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $chemispostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $chemispostponed['lecturer_fname']; ?>
                        <?php echo  $chemispostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $chemispostponed['faculty_name']; ?></td>
                    <td><?php echo  $chemispostponed['dept_name']; ?></td>
                    <td><?php echo  $chemispostponed['course_code']; ?></td>
                    <td><?php echo  $chemispostponed['level_name']; ?></td>
                    <td><?php echo  $chemispostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $chemispostponed['start_time']; ?> -  
                        <?php echo  $chemispostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $chemisothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemisothers['service_no']; ?></td>

                    <td>
                        <?php echo $chemisothers['lecturer_fname']; ?>
                        <?php echo $chemisothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemisothers['faculty_name']; ?></td>
                    <td><?php echo $chemisothers['dept_name']; ?></td>
                    <td><?php echo $chemisothers['course_code']; ?></td>
                    <td><?php echo $chemisothers['level_name']; ?></td>
                    <td><?php echo $chemisothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemisothers['start_time']; ?> - 
                        <?php echo $chemisothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

