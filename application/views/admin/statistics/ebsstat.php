<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Entrepreneurship and Business Studies Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $ebsheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ebsheld['service_no']; ?></td>

                    <td>
                        <?php echo $ebsheld['lecturer_fname']; ?>
                        <?php echo $ebsheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ebsheld['faculty_name']; ?></td>
                    <td><?php echo $ebsheld['dept_name']; ?></td>
                    <td><?php echo $ebsheld['course_code']; ?></td>
                    <td><?php echo $ebsheld['level_name']; ?></td>
                    <td><?php echo $ebsheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $ebsheld['start_time']; ?> -  <?php echo $ebsheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $ebsabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ebsabsent['service_no']; ?></td>

                    <td>
                        <?php echo $ebsabsent['lecturer_fname']; ?>
                        <?php echo $ebsabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ebsabsent['faculty_name']; ?></td>
                    <td><?php echo $ebsabsent['dept_name']; ?></td>
                    <td><?php echo $ebsabsent['course_code']; ?></td>
                    <td><?php echo $ebsabsent['level_name']; ?></td>
                    <td><?php echo $ebsabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $ebsabsent['start_time']; ?> -  <?php echo $ebsabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $ebspostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $ebspostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $ebspostponed['lecturer_fname']; ?>
                        <?php echo  $ebspostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $ebspostponed['faculty_name']; ?></td>
                    <td><?php echo  $ebspostponed['dept_name']; ?></td>
                    <td><?php echo  $ebspostponed['course_code']; ?></td>
                    <td><?php echo  $ebspostponed['level_name']; ?></td>
                    <td><?php echo  $ebspostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $ebspostponed['start_time']; ?> -  <?php echo $ebspostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $ebsothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $ebsothers['service_no']; ?></td>

                    <td>
                        <?php echo $ebsothers['lecturer_fname']; ?>
                        <?php echo $ebsothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $ebsothers['faculty_name']; ?></td>
                    <td><?php echo $ebsothers['dept_name']; ?></td>
                    <td><?php echo $ebsothers['course_code']; ?></td>
                    <td><?php echo $ebsothers['level_name']; ?></td>
                    <td><?php echo $ebsothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $ebsothers['start_time']; ?> -  <?php echo $ebsothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

