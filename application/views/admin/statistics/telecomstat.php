<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Telecommunication Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $telecomheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $telecomheld['service_no']; ?></td>

                    <td>
                        <?php echo $telecomheld['lecturer_fname']; ?>
                        <?php echo $telecomheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $telecomheld['faculty_name']; ?></td>
                    <td><?php echo $telecomheld['dept_name']; ?></td>
                    <td><?php echo $telecomheld['course_code']; ?></td>
                    <td><?php echo $telecomheld['level_name']; ?></td>
                    <td><?php echo $telecomheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $telecomheld['start_time']; ?> -  <?php echo $telecomheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $telecomabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $telecomabsent['service_no']; ?></td>

                    <td>
                        <?php echo $telecomabsent['lecturer_fname']; ?>
                        <?php echo $telecomabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $telecomabsent['faculty_name']; ?></td>
                    <td><?php echo $telecomabsent['dept_name']; ?></td>
                    <td><?php echo $telecomabsent['course_code']; ?></td>
                    <td><?php echo $telecomabsent['level_name']; ?></td>
                    <td><?php echo $telecomabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $telecomabsent['start_time']; ?> -  <?php echo $telecomabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $telecompostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $telecompostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $telecompostponed['lecturer_fname']; ?>
                        <?php echo  $telecompostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $telecompostponed['faculty_name']; ?></td>
                    <td><?php echo  $telecompostponed['dept_name']; ?></td>
                    <td><?php echo  $telecompostponed['course_code']; ?></td>
                    <td><?php echo  $telecompostponed['level_name']; ?></td>
                    <td><?php echo  $telecompostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $telecompostponed['start_time']; ?> -  <?php echo $telecompostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $telecomothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $telecomothers['service_no']; ?></td>

                    <td>
                        <?php echo $telecomothers['lecturer_fname']; ?>
                        <?php echo $telecomothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $telecomothers['faculty_name']; ?></td>
                    <td><?php echo $telecomothers['dept_name']; ?></td>
                    <td><?php echo $telecomothers['course_code']; ?></td>
                    <td><?php echo $telecomothers['level_name']; ?></td>
                    <td><?php echo $telecomothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $telecomothers['start_time']; ?> -  <?php echo $telecomothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

