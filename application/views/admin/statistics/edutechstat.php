<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em">Educational Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $edutechheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $edutechheld['service_no']; ?></td>

                    <td>
                        <?php echo $edutechheld['lecturer_fname']; ?>
                        <?php echo $edutechheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $edutechheld['faculty_name']; ?></td>
                    <td><?php echo $edutechheld['dept_name']; ?></td>
                    <td><?php echo $edutechheld['course_code']; ?></td>
                    <td><?php echo $edutechheld['level_name']; ?></td>
                    <td><?php echo $edutechheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $edutechheld['start_time']; ?> -  <?php echo $edutechheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $edutechabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $edutechabsent['service_no']; ?></td>

                    <td>
                        <?php echo $edutechabsent['lecturer_fname']; ?>
                        <?php echo $edutechabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $edutechabsent['faculty_name']; ?></td>
                    <td><?php echo $edutechabsent['dept_name']; ?></td>
                    <td><?php echo $edutechabsent['course_code']; ?></td>
                    <td><?php echo $edutechabsent['level_name']; ?></td>
                    <td><?php echo $edutechabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $edutechabsent['start_time']; ?> -  <?php echo $edutechabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $edutechpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $edutechpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $edutechpostponed['lecturer_fname']; ?>
                        <?php echo  $edutechpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $edutechpostponed['faculty_name']; ?></td>
                    <td><?php echo  $edutechpostponed['dept_name']; ?></td>
                    <td><?php echo  $edutechpostponed['course_code']; ?></td>
                    <td><?php echo  $edutechpostponed['level_name']; ?></td>
                    <td><?php echo  $edutechpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $edutechpostponed['start_time']; ?> -  <?php echo $edutechpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $edutechothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $edutechothers['service_no']; ?></td>

                    <td>
                        <?php echo $edutechothers['lecturer_fname']; ?>
                        <?php echo $edutechothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $edutechothers['faculty_name']; ?></td>
                    <td><?php echo $edutechothers['dept_name']; ?></td>
                    <td><?php echo $edutechothers['course_code']; ?></td>
                    <td><?php echo $edutechothers['level_name']; ?></td>
                    <td><?php echo $edutechothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $edutechothers['start_time']; ?> -  <?php echo $edutechothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

