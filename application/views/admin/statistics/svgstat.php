<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Survey and Geoinformatics Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $svgheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $svgheld['service_no']; ?></td>

                    <td>
                        <?php echo $svgheld['lecturer_fname']; ?>
                        <?php echo $svgheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $svgheld['faculty_name']; ?></td>
                    <td><?php echo $svgheld['dept_name']; ?></td>
                    <td><?php echo $svgheld['course_code']; ?></td>
                    <td><?php echo $svgheld['level_name']; ?></td>
                    <td><?php echo $svgheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $svgheld['start_time']; ?> -  <?php echo $svgheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $svgabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $svgabsent['service_no']; ?></td>

                    <td>
                        <?php echo $svgabsent['lecturer_fname']; ?>
                        <?php echo $svgabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $svgabsent['faculty_name']; ?></td>
                    <td><?php echo $svgabsent['dept_name']; ?></td>
                    <td><?php echo $svgabsent['course_code']; ?></td>
                    <td><?php echo $svgabsent['level_name']; ?></td>
                    <td><?php echo $svgabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $svgabsent['start_time']; ?> -  <?php echo $svgabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $svgpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $svgpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $svgpostponed['lecturer_fname']; ?>
                        <?php echo  $svgpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $svgpostponed['faculty_name']; ?></td>
                    <td><?php echo  $svgpostponed['dept_name']; ?></td>
                    <td><?php echo  $svgpostponed['course_code']; ?></td>
                    <td><?php echo  $svgpostponed['level_name']; ?></td>
                    <td><?php echo  $svgpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $svgpostponed['start_time']; ?> -  <?php echo $svgpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $svgothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $svgothers['service_no']; ?></td>

                    <td>
                        <?php echo $svgothers['lecturer_fname']; ?>
                        <?php echo $svgothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $svgothers['faculty_name']; ?></td>
                    <td><?php echo $svgothers['dept_name']; ?></td>
                    <td><?php echo $svgothers['course_code']; ?></td>
                    <td><?php echo $svgothers['level_name']; ?></td>
                    <td><?php echo $svgothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $svgothers['start_time']; ?> -  <?php echo $svgothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>