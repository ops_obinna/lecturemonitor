<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Cyber Security Science Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $cssheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cssheld['service_no']; ?></td>

                    <td>
                        <?php echo $cssheld['lecturer_fname']; ?>
                        <?php echo $cssheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cssheld['faculty_name']; ?></td>
                    <td><?php echo $cssheld['dept_name']; ?></td>
                    <td><?php echo $cssheld['course_code']; ?></td>
                    <td><?php echo $cssheld['level_name']; ?></td>
                    <td><?php echo $cssheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $cssheld['start_time']; ?> -  <?php echo $cssheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $cssabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cssabsent['service_no']; ?></td>

                    <td>
                        <?php echo $cssabsent['lecturer_fname']; ?>
                        <?php echo $cssabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cssabsent['faculty_name']; ?></td>
                    <td><?php echo $cssabsent['dept_name']; ?></td>
                    <td><?php echo $cssabsent['course_code']; ?></td>
                    <td><?php echo $cssabsent['level_name']; ?></td>
                    <td><?php echo $cssabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $cssabsent['start_time']; ?> -  <?php echo $cssabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $csspostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $csspostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $csspostponed['lecturer_fname']; ?>
                        <?php echo  $csspostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $csspostponed['faculty_name']; ?></td>
                    <td><?php echo  $csspostponed['dept_name']; ?></td>
                    <td><?php echo  $csspostponed['course_code']; ?></td>
                    <td><?php echo  $csspostponed['level_name']; ?></td>
                    <td><?php echo  $csspostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $csspostponed['start_time']; ?> -  <?php echo $csspostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $cssothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cssothers['service_no']; ?></td>

                    <td>
                        <?php echo $cssothers['lecturer_fname']; ?>
                        <?php echo $cssothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cssothers['faculty_name']; ?></td>
                    <td><?php echo $cssothers['dept_name']; ?></td>
                    <td><?php echo $cssothers['course_code']; ?></td>
                    <td><?php echo $cssothers['level_name']; ?></td>
                    <td><?php echo $cssothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $cssothers['start_time']; ?> -  <?php echo $cssothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

