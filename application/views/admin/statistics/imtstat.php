<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Information and Media Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $imtheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $imtheld['service_no']; ?></td>

                    <td>
                        <?php echo $imtheld['lecturer_fname']; ?>
                        <?php echo $imtheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $imtheld['faculty_name']; ?></td>
                    <td><?php echo $imtheld['dept_name']; ?></td>
                    <td><?php echo $imtheld['course_code']; ?></td>
                    <td><?php echo $imtheld['level_name']; ?></td>
                    <td><?php echo $imtheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $imtheld['start_time']; ?> -  <?php echo $imtheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $imtabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $imtabsent['service_no']; ?></td>

                    <td>
                        <?php echo $imtabsent['lecturer_fname']; ?>
                        <?php echo $imtabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $imtabsent['faculty_name']; ?></td>
                    <td><?php echo $imtabsent['dept_name']; ?></td>
                    <td><?php echo $imtabsent['course_code']; ?></td>
                    <td><?php echo $imtabsent['level_name']; ?></td>
                    <td><?php echo $imtabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $imtabsent['start_time']; ?> -  <?php echo $imtabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $imtpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $imtpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $imtpostponed['lecturer_fname']; ?>
                        <?php echo  $imtpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $imtpostponed['faculty_name']; ?></td>
                    <td><?php echo  $imtpostponed['dept_name']; ?></td>
                    <td><?php echo  $imtpostponed['course_code']; ?></td>
                    <td><?php echo  $imtpostponed['level_name']; ?></td>
                    <td><?php echo  $imtpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $imtpostponed['start_time']; ?> -  <?php echo $imtpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $imtothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $imtothers['service_no']; ?></td>

                    <td>
                        <?php echo $imtothers['lecturer_fname']; ?>
                        <?php echo $imtothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $imtothers['faculty_name']; ?></td>
                    <td><?php echo $imtothers['dept_name']; ?></td>
                    <td><?php echo $imtothers['course_code']; ?></td>
                    <td><?php echo $imtothers['level_name']; ?></td>
                    <td><?php echo $imtothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $imtothers['start_time']; ?> -  <?php echo $imtothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

