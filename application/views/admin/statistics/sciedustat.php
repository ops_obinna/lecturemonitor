<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em">Science Education Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $scieduheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $scieduheld['service_no']; ?></td>

                    <td>
                        <?php echo $scieduheld['lecturer_fname']; ?>
                        <?php echo $scieduheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $scieduheld['faculty_name']; ?></td>
                    <td><?php echo $scieduheld['dept_name']; ?></td>
                    <td><?php echo $scieduheld['course_code']; ?></td>
                    <td><?php echo $scieduheld['level_name']; ?></td>
                    <td><?php echo $scieduheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $scieduheld['start_time']; ?> -  <?php echo $scieduheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $scieduabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $scieduabsent['service_no']; ?></td>

                    <td>
                        <?php echo $scieduabsent['lecturer_fname']; ?>
                        <?php echo $scieduabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $scieduabsent['faculty_name']; ?></td>
                    <td><?php echo $scieduabsent['dept_name']; ?></td>
                    <td><?php echo $scieduabsent['course_code']; ?></td>
                    <td><?php echo $scieduabsent['level_name']; ?></td>
                    <td><?php echo $scieduabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $scieduabsent['start_time']; ?> -  <?php echo $scieduabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $sciedupostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $sciedupostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $sciedupostponed['lecturer_fname']; ?>
                        <?php echo  $sciedupostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $sciedupostponed['faculty_name']; ?></td>
                    <td><?php echo  $sciedupostponed['dept_name']; ?></td>
                    <td><?php echo  $sciedupostponed['course_code']; ?></td>
                    <td><?php echo  $sciedupostponed['level_name']; ?></td>
                    <td><?php echo  $sciedupostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $sciedupostponed['start_time']; ?> -  <?php echo $sciedupostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $scieduothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $scieduothers['service_no']; ?></td>

                    <td>
                        <?php echo $scieduothers['lecturer_fname']; ?>
                        <?php echo $scieduothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $scieduothers['faculty_name']; ?></td>
                    <td><?php echo $scieduothers['dept_name']; ?></td>
                    <td><?php echo $scieduothers['course_code']; ?></td>
                    <td><?php echo $scieduothers['level_name']; ?></td>
                    <td><?php echo $scieduothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $scieduothers['start_time']; ?> -  <?php echo $scieduothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

