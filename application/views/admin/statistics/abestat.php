<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Agriculture and Bio-resource Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $abeheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $abeheld['service_no']; ?></td>

                    <td>
                        <?php echo $abeheld['lecturer_fname']; ?>
                        <?php echo $abeheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $abeheld['faculty_name']; ?></td>
                    <td><?php echo $abeheld['dept_name']; ?></td>
                    <td><?php echo $abeheld['course_code']; ?></td>
                    <td><?php echo $abeheld['level_name']; ?></td>
                    <td><?php echo $abeheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $abeheld['start_time']; ?> -  <?php echo $abeheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $abeabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $abeabsent['service_no']; ?></td>

                    <td>
                        <?php echo $abeabsent['lecturer_fname']; ?>
                        <?php echo $abeabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $abeabsent['faculty_name']; ?></td>
                    <td><?php echo $abeabsent['dept_name']; ?></td>
                    <td><?php echo $abeabsent['course_code']; ?></td>
                    <td><?php echo $abeabsent['level_name']; ?></td>
                    <td><?php echo $abeabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $abeabsent['start_time']; ?> -  <?php echo $abeabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $abepostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $abepostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $abepostponed['lecturer_fname']; ?>
                        <?php echo  $abepostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $abepostponed['faculty_name']; ?></td>
                    <td><?php echo  $abepostponed['dept_name']; ?></td>
                    <td><?php echo  $abepostponed['course_code']; ?></td>
                    <td><?php echo  $abepostponed['level_name']; ?></td>
                    <td><?php echo  $abepostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $abepostponed['start_time']; ?> -  <?php echo $abepostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $abeothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $abeothers['service_no']; ?></td>

                    <td>
                        <?php echo $abeothers['lecturer_fname']; ?>
                        <?php echo $abeothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $abeothers['faculty_name']; ?></td>
                    <td><?php echo $abeothers['dept_name']; ?></td>
                    <td><?php echo $abeothers['course_code']; ?></td>
                    <td><?php echo $abeothers['level_name']; ?></td>
                    <td><?php echo $abeothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $abeothers['start_time']; ?> -  <?php echo $abeothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

