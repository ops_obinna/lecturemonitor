<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Computer Science Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $cptheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cptheld['service_no']; ?></td>

                    <td>
                        <?php echo $cptheld['lecturer_fname']; ?>
                        <?php echo $cptheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cptheld['faculty_name']; ?></td>
                    <td><?php echo $cptheld['dept_name']; ?></td>
                    <td><?php echo $cptheld['course_code']; ?></td>
                    <td><?php echo $cptheld['level_name']; ?></td>
                    <td><?php echo $cptheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $cptheld['start_time']; ?> -  <?php echo $cptheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $cptabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cptabsent['service_no']; ?></td>

                    <td>
                        <?php echo $cptabsent['lecturer_fname']; ?>
                        <?php echo $cptabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cptabsent['faculty_name']; ?></td>
                    <td><?php echo $cptabsent['dept_name']; ?></td>
                    <td><?php echo $cptabsent['course_code']; ?></td>
                    <td><?php echo $cptabsent['level_name']; ?></td>
                    <td><?php echo $cptabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $cptabsent['start_time']; ?> -  <?php echo $cptabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $cptpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $cptpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $cptpostponed['lecturer_fname']; ?>
                        <?php echo  $cptpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $cptpostponed['faculty_name']; ?></td>
                    <td><?php echo  $cptpostponed['dept_name']; ?></td>
                    <td><?php echo  $cptpostponed['course_code']; ?></td>
                    <td><?php echo  $cptpostponed['level_name']; ?></td>
                    <td><?php echo  $cptpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $cptpostponed['start_time']; ?> -  <?php echo $cptpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $cptothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $cptothers['service_no']; ?></td>

                    <td>
                        <?php echo $cptothers['lecturer_fname']; ?>
                        <?php echo $cptothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $cptothers['faculty_name']; ?></td>
                    <td><?php echo $cptothers['dept_name']; ?></td>
                    <td><?php echo $cptothers['course_code']; ?></td>
                    <td><?php echo $cptothers['level_name']; ?></td>
                    <td><?php echo $cptothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $cptothers['start_time']; ?> -  <?php echo $cptothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

