
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SEMT Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $ebsheldsummary; ?>", 
                   "<?php echo $pmtheldsummary; ?>", 
                   "<?php echo $tmtheldsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],

            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebsheldsummary; ?>", 
                   "<?php echo $pmtheldsummary; ?>", 
                   "<?php echo $tmtheldsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>HELD</td>
        <td>
            <?php echo $ebsheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>HELD</td>
        <td><?php echo $pmtheldsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>HELD</td>
        <td><?php echo $tmtheldsummary;?>
         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SEMT Departments and Total Number of lectures postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: ["<?php echo $ebspostponedsummary; ?>", 
                   "<?php echo $pmtpostponedsummary; ?>", 
                   "<?php echo $tmtpostponedsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebspostponedsummary; ?>", 
                   "<?php echo $pmtpostponedsummary; ?>", 
                   "<?php echo $tmtpostponedsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Postponed Lectures The Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>POSTPONED</td>
        <td><?php echo $ebspostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>POSTPONED</td>
        <td><?php echo $pmtpostponedsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>POSTPONED</td>
        <td><?php echo $tmtpostponedsummary;?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SEMT Departments and Total Number of lecturers Absent for the Semesters - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lecturers Absent by department',
            data: ["<?php echo $ebsabsentsummary; ?>", 
                   "<?php echo $pmtabsentsummary; ?>", 
                   "<?php echo $tmtabsentsummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebsabsentsummary; ?>", 
                   "<?php echo $pmtabsentsummary; ?>", 
                   "<?php echo $tmtabsentsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent for The Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>ABSENT</td>
        <td><?php echo $ebsabsentsummary;?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>ABSENT</td>
        <td><?php echo $pmtabsentsummary;?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>ABSENT</td>
        <td><?php echo $tmtabsentsummary;?>
         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SEMT Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $ebsotherssummary; ?>", 
                   "<?php echo $pmtotherssummary; ?>", 
                   "<?php echo $tmtotherssummary; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebsotherssummary; ?>", 
                   "<?php echo $pmtotherssummary; ?>", 
                   "<?php echo $tmtotherssummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Others  For the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>OTHERS</td>
        <td><?php echo $ebsotherssummary;?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>OTHERS</td>
        <td><?php echo $pmtotherssummary;?>  
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>OTHERS</td> 
        <td><?php echo $tmtotherssummary;?>
         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>