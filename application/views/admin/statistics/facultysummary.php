
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing Faculties and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of  lectures held by faculty',
            data: ["<?php echo $sictheldsummary; ?>", 
                   "<?php echo $seetheldsummary; ?>", 
                   "<?php echo $setheldsummary; ?>", 
                   "<?php echo $semtheldsummary; ?>", 
                   "<?php echo $saatheldsummary; ?>", 
                   "<?php echo $spsheldsummary; ?>",
                   "<?php echo $steheldsummary; ?>",
                   "<?php echo $slsheldsummary; ?>"
                  ],
            
                
                backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictheldsummary; ?>", 
                   "<?php echo $seetheldsummary; ?>", 
                   "<?php echo $setheldsummary; ?>", 
                   "<?php echo $semtheldsummary; ?>", 
                   "<?php echo $saatheldsummary; ?>", 
                   "<?php echo $spsheldsummary; ?>",
                   "<?php echo $steheldsummary; ?>",
                   "<?php echo $slsheldsummary; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
    
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary HELD Lectures  
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>HELD</td>
        <td>
            <?php echo $sictheldsummary;?>         
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>HELD</td>
        <td>
            <?php echo $seetheldsummary;?> 
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>HELD</td>
        <td>
            
            <?php echo $semtheldsummary;?> 

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>HELD</td>
        <td>
            <?php echo $saatheldsummary;?> 
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>HELD</td>
        <td>
        
            <?php echo $setheldsummary;?> 

        <td>
    </tr>
     <tr>
        <td>6</td>
        <td>SPS</td>
        <td>HELD</td>
        <td>
            <?php echo $spsheldsummary;?> 
        </td>
    </tr>
     <tr>
        <td>7</td>
        <td>SLS</td>
        <td>HELD</td>
        <td><?php echo $slsheldsummary;?> 
    </tr>
     <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>HELD</td>
        <td>
         
         <?php echo $steheldsummary;?> 
         </td>
    </tr>
  
</table>
    
    
</div>
</div>

<br><br>
<!--Script Displaying Statistices for postponed Lectures for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing Faculties  and Total Number of postponed lectures for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartPostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartPostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lectures postponed by faculty',
            data: ["<?php echo $sictpostponedsummary; ?>", 
                   "<?php echo $seetpostponedsummary; ?>", 
                   "<?php echo $setpostponedsummary; ?>", 
                   "<?php echo $semtpostponedsummary; ?>", 
                   "<?php echo $saatpostponedsummary; ?>", 
                   "<?php echo $spspostponedsummary; ?>",
                   "<?php echo $stepostponedsummary; ?>",
                   "<?php echo $slspostponedsummary; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
           
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictpostponedsummary; ?>", 
                   "<?php echo $seetpostponedsummary; ?>", 
                   "<?php echo $setpostponedsummary; ?>", 
                   "<?php echo $semtpostponedsummary; ?>", 
                   "<?php echo $saatpostponedsummary; ?>", 
                   "<?php echo $spspostponedsummary; ?>",
                   "<?php echo $stepostponedsummary; ?>",
                   "<?php echo $slspostponedsummary; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary  Postponed Lectures 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $sictpostponedsummary;?>

        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $seetpostponedsummary;?>
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $semtpostponedsummary;?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $saatpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>POSTPONED</td>
        <td>   
            <?php echo $setpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $spspostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $slspostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>POSTPONED</td>
        <td>
            <?php echo $stepostponedsummary;?>
        </td>
    </tr>
  
</table>
    </div>
</div>


<br><br>
<!--Script Displaying Statistices for Absent Lecturers for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (3) Showing Faculties and Total Number of Absent lecturers for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lecturers Absent by faculty',
            data: ["<?php echo $sictabsentsummary; ?>", 
                   "<?php echo $seetabsentsummary; ?>", 
                   "<?php echo $setabsentsummary; ?>", 
                   "<?php echo $semtabsentsummary; ?>", 
                   "<?php echo $saatabsentsummary; ?>", 
                   "<?php echo $spsabsentsummary; ?>",
                   "<?php echo $steabsentsummary; ?>",
                   "<?php echo $slsabsentsummary; ?>"
                  ],
             backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictabsentsummary; ?>", 
                   "<?php echo $seetabsentsummary; ?>", 
                   "<?php echo $setabsentsummary; ?>", 
                   "<?php echo $semtabsentsummary; ?>", 
                   "<?php echo $saatabsentsummary; ?>", 
                   "<?php echo $spsabsentsummary; ?>",
                   "<?php echo $steabsentsummary; ?>",
                   "<?php echo $slsabsentsummary; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary for Absent Lecturers  
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $sictabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>ABSENT</td>
        <td>
            
            <?php echo $seetabsentsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $semtabsentsummary;?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>ABSENT</td>
        <td>
            <?php echo $saatabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>ABSENT</td>
        <td>
            <?php echo $setabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>ABSENT</td>
        <td>
            <?php echo $spsabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>ABSENT</td>
        <td>
            <?php echo $slsabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>ABSENT</td>
        <td>
            <?php echo $steabsentsummary;?>
        </td>
    </tr>
  
</table>
    </div>
</div>

<br><br>
<!--Script Displaying Statistices for Absent Lecturers for the Present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing Faculties and Total Number of Lecturers Absent for Other reasons for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets: [{
            label: 'Number of Lecturers Absent for other reasons',
            data: ["<?php echo $sictotherssummary; ?>", 
                   "<?php echo $seetotherssummary; ?>", 
                   "<?php echo $setotherssummary; ?>", 
                   "<?php echo $semtotherssummary; ?>", 
                   "<?php echo $saatotherssummary; ?>", 
                   "<?php echo $spsotherssummary; ?>",
                   "<?php echo $steotherssummary; ?>",
                   "<?php echo $slsotherssummary; ?>"
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],

            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["SICT", "SEET", "SET", "SEMT", "SAAT", "SPS", "STE", "SLS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891', '#eff367'],
            data: ["<?php echo $sictotherssummary; ?>", 
                   "<?php echo $seetotherssummary; ?>", 
                   "<?php echo $setotherssummary; ?>", 
                   "<?php echo $semtotherssummary; ?>", 
                   "<?php echo $saatotherssummary; ?>", 
                   "<?php echo $spsotherssummary; ?>",
                   "<?php echo $steotherssummary; ?>",
                   "<?php echo $slsotherssummary; ?>"
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
        <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
Summary for Other Reasons 
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>FACULTY</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>SICT</td>
        <td>OTHERS</td>
        <td>
            <?php echo $sictotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SEET</td>
        <td>OTHERS</td>
        <td><?php echo $seetotherssummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>SEMT</td>
        <td>OTHERS</td>
        <td><?php echo $semtotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>SAAT</td>
        <td>OTHERS</td>
        <td><?php echo $saatotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SET</td>
        <td>OTHERS</td>
        <td><?php echo $setotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPS</td>
        <td>OTHERS</td>
        <td>
            <?php echo $spsotherssummary;?>
        
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>SLS</td>
        <td>OTHERS</td>
        <td><?php echo $slsotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>SSTE</td>
        <td>OTHERS</td>
        <td><?php echo $steotherssummary;?>
        </td>
    </tr>
  
</table>
    </div>
</div>
<br><br>


    

    




