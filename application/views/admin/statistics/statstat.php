
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Statistics Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $statheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $statheld['service_no']; ?></td>

                    <td>
                        <?php echo $statheld['lecturer_fname']; ?>
                        <?php echo $statheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $statheld['faculty_name']; ?></td>
                    <td><?php echo $statheld['dept_name']; ?></td>
                    <td><?php echo $statheld['course_code']; ?></td>
                    <td><?php echo $statheld['level_name']; ?></td>
                    <td><?php echo $statheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $statheld['start_time']; ?> -  
                        <?php echo $statheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $statabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $statabsent['service_no']; ?></td>

                    <td>
                        <?php echo $statabsent['lecturer_fname']; ?>
                        <?php echo $statabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $statabsent['faculty_name']; ?></td>
                    <td><?php echo $statabsent['dept_name']; ?></td>
                    <td><?php echo $statabsent['course_code']; ?></td>
                    <td><?php echo $statabsent['level_name']; ?></td>
                    <td><?php echo $statabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $statabsent['start_time']; ?> - 
                        <?php echo $statabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $statpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $statpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $statpostponed['lecturer_fname']; ?>
                        <?php echo  $statpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $statpostponed['faculty_name']; ?></td>
                    <td><?php echo  $statpostponed['dept_name']; ?></td>
                    <td><?php echo  $statpostponed['course_code']; ?></td>
                    <td><?php echo  $statpostponed['level_name']; ?></td>
                    <td><?php echo  $statpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $statpostponed['start_time']; ?> -  
                        <?php echo  $statpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $statothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $statothers['service_no']; ?></td>

                    <td>
                        <?php echo $statothers['lecturer_fname']; ?>
                        <?php echo $statothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $statothers['faculty_name']; ?></td>
                    <td><?php echo $statothers['dept_name']; ?></td>
                    <td><?php echo $statothers['course_code']; ?></td>
                    <td><?php echo $statothers['level_name']; ?></td>
                    <td><?php echo $statothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $statothers['start_time']; ?> - 
                        <?php echo $statothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

