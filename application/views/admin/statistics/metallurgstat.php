<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Metallurgical Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $metallurgheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $metallurgheld['service_no']; ?></td>

                    <td>
                        <?php echo $metallurgheld['lecturer_fname']; ?>
                        <?php echo $metallurgheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $metallurgheld['faculty_name']; ?></td>
                    <td><?php echo $metallurgheld['dept_name']; ?></td>
                    <td><?php echo $metallurgheld['course_code']; ?></td>
                    <td><?php echo $metallurgheld['level_name']; ?></td>
                    <td><?php echo $metallurgheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $metallurgheld['start_time']; ?> -  <?php echo $metallurgheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $metallurgabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $metallurgabsent['service_no']; ?></td>

                    <td>
                        <?php echo $metallurgabsent['lecturer_fname']; ?>
                        <?php echo $metallurgabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $metallurgabsent['faculty_name']; ?></td>
                    <td><?php echo $metallurgabsent['dept_name']; ?></td>
                    <td><?php echo $metallurgabsent['course_code']; ?></td>
                    <td><?php echo $metallurgabsent['level_name']; ?></td>
                    <td><?php echo $metallurgabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $metallurgabsent['start_time']; ?> -  <?php echo $metallurgabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $metallurgpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $metallurgpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $metallurgpostponed['lecturer_fname']; ?>
                        <?php echo  $metallurgpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $metallurgpostponed['faculty_name']; ?></td>
                    <td><?php echo  $metallurgpostponed['dept_name']; ?></td>
                    <td><?php echo  $metallurgpostponed['course_code']; ?></td>
                    <td><?php echo  $metallurgpostponed['level_name']; ?></td>
                    <td><?php echo  $metallurgpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo $metallurgpostponed['start_time']; ?> -  <?php echo $metallurgpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $metallurgothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $metallurgothers['service_no']; ?></td>

                    <td>
                        <?php echo $metallurgothers['lecturer_fname']; ?>
                        <?php echo $metallurgothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $metallurgothers['faculty_name']; ?></td>
                    <td><?php echo $metallurgothers['dept_name']; ?></td>
                    <td><?php echo $metallurgothers['course_code']; ?></td>
                    <td><?php echo $metallurgothers['level_name']; ?></td>
                    <td><?php echo $metallurgothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $metallurgothers['start_time']; ?> -  <?php echo $metallurgothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

