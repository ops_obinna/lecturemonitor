
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SEMT Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $ebstoday; ?>", 
                   "<?php echo $pmttoday; ?>", 
                   "<?php echo $tmttoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebstoday; ?>", 
                   "<?php echo $pmttoday; ?>", 
                   "<?php echo $tmttoday; ?>"                   
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>HELD</td>
        <td>
            <?php echo $ebstoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ebssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>HELD</td>
        <td><?php echo $pmttoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($pmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>      

        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>HELD</td>
        <td><?php echo $tmttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($tmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>      

         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SEMT Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: ["<?php echo $ebspostponedtoday; ?>", 
                   "<?php echo $pmtpostponedtoday; ?>", 
                   "<?php echo $tmtpostponedtoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebspostponedtoday; ?>", 
                   "<?php echo $pmtpostponedtoday; ?>", 
                   "<?php echo $tmtpostponedtoday; ?>"                   
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Postponed Lectures - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>POSTPONED</td>
        <td><?php echo $ebspostponedtoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ebssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>POSTPONED</td>
        <td><?php echo $pmtpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($pmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?> 
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>POSTPONED</td>
        <td><?php echo $tmtpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($tmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SEMT Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lecturers Absent by department',
            data: ["<?php echo $ebsabsenttoday; ?>", 
                   "<?php echo $pmtabsenttoday; ?>", 
                   "<?php echo $tmtabsenttoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebsabsenttoday; ?>", 
                   "<?php echo $pmtabsenttoday; ?>", 
                   "<?php echo $tmtabsenttoday; ?>"                   
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>ABSENT</td>
        <td><?php echo $ebsabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ebssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>ABSENT</td>
        <td><?php echo $pmtabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($pmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>ABSENT</td>
        <td><?php echo $tmtabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($tmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SEMT Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEMTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEMTothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["EBS", "PMT", "TMT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $ebsotherstoday; ?>", 
                   "<?php echo $pmtotherstoday; ?>", 
                   "<?php echo $tmtotherstoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["EBS", "PMT", "TMT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $ebsotherstoday; ?>", 
                   "<?php echo $pmtotherstoday; ?>", 
                   "<?php echo $tmtotherstoday; ?>"                   
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Others  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>EBS</td>
        <td>OTHERS</td>
        <td><?php echo $ebsotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($ebssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>PMT</td>
        <td>OTHERS</td>
        <td><?php echo $pmtotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($pmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>TMT</td>
        <td>OTHERS</td>
        <td><?php echo $tmtotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($tmtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>  
         </td>
    </tr>
  
</table>
</div>
</div>

<br><br>


    
    