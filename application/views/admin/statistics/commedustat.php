<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Communication Education Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $comeduheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $comeduheld['service_no']; ?></td>

                    <td>
                        <?php echo $comeduheld['lecturer_fname']; ?>
                        <?php echo $comeduheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $comeduheld['faculty_name']; ?></td>
                    <td><?php echo $comeduheld['dept_name']; ?></td>
                    <td><?php echo $comeduheld['course_code']; ?></td>
                    <td><?php echo $comeduheld['level_name']; ?></td>
                    <td><?php echo $comeduheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $comeduheld['start_time']; ?> -  <?php echo $comeduheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $commeduabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $commeduabsent['service_no']; ?></td>

                    <td>
                        <?php echo $commeduabsent['lecturer_fname']; ?>
                        <?php echo $commeduabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $commeduabsent['faculty_name']; ?></td>
                    <td><?php echo $commeduabsent['dept_name']; ?></td>
                    <td><?php echo $commeduabsent['course_code']; ?></td>
                    <td><?php echo $commeduabsent['level_name']; ?></td>
                    <td><?php echo $commeduabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $commeduabsent['start_time']; ?> -  <?php echo $commeduabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $commedupostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $commedupostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $commedupostponed['lecturer_fname']; ?>
                        <?php echo  $commedupostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $commedupostponed['faculty_name']; ?></td>
                    <td><?php echo  $commedupostponed['dept_name']; ?></td>
                    <td><?php echo  $commedupostponed['course_code']; ?></td>
                    <td><?php echo  $commedupostponed['level_name']; ?></td>
                    <td><?php echo  $commedupostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $commedupostponed['start_time']; ?> -  <?php echo $commedupostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $commeduothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $commeduothers['service_no']; ?></td>

                    <td>
                        <?php echo $commeduothers['lecturer_fname']; ?>
                        <?php echo $commeduothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $commeduothers['faculty_name']; ?></td>
                    <td><?php echo $commeduothers['dept_name']; ?></td>
                    <td><?php echo $commeduothers['course_code']; ?></td>
                    <td><?php echo $commeduothers['level_name']; ?></td>
                    <td><?php echo $commeduothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $commeduothers['start_time']; ?> -  <?php echo $commeduothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

