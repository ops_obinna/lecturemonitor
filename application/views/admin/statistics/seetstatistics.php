
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SEET Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $abetoday; ?>", 
                   "<?php echo $electtoday; ?>", 
                   "<?php echo $mechtoday; ?>", 
                   "<?php echo $mechtrotoday; ?>", 
                   "<?php echo $comptoday; ?>", 
                   "<?php echo $telecomtoday; ?>", 
                   "<?php echo $metallurgtoday; ?>",
                   "<?php echo $civiltoday; ?>", 
                   "<?php echo $chemtoday; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderColor: [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.",
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            data: ["<?php echo $abetoday; ?>", 
                   "<?php echo $electtoday; ?>", 
                   "<?php echo $mechtoday; ?>", 
                   "<?php echo $mechtrotoday; ?>", 
                   "<?php echo $comptoday; ?>", 
                   "<?php echo $telecomtoday; ?>", 
                   "<?php echo $metallurgtoday; ?>",
                   "<?php echo $civiltoday; ?>", 
                   "<?php echo $chemtoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>HELD</td>
        <td><?php echo $abetoday . " out of";?>
            
            <?php
            foreach($abesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>HELD</td>
        <td><?php echo $electtoday . " out of";?>
            <?php
            foreach($electsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>HELD</td>
        <td><?php echo $mechtoday . " out of";?>
            <?php
            foreach($mechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>HELD</td>
        <td><?php echo $mechtrotoday . " out of";?>
            <?php
            foreach($mechtrosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>HELD</td>
        <td><?php echo $comptoday . " out of";?>
            <?php
            foreach($compsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>HELD</td>
        <td><?php echo $telecomtoday . " out of";?>
            <?php
            foreach($telecomsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>HELD</td>
        <td><?php echo $metallurgtoday . " out of";?>
            <?php
            foreach($metallurgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>HELD</td>
        <td><?php echo $civiltoday . " out of";?>
            <?php
            foreach($civilsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>HELD</td>
        <td><?php echo $chemtoday . " out of";?>
            <?php
            foreach($chemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SEET Departments and Total Number of lectures Postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $abepostponedtoday; ?>", 
                   "<?php echo $electpostponedtoday; ?>", 
                   "<?php echo $mechpostponedtoday; ?>", 
                   "<?php echo $mechtropostponedtoday; ?>", 
                   "<?php echo $comppostponedtoday; ?>", 
                   "<?php echo $telecompostponedtoday; ?>", 
                   "<?php echo $metallurgpostponedtoday; ?>",
                   "<?php echo $civilpostponedtoday; ?>", 
                   "<?php echo $chempostponedtoday; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderColor: [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
           data: [
                   "<?php echo $abepostponedtoday; ?>", 
                   "<?php echo $electpostponedtoday; ?>", 
                   "<?php echo $mechpostponedtoday; ?>", 
                   "<?php echo $mechtropostponedtoday; ?>", 
                   "<?php echo $comppostponedtoday; ?>", 
                   "<?php echo $telecompostponedtoday; ?>", 
                   "<?php echo $metallurgpostponedtoday; ?>",
                   "<?php echo $civilpostponedtoday; ?>", 
                   "<?php echo $chempostponedtoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $abepostponedtoday . " out of";?>
            
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($abesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>HELD</td>
        <td><?php echo $electpostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($electsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $mechpostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>POSTPONED</td>
        <td><?php echo $mechtropostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechtrosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $comppostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($compsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $telecompostponedtoday . " out of";?>
        <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($telecomsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $metallurgpostponedtoday. " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($metallurgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $civilpostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($civilsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>POSTPONED</td>
        <td><?php echo $chempostponedtoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($chemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers absent for today -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SEET Departments and lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: [
                   "<?php echo $abeabsenttoday; ?>", 
                   "<?php echo $electabsenttoday; ?>", 
                   "<?php echo $mechabsenttoday; ?>", 
                   "<?php echo $mechtroabsenttoday; ?>", 
                   "<?php echo $compabsenttoday; ?>", 
                   "<?php echo $telecomabsenttoday; ?>", 
                   "<?php echo $metallurgabsenttoday; ?>",
                   "<?php echo $civilabsenttoday; ?>", 
                   "<?php echo $chemabsenttoday; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderColor: [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETcanceled" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETcanceled");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
          data: [
                   "<?php echo $abeabsenttoday; ?>", 
                   "<?php echo $electabsenttoday; ?>", 
                   "<?php echo $mechabsenttoday; ?>", 
                   "<?php echo $mechtroabsenttoday; ?>", 
                   "<?php echo $compabsenttoday; ?>", 
                   "<?php echo $telecomabsenttoday; ?>", 
                   "<?php echo $metallurgabsenttoday; ?>",
                   "<?php echo $civilabsenttoday; ?>", 
                   "<?php echo $chemabsenttoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $abeabsenttoday . " out of";?>
            
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($abesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>ABSENT</td>
        <td><?php echo $electabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($electsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $mechabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>ABSENT</td>
        <td><?php echo $mechtroabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechtrosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $compabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($compsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $telecomabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($telecomsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $metallurgabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($metallurgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $civilabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($civilsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>ABSENT</td>
        <td><?php echo $chemabsenttoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($chemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
  
</table>
</div>
</div>

<!--Script displaying statistices for lecturers absent for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SEET Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSEETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSEETothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {        labels: [
            "Agric Bio. Eng.", 
            "Elect. Eng", 
            "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $abeotherstoday; ?>", 
                   "<?php echo $electotherstoday; ?>", 
                   "<?php echo $mechotherstoday; ?>", 
                   "<?php echo $mechtrootherstoday; ?>", 
                   "<?php echo $compotherstoday; ?>", 
                   "<?php echo $telecomotherstoday; ?>", 
                   "<?php echo $metallurgotherstoday; ?>",
                   "<?php echo $civilotherstoday; ?>", 
                   "<?php echo $chemotherstoday; ?>"
                   
                  ],
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderColor: [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEETothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
     labels: [
            "Agric Bio. Eng.",
            "Elect. Eng", "Mech. Eng", 
            "Mechatro. Eng",
            "Computer Eng.",
            "Telecom Eng",
            "Material Metallurg. Eng",
            "Civil Eng.",
            "Chemical Eng"
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: 
            [
                '#f1c40f',
                '#e67e22',
                '#16a085',
                '#2980b9',
                '#298009', 
                '#f70341',
                '#f095f3',
                '#f00891', 
                '#eff367'
            ],
            data: ["<?php echo $abeotherstoday; ?>", 
                   "<?php echo $electotherstoday; ?>", 
                   "<?php echo $mechotherstoday; ?>", 
                   "<?php echo $mechtrootherstoday; ?>", 
                   "<?php echo $compotherstoday; ?>", 
                   "<?php echo $telecomotherstoday; ?>", 
                   "<?php echo $metallurgotherstoday; ?>",
                   "<?php echo $civilotherstoday; ?>", 
                   "<?php echo $chemotherstoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Absent for Other Reasons  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>Agric. Bio-resources Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $abeotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($abesummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Elect/Elect</td>
        <td>OTHERS</td>
        <td><?php echo $electotherstoday . " out of";?>
        <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($electsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>Mech Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $mechotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mechatronics</td>
        <td>OTHERS</td>
        <td><?php echo $mechtrootherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($mechtrosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Computer Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $compotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($compsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Telecom Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $telecomotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($telecomsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Metallurgical Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $metallurgotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($metallurgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Civil Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $civilotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($civilsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Chemical Eng.</td>
        <td>OTHERS</td>
        <td><?php echo $chemotherstoday . " out of";?>
            <!--display summary of lectures held today as against postponed -->
            <?php
            foreach($chemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>

        </td>
    </tr>
  
</table>
</div>
</div>
