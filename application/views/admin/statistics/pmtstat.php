<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Project Management Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $pmtheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $pmtheld['service_no']; ?></td>

                    <td>
                        <?php echo $pmtheld['lecturer_fname']; ?>
                        <?php echo $pmtheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $pmtheld['faculty_name']; ?></td>
                    <td><?php echo $pmtheld['dept_name']; ?></td>
                    <td><?php echo $pmtheld['course_code']; ?></td>
                    <td><?php echo $pmtheld['level_name']; ?></td>
                    <td><?php echo $pmtheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $pmtheld['start_time']; ?> -  <?php echo $pmtheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $pmtabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $pmtabsent['service_no']; ?></td>

                    <td>
                        <?php echo $pmtabsent['lecturer_fname']; ?>
                        <?php echo $pmtabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $pmtabsent['faculty_name']; ?></td>
                    <td><?php echo $pmtabsent['dept_name']; ?></td>
                    <td><?php echo $pmtabsent['course_code']; ?></td>
                    <td><?php echo $pmtabsent['level_name']; ?></td>
                    <td><?php echo $pmtabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $pmtabsent['start_time']; ?> -  <?php echo $pmtabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $pmtpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $pmtpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $pmtpostponed['lecturer_fname']; ?>
                        <?php echo  $pmtpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $pmtpostponed['faculty_name']; ?></td>
                    <td><?php echo  $pmtpostponed['dept_name']; ?></td>
                    <td><?php echo  $pmtpostponed['course_code']; ?></td>
                    <td><?php echo  $pmtpostponed['level_name']; ?></td>
                    <td><?php echo  $pmtpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $pmtpostponed['start_time']; ?> -  <?php echo $pmtpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $pmtothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $pmtothers['service_no']; ?></td>

                    <td>
                        <?php echo $pmtothers['lecturer_fname']; ?>
                        <?php echo $pmtothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $pmtothers['faculty_name']; ?></td>
                    <td><?php echo $pmtothers['dept_name']; ?></td>
                    <td><?php echo $pmtothers['course_code']; ?></td>
                    <td><?php echo $pmtothers['level_name']; ?></td>
                    <td><?php echo $pmtothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $pmtothers['start_time']; ?> -  <?php echo $pmtothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

