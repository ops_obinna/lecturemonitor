<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SSTE Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $iteheldsummary; ?>", 
                   "<?php echo $scieduheldsummary; ?>", 
                   "<?php echo $edutechheldsummary; ?>",
                   "<?php echo $comeduheldsummary; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                  "<?php echo $iteheldsummary; ?>", 
                   "<?php echo $scieduheldsummary; ?>", 
                   "<?php echo $edutechheldsummary; ?>",
                   "<?php echo $comeduheldsummary; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held for the Summary
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>HELD</td>
        <td><?php  echo $iteheldsummary; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>HELD</td>
        <td><?php echo $scieduheldsummary; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>HELD</td>
        <td><?php echo $edutechheldsummary; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>HELD</td>
        <td><?php echo $comeduheldsummary; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SSTE Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $itepostponedsummary; ?>", 
                   "<?php echo $sciedupostponedsummary; ?>", 
                   "<?php echo $edutechpostponedsummary; ?>",
                   "<?php echo $comedupostponedsummary; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $itepostponedsummary; ?>", 
                   "<?php echo $sciedupostponedsummary; ?>", 
                   "<?php echo $edutechpostponedsummary; ?>",
                   "<?php echo $comedupostponedsummary; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures postponed for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>POSTPONED</td>
        <td><?php  echo $itepostponedsummary; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>POSTPONED</td>
        <td><?php echo $sciedupostponedsummary; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>POSTPONED</td>
        <td><?php echo $edutechpostponedsummary; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>POSTPONED</td>
        <td><?php echo $comedupostponedsummary; ?>
        </td>
    </tr>  
</table>
</div>
</div>



<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SSTE Departments and Total Number of lecturers Absent for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: [
                   "<?php echo $iteabsentsummary; ?>", 
                   "<?php echo $scieduabsentsummary; ?>", 
                   "<?php echo $edutechabsentsummary; ?>",
                   "<?php echo $comeduabsentsummary; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                    "<?php echo $iteabsentsummary; ?>", 
                   "<?php echo $scieduabsentsummary; ?>", 
                   "<?php echo $edutechabsentsummary; ?>",
                   "<?php echo $comeduabsentsummary; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures absent for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>ABSENT</td>
        <td><?php  echo $iteabsentsummary; ?>
           
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>ABSENT</td>
        <td><?php echo $scieduabsentsummary; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>ABSENT</td>
        <td><?php echo $edutechabsentsummary; ?>
        </td>
    </tr> 
    <tr>
        <td>4</td>
        <td>COMMUNICATION EDU.</td>
        <td>ABSENT</td>
        <td><?php echo $comeduabsentsummary; ?>    
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SSTE Departments and Total Number of lecturers Absent for other reasons for the Semester- <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>


<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSSTEothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSSTEothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets: [{
            label: 'Number of  lecturers absent for other reasosn',
            data: [
                   "<?php echo $iteotherssummary; ?>", 
                   "<?php echo $scieduotherssummary; ?>", 
                   "<?php echo $edutechotherssummary; ?>",
                   "<?php echo $comeduotherssummary; ?>"
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSSTEothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSSTEothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [
            "IND TECH EDU", 
            "SCIENCE EDU.", 
            "EDU. TECH.",
            "COMM. EDU."
        ],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#16f0f0'],
             data: [
                   "<?php echo $iteotherssummary; ?>", 
                   "<?php echo $scieduotherssummary; ?>", 
                   "<?php echo $edutechotherssummary; ?>",
                   "<?php echo $comeduotherssummary; ?>"
                   
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent others  for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IND. TECH. EDU</td>
        <td>OTHERS</td>
        <td><?php  echo $iteotherssummary; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>SCIENCE EDU</td>
        <td>OTHERS</td>
        <td><?php echo $scieduotherssummary; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>EDUCATION TECH</td>
        <td>OTHERS</td>
        <td><?php echo $edutechotherssummary; ?>
        </td>
    </tr> 
    <tr>
        <td>3</td>
        <td>COMMUNICATION EDU.</td>
        <td>OTHERS</td>
        <td><?php echo $comeduotherssummary; ?>
        </td>
    </tr>  
</table>
</div>
</div>


<br><br>