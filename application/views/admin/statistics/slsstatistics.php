
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SLS Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: [
                   "<?php echo $biotoday; ?>", 
                   "<?php echo $biochemtoday; ?>", 
                   "<?php echo $microbiotoday; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $biotoday; ?>", 
                   "<?php echo $biochemtoday; ?>", 
                   "<?php echo $microbiotoday; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>HELD</td>
        <td><?php echo $biotoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>HELD</td>
        <td><?php echo $biochemtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biochemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>HELD</td>
        <td><?php echo $microbiotoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($microsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SLS Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $biopostponedtoday; ?>", 
                   "<?php echo $biochempostponedtoday; ?>", 
                   "<?php echo $microbiopostponedtoday; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
                
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $biopostponedtoday; ?>", 
                   "<?php echo $biochempostponedtoday; ?>", 
                   "<?php echo $microbiopostponedtoday; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures postponed   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>POSTPONED</td>
        <td><?php echo $biopostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>POSTPONED</td>
        <td><?php echo $biochempostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biochemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>POSTPONED</td>
        <td><?php echo $microbiopostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($microsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>



<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SLS Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lecturers Absent by department',
            data: [
                   "<?php echo $bioabsenttoday; ?>", 
                   "<?php echo $biochemabsenttoday; ?>", 
                   "<?php echo $microbioabsenttoday; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
                
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $bioabsenttoday; ?>", 
                   "<?php echo $biochemabsenttoday; ?>", 
                   "<?php echo $microbioabsenttoday; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>ABSENT</td>
        <td><?php echo $bioabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>ABSENT</td>
        <td><?php echo $biochemabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biochemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>ABSENT</td>
        <td><?php echo $microbioabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($microsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SLS Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>


<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSLSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSLSothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets: [{
            label: 'Number of  lecturers Absent for other reasons',
            data: [
                   "<?php echo $biootherstoday; ?>", 
                   "<?php echo $biochemotherstoday; ?>", 
                   "<?php echo $microbiootherstoday; ?>" 
                   
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
                
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSLSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSLSothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["BIOLOGICAL SCIENCE", "BIO-CHEMISTRY", "MICROBIOLOGY"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085'],
             data: [
                   "<?php echo $biootherstoday; ?>", 
                   "<?php echo $biochemotherstoday; ?>", 
                   "<?php echo $microbiootherstoday; ?>" 
                   
                ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lecturers Absent others  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>BIOLOGICAL SCIENCE</td>
        <td>OTHERS</td>
        <td><?php echo $biootherstoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biosummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BIO-CHEMISTRY</td>
        <td>OTHERS</td>
        <td><?php echo $biochemotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($biochemsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>MICROBIOLOGY</td>
        <td>OTHERS</td>
        <td><?php echo $microbiootherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($microsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>  
</table>
</div>
</div>


<br><br>


    
    