
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SICT Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $imttoday; ?>", 
                   "<?php echo $cpttoday; ?>", 
                   "<?php echo $csstoday; ?>", 
                   "<?php echo $littoday; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imttoday; ?>", 
                   "<?php echo $cpttoday; ?>", 
                   "<?php echo $csstoday; ?>", 
                   "<?php echo $littoday; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>HELD</td>
        <td><?php echo $imttoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($imtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>HELD</td>
        <td><?php echo $cpttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($cptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>HELD</td>
        <td><?php echo $csstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($csssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>HELD</td>
        <td><?php echo $littoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($litsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SICT Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: ["<?php echo $imtpostponedtoday; ?>", 
                   "<?php echo $cptpostponedtoday; ?>", 
                   "<?php echo $csspostponedtoday; ?>", 
                   "<?php echo $litpostponedtoday; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtpostponedtoday; ?>", 
                   "<?php echo $cptpostponedtoday; ?>", 
                   "<?php echo $csspostponedtoday; ?>", 
                   "<?php echo $litpostponedtoday; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Postponed Lectures   - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>POSTPONED</td>
        <td>
            
            <?php echo $imtpostponedtoday . " out of";?>
            
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($imtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>POSTPONED</td>
        <td><?php echo $cptpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($cptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>POSTPONED</td>
        <td><?php echo $csspostponedtoday . " out of";?>
        <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($csssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>POSTPONED</td>
        <td><?php echo $litpostponedtoday . " out of" ;?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($litsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SICT Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lecturers absent by department',
            data: ["<?php echo $imtabsenttoday; ?>", 
                   "<?php echo $cptabsenttoday; ?>", 
                   "<?php echo $cssabsenttoday; ?>", 
                   "<?php echo $litabsenttoday; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtabsenttoday; ?>", 
                   "<?php echo $cptabsenttoday; ?>", 
                   "<?php echo $cssabsenttoday; ?>", 
                   "<?php echo $litabsenttoday; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
<div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Lecturers  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>ABSENT</td>
        <td><?php echo $imtabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($imtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>ABSENT</td>
        <td><?php echo $cptabsenttoday . " out of";?>
            <?php
            foreach($cptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>ABSENT</td>
        <td><?php echo $cssabsenttoday . " out of";?>
            <?php
            foreach($csssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>LIT</td>
        <td>ABSENT</td>
        <td><?php echo $litabsenttoday . " out of";?>
            <?php
            foreach($litsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>
<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SICT Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSICTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSICTothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $imtotherstoday; ?>", 
                   "<?php echo $cptotherstoday; ?>", 
                   "<?php echo $cssotherstoday; ?>", 
                   "<?php echo $litotherstoday; ?>", 
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSICTothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSICTothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["IMT", "CPT", "CSS", "LIT"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9'],
            data: ["<?php echo $imtotherstoday; ?>", 
                   "<?php echo $cptotherstoday; ?>", 
                   "<?php echo $cssotherstoday; ?>", 
                   "<?php echo $litotherstoday; ?>", 
                   
                  ]
            }
        ]
         
    },
    options: {
        cutoutPercentage: 50,
        animation:{
        animateScale: true
    }
  }
});
</script>
    </div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Others Reasons  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>IMT</td>
        <td>OTHERS</td>
        <td><?php echo $imtotherstoday . " out of";?>
            <?php
            foreach($imtsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>CPT</td>
        <td>OTHERS</td>
        <td><?php echo $cptotherstoday . " out of";?>
            <?php
            foreach($cptsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>CSS</td>
        <td>OTHERS</td>
        <td><?php echo $cssotherstoday . " out of";?>
            <?php
            foreach($csssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>LIT</td>
        <td>OTHERS</td>
        <td><?php echo $litotherstoday . " out of";?>
            <?php
            foreach($litsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>
<br><br>


    
    