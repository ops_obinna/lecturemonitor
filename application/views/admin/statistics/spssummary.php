
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (1) Showing SPS Departments and Total Number of lectures held for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $chemisheldsummary; ?>", 
                   "<?php echo $geoheldsummary; ?>", 
                   "<?php echo $geologheldsummary; ?>", 
                   "<?php echo $mathheldsummary; ?>", 
                   "<?php echo $phyheldsummary; ?>", 
                   "<?php echo $statheldsummary; ?>"
                   
                  ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             data: ["<?php echo $chemisheldsummary; ?>", 
                   "<?php echo $geoheldsummary; ?>", 
                   "<?php echo $geologheldsummary; ?>", 
                   "<?php echo $mathheldsummary; ?>", 
                   "<?php echo $phyheldsummary; ?>", 
                   "<?php echo $statheldsummary; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Lectures held for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>HELD</td>
        <td><?php echo $chemisheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>HELD</td>
        <td><?php echo $geoheldsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>HELD</td>
        <td><?php echo $geologheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>HELD</td>
        <td><?php echo $mathheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>HELD</td>
        <td><?php echo $phyheldsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>HELD</td>
        <td><?php echo $statheldsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (2) Showing SPS Departments and Total Number of lectures postponed for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lectures postponed by department',
            data: [
                   "<?php echo $chemispostponedsummary; ?>", 
                   "<?php echo $geopostponedsummary; ?>", 
                   "<?php echo $mathpostponedsummary; ?>", 
                   "<?php echo $mathpostponedsummary; ?>", 
                   "<?php echo $phypostponedsummary; ?>", 
                   "<?php echo $statpostponedsummary; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
             
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
           data: [
                   "<?php echo $chemispostponedsummary; ?>", 
                   "<?php echo $geopostponedsummary; ?>", 
                   "<?php echo $mathpostponedsummary; ?>", 
                   "<?php echo $mathpostponedsummary; ?>", 
                   "<?php echo $phypostponedsummary; ?>", 
                   "<?php echo $statpostponedsummary; ?>"
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Postponed Lectures for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>POSTPONED</td>
        <td><?php echo $chemispostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>POSTPONED</td>
        <td><?php echo $geopostponedsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>POSTPONED</td>
        <td><?php echo $geologpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>POSTPONED</td>
        <td><?php echo $mathpostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>POSTPONED</td>
        <td><?php echo $phypostponedsummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>POSTPONED</td>
        <td><?php echo $statpostponedsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SPS Departments and Total Number of lecturers Absent for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lecturers absent department',
            data: [
                   "<?php echo $chemisabsentsummary; ?>", 
                   "<?php echo $geoabsentsummary; ?>", 
                   "<?php echo $geologabsentsummary; ?>", 
                   "<?php echo $mathabsentsummary; ?>", 
                   "<?php echo $phyabsentsummary; ?>", 
                   "<?php echo $statabsentsummary; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            data: [
                   "<?php echo $chemisabsentsummary; ?>", 
                   "<?php echo $geoabsentsummary; ?>", 
                   "<?php echo $geologabsentsummary; ?>", 
                   "<?php echo $mathabsentsummary; ?>", 
                   "<?php echo $phyabsentsummary; ?>", 
                   "<?php echo $statabsentsummary; ?>"
                ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Lecturers for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>ABSENT</td>
        <td><?php echo $chemisabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>ABSENT</td>
        <td><?php echo $geoabsentsummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>ABSENT</td>
        <td><?php echo $geologabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>ABSENT</td>
        <td><?php echo $mathabsentsummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>ABSENT</td>
        <td><?php echo $phyabsentsummary;?>

        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>ABSENT</td>
        <td><?php echo $statabsentsummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (4) Showing SPS Departments and Total Number of lecturers Absent for other reasons  for the Semester - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSPSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSPSothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets: [{
            label: 'Number of  lecturers absent other reasons',
            data: [
                   "<?php echo $chemisotherssummary; ?>", 
                   "<?php echo $geootherssummary; ?>", 
                   "<?php echo $geologotherssummary; ?>", 
                   "<?php echo $mathotherssummary; ?>", 
                   "<?php echo $phyotherssummary; ?>", 
                   "<?php echo $statotherssummary; ?>"
                ],
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
    
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSPSothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSPSothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["CHEMISTRY", "GEOGRAPHY", "GEOLOGY", "MATHEMATICS", "PHYSICS", "STATISTICS"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#f1c40f','#e67e22','#16a085','#2980b9', '#f00891'],
            data: [
                   "<?php echo $chemisotherssummary; ?>", 
                   "<?php echo $geootherssummary; ?>", 
                   "<?php echo $geologotherssummary; ?>", 
                   "<?php echo $mathotherssummary; ?>", 
                   "<?php echo $phyotherssummary; ?>", 
                   "<?php echo $statotherssummary; ?>"
                ]
            }
        ]
         
    },
    options: {
        
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.5em">
 Absent Others for the Semester
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>CHEMISTRY</td>
        <td>OTHERS</td>
        <td><?php echo $chemisotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>GEOGRAPHY</td>
        <td>OTHERS</td>
        <td><?php echo $geootherssummary;?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <td>GEOLOGY</td>
        <td>OTHERS</td>
        <td><?php echo $geologotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>MATHEMATICS</td>
        <td>OTHERS</td>
        <td><?php echo $mathotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>PHYSICS</td>
        <td>OTHERS</td>
        <td><?php echo $phyotherssummary;?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>STATISTICS</td>
        <td>OTHERS</td>
        <td><?php echo $statotherssummary;?>
        </td>
    </tr>  
</table>
</div>
</div>

<br><br>


    
    