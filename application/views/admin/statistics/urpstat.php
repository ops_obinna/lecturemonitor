<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Urban and Regonal Planning Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $urpheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $urpheld['service_no']; ?></td>

                    <td>
                        <?php echo $urpheld['lecturer_fname']; ?>
                        <?php echo $urpheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $urpheld['faculty_name']; ?></td>
                    <td><?php echo $urpheld['dept_name']; ?></td>
                    <td><?php echo $urpheld['course_code']; ?></td>
                    <td><?php echo $urpheld['level_name']; ?></td>
                    <td><?php echo $urpheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $urpheld['start_time']; ?> -  <?php echo $svgheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $urpabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $urpabsent['service_no']; ?></td>

                    <td>
                        <?php echo $urpabsent['lecturer_fname']; ?>
                        <?php echo $urpabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $urpabsent['faculty_name']; ?></td>
                    <td><?php echo $urpabsent['dept_name']; ?></td>
                    <td><?php echo $urpabsent['course_code']; ?></td>
                    <td><?php echo $urpabsent['level_name']; ?></td>
                    <td><?php echo $urpabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $urpabsent['start_time']; ?> -  <?php echo $urpabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $urppostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $urppostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $urppostponed['lecturer_fname']; ?>
                        <?php echo  $urppostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $urppostponed['faculty_name']; ?></td>
                    <td><?php echo  $urppostponed['dept_name']; ?></td>
                    <td><?php echo  $urppostponed['course_code']; ?></td>
                    <td><?php echo  $urppostponed['level_name']; ?></td>
                    <td><?php echo  $urppostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo $urppostponed['start_time']; ?> -  <?php echo $urppostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $urpothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $urpothers['service_no']; ?></td>

                    <td>
                        <?php echo $urpothers['lecturer_fname']; ?>
                        <?php echo $urpothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $urpothers['faculty_name']; ?></td>
                    <td><?php echo $urpothers['dept_name']; ?></td>
                    <td><?php echo $urpothers['course_code']; ?></td>
                    <td><?php echo $urpothers['level_name']; ?></td>
                    <td><?php echo $urpothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $urpothers['start_time']; ?> -  <?php echo $urpothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>