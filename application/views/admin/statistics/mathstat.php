
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Mathematics Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $mathheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mathheld['service_no']; ?></td>

                    <td>
                        <?php echo $mathheld['lecturer_fname']; ?>
                        <?php echo $mathheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mathheld['faculty_name']; ?></td>
                    <td><?php echo $mathheld['dept_name']; ?></td>
                    <td><?php echo $mathheld['course_code']; ?></td>
                    <td><?php echo $mathheld['level_name']; ?></td>
                    <td><?php echo $mathheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $mathheld['start_time']; ?> -  
                        <?php echo $mathheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $mathabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mathabsent['service_no']; ?></td>

                    <td>
                        <?php echo $mathabsent['lecturer_fname']; ?>
                        <?php echo $mathabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mathabsent['faculty_name']; ?></td>
                    <td><?php echo $mathabsent['dept_name']; ?></td>
                    <td><?php echo $mathabsent['course_code']; ?></td>
                    <td><?php echo $mathabsent['level_name']; ?></td>
                    <td><?php echo $mathabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $mathabsent['start_time']; ?> - 
                        <?php echo $mathabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $mathpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $mathpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $mathpostponed['lecturer_fname']; ?>
                        <?php echo  $mathpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $mathpostponed['faculty_name']; ?></td>
                    <td><?php echo  $mathpostponed['dept_name']; ?></td>
                    <td><?php echo  $mathpostponed['course_code']; ?></td>
                    <td><?php echo  $mathpostponed['level_name']; ?></td>
                    <td><?php echo  $mathpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $mathpostponed['start_time']; ?> -  
                        <?php echo  $mathpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $mathothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mathothers['service_no']; ?></td>

                    <td>
                        <?php echo $mathothers['lecturer_fname']; ?>
                        <?php echo $mathothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mathothers['faculty_name']; ?></td>
                    <td><?php echo $mathothers['dept_name']; ?></td>
                    <td><?php echo $mathothers['course_code']; ?></td>
                    <td><?php echo $mathothers['level_name']; ?></td>
                    <td><?php echo $mathothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $mathothers['start_time']; ?> - 
                        <?php echo $mathothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

