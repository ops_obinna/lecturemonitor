<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Quantity Survey Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $qtsheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $qtsheld['service_no']; ?></td>

                    <td>
                        <?php echo $qtsheld['lecturer_fname']; ?>
                        <?php echo $qtsheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $qtsheld['faculty_name']; ?></td>
                    <td><?php echo $qtsheld['dept_name']; ?></td>
                    <td><?php echo $qtsheld['course_code']; ?></td>
                    <td><?php echo $qtsheld['level_name']; ?></td>
                    <td><?php echo $qtsheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $qtsheld['start_time']; ?> -  <?php echo $qtsheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $qtsabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $qtsabsent['service_no']; ?></td>

                    <td>
                        <?php echo $qtsabsent['lecturer_fname']; ?>
                        <?php echo $qtsabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $qtsabsent['faculty_name']; ?></td>
                    <td><?php echo $qtsabsent['dept_name']; ?></td>
                    <td><?php echo $qtsabsent['course_code']; ?></td>
                    <td><?php echo $qtsabsent['level_name']; ?></td>
                    <td><?php echo $qtsabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $qtsabsent['start_time']; ?> -  <?php echo $qtsabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $qtspostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $qtspostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $qtspostponed['lecturer_fname']; ?>
                        <?php echo  $qtspostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $qtspostponed['faculty_name']; ?></td>
                    <td><?php echo  $qtspostponed['dept_name']; ?></td>
                    <td><?php echo  $qtspostponed['course_code']; ?></td>
                    <td><?php echo  $qtspostponed['level_name']; ?></td>
                    <td><?php echo  $qtspostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $qtspostponed['start_time']; ?> -  <?php echo $qtspostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $qtsothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $qtsothers['service_no']; ?></td>

                    <td>
                        <?php echo $qtsothers['lecturer_fname']; ?>
                        <?php echo $qtsothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $qtsothers['faculty_name']; ?></td>
                    <td><?php echo $qtsothers['dept_name']; ?></td>
                    <td><?php echo $qtsothers['course_code']; ?></td>
                    <td><?php echo $qtsothers['level_name']; ?></td>
                    <td><?php echo $qtsothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $qtsothers['start_time']; ?> -  <?php echo $qtsothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>