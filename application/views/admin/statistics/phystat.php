
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Physics Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $phyheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $phyheld['service_no']; ?></td>

                    <td>
                        <?php echo $phyheld['lecturer_fname']; ?>
                        <?php echo $phyheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $phyheld['faculty_name']; ?></td>
                    <td><?php echo $phyheld['dept_name']; ?></td>
                    <td><?php echo $phyheld['course_code']; ?></td>
                    <td><?php echo $phyheld['level_name']; ?></td>
                    <td><?php echo $phyheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $phyheld['start_time']; ?> -  
                        <?php echo $phyheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $phyabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $phyabsent['service_no']; ?></td>

                    <td>
                        <?php echo $phyabsent['lecturer_fname']; ?>
                        <?php echo $phyabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $phyabsent['faculty_name']; ?></td>
                    <td><?php echo $phyabsent['dept_name']; ?></td>
                    <td><?php echo $phyabsent['course_code']; ?></td>
                    <td><?php echo $phyabsent['level_name']; ?></td>
                    <td><?php echo $phyabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $phyabsent['start_time']; ?> - 
                        <?php echo $phyabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $phypostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $phypostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $phypostponed['lecturer_fname']; ?>
                        <?php echo  $phypostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $phypostponed['faculty_name']; ?></td>
                    <td><?php echo  $phypostponed['dept_name']; ?></td>
                    <td><?php echo  $phypostponed['course_code']; ?></td>
                    <td><?php echo  $phypostponed['level_name']; ?></td>
                    <td><?php echo  $phypostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $phypostponed['start_time']; ?> -  
                        <?php echo  $phypostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $statothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $phyothers['service_no']; ?></td>

                    <td>
                        <?php echo $phyothers['lecturer_fname']; ?>
                        <?php echo $phyothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $phyothers['faculty_name']; ?></td>
                    <td><?php echo $phyothers['dept_name']; ?></td>
                    <td><?php echo $phyothers['course_code']; ?></td>
                    <td><?php echo $phyothers['level_name']; ?></td>
                    <td><?php echo $phyothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $phyothers['start_time']; ?> - 
                        <?php echo $phyothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

