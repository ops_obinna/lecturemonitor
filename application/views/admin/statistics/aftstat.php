
<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Aquaculture and fisheries Technology Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $aftheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aftheld['service_no']; ?></td>

                    <td>
                        <?php echo $aftheld['lecturer_fname']; ?>
                        <?php echo $aftheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aftheld['faculty_name']; ?></td>
                    <td><?php echo $aftheld['dept_name']; ?></td>
                    <td><?php echo $aftheld['course_code']; ?></td>
                    <td><?php echo $aftheld['level_name']; ?></td>
                    <td><?php echo $aftheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $aftheld['start_time']; ?> -  
                        <?php echo $aftheld ['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $aftabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aftabsent['service_no']; ?></td>

                    <td>
                        <?php echo $aftabsent['lecturer_fname']; ?>
                        <?php echo $aftabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aftabsent['faculty_name']; ?></td>
                    <td><?php echo $aftabsent['dept_name']; ?></td>
                    <td><?php echo $aftabsent['course_code']; ?></td>
                    <td><?php echo $aftabsent['level_name']; ?></td>
                    <td><?php echo $aftabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $aftabsent['start_time']; ?> -  <?php echo $aftabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $aftpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $aftpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $aftpostponed['lecturer_fname']; ?>
                        <?php echo  $aftpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $aftpostponed['faculty_name']; ?></td>
                    <td><?php echo  $aftpostponed['dept_name']; ?></td>
                    <td><?php echo  $aftpostponed['course_code']; ?></td>
                    <td><?php echo  $aftpostponed['level_name']; ?></td>
                    <td><?php echo  $aftpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $aftpostponed['start_time']; ?> -  <?php echo $aftpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $aftothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $aftothers['service_no']; ?></td>

                    <td>
                        <?php echo $aftothers['lecturer_fname']; ?>
                        <?php echo $aftothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $aftothers['faculty_name']; ?></td>
                    <td><?php echo $aftothers['dept_name']; ?></td>
                    <td><?php echo $aftothers['course_code']; ?></td>
                    <td><?php echo $aftothers['level_name']; ?></td>
                    <td><?php echo $aftothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $aftothers['start_time']; ?> -  <?php echo $aftothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

