<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Chemical Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $chemheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemheld['service_no']; ?></td>

                    <td>
                        <?php echo $chemheld['lecturer_fname']; ?>
                        <?php echo $chemheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemheld['faculty_name']; ?></td>
                    <td><?php echo $chemheld['dept_name']; ?></td>
                    <td><?php echo $chemheld['course_code']; ?></td>
                    <td><?php echo $chemheld['level_name']; ?></td>
                    <td><?php echo $chemheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemheld['start_time']; ?> -  <?php echo $chemheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $chemabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemabsent['service_no']; ?></td>

                    <td>
                        <?php echo $chemabsent['lecturer_fname']; ?>
                        <?php echo $chemabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemabsent['faculty_name']; ?></td>
                    <td><?php echo $chemabsent['dept_name']; ?></td>
                    <td><?php echo $chemabsent['course_code']; ?></td>
                    <td><?php echo $chemabsent['level_name']; ?></td>
                    <td><?php echo $chemabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemabsent['start_time']; ?> -  <?php echo $chemabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $chempostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $chempostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $chempostponed['lecturer_fname']; ?>
                        <?php echo  $chempostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $chempostponed['faculty_name']; ?></td>
                    <td><?php echo  $chempostponed['dept_name']; ?></td>
                    <td><?php echo  $chempostponed['course_code']; ?></td>
                    <td><?php echo  $chempostponed['level_name']; ?></td>
                    <td><?php echo  $chempostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo $chempostponed['start_time']; ?> -  <?php echo $chempostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $chemothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $chemothers['service_no']; ?></td>

                    <td>
                        <?php echo $chemothers['lecturer_fname']; ?>
                        <?php echo $chemothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $chemothers['faculty_name']; ?></td>
                    <td><?php echo $chemothers['dept_name']; ?></td>
                    <td><?php echo $chemothers['course_code']; ?></td>
                    <td><?php echo $chemothers['level_name']; ?></td>
                    <td><?php echo $chemothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $chemothers['start_time']; ?> -  <?php echo $chemothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

