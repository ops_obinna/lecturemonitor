
<!--Script displaying statistices for lectures held for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (1) Showing SET Departments and Total Number of lectures held for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETHeld").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $arctoday; ?>", 
                   "<?php echo $bldtoday; ?>", 
                   "<?php echo $esttoday; ?>",
                   "<?php echo $qtstoday; ?>",
                   "<?php echo $svgtoday; ?>",
                   "<?php echo $urptoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    
</div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETHeld" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETHeld");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arctoday; ?>", 
                   "<?php echo $bldtoday; ?>", 
                   "<?php echo $esttoday; ?>",
                   "<?php echo $qtstoday; ?>",
                   "<?php echo $svgtoday; ?>",
                   "<?php echo $urptoday; ?>"
                   
                  ],
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
    
    <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>HELD</td>
        <td><?php echo $arctoday . " out of";?>
            
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($arcsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>HELD</td>
        <td><?php echo $bldtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($bldsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>HELD</td>
        <td><?php echo $esttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($estsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>HELD</td>
        <td><?php echo $qtstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($qtssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>HELD</td>
        <td><?php echo $svgtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($svgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>HELD</td>
        <td><?php echo $urptoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($urpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lectures postponed for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (2) Showing SET Departments and Total Number of lectures postponed for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETpostponed").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lectures held by department',
            data: ["<?php echo $arcpostponedtoday; ?>", 
                   "<?php echo $bldpostponedtoday; ?>", 
                   "<?php echo $estpostponedtoday; ?>",
                   "<?php echo $qtspostponedtoday; ?>",
                   "<?php echo $svgpostponedtoday; ?>",
                   "<?php echo $urppostponedtoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSEMTpostponed" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSEMTpostponed");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcpostponedtoday; ?>", 
                   "<?php echo $bldpostponedtoday; ?>", 
                   "<?php echo $estpostponedtoday; ?>",
                   "<?php echo $qtspostponedtoday; ?>",
                   "<?php echo $svgpostponedtoday; ?>",
                   "<?php echo $urppostponedtoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>POSTPONED</td>
        <td><?php echo $arcpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($arcsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>POSTPONED</td>
        <td><?php echo $bldpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($bldsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>POSTPONED</td>
        <td><?php echo $estpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($estsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>POSTPONED</td>
        <td><?php echo $qtspostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($qtssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>POSTPONED</td>
        <td><?php echo $svgpostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($svgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>POSTPONED</td>
        <td><?php echo $urppostponedtoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($urpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for the present date -->
<div class="alert alert-success" style="text-align:center; font-size:1.5em">
Chart (3) Showing SET Departments and Total Number of lecturers Absent for the Day - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>

<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETabsent").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $arcabsenttoday; ?>", 
                   "<?php echo $bldabsenttoday; ?>", 
                   "<?php echo $estabsenttoday; ?>",
                   "<?php echo $qtsabsenttoday; ?>",
                   "<?php echo $svgabsenttoday; ?>",
                   "<?php echo $urpabsenttoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETabsent" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETabsent");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcabsenttoday; ?>", 
                   "<?php echo $bldabsenttoday; ?>", 
                   "<?php echo $estabsenttoday; ?>",
                   "<?php echo $qtsabsenttoday; ?>",
                   "<?php echo $svgabsenttoday; ?>",
                   "<?php echo $urpabsenttoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>ABSENT</td>
        <td><?php echo $arcabsenttoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($arcsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>ABSENT</td>
        <td><?php echo $bldabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($bldsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>ABSENT</td>
        <td><?php echo $estabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($estsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>ABSENT</td>
        <td><?php echo $qtsabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($qtssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>ABSENT</td>
        <td><?php echo $svgabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($svgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>ABSENT</td>
        <td><?php echo $urpabsenttoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($urpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>
<!--Script displaying statistices for lecturers Absent for other reasons present date for other reasons -->
<div class="alert alert-success" style="text-align:center; font-size:1.3em">
Chart (4) Showing SET Departments and Total Number of lecturers Absent for other reasons - <?php echo $today = date("F j, Y, g:i a"); ?>
</div>
<br><br>
<div class="row">

<div class="col-md-4">
<!--javascript -->
    
<canvas id="myBarChartSETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myBarChartSETothers").getContext('2d');
    
var myChart = new Chart(ctx, {
    type: 'bar',
     data: {
        labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets: [{
            label: 'Number of  lecturers absent for other reasons',
            data: ["<?php echo $arcotherstoday; ?>", 
                   "<?php echo $bldotherstoday; ?>", 
                   "<?php echo $estotherstoday; ?>",
                   "<?php echo $qtsotherstoday; ?>",
                   "<?php echo $svgotherstoday; ?>",
                   "<?php echo $urpotherstoday; ?>"
                   
                  ],
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderColor:['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
    </div>
    

<div class="col-md-4">
    <!--javascript -->
    
<canvas id="myPieChartSETothers" width="400" height="250"></canvas>
<script>
var ctx = document.getElementById("myPieChartSETothers");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["ARC", "BLD", "EST", "QTS", "SVG", "URP"],
        datasets : [
            {
            label: 'Points',
            backgroundColor: ['#e67e22','#16a085','#2980b9', '#f70341','#f095f3','#f00891'],
            data: ["<?php echo $arcotherstoday; ?>", 
                   "<?php echo $bldotherstoday; ?>", 
                   "<?php echo $estotherstoday; ?>",
                   "<?php echo $qtsotherstoday; ?>",
                   "<?php echo $svgotherstoday; ?>",
                   "<?php echo $urpotherstoday; ?>"
                   
                  ]
            }
        ]
         
    },
    options: {
        animation:{
        animateScale: true
    }
  }
});
</script>
</div>
     <div class="col-md-4">
        <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturer Absent Other Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
<table class="table table-hover">
    <th>#SN</th>
    <th>DEPARTMENT</th>
    <th>STATUS</th>
    <th>STATISTICS</th>
    <tr>
        <td>1</td>
        <td>ARC</td>
        <td>OTHERS</td>
        <td><?php echo $arcotherstoday . " out of";?>
        
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($arcsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>BLD</td>
        <td>OTHERS</td>
        <td><?php echo $bldotherstoday . " out of";?>
            <!-- Output Summmary of Supposed lecture to be held-->
            <?php
            foreach($bldsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    
     <tr>
        <td>3</td>
        <td>EST</td>
        <td>OTHERS</td>
        <td><?php echo $estotherstoday . " out of";?>
            <?php
            foreach($estsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
         </td>
    </tr>
    <tr>
        <td>4</td>
        <td>QTS</td>
        <td>OTHERS</td>
        <td><?php echo $qtsotherstoday . " out of";?>
            <?php
            foreach($qtssummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>SVG</td>
        <td>OTHERS</td>
        <td><?php echo $svgotherstoday . " out of";?>
        
            <?php
            foreach($svgsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>URP</td>
        <td>OTHERS</td>
        <td><?php echo $urpotherstoday . " out of";?>
            <?php
            foreach($urpsummary as $num_lecture): ?>
            <?php echo $num_lecture->num_lecture .  " ". "Lectures"; ?>
            <?php endforeach; ?>
        </td>
    </tr>
  
</table>
</div>
</div>

<br><br>