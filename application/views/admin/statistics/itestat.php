<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em">Industrial and Technical Education Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $iteheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $iteheld['service_no']; ?></td>

                    <td>
                        <?php echo $iteheld['lecturer_fname']; ?>
                        <?php echo $iteheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $iteheld['faculty_name']; ?></td>
                    <td><?php echo $iteheld['dept_name']; ?></td>
                    <td><?php echo $iteheld['course_code']; ?></td>
                    <td><?php echo $iteheld['level_name']; ?></td>
                    <td><?php echo $iteheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $iteheld['start_time']; ?> -  <?php echo $iteheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $iteabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $iteabsent['service_no']; ?></td>

                    <td>
                        <?php echo $iteabsent['lecturer_fname']; ?>
                        <?php echo $iteabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $iteabsent['faculty_name']; ?></td>
                    <td><?php echo $iteabsent['dept_name']; ?></td>
                    <td><?php echo $iteabsent['course_code']; ?></td>
                    <td><?php echo $iteabsent['level_name']; ?></td>
                    <td><?php echo $iteabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $iteabsent['start_time']; ?> -  <?php echo $iteabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $itepostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $itepostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $itepostponed['lecturer_fname']; ?>
                        <?php echo  $itepostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $itepostponed['faculty_name']; ?></td>
                    <td><?php echo  $itepostponed['dept_name']; ?></td>
                    <td><?php echo  $itepostponed['course_code']; ?></td>
                    <td><?php echo  $itepostponed['level_name']; ?></td>
                    <td><?php echo  $itepostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $itepostponed['start_time']; ?> -  <?php echo $itepostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $iteothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $iteothers['service_no']; ?></td>

                    <td>
                        <?php echo $iteothers['lecturer_fname']; ?>
                        <?php echo $iteothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $iteothers['faculty_name']; ?></td>
                    <td><?php echo $iteothers['dept_name']; ?></td>
                    <td><?php echo $iteothers['course_code']; ?></td>
                    <td><?php echo $iteothers['level_name']; ?></td>
                    <td><?php echo $iteothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $iteothers['start_time']; ?> -  <?php echo $iteothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

