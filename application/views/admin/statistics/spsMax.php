
<hr>
<div class="container"><h2>Register Daily Maximum Lectures for the Day for Each Department</h2></div>

<div id="exTab2" class="container">	
        <ul class="nav nav-tabs">
			<li class="active"> 
            <a href="#1" data-toggle="tab">Mathematics</a></li>
			<li><a href="#2" data-toggle="tab">Physics</a></li>
			<li><a href="#3" data-toggle="tab">Chemistry</a></li>
            <li><a href="#4" data-toggle="tab">Geography</a></li>
            <li><a href="#5" data-toggle="tab">Geology</a></li>
            <li><a href="#6" data-toggle="tab">Statistics</a></li>
            
		</ul>

			<div class="tab-content ">
            <div class="tab-pane active" id="1">
            <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
          
                <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger alert-dismissible">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Mathematics');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxmath' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
              </div>
            <div class="tab-pane" id="2">  <!-- Tab Pane for SEET-->
                    <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
           
                <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Physics');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxphy' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
            
                    
                    
                    
</div>
            <div class="tab-pane" id="3">  <!-- Tab Pane for SAAT-->
                    <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
            <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Chemistry');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxchemis' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
            
                    
                    
                    
</div>
            <div class="tab-pane" id="4">  <!-- Tab Pane for SEET-->
                    <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
            <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Geography');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxgeo' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
            
                    
                    
                    
</div>   
            <div class="tab-pane" id="5">  <!-- Tab Pane for SEET-->
                    <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
            <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Geology');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxgeol' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
            
                    
                    
                    
</div>
            <div class="tab-pane" id="6">  <!-- Tab Pane for SEET-->
                    <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
            <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Submit Daily Maximum Lectures - Statistics');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Daily_max_lecture_controller/submitdailymaxstat' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[0]" value="Monday">
							<input type="text" class="form-control" name = "num_lecture[0]" placeholder="Monday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[1]" value="Tuesday">
							<input type="text" class="form-control" name = "num_lecture[1]" placeholder="Tuesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[2]" value="Wednesday">
							<input type="text" class="form-control" name = "num_lecture[2]" placeholder="Wednesday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[3]" value="Thursday">
							<input type="text" class="form-control" name = "num_lecture[3]" placeholder="Thursday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[4]" value="Friday">
							<input type="text" class="form-control" name = "num_lecture[4]" placeholder="Friday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[5]" value="Saturday">
							<input type="text" class="form-control" name = "num_lecture[5]" placeholder="Saturday" autofocus>
						</div>
					</div>
					</div>
                    <div class="form-group">
					<div class="col-sm-12">
						
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
                            <input type="hidden" class="form-control" name = "week_day[6]" value="Sunday">
							<input type="text" class="form-control" name = "num_lecture[6]" placeholder="Sunday" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit Daily Max');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
            
                    
                    
                    
</div>
        
			</div>
  </div>

<hr>
