<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Electrical Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $electheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $electheld['service_no']; ?></td>

                    <td>
                        <?php echo $electheld['lecturer_fname']; ?>
                        <?php echo $electheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $electheld['faculty_name']; ?></td>
                    <td><?php echo $electheld['dept_name']; ?></td>
                    <td><?php echo $electheld['course_code']; ?></td>
                    <td><?php echo $electheld['level_name']; ?></td>
                    <td><?php echo $electheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $electheld['start_time']; ?> -  <?php echo $electheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $electabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $electabsent['service_no']; ?></td>

                    <td>
                        <?php echo $electabsent['lecturer_fname']; ?>
                        <?php echo $electabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $electabsent['faculty_name']; ?></td>
                    <td><?php echo $electabsent['dept_name']; ?></td>
                    <td><?php echo $electabsent['course_code']; ?></td>
                    <td><?php echo $electabsent['level_name']; ?></td>
                    <td><?php echo $electabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $electabsent['start_time']; ?> -  <?php echo $electabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $electpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $electpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $electpostponed['lecturer_fname']; ?>
                        <?php echo  $electpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $electpostponed['faculty_name']; ?></td>
                    <td><?php echo  $electpostponed['dept_name']; ?></td>
                    <td><?php echo  $electpostponed['course_code']; ?></td>
                    <td><?php echo  $electpostponed['level_name']; ?></td>
                    <td><?php echo  $electpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $electpostponed['start_time']; ?> -  <?php echo $electpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $electothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $electothers['service_no']; ?></td>

                    <td>
                        <?php echo $electothers['lecturer_fname']; ?>
                        <?php echo $electothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $electothers['faculty_name']; ?></td>
                    <td><?php echo $electothers['dept_name']; ?></td>
                    <td><?php echo $electothers['course_code']; ?></td>
                    <td><?php echo $electothers['level_name']; ?></td>
                    <td><?php echo $electothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $electothers['start_time']; ?> -  <?php echo $electothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

