<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Mechanical Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $mechheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechheld['service_no']; ?></td>

                    <td>
                        <?php echo $mechheld['lecturer_fname']; ?>
                        <?php echo $mechheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechheld['faculty_name']; ?></td>
                    <td><?php echo $mechheld['dept_name']; ?></td>
                    <td><?php echo $mechheld['course_code']; ?></td>
                    <td><?php echo $mechheld['level_name']; ?></td>
                    <td><?php echo $mechheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechheld['start_time']; ?> -  <?php echo $mechheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $mechabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechabsent['service_no']; ?></td>

                    <td>
                        <?php echo $mechabsent['lecturer_fname']; ?>
                        <?php echo $mechabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechabsent['faculty_name']; ?></td>
                    <td><?php echo $mechabsent['dept_name']; ?></td>
                    <td><?php echo $mechabsent['course_code']; ?></td>
                    <td><?php echo $mechabsent['level_name']; ?></td>
                    <td><?php echo $mechabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechabsent['start_time']; ?> -  <?php echo $mechabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $mechpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $$mechpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $mechpostponed['lecturer_fname']; ?>
                        <?php echo  $mechpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $mechpostponed['faculty_name']; ?></td>
                    <td><?php echo  $mechpostponed['dept_name']; ?></td>
                    <td><?php echo  $mechpostponed['course_code']; ?></td>
                    <td><?php echo  $mechpostponed['level_name']; ?></td>
                    <td><?php echo  $mechpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $mechpostponed['start_time']; ?> -  <?php echo $mechpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $mechothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $mechothers['service_no']; ?></td>

                    <td>
                        <?php echo $mechothers['lecturer_fname']; ?>
                        <?php echo $mechothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $mechothers['faculty_name']; ?></td>
                    <td><?php echo $mechothers['dept_name']; ?></td>
                    <td><?php echo $mechothers['course_code']; ?></td>
                    <td><?php echo $mechothers['level_name']; ?></td>
                    <td><?php echo $mechothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $mechothers['start_time']; ?> -  <?php echo $mechothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

