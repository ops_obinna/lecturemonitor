<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Civil Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $civilheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $civilheld['service_no']; ?></td>

                    <td>
                        <?php echo $civilheld['lecturer_fname']; ?>
                        <?php echo $civilheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $civilheld['faculty_name']; ?></td>
                    <td><?php echo $civilheld['dept_name']; ?></td>
                    <td><?php echo $civilheld['course_code']; ?></td>
                    <td><?php echo $civilheld['level_name']; ?></td>
                    <td><?php echo $civilheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $civilheld['start_time']; ?> -  <?php echo $civilheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $civilabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $civilabsent['service_no']; ?></td>

                    <td>
                        <?php echo $civilabsent['lecturer_fname']; ?>
                        <?php echo $civilabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $civilabsent['faculty_name']; ?></td>
                    <td><?php echo $civilabsent['dept_name']; ?></td>
                    <td><?php echo $civilabsent['course_code']; ?></td>
                    <td><?php echo $civilabsent['level_name']; ?></td>
                    <td><?php echo $civilabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $civilabsent['start_time']; ?> -  <?php echo $civilabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $civilpostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $civilpostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $civilpostponed['lecturer_fname']; ?>
                        <?php echo  $civilpostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $civilpostponed['faculty_name']; ?></td>
                    <td><?php echo  $civilpostponed['dept_name']; ?></td>
                    <td><?php echo  $civilpostponed['course_code']; ?></td>
                    <td><?php echo  $civilpostponed['level_name']; ?></td>
                    <td><?php echo  $civilpostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo $civilpostponed['start_time']; ?> -  <?php echo $civilpostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $civilothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $civilothers['service_no']; ?></td>

                    <td>
                        <?php echo $civilothers['lecturer_fname']; ?>
                        <?php echo $civilothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $civilothers['faculty_name']; ?></td>
                    <td><?php echo $civilothers['dept_name']; ?></td>
                    <td><?php echo $civilothers['course_code']; ?></td>
                    <td><?php echo $civilothers['level_name']; ?></td>
                    <td><?php echo $civilothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $civilothers['start_time']; ?> -  <?php echo $civilothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

