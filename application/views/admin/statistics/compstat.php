<div class="row"><div class="alert alert-success" style="text-align:center; font-size:1.9em"> Computer Engineering Daily Lecture Summary</div></div>
<br>
<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Held Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_held) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_held as $compheld) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $compheld['service_no']; ?></td>

                    <td>
                        <?php echo $compheld['lecturer_fname']; ?>
                        <?php echo $compheld['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $compheld['faculty_name']; ?></td>
                    <td><?php echo $compheld['dept_name']; ?></td>
                    <td><?php echo $compheld['course_code']; ?></td>
                    <td><?php echo $compheld['level_name']; ?></td>
                    <td><?php echo $compheld['lecture_status']; ?></td>
                    <td>
                        <?php echo $compheld['start_time']; ?> -  <?php echo $compheld['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecture Held Today </p>
	<?php endif; ?>
    
    
       
    
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Absent Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_absent) : ?>

<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_absent as $compabsent) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $compabsent['service_no']; ?></td>

                    <td>
                        <?php echo $compabsent['lecturer_fname']; ?>
                        <?php echo $compabsent['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $compabsent['faculty_name']; ?></td>
                    <td><?php echo $compabsent['dept_name']; ?></td>
                    <td><?php echo $compabsent['course_code']; ?></td>
                    <td><?php echo $compabsent['level_name']; ?></td>
                    <td><?php echo $compabsent['lecture_status']; ?></td>
                    <td>
                        <?php echo $compabsent['start_time']; ?> -  <?php echo $compabsent['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No absent Lecturer </p>
	<?php endif; ?>
    
    
    
    
    
    </div>    

</div>

<div class="row">
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lectures Postponed Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_postponed) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_postponed as  $comppostponed) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo  $comppostponed['service_no']; ?></td>

                    <td>
                        <?php echo  $comppostponed['lecturer_fname']; ?>
                        <?php echo  $comppostponed['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo  $comppostponed['faculty_name']; ?></td>
                    <td><?php echo  $comppostponed['dept_name']; ?></td>
                    <td><?php echo  $comppostponed['course_code']; ?></td>
                    <td><?php echo  $comppostponed['level_name']; ?></td>
                    <td><?php echo  $comppostponed['lecture_status']; ?></td>
                    <td>
                        <?php echo  $comppostponed['start_time']; ?> -  <?php echo $comppostponed['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
     <?php else : ?>
	<p> No Postponed Lecture Today </p>
	<?php endif; ?>
    
    </div>
    
<div class="col-md-6">
    <div class="alert alert-warning" style="text-align:center; font-size:1.3em">
 Lecturers Absent for Other Reasons Today  - <?php echo $today = date("F j, Y, g:i a"); ?>
        </div>
    <?php if($lecture_others) : ?>
<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					
					
					
					<th>PF.No</th>

                    <th>Name</th>
                    <th>Faculty</th>
                    <th>dept_name</th>
                    <th>Course</th>
                    <th>Level</th>
                    <th>status</th>
                    <th>Period</th>

					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lecture_others as $compothers) : ?>
				   
				<tr class="odd gradeX">
					
					
					<td><?php echo $compothers['service_no']; ?></td>

                    <td>
                        <?php echo $compothers['lecturer_fname']; ?>
                        <?php echo $compothers['lecturer_sname']; ?>
                    
                    </td>
					<td><?php echo $compothers['faculty_name']; ?></td>
                    <td><?php echo $compothers['dept_name']; ?></td>
                    <td><?php echo $compothers['course_code']; ?></td>
                    <td><?php echo $compothers['level_name']; ?></td>
                    <td><?php echo $compothers['lecture_status']; ?></td>
                    <td>
                        <?php echo $compothers['start_time']; ?> -  <?php echo $compothers['end_time']; ?>
                    
                    </td>

					
									
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
    <?php else : ?>
	<p> No Lecturer Absent for any other reasons </p>
	<?php endif; ?>
    </div>  
    

</div>

