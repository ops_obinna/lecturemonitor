
<hr>

<div id="exTab2" class="container">	
        <ul class="nav nav-tabs">
			<li class="active"> 
            <a href="#1" data-toggle="tab">Semester/Session Settings</a></li>
            
		</ul>

			<div class="tab-content ">
            <div class="tab-pane active" id="1">
            <br>
            <br>
            <!--<?php echo validation_errors(); ?> -->
                <?php if($this->session->flashdata('danger')) : ?>
                <?php echo  '<div class="alert alert-danger alert-dismissible">'.$this->session->flashdata('danger').'</div>'; ?>
            <?php endif; ?>


            <div class="row">
            <div class="col-md-8">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                        <div class="panel-title" >
                            <i class="entypo-plus-circled"></i>
                            <?php echo ('Update Current Session e.g ... 2009/2010     or    First Semester');?>
                        </div>
                    </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Settings/session_semester' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					 <div class="form-group">
                                
                                
                                <label class="col-sm-3 control-label"><?php echo ('session_name');?></label>
								<div class="col-sm-5">
								<div style="color:red"><?php echo form_error('session_name'); ?></div>
                                <input type="text" class="form-control" name="session_name" placeholder="e.g 2009-2010">
                                    <br>
                                    	
                            </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Semester</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('semester_name'); ?></div>
							<select name="semester_name" class="form-control" data-validate="required" >
                              <option value="FIRST SEMESTER">FIRST SEMESTER</option>
							  <option value="SECOND SEMESTER">SECOND SEMESTER</option>
                            </select>
						</div> 
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('UPDATE SESSION');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>  
              </div>
            
            
               
            
            
            
            
   </div>
  </div>

<hr>
