<h3><span class="label label-primary">Today Lecture</span> Transport Management Technology</h3>
		<br />

		<?php if($tranmgt): ?>		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>Update_id</th>
					<th>Course_title</th>
					<th>Course_code</th>
					<th>Venue</th>
					<th>Department</th>
					<th>Faculty</th>
					<th>lecture_date</th>
					<th>Start_time</th>
					<th>End_time</th>
					<th>Status</th>
					<th>Service_No</th>
					<th>Matric_No</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($tranmgt as $tranmgt_list) : ?>
				   
				<tr class="odd gradeX">
					<td><?php echo $tranmgt_list['update_id']; ?></td>
					<td><?php echo $tranmgt_list['course_title']; ?></td>
					<td><?php echo $tranmgt_list['course_code']; ?></td>
					<td class="center"><?php echo $tranmgt_list['lecture_venue']; ?></td>
					<td class="center"><?php echo $tranmgt_list['dept_name']; ?></td>
					<td class="center"><?php echo $tranmgt_list['faculty_name']; ?></td>
					<td class="center"><?php echo $tranmgt_list['date']; ?></td>
					<td class="center"><?php echo $tranmgt_list['start_time']; 	?></td>
					<td><?php echo $tranmgt_list['end_time']; ?></td>
					<td><?php echo $tranmgt_list['lecture_status']; ?></td>
					<td><?php echo $tranmgt_list['service_no']; ?></td>
					<td><?php echo $tranmgt_list['rep_matric_no']; ?></td>
				
				</tr>

			</tbody>
			
		<?php endforeach; ?>
		</table>
	<?php else : ?>
	<p> No Lecture Update For Now </p>
	<?php endif; ?>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>