<br>
<br>
<!--<?php echo validation_errors(); ?> -->

<?php if($this->session->flashdata('success')) : ?>
	<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
<?php endif; ?>

<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Register Class Rep');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Registration_controller/registercourserep' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				
				
				
					<div class="form-group">
					<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('rep_matric_no'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-key"></i></span>
							<input type="text" class="form-control" name = "rep_matric_no" placeholder="Matriculation Number" value="<?php echo set_value('rep_matric_no'); ?>" autofocus>
						</div>
					</div>
					</div>

					
					<div class="form-group">
					<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('rep_fname'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-user"></i></span>
							<input type="text" class="form-control" name = "rep_fname" placeholder="First Name" value="<?php echo set_value('rep_fname'); ?>" autofocus>
						</div>
					</div>
					</div>

					

					<div class="form-group">
					<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('rep_sname'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-user"></i></span>
							<input type="text" class="form-control" name = "rep_sname" placeholder="Surname Name" value="<?php echo set_value('rep_sname'); ?>" autofocus>
						</div>
					</div>
					</div>



					<div class="form-group">
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('level_id'); ?></div>
							<select name="level_id" class="form-control" data-validate="required" id="level_id" onchange="return get_class_sections(this.value)">
                              <option value="">Level ID</option>
                             	
                             	<?php 
								$levels_table = $this->db->get('level_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['level_id'];?>">
											<?php echo $row['level_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>



					<div class="form-group">
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('dept_id'); ?></div>
							<select name="dept_id" class="form-control" data-validate="required" id="dept_id" onchange="return get_class_sections(this.value)">
                              <option value="">DEPARTMENT ID</option>

                              <?php 
								$levels_table = $this->db->get('department_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['dept_id'];?>">
											<?php echo $row['dept_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
						</div> 
					</div>



				<div class="form-group">
						<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('phone'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-phone"></i></span>
							<input type="text" class="form-control" name = "phone" placeholder="Phone" value="<?php echo set_value('phone'); ?>" autofocus>
						</div>
					</div>
					</div>


					<div class="form-group">
						<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('email'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-mail"></i></span>
							<input type="text" class="form-control" name = "email" placeholder="Email" value="<?php echo set_value('email'); ?>" autofocus>
						</div>
					</div>
					</div>


					<div class="form-group">
						<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('password'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-lock"></i></span>
							<input type="text" class="form-control" name = "password" placeholder="Password">
						</div>
					</div>
					</div>

					

				
							
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Image Upload</label>
								
								<div class="col-sm-5">
									<div style="color:red"><?php echo form_error('rep_img'); ?></div>
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
											<img src="http://placehold.it/200x150" alt="...">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 6px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="rep_img" accept="image/*">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
									
								</div>
							</div>
						


					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

<div class="col-md-4">
		<blockquote class="blockquote-blue">
			<p>
				<strong>Class Representative Registration Note</strong>
			</p>
			<p>
				Take note that the password you created should be used as provided by the class representative during registrattion because this will be used for subsequent login
			</p>
		</blockquote>
	</div>
</div>



