<br>
<br>
<!--<?php echo validation_errors(); ?> -->
<?php if($this->session->flashdata('success')) : ?>
<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
<?php endif; ?>


<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Create Department');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
            
			


				<div class="panel-body">

				<?php echo form_open('admin/Registration_controller/registerdept' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
				
								
					<div class="form-group">
					<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('dept_name'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<input type="text" class="form-control" name = "dept_name" placeholder="Department Name" value="<?php echo set_value('dept_name'); ?>" autofocus>
						</div>
					</div>
					</div>

					
					<div class="form-group">
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('faculty_id'); ?></div>
							<select name="faculty_id" class="form-control" data-validate="required" id="faculty_id" onchange="return get_class_sections(this.value)">
                              <option value="">FACULTY ID</option>

                              <?php 
								$faculty_table = $this->db->get('faculty_table')->result_array();
								foreach($faculty_table as $row):
									?>
                            		<option value="<?php echo $row['faculty_id'];?>">
											<?php echo $row['faculty_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
						</div> 
					</div>



					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Create Department');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->


</div>