<br>
<br>
<!--<?php echo validation_errors(); ?> -->
<?php if($this->session->flashdata('success')) : ?>
	<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
<?php endif; ?>


<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Create Lecture Venue');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Registration_controller/registervenue' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

				

				
								
					<div class="form-group">
					<div class="col-sm-12">
						<div style="color:red"><?php echo form_error('venue_name'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<input type="text" class="form-control" name = "venue_name" placeholder="Venue Name" value="<?php echo set_value('venue_name'); ?>" autofocus>
						</div>
					</div>
					</div>

					

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Create Venue');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

	
</div>