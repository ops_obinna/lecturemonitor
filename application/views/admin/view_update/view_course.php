<h3><span class="label label-success">List of Registered Courses with QAP</span> View and Update Courses</h3>
		<br />

		<!-- The below PHP tags Displays Success Message onlly when a course is been updated-->
		<?php if($this->session->flashdata('success')) : ?>
			<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>course_id</th>
					<th>Course_title</th>
					<th>Course_code</th>
					<th>credit_unit</th>
					<th>credit_hour</th>
                    <th>Lecture_venue</th>
					<th>department</th>
					<th>level</th>
					<th>Actions</th>
					
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_course as $courses_list) : ?>
				   
				<tr class="odd gradeX">
					<td><?php echo $courses_list->course_id; ?></td>
					<td><?php echo $courses_list->course_title; ?></td>
					<td><?php echo $courses_list->course_code; ?></td>
					<td class="center"><?php echo $courses_list->credit_unit; ?></td>
					<td class="center"><?php echo $courses_list->credit_hour; ?></td>
                    <td class="center"><?php echo $courses_list->venue_name; ?></td>
					<td class="center"><?php echo $courses_list->dept_name; ?></td>
					<td class="center"><?php echo $courses_list->level_name; ?></td>
					<td>
                        
                        
                        <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT COURSES LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Update_view_controller/edit_courses/' .$courses_list->course_id.'','Edit','class="entypo-pencil"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE LINK -->
                                        <li>
                                            
                                            <?php echo anchor('admin/Update_view_controller/delete_courses/' .$courses_list->course_id.'','Delete','class="entypo-trash"'); ?>
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>   
                        
                        
						
						
					</td>
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>