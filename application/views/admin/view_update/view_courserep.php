<h3><span class="label label-success">List of Registered Course Representatives with QAP</span> View and Update Course Reps</h3>
		<br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>Image</th>
					<th>Matric Number</th>
					<th>First Name</th>
					<th>Surname</th>
					<th>Level</th>
					<th>Department</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Actions</th>				
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_courserep as $reps_list) : ?>
<!--<img src="<?//php echo base_url().'upload/classreps/'.$reps_list->rep_img; ?>" class="img-circle" width="30" >-->
				<tr class="odd gradeX">
                    
					<td><img src="<?php echo $this->Update_view_model->get_image_url($reps_list->rep_id);?>" class="img-circle" width="30" /> </td>
					<td><?php echo $reps_list->rep_matric_no; ?></td>
					<td><?php echo $reps_list->rep_fname; ?></td>
					<td><?php echo $reps_list->rep_sname; ?></td>
					<td class="center"><?php echo $reps_list->level_name; ?></td>
					<td class="center"><?php echo $reps_list->dept_name; ?></td>
					<td class="center"><?php echo $reps_list->email; ?></td>
					<td class="center"><?php echo $reps_list->phone; ?></td>
					<td>
						 <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT CLASS REPS LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Update_view_controller/edit_coursereps/' .$reps_list->rep_id.'','Edit', 'class="entypo-pencil"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE PROFILE LINK -->
                                        <li>
                                            
                                            <?php echo anchor('admin/Update_view_controller/delete_coursereps/' .$reps_list->rep_id.'','Delete','class="entypo-trash"'); ?> 
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>
					</td>
                    
						
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>





