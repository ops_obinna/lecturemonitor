<h3><span class="label label-success">List of Registered Course Lecturers with QAP</span> View and Update Course Lecturers</h3>
		<br />
		
		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>ID</th>
					<th>Image</th>
					<th>First Name</th>
					<th>Surname</th>
					<th>Service No</th>
					<th>Dept_ID</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Actions</th>
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_lecturer as $lecturer_list) : ?>
				   
				<tr class="odd gradeX">
					<td><?php echo $lecturer_list->lecturer_id; ?></td>
					<td><img src="<?php echo $this->Update_view_model->get_image_url($lecturer_list->lecturer_id);?>" class="img-circle" width="30" /></td>
					<td><?php echo $lecturer_list->lecturer_fname; ?></td>
					<td><?php echo $lecturer_list->lecturer_sname; ?></td>
					<td class="center"><?php echo $lecturer_list->service_no; ?></td>
					<td class="center"><?php echo $lecturer_list->dept_name; ?></td>
					<td class="center"><?php echo $lecturer_list->phone; ?></td>
					<td class="center"><?php echo $lecturer_list->email_address; ?></td>
					<td>
                        
						<div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT LECTURERS LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Update_view_controller/edit_lecturers/' .$lecturer_list->lecturer_id.'','Edit','class="entypo-pencil"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE PROFILE LINK -->
                                        <li>
                                            
                                            <?php echo anchor('admin/Update_view_controller/delete_lecturers/' .$lecturer_list->lecturer_id.'','Delete','class="entypo-trash"'); ?>
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>                   
                        
					</td>
					
				
				</tr>

			<?php endforeach ; ?>
				
			</tbody>
			
		</table>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>