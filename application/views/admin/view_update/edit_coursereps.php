<br>
<br>
<!--<?php echo validation_errors(); ?> -->



<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Update Course Representative Portal');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php 
				$multi = array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data');

				echo form_open('admin/Update_view_controller/edit_coursereps/'. $item->rep_id , $multi);?>

				

				
				<div class="form-group">
					<?php
							$data  = array(
								'name' => 'rep_matric_no', 
								'id'	=> 'rep_matric_no',
								'placeholder' => 'Registration Number',
								'class' => 'form-control',
								'value' => $item->rep_matric_no

								);
							?>
						<div style="color:red"><?php echo form_error('rep_matric_no'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-key"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
				</div>	

					
					<div class="form-group">
						<?php
							$data  = array(
								'name' => 'rep_fname', 
								'id'	=> 'rep_fname',
								'placeholder' => 'First Name',
								'class' => 'form-control',
								'value' => $item->rep_fname

								);
							?>
						<div style="color:red"><?php echo form_error('rep_fname'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-user"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
					</div>

					

					<div class="form-group">
						<?php
							$data  = array(
								'name' => 'rep_sname', 
								'id'	=> 'rep_sname',
								'placeholder' => 'Surname Name',
								'class' => 'form-control',
								'value' => $item->rep_sname

								);
							?>
					
						<div style="color:red"><?php echo form_error('rep_sname'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-user"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
					</div>



					<div class="form-group">
						<div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('level_id'); ?></div>
							<select name="level_id" class="form-control" data-validate="required" id="level_id" onchange="return get_class_sections(this.value)">
                              <option value="">Level ID</option>
                             	
                             	<?php 
								$levels_table = $this->db->get('level_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['level_id'];?>">
											<?php echo $row['level_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
                         </div>
					</div>



					<div class="form-group">
						<div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('dept_id'); ?></div>
							<select name="dept_id" class="form-control" data-validate="required" id="dept_id" onchange="return get_class_sections(this.value)">
                              <option value="">DEPARTMENT ID</option>

                              <?php 
								$levels_table = $this->db->get('department_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['dept_id'];?>">
											<?php echo $row['dept_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
                         </div>
					</div>



				<div class="form-group">
					<?php
							$data  = array(
								'name' => 'phone', 
								'id'	=> 'phone',
								'placeholder' => 'Phone',
								'class' => 'form-control',
								'value' => $item->phone

								);
							?>
						<div style="color:red"><?php echo form_error('phone'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-phone"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
					</div>


					<div class="form-group">
						<?php
							$data  = array(
								'name' => 'email', 
								'id'	=> 'email',
								'placeholder' => 'Email Address',
								'class' => 'form-control',
								'value' => $item->email

								);
							?>

						<div style="color:red"><?php echo form_error('email'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-mail"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
					</div>


					<div class="form-group">
						<?php
							$data  = array(
								'name' => 'password', 
								'id'	=> 'password',
								'placeholder' => 'password',
								'class' => 'form-control',
								'value' => $item->password

								);
							?>
						<div style="color:red"><?php echo form_error('password'); ?></div>
						<div class="col-sm-12">
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-lock"></i></span>
							<?php echo form_input($data); ?>
						</div>
						</div>
					</div>

					

				
							
							
							<div class="form-group">
                                
								<label class="col-sm-3 control-label">Image Upload</label>
									<div class="col-sm-5">
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
											<img src="<?php echo $this->Update_view_model->get_image_url($item->rep_id);?>" alt="...">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 6px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="rep_img" accept="image/*">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
								</div>
							</div>
						


					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Update');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

<div class="col-md-4">
		<blockquote class="blockquote-blue">
			<p>
				<strong>Class Representative Registration Note</strong>
			</p>
			<p>
				Take note that the password you created should be used as provided by the class representative during registrattion because this will be used for subsequent login
			</p>
		</blockquote>
	</div>
</div>



