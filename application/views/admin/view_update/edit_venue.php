<br>
<br>

<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Update Lecture Venue');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body">

				<?php echo form_open('admin/Update_view_controller/edit_venue/'. $item->venue_id); ?>

					
								
					<div class="form-group">
						<?php

							$data  = array(
								'name' => 'venue_name', 
								'id'	=> 'venue_name',
								'placeholder' => 'venue name',
								'class' => 'form-control',
								'value' => $item->venue_name

								);
							?>
					
						<div style="color:red"><?php echo form_error('venue_name'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<?php echo form_input($data); ?>
						</div>
					
					</div>
	

									

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<?php echo form_submit('mysubmit','Update Venue', array('class' => 'btn btn-primary')); ?>
						</div>
					</div>
                <?php echo form_close();?>


</div> <!-- Panel Close-->
</div> <!-- Panel Close-->
</div> <!-- Panel Close-->
</div>