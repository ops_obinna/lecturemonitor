<h3><span class="label label-success">List of Registered Faculty with QAP</span> View and Update Faculty</h3>
		<br />
		<!-- The below PHP tags Displays Success Message only when a faculty is been updated-->
		<?php if($this->session->flashdata('success')) : ?>
			<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>Faculty ID</th>
					<th>Faculty Name</th>
					<th>Actions</th>
					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_faculty as $faculty_list) : ?>
				   
				<tr class="odd gradeX">
					<td><?php echo $faculty_list->faculty_id; ?></td>
					<td><?php echo $faculty_list->faculty_name; ?></td>
					<td>
                        
                        
                        
                        <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT FACULTY LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Update_view_controller/edit_faculty/' .$faculty_list->faculty_id.'','Edit','class="entypo-pencil"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE PROFILE LINK -->
                                        <li>
                                            
<!--                                            <//?php echo anchor('admin/Update_view_controller/delete_faculty/' .$faculty_list->faculty_id.'','Delete','class="entypo-trash"'); ?>-->
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>      
					</td>
					
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>