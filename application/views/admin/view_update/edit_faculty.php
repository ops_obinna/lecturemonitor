<br>
<br>

<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Faculty Update Form');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			
				<div class="panel-body">

				<?php echo form_open('admin/Update_view_controller/edit_faculty/'. $item->faculty_id); ?>						
					<div class="form-group">

						<?php
						$data = array(
							'name' 			=> 'faculty_name', 
							'id'			=> 'faculty_name',
							'placeholder'	=> 'Faculty Name',
							'class' 		=> 'form-control',
							'value' 		=>  $item->faculty_name
							); 
							?>

					
						<div style="color:red"><?php echo form_error('faculty_name'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<?php echo form_input($data); ?>
						</div>
					</div>		

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Update Faculty');?></button>
						</div>
					</div>
                <?php echo form_close();?>
	</div> <!-- Panel Close-->
</div> <!-- Panel Close-->
</div> <!-- Panel Close-->
</div>