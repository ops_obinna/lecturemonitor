<br>
<br>




<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Update Department');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
            
			


				<div class="panel-body">

				<?php echo form_open('admin/Update_view_controller/edit_department/'. $item->dept_id);?>

				

				
								
					<div class="form-group">
						<?php

							$data  = array(
								'name' => 'dept_name', 
								'id'	=> 'dept_name',
								'placeholder' => 'Department name',
								'class' => 'form-control',
								'value' => $item->dept_name

								);
							?>
					<div style="color:red"><?php echo form_error('dept_name'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<?php echo form_input($data); ?>
						</div>
					</div>

					

									
					<div class="form-group">
					 
					 	<div style="color:red"><?php echo form_error('faculty_id'); ?></div>
							<select name="faculty_id" class="form-control" data-validate="required" id="faculty_id" onchange="return get_class_sections(this.value)">
                              <option value="">FACULTY ID</option>

                              <?php 
								$faculty_table = $this->db->get('faculty_table')->result_array();
								foreach($faculty_table as $row):
									?>
                            		<option value="<?php echo $row['faculty_id'];?>">
											<?php echo $row['faculty_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
						
					</div>



					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Update Department');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->


</div>