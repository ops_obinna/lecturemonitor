<h3><span class="label label-success">List of Registered Lecture Venues with QAP</span> View and Update Lecture Venues</h3>
		<br />

		<!-- The below PHP tags Displays Success Message onlly when a venue is been updated-->
		<?php if($this->session->flashdata('success')) : ?>
			<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>Venue ID</th>
					<th>Venue Name</th>
					<th>Actions</th>
					
								
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_venue as $venues_list) : ?>
				   
				<tr class="odd gradeX">
					<td><?php echo $venues_list->venue_id; ?></td>
					<td><?php echo $venues_list->venue_name; ?></td>
					<td>
                        
                        
                        <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT VENUE LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Update_view_controller/edit_venue/' .$venues_list->venue_id.'','Edit','class="entypo-pencil"'); ?>
                                                                                                  
                                        </li>

                                        
                                        <!-- DELETE LINK -->
                                        <li>
                                            
                                            <?php echo anchor('admin/Update_view_controller/delete_venue/' .$venues_list->venue_id.'','Delete','class="entypo-trash"'); ?>
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>   
						
							</td>
					
					
				</tr>
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-4").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>  