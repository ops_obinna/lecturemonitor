<br>
<br>

<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('Update Course Portal');?>
            	</div>
            </div>

            
				<div class="panel-body">
                     <?php $att = array(
                                'class' => 'form-horizontal form-groups-bordered validate', 
                                'enctype' => 'multipart/form-data') ;?>
				<?php echo form_open('admin/Update_view_controller/edit_courses/'. $item->course_id , $att); ?>
					<div class="form-group">
                        <div class="col-sm-12">
						<?php
							$data  = array(
								'name' 			=> 'course_title', 
								'id'			=> 'course_title',
								'placeholder'	=> 'Course Title',
								'class' 		=> 'form-control',
								'value' 		=> $item->course_title
								);
							?>
						<div style="color:red"><?php echo form_error('course_title'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<?php echo form_input($data); ?>
						</div>
                        </div>
					</div>

					

					<div class="form-group">
                        <div class="col-sm-12">
						<?php
							$data  = array(
								'name' 			=> 'course_code', 
								'id'			=> 'course_code',
								'placeholder' 	=> 'Course Code',
								'class' 		=> 'form-control',
								'value' 		=> $item->course_code
								);
							?>
					<div style="color:red"><?php echo form_error('course_code'); ?></div>
						<div class="input-group minimal">
							<span class="input-group-addon"><i class="entypo-book"></i></span>
							<?php echo form_input($data); ?>
						</div>
                        </div>
					</div>

                    
                <div class="form-group">
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('faculty_id'); ?></div>
							<select name="faculty_id" class="form-control" data-validate="required" id="faculty_id" onchange="return get_class_sections(this.value)">
                              <option value="">FACULTY ID</option>

                              <?php 
								$faculty_table = $this->db->get('faculty_table')->result_array();
								foreach($faculty_table as $row):
									?>
                            		<option value="<?php echo $row['faculty_id'];?>">
											<?php echo $row['faculty_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
						</div> 
					</div>

					<div class="form-group">
                        <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('dept_id'); ?></div>
							<select name="dept_id" class="form-control" data-validate="required" id="dept_id" onchange="return get_class_sections(this.value)">
                              <option value="">DEPARTMENT ID</option>
                              <?php 
								$levels_table = $this->db->get('department_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['dept_id'];?>">
											<?php echo $row['dept_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                             </select>
                        </div>
					</div>
                    
                    <div class="form-group">
                        <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('lecturer_id'); ?></div>
							<select name="lecturer_id" class="form-control" data-validate="required" id="lecturer_id" onchange="return get_class_sections(this.value)">
                              <option value="">TAUGHT BY</option>

                              <?php 
								$lecturer_table = $this->db->get('lecturer_table')->result_array();
								foreach($lecturer_table as $row):
									?>
                            		<option value="<?php echo $row['lecturer_id'];?>">
											<?php echo $row['lecturer_fname'] . ' '.  ''. $row['lecturer_sname'] . ' '.  ''. '---'.$row['service_no'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                              
                             </select>
                        </div>
					</div>






					<div class="form-group">
					   <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('venue_id'); ?></div>
							<select name="venue_id" class="form-control" data-validate="required" id="venue_id" onchange="return get_class_sections(this.value)">
                              <option value="">VENUE ID</option>
                              <?php 
								$levels_table = $this->db->get('venue_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['venue_id'];?>">
											<?php echo $row['venue_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                             </select>
                        </div>
					</div>




					<div class="form-group">
                        <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('level_id'); ?></div>
							<select name="level_id" class="form-control" data-validate="required" id="level_id" onchange="return get_class_sections(this.value)">
                              <option value="">Level ID</option>
                             	
                             	<?php 
								$levels_table = $this->db->get('level_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['level_id'];?>">
											<?php echo $row['level_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                             </select>
                        </div>
					</div>

					<div class="form-group">
                        <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('credit_unit'); ?></div>
							<select name="credit_unit" class="form-control" data-validate="required" id="credit_unit" onchange="return get_class_sections(this.value)">
                              	<option value="">CREDIT UNIT</option>
                             	<option value="1"> 1 </option>
                             	<option value="2"> 2 </option>
                             	<option value="3"> 3 </option>
                                <option value="4"> 4 </option>
                                <option value="5"> 5 </option>
                                <option value="6"> 6 </option>				

                             </select>
                        </div>
					</div>

					<div class="form-group">
                        <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('credit_hour'); ?></div>
							<select name="credit_hour" class="form-control" data-validate="required" id="credit_hour" onchange="return get_class_sections(this.value)">
                              	<option value="">CREDIT HOUR</option>
                             	<option value="1"> 1 </option>
                             	<option value="2"> 2 </option>
                             	<option value="3"> 3 </option>
                                <option value="4"> 4 </option>
                                <option value="5"> 5 </option>
                                <option value="6"> 6 </option>				

                             </select>
                        </div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Update Course');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->


</div>