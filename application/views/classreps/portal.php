<br>
<br>
<div class="row">
	<div class="col-md-8">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo ('LECTURE UPDATE PORTAL....! ') . " ". "welcome". " ". $this->session->userdata('email');?>
            	</div>
            </div>

            <!-- Display Form Validation Errors if any -->
			


				<div class="panel-body alert-success">

				<?php echo form_open('classreps/Portal/updateLecture ' , array('class' => 'form-horizontal form-groups-bordered validate'));?>

          
		
					<div class="form-group">
                        <label class="col-sm-3 control-label">SESSION</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('session_name'); ?></div>
							<?php 
							$this->db->select('*');
							$this->db->from('session_table');
							$query = $this->db->get()->result();
							foreach($query as $row_session):
							?>
							<input type="text" class="form-control" name="session_name" value="<?php echo $row_session->session_name; ?>"  />
							
						</div> 
					</div>
                    
					
					
					<div class="form-group">
                        <label class="col-sm-3 control-label">SEMESTER</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('semester_name'); ?></div>
					
					<input type="text" class="form-control" name="semester_name" value="<?php echo $row_session->semester_name; ?>"  />
							<?php endforeach; ?>
						</div> 
					</div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">FACULTY</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('faculty_name'); ?></div>
							<select name="faculty_name" class="form-control" data-validate="required" id="facultyDrp" onchange="return get_class_sections(this.value)">
                              <option value="">FACULTY</option>
                             	
                             	<?php 
								
								foreach($facultyDrp as $row):
									?>
                            		<option value="<?php echo $row['faculty_id'];?>">
											<?php echo $row['faculty_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>
                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">DEPARTMENT</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('dept_name'); ?></div>
							<select name="dept_name" class="form-control" data-validate="required" id="deptDrp" onchange="return get_class_sections(this.value)">
                              <option value="">DEPARTMENT</option>
                             	
                             	<?php 
								
								foreach($deptDrp as $row):
									?>
                            		<option value="<?php echo $row['dept_name'];?>">
											<?php echo $row['dept_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>

				
                    
                     <div class="form-group">
                        <label class="col-sm-3 control-label">COURSE TITLE</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('course_title'); ?></div>
							<select name="course_title" class="form-control" data-validate="required" id="courseTitleDrp" onchange="return get_class_sections(this.value)">
                              <option value="">COURSE TITLE</option>
                             	
                             	<?php 
								
								foreach($courseTitleDrp as $row):
									?>
                            		<option value="<?php echo $row['course_id'];?>">
											<?php echo $row['course_id'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>


                     <div class="form-group">
                        <label class="col-sm-3 control-label">COURSE CODE</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('course_code'); ?></div>
							<select name="course_code" class="form-control" data-validate="required" id="courseCodeDrp" onchange="return get_class_sections(this.value)">
                              <option value="">COURSE CODE</option>
                             	
                             	<?php 
								
								foreach($courseCodeDrp as $row):
									?>
                            		<option value="<?php echo $row['course_id'];?>">
											<?php echo $row['course_code'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ASSIGNED VENUE</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('lecture_venue'); ?></div>
							<select name="lecture_venue" class="form-control" data-validate="required" id="venueDrp" onchange="return get_class_sections(this.value)">
                              <option value="">VENUE</option>
                             	
                             	
                             </select>
						</div> 
					</div>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">NEW VENUE</label>
					 <div class="col-sm-5">
					 	
							<select name="new_venue" class="form-control" data-validate="required" id="newVenueDrp" onchange="return get_class_sections(this.value)">
                              <option value="">VENUE</option>
                             	
                             	<?php 
								$venue_table = $this->db->get('venue_table')->result_array();
								foreach($venue_table as $row):
									?> 
                            		<option value="<?php echo $row['venue_name'];?>">
											<?php echo $row['venue_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">LEVEL</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('level_id'); ?></div>
							<select name="level_id" class="form-control" data-validate="required" id="level" onchange="return get_class_sections(this.value)">
                              <option value=""> LEVEL </option>
                             	
                             	<?php 
								$levels_table = $this->db->get('level_table')->result_array();
								foreach($levels_table as $row):
									?>
                            		<option value="<?php echo $row['level_id'];?>">
											<?php echo $row['level_name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>



				
                    
                    <div class="form-group">
								<label class="col-sm-3 control-label">LECTURE DATE</label>
								
								<div class="col-sm-4">
									<div class="input-group">
                                        <div style="color:red"><?php echo form_error('date'); ?></div>
										<input type="text" name="date" class="form-control datepicker" data-format=" yyyy-mm-dd" placeholder="Lecture Date">
										
										<div class="input-group-addon">
											<a href="#"><i class="entypo-calendar"></i></a>
										</div>
									</div>
								</div>
							</div>
                    
                    
                    <div class="form-group">
								<label class="col-sm-3 control-label">START TIME</label>
								
								<div class="col-sm-3">
									<div class="input-group">
                                        <div style="color:red"><?php echo form_error('start_time'); ?></div>
										<input type="text" name="start_time" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="11:25 AM" data-show-meridian="true" data-minute-step="5" data-second-step="5" />
										
										<div class="input-group-addon">
											<a href="#"><i class="entypo-clock"></i></a>
										</div>
									</div>
								</div>
							</div>
                    
                    
                    <div class="form-group">
								<label class="col-sm-3 control-label">END TIME</label>
								
								<div class="col-sm-3">
									<div class="input-group">
                                        <div style="color:red"><?php echo form_error('end_time'); ?></div>
										<input type="text" name="end_time" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="11:25 AM" data-show-meridian="true" data-minute-step="5" data-second-step="5" />
										
										<div class="input-group-addon">
											<a href="#"><i class="entypo-clock"></i></a>
										</div>
									</div>
								</div>
							</div>



					<div class="form-group">
                        <label class="col-sm-3 control-label">STATUS</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('lecture_status'); ?></div>
							<select name="lecture_status" class="form-control" data-validate="required" id="status" onchange="return get_class_sections(this.value)">
                              <option value="">LECTURE STATUS</option>
                             	
                             	<?php 
								$lecturestatus_table = $this->db->get('lecturestatus_table')->result_array();
								foreach($lecturestatus_table as $row):
									?>
                            		<option value="<?php echo $row['status'];?>">
											<?php echo $row['status'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>
                

                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">LECTURER</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('service_no'); ?></div>
							<select name="service_no" class="form-control" data-validate="required" id="lecturerDrp" onchange="return get_class_sections(this.value)">
                              <option value="">LECTURER</option>
                             	
                             	<?php foreach($lecturerDrp as $row):	?>
                            		<option value="<?php echo $row['service_no'];?>">
											<?php echo $row['service_no'];?>
                                    </option>
                                <?php endforeach; ?>

                             </select>
						</div> 
					</div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">LECTURER EMAIL</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('lecturer_email'); ?></div>
							<select name="lecturer_email" class="form-control" data-validate="required" id="lecturerEmailDrp" onchange="return get_class_sections(this.value)">
                              <option value="">LECTURER email</option>
                             </select>
						</div> 
					</div>
                    
                    
                    
                    
                    
                
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">MATRIC</label>
					 <div class="col-sm-5">
					 	<div style="color:red"><?php echo form_error('rep_matric_no'); ?></div>
							<select name="rep_matric_no" class="form-control" data-validate="required" id="rep_matric_no" onchange="return get_class_sections(this.value)">
                              
                             	
                             	<?php 
                                $email = $this->session->userdata('email');
                                $this->db->select('rep_id,rep_matric_no');
                                $this->db->from('course_reps_table');
                                $this->db->where('email', $email );
                                $query = $this->db->get()->result_array();
                                
								foreach($query as $row):
									?>
                            		<option value="<?php echo $row['rep_matric_no'];?>">
											<?php echo $row['rep_matric_no'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>

                             </select>
						</div> 
					</div>
                        
                    
    
					
                    
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo ('Submit');?></button>
						</div>
					</div>
                <?php echo form_close();?>


	</div> <!-- Panel Close-->



</div> <!-- Panel Close-->
</div> <!-- Panel Close-->

<div class="col-md-4">
		<blockquote class="blockquote-blue">
			<p>
				<strong>Class Representative Registration Note</strong>
			</p>
			<p>
				Take note that the password you created should be used as provided by the class representative during registrattion because this will be used for subsequent login
			</p>
		</blockquote>
	</div>
</div>



