<hr />
	



		
			
				
			
				
			

			<!----     ---------  -->
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">

			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-user"></i> 
					<?php echo ('Manage Profile');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div class="tab-content">
        <br>
        	<!----EDITING FORM STARTS---->
			<div class="tab-pane box active panel-body alert-success" id="list" style="padding: 5px">
                <div class="box-content">
						<?php 
                            foreach($rep as $rep_list):
                        ?>
                    
                        <?php 
                    
                        $multi = array(
                            'class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'
                                    );
                        ?>

				        
                       <?php echo form_open(base_url() . '/classreps/Manageprofile/rep_profile', $multi); ?> 
				
                            <div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-5"><?php echo validation_errors('<p class="alert alert-danger">'); ?></div>
							<div class="col-md-4"></div>
							</div>
                            <div class="form-group">
                                
                                
                                <label class="col-sm-3 control-label"><?php echo ('Email');?></label>
                                <div class="col-sm-5">
                                <input type="text" class="form-control" name="email" value="<?php echo $rep_list->email ; ?>">
                                    <br>
                                    	
                                
                                </div>
                            </div>

                            

                           <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ('Photo');?></label>
                                
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                        <img src="<?php echo $this->Manageprofile_model->get_image_url_rep($rep_list->rep_id);?>" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo ('Update Profile');?></button>
                              </div>
								</div>
                        <?php echo form_close(); ?>
						<?php
                    endforeach;
                    ?>
                </div>
			</div>
            <!----EDITING FORM ENDS-->
            
		</div>
	</div>
</div>

<br>

