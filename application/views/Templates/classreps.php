<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Course Representative | Dashboard</title>

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/purple.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.js"></script>  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.bundle.min.js"></script>

      
	<script>$.noConflict();</script>


	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
    
                   <script type="text/javascript"> 
                   
                   //fillup the department dropdown based on faculty selected
                  $(document).ready(function() {  
                     $("#facultyDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo  
                        base_url();?>classreps/Portal/buildDrpDepts",  
                        data: {id: $(this).val()},  
                        type: "POST",  
                        success:function(data){$("#deptDrp").html(data);  
                     }  
                  });  
               });
                   
                   
                   //fill up course title dropdown based on faculty selected
                   $("#deptDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>classreps/Portal/buildDrpCourseTitle",  
                        data: {id: $(this).val()},  
                        type: "POST",  
                        success:function(data){$("#courseTitleDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up course code dropdown based on faculty selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>classreps/Portal/buildDrpCourseCode",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#courseCodeDrp").html(data);}  
                  });  
               });
                   
                   
                   
                    //fill up course lecturer dropdown based on course code selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>classreps/Portal/buildDrpLecturer",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#lecturerDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up course lecturer dropdown based on course code selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>classreps/Portal/buildDrpLecturerEmail",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#lecturerEmailDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up  lecture venue dropdown based on course title selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>classreps/Portal/buildVenueDrp",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#venueDrp").html(data);}  
                  });  
               });
                   
                   
                   
                   
                   
                   
            });  
         </script>  


</head>
<body class="page-body  page-fade gray" data-url="http://neon.dev">
     <?php if($this->session->userdata('rep_id')){
    
} else{
    redirect('login/user_login');;
}
    ?>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/futlogo.png" width="120"  style="max-height:120px; margin-right:10px;" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="opened active">
					<a href="index.html">
						<i class="entypo-gauge"></i>
						<span class="title">View Activites</span>
					</a>
					<ul>
						<li>
						<?php echo anchor('classreps/Portal/today_activities/','Today List','class="entypo-chart-line"'); ?>
							
						</li>
						<li>
						<?php echo anchor('classreps/Portal/previous_activities/' ,'Previous List','class="entypo-chart-line"'); ?>
				
						</li>            
					</ul>
				</li>
				                

				<li>
					<a href="mailbox.html">
						<i class="entypo-mail"></i>
						<span class="title">Mailbox</span>
						<span class="badge badge-secondary">8</span>
					</a>
					<ul>
						<li>
							<a href="mailbox.html">
								<i class="entypo-inbox"></i>
								<span class="title">Inbox</span>
							</a>
						</li>
						<li>
							<a href="mailbox-compose.html">
								<i class="entypo-pencil"></i>
								<span class="title">Compose Message</span>
							</a>
						</li>
						<li>
							<a href="mailbox-message.html">
								<i class="entypo-attach"></i>
								<span class="title">View Message</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
			
		</div>

	</div>

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">

							 <!--To display class rep profile image using session variable -->
							<img src="<?php echo $this->Manageprofile_model->get_image_url_rep($this->session->userdata('rep_id'));?>" alt="QAP" class="img-circle" width="80" />
							

						<div  style="font-size:1.0em; color:purple" class="label label-info"><?php echo $this->session->userdata('email'); ?></div>
					
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="<?php echo base_url();?>classreps/Manageprofile/rep_profile" >
                        	     <i class="entypo-info"></i>
							     <span><?php echo 'edit_profile';?></span>
						          </a>
							</li>
		
							<li>
								<a href="<?php echo base_url();?>classreps/Manageprofile/update_rep_password/" >
                        	     <i class="entypo-key"></i>
							     <span><?php echo 'change_password';?></span>
						          </a>
                            </li>
						</ul>
					</li>
				</ul>		
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix">
		
				<ul class="list-inline links-list pull-right">
		
					<li>
                        <a href="../fms">
					<h4><?php echo anchor('login/logout_rep', 'Log Out', 'title="logout"' ) ?>
					<i class="entypo-logout right"></i></h4>
					</a>
					</li>
					<?php echo "|" ; ?>
					<li>
                     <a href="../fms">
					<h4><?php echo anchor('classreps/portal', 'Home', 'title="home"' ) ?>
					<i class="entypo-home right"></i></h4>
					</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			// Sample Toastr Notification
			setTimeout(function()
			{			
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
					"toastClass": "green",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
		
				toastr.success("<?php echo $this->session->userdata('email'); ?>!", "You Have succesfully been Looged-In", opts);
			}, 3000);
			
			});
		
		
		function getRandomInt(min, max) 
		{
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		</script>
        
        <!-- Display Login Success by System Admin -->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
		
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
		<div class="row">
		<div class="col-md-3">		
		<strong><h4>
		<?php echo $today = date("F j, Y, g:i a"); ?>
		</h4></strong>
		</div>
		<div class="col-md-4">
		<?php 
		
		$this->db->select('*');
		$this->db->from('session_table');
		$query = $this->db->get()->result();
        foreach($query as $row_session){
			
			echo '<div class="alert alert-warning">';
			echo $row_session->session_name . " " . "Academic Session" . " - " . $row_session->semester_name;
			echo '</div>';
		}
		
		
		
		?>
		</div>
		</div>
		
		
		<hr>
		 <div class="alert alert-info" style="text-align:center; font-size:1.5em; font-style: oblique;font-weight: bold">
            Quality Assurance and Productivity Welcome to Your Portal - <?php echo $this->session->userdata('rep_matric_no'); ?>
        </div>
        
        <!-- Load Main View-->
   			<?php $this->load->view($main) ?>
            
		<!-- Footer -->
		<footer class="main" style="text-align:center">
			
			&copy; 2018 <strong>Futminna Quality Assurance and Productivity</strong> Designed by <a href="#">Johnson Obinna Innocent (2013/2/48900CI)</a>
		
		</footer>
	</div>
	
</div>





	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/rickshaw/rickshaw.min.css">

	<!-- Bottom scripts (common) -->
	<script src="<?php echo base_url(); ?>assets/js/gsap/main-gsap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/joinable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-api.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>


	<!-- Imported scripts on this page -->
	<script src="<?php echo base_url(); ?>assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/raphael-min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/morris.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/toastr.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-chat.js"></script>
    


	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url(); ?>assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="<?php echo base_url(); ?>assets/js/neon-demo.js"></script>

</body>
</html>