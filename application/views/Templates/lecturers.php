<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Lecturer | Portal</title>

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/green.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.js"></script>  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.bundle.min.js"></script>

      
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
    
    
    
    <script type="text/javascript"> 
                   
                   //fillup the department dropdown based on faculty selected
                  $(document).ready(function() {  
                     $("#facultyDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo  
                        base_url();?>lecturers/Portal_lecturer/buildDrpDepts",  
                        data: {id: $(this).val()},  
                        type: "POST",  
                        success:function(data){$("#deptDrp").html(data);  
                     }  
                  });  
               });
                   
                   
                   //fill up course title dropdown based on faculty selected
                   $("#deptDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>lecturers/Portal_lecturer/buildDrpCourseTitle",  
                        data: {id: $(this).val()},  
                        type: "POST",  
                        success:function(data){$("#courseTitleDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up course code dropdown based on faculty selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>lecturers/Portal_lecturer/buildDrpCourseCode",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#courseCodeDrp").html(data);}  
                  });  
               });
                   
                   
                   
                    //fill up course lecturer dropdown based on course code selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>lecturers/Portal_lecturer/buildDrpLecturer",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#lecturerDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up course lecturer dropdown based on course code selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>lecturers/Portal_lecturer/buildDrpLecturerEmail",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#lecturerEmailDrp").html(data);}  
                  });  
               });
                   
                   
                   //fill up  lecture venue dropdown based on course title selected
                   $("#courseTitleDrp").change(function(){  
                     /*dropdown post *///  
                     $.ajax({  
                        url:"<?php echo base_url();?>lecturers/Portal_lecturer/buildVenueDrp",  
                        data: {id:$(this).val()},  
                        type: "POST",  
                        success:function(data){$("#venueDrp").html(data);}  
                  });  
               });
                   
                   
                   
                   
                   
                   
            });  
         </script>  
    
 
 
</head>
<body class="page-body page-fade skin-green" data-url="http://neon.dev">
    <?php if($this->session->userdata('lecturer_id')){
    
} else{
    redirect('login/user_login');
}
    ?>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/futlogo.png" width="120"  style="max-height:120px; margin-right:10px;" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>	
			</header>
			

								
			<ul id="main-menu" class="main-menu multiple-expanded">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

				<!--View Lectures by Department -->
				<li class="active">
					<a href="<?php echo base_url(); ?>lecturers/Portal_lecturer">
                		<i class="entypo-gauge"></i>
                		<span>Todays Activities</span>
            		</a>
					
				</li>

				<!--View Semesteral Summary  -->
				<li>
					<a href="layout-api.html">
						<i class=" entypo-eye"></i>
						<span class="title">Semesterial Summary</span>
					</a>
					<ul>
						<li>
							 <a href="<?php echo base_url(); ?>lecturers/Portal_lecturer/semesterial_summary">
								<span class="title">Activities Summary</span>
							</a>
						</li>												
										
					</ul>
				</li>
                
                <!-Perform Lecture update -->
				<li class="">
					<a href="<?php echo base_url(); ?>lecturers/Portal_lecturer/update_activity">
                		<i class="entypo-pencil"></i>
                		<span>Update Lecture Activites</span>
            		</a>
					
				</li>
                
                <!-Perform Lecture update -->
				<li class="">
					<a href="<?php echo base_url(); ?>lecturers/Portal_lecturer/AORform">
                		<i class="entypo-pencil"></i>
                		<span>AOR FORM</span>
            		</a>
					
				</li>
                
                
                <!--View update activity by lecturer-->
				<li class="">
					<a href="<?php echo base_url(); ?>lecturers/Portal_lecturer/myupdates">
                		<i class="entypo-eye"></i>
                		<span>View Lecture Activites</span>
            		</a>
					
				</li>
            </ul>
			
		</div> 

	</div><!--  Close Sidebar -->

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">

							 <!--To display Admin profile image using session variable -->
							<img src="<?php echo $this->Manageprofile_model->get_image_url_lecturer($this->session->userdata('lecturer_id'));?>" alt="QAP" class="img-circle" width="80" />
							Welcome <?php echo $this->session->userdata('lecturer_fname'). " " . $this->session->userdata('lecturer_sname');?>
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
                                
                                
								<!-- <//?php echo anchor('admin/manageprofile/admin_profile/' . $this->session->userdata('admin_id') . '', 'Edit Profile', 'class="entypo-user"'); ?>-->
								<a href="<?php echo base_url();?>lecturers/Manageprofile/lecturer_profile" >
                        	     <i class="entypo-info"></i>
							     <span><?php echo 'edit_profile';?></span>
						          </a>
                                
							</li>	
                            
							<li>
                                <a href="<?php echo base_url();?>lecturers/Manageprofile/update_lecturer_password/" >
                        	     <i class="entypo-key"></i>
							     <span><?php echo 'change_password';?></span>
						          </a>
							</li>
		
						</ul>
					</li>
				</ul>
				
				
			
			</div><!--Close Profile Notification-- >
		
		
			<!-- Raw Links -->			
			<div class="col-md-6 col-sm-4 clearfix">
		
				<ul class="list-inline links-list pull-right">
		
					<li>
                        <a href="../fms">
					<h4><?php echo anchor('login/logout_lecturer', 'Log Out', 'title="logout"' ) ?>
					<i class="entypo-logout right"></i></h4>
					</a>
					</li>
					<?php echo "|" ; ?>
					<li>
                     <a href="../fms">
					<h4><?php echo anchor('lecturers/Portal_lecturer', 'Home', 'title="home"' ) ?>
					<i class="entypo-home right"></i></h4>
					</a>
					</li>
				</ul>
		
			</div><!--Close Raw Links-->
		
		</div>
		 <br>
		<div class="row"> 
		<div class="col-md-2">
		<div  style="font-size:1em" class="label label-primary"><?php echo $this->session->userdata('email'); ?></div>
		</div>
		<div class="col-md-2">
		<?php echo $today = date("F j, Y, g:i a"); ?>
		</div>
		<div class="col-md-2">
		
		<?php echo $today = date("l"); ?>
		</div>
		
		<div class="col-md-4">
		<?php 
		
		$this->db->select('*');
		$this->db->from('session_table');
		$query = $this->db->get()->result();
        foreach($query as $row_session){
			
			echo '<button type="button" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="glyphicon glyphicon-info"></i></span>';
			echo $row_session->session_name . " " . "Academic Session" . " - " . $row_session->semester_name;
			echo '</button>';
		}
		
		
		
		?>
		</div>
		</div>
		
		<hr />
        
<!--
        <script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			// Sample Toastr Notification
			setTimeout(function()
			{			
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
					"toastClass": "green",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
		
				toastr.success("<//?php echo $this->session->userdata('email'); ?>", "You Have succesfully been Looged-In", opts);
			}, 3000);
			
			});
		
		
		function getRandomInt(min, max) 
		{
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		</script>
        
-->
        <!-- Display Login Success by System Admin -->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        

	
        <div class="alert alert-info" style="text-align:center; font-size:1.5em; font-style: oblique;font-weight: bold">
            QAP LECTURE EVALUATION AND MONITORING INFORMATION SYSTEM DASHBOARD
        </div>
	
		<hr />
		
		<br>
			<div>
				
				<!-- Load View into the template for admin-->
                
				<?php $this->load->view($main); ?>
				
				
				
				
				
								
		
		
		<!-- Footer -->
		<footer class="main" style="text-align:center">
			
			&copy; 2018 <strong>Futminna Quality Assurance and Productivity</strong> Designed by <a href="#">Johnson Obinna Innocent (2013/2/48900CI)</a>
		
		</footer>
	</div>
	
</div>


</div>


	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/zurb-responsive-tables/responsive-tables.css">

	<!-- Bottom scripts (common) -->
	<script src="<?php echo base_url(); ?>assets/js/gsap/main-gsap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/joinable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-api.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script> 
	<script src="<?php echo base_url(); ?>assets/js/datatables/TableTools.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="<?php echo base_url(); ?>assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-chat.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fileinput.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/lodash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/responsive/js/datatables.responsive.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-chat.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/toastr.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url(); ?>assets/js/neon-custom.js"></script>
	


	<!-- Demo Settings -->
	<script src="<?php echo base_url(); ?>assets/js/neon-demo.js"></script>

</body>
</html>