<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Admin | Dashboard</title>

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/purple.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.js"></script>  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.bundle.min.js"></script>

      
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
    
    
<!--
    <script type="text/javascript"> 
        $(document).ready(function() {  
          setInterval(function(){
             $('table-4').load("//<//?php echo base_url();?>/admin/Dashboard/index").fadeIn("slow") 
          }, 1000);          
                   
        });  
    </script>  
-->
 
 
</head>
<body class="page-body page-fade skin-purple" data-url="http://neon.dev">
    <?php if($this->session->userdata('admin_id')){
    
} else{
    redirect('login/user_login');
}
    ?>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/futlogo.png" width="120"  style="max-height:120px; margin-right:10px;" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>	
			</header>
			

								
			<ul id="main-menu" class="main-menu multiple-expanded">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

				<!--View Lectures by Department -->
				<li class="active">
					<a href="<?php echo base_url(); ?>admin">
                		<i class="entypo-gauge"></i>
                		<span>Dashboard</span>
            		</a>
					
				</li>

				<!--SICT View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class=" entypo-eye"></i>
						<span class="title">SICT</span>
					</a>
					<ul>
						<li>
							 <a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/infotech">
								<span class="title">Information And Media Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/compsci">
								<span class="title">Computer Science</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/cybersec">
								<span class="title">Cyber Security Science</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/libinfotech">
								<span class="title">Library Information Technology</span>
							</a>
						</li>
												
										
					</ul>
				</li>


				<!--SEET View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class="entypo-eye"></i>
						<span class="title">SEET</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/agricbiores">
								<span class="title">Agriculture and Bioresources Engineering</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/chemeng">
								<span class="title">Chemical Engineering</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/mecheng">
								<span class="title">Mechanical Engineering</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/civileng">
								<span class="title">Civil Engineering</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/compeng">
								<span class="title">Computer Engineering</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/telecom">
								<span class="title">Telecommunication Engineering</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/electeng">
								<span class="title">Electrical and Electronics Engineering</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/mechatroeng">
								<span class="title">Mechatronics Engineering</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/metallureng">
								<span class="title">Meterials and Metallurgical Engineering</span>
							</a>
						</li>
												
										
					</ul>
				</li>



				<!--SAAT View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class="entypo-eye"></i>
						<span class="title">SAAT</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/agricext">
								<span class="title">Agricultural Economics and Extension Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/animalprod">
								<span class="title">Animal Production</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/cropprod">
								<span class="title">Crop Production</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/foodtech">
								<span class="title">Food Science and Nutrition Technology</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/soilsci">
								<span class="title">Soil Science and Land management and Water Resources</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/aquafishtech">
								<span class="title">Aquaculture and Fisheries Technology</span>
							</a>
						</li>										
										
					</ul>
				</li>


				<!--SET View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class=" entypo-eye"></i>
						<span class="title">SET</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/archi">
								<span class="title">Architecture</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/building">
								<span class="title">Building Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/estatemgt">
								<span class="title">Estate survey and Valuation</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/quantsurv">
								<span class="title">Quantity Surveying</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/svg">
								<span class="title">Surveying and Geo-informatics</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/urp">
								<span class="title">Urban and Regional Planning</span>
							</a>
						</li>										
										
					</ul>
				</li>



				<!--SEMT View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class="entypo-eye"></i>
						<span class="title">SEMT</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/projmgt">
								<span class="title">Project Management Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/tranmgt">
								<span class="title">Transport Management Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/ebs">
								<span class="title">Entrepreneurship and Business Studies</span>
							</a>
						</li>
						<li>
																				
					</ul>
				</li>


				<!--SPS View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class=" entypo-eye"></i>
						<span class="title">SPS</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/chem">
								<span class="title">Chemistry</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/geography">
								<span class="title">Geography</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/geology">
								<span class="title">Geology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/math">
								<span class="title"> Mathematics</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/physics">
								<span class="title">Physics </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/stat">
								<span class="title">Statistics</span>
							</a>
						</li>										
										
					</ul>
				</li>



				<!--SLS View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class=" entypo-eye"></i>
						<span class="title">SLS</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/biosci">
								<span class="title">Biological Sciences</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/biochem">
								<span class="title">Biochemistry</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/microbio">
								<span class="title">Microbiology</span>
							</a>
						</li>									
					</ul>
				</li>



				<!--SSTE View Lecture Section  -->
				<li>
					<a href="layout-api.html">
						<i class="entypo-eye"></i>
						<span class="title">SSTE</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/indtechedu">
								<span class="title">Industrial and Technology Education</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/sciedu">
								<span class="title">Science Education</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/edutech">
								<span class="title">Educational Technology</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Ongoinlecture_controller/commedu">
								<span class="title">Communication Education</span>
							</a>
						</li>									
										
					</ul>
				</li>


				<!---Registration Forms -->
				<li>
					<a href="forms-main.html">
						<i class="entypo-doc-text"></i>
						<span class="title">Registrations</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registercourserep">
								<span class="title">Class Representative</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registerlecturer">
								<span class="title">Lecturers</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registerfaculty">
								<span class="title">Faculties</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registerdept">
								<span class="title">Departments</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registervenue">
								<span class="title">Venues</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Registration_controller/registercourse">
								<span class="title">Courses</span>
							</a>
						</li>
						
					</ul>
				</li>



				<!---View database contents and update -->
				<li>
					<a href="forms-main.html">
						<i class="entypo-pencil"></i>
						<span class="title">View and Update</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_courserep">
								<span class="title">Class Representative</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_lecturer">
								<span class="title">Lecturers</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_faculty">
								<span class="title">Faculties</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_department">
								<span class="title">Departments</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_venue">
								<span class="title">Venues</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>admin/Update_view_controller/view_course">
								<span class="title">Courses</span>
							</a>
						</li>
						
					</ul>
				</li>
                
                
                <!--- Statistics -->
                <li>
					<a href="layout-api.html">
						<i class="entypo-chart-bar"></i>
						<span class="title">STATISTICS</span>
					</a>
					<ul>
						<li>
                            <a href="<?php echo base_url(); ?>admin/Statistics_controller/index">
								<span class="title">FACULTIES STATISTICS</span>
							</a>
						</li>
						
                        <!--LINK SICT STATISTICS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SICT</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sict_statistics">
										<span class="title">SICT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/imt_statistics">
										<span class="title">Information and Media Tech</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/cpt_statistics">
										<span class="title">Computer Science</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/css_statistics">
										<span class="title">Cyber Security Science</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/lit_statistics">
										<span class="title">Library and Information Science</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!-- LINK SEET STATISTICS -->
				        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEET</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/seet_statistics">
										<span class="title">SEET - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/abe_statistics">
										<span class="title">Agric Bioresources Engineering</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/elect_statistics">
										<span class="title">Electrical Engineering</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/mech_statistics">
										<span class="title">Mechanical Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/mecha_statistics">
										<span class="title">Mechatronics Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/comp_statistics">
										<span class="title">Computer Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/telecom_statistics">
										<span class="title">Telecommunication Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/metallurg_statistics">
										<span class="title">Metallurgical Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/civil_statistics">
										<span class="title">Civil Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/chem_statistics">
										<span class="title">Chemical Engineering</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SEMT STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEMT</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/semt_statistics">
										<span class="title">SEMT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/ebs_statistics">
										<span class="title">Entrepreneurship and Business Studies</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/pmt_statistics">
										<span class="title">Project Management</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/tmt_statistics">
										<span class="title">Transport Management</span>
									</a>
								</li>
                               
							</ul>
						</li>
                        
                        <!--LINK FOR SET STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SET</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/set_statistics">
										<span class="title">SET - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/arc_statistics">
										<span class="title">Architecture</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/bld_statistics">
										<span class="title">Building Technology</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/est_statistics">
										<span class="title">Estate survey and Valuation</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/qts_statistics">
										<span class="title">Quantity Surveying</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/svg_statistics">
										<span class="title">Surveying and Geo-informatics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/urp_statistics">
										<span class="title">Urban and Regional Planning</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SAAT -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SAAT</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/saat_statistics">
										<span class="title">SAAT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/aet_statistics">
										<span class="title">Agricultural Economics and Extension Technology</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/apt_statistics">
										<span class="title">Animal Production</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/crp_statistics">
										<span class="title">Crop Production</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/fst_statistics">
										<span class="title">Food Science and Technology</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/ssd_statistics">
										<span class="title">Soil Science and Land management and Water Resources</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/aft_statistics">
										<span class="title">Aquaculture and Fisheries Technology</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SPS STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SPS</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sps_statistics">
										<span class="title">SPS - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/chem_statistics">
										<span class="title">Chemistry</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/geo_statistics">
										<span class="title">Geography</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/geol_statistics">
										<span class="title">Geology</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/math_statistics">
										<span class="title">Mathematics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/phy_statistics">
										<span class="title">Physics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/stat_statistics">
										<span class="title">Statistics</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SLS STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SLS</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sls_statistics">
										<span class="title">SLS - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/bio_statistics">
										<span class="title">Biological Sciences</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/biochem_statistics">
										<span class="title">Biochemistry</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/micro_statistics">
										<span class="title">Microbiology</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SSTE STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SSTE</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/ste_statistics">
										<span class="title">SSTE - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/ite_statistics">
										<span class="title">Industrial and Technology Education</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/sciedu_statistics">
										<span class="title">Science Education</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/edutech_statistics">
										<span class="title">Educational Technology</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/commedu_statistics">
										<span class="title">Communication Education</span>
									</a>
								</li>
							</ul>
						</li>            
					</ul>
				</li>
                <!-- END sTATISTICS -->
                
                <!--- Registration for Daily Summary for Faculties Departments -->
                <li>
					<a href="layout-api.html">
						<i class="entypo-doc-text"></i>
						<span class="title">REG. DAILY LECTURE LIMIT</span>
					</a>
					<ul>
						<li>
                            <a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/facultyMax">
								<span class="title">FACULTIES LIMIT REG.</span>
							</a>
						</li>
						
                        <!--LINK SICT DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SICT Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/sictMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SEET DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEET Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/seetMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SAAT DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SAAT Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/saatMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SEMT DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEMT Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/semtMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SET DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SET Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/setMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SPS DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SPS Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/spsMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SLS DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SLS Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/slsMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK STE DAILY LECTURE LIMITS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">STE Daily Limit</span>
							</a>
							<ul>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Daily_max_lecture_controller/steMax">
										<span class="title">Depts Daily Limits</span>
									</a>
								</li>
							</ul>
						</li>
                                  
					</ul>
				</li>
                <!--- End for Registration for Daily Summary for Departments  -->
                
                 <!--- START SEMESTERIAL SUMMARY STATISTICS -->
                <li>
					<a href="layout-api.html">
						<i class="entypo-chart-pie"></i>
						<span class="title">SEMESTERIAL STATISTICS</span>
					</a>
					<ul>
						<li>
                            <a href="<?php echo base_url(); ?>admin/Statistics_controller/facultysummary">
								<span class="title">Faculties Semesterial Summary</span>
							</a>
						</li>
						
                        <!--LINK SICT STATISTICS -->
						<li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SICT Sumamry</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sict_statisticssummary">
										<span class="title">SICT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/imt_statisticssummary">
										<span class="title">Information and Media Tech</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/cpt_statisticssummary">
										<span class="title">Computer Science</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/css_statisticssummary">
										<span class="title">Cyber Security Science</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Sictdept_controller/lit_statisticssummary">
										<span class="title">Library and Information Science</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!-- LINK SEET STATISTICS -->
				        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEET Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/seet_statisticssummary">
										<span class="title">SEET - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/abe_statisticssummary">
										<span class="title">Agric Bioresources Engineering</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/elect_statisticssummary">
										<span class="title">Electrical Engineering</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/mech_statisticssummary">
										<span class="title">Mechanical Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/mecha_statisticssummary">
										<span class="title">Mechatronics Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/cpe_statisticssummary">
										<span class="title">Computer Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/telecom_statisticssummary">
										<span class="title">Telecommunication Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/metallurg_statisticssummary">
										<span class="title">Metallurgical Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/civil_statisticssummary">
										<span class="title">Civil Engineering</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Seetdept_controller/chem_statisticssummary">
										<span class="title">Chemical Engineering</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK SEMT STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SEMT Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/semt_statisticssummary">
										<span class="title">SEMT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/ebs_statisticssummary">
										<span class="title">Entrepreneurship and Business Studies</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/pmt_statisticssummary">
										<span class="title">Project Management</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Semtdept_controller/tmt_statisticssummary">
										<span class="title">Transport Management</span>
									</a>
								</li>
                               
							</ul>
						</li>
                        
                        <!--LINK FOR SET STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SET Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/set_statisticssummary">
										<span class="title">SET - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/arc_statisticssummary">
										<span class="title">Architecture</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/bld_statisticssummary">
										<span class="title">Building Technology</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/est_statisticssummary">
										<span class="title">Estate survey and Valuation</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/qts_statisticssummary">
										<span class="title">Quantity Surveying</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/svg_statisticssummary">
										<span class="title">Surveying and Geo-informatics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Setdept_controller/urp_statisticssummary">
										<span class="title">Urban and Regional Planning</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SAAT -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SAAT Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/saat_statisticssummary">
										<span class="title">SAAT - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/aet_statisticssummary">
										<span class="title">Agricultural Economics and Extension Technology</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/apt_statisticssummary">
										<span class="title">Animal Production</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/crp_statisticssummary">
										<span class="title">Crop Production</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/fst_statisticssummary">
										<span class="title">Food Science and Technology</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/ssd_statisticssummary">
										<span class="title">Soil Science and Land management and Water Resources</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Saatdept_controller/aft_statisticssummary">
										<span class="title">Aquaculture and Fisheries Technology</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SPS STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SPS Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sps_statisticssummary">
										<span class="title">SPS - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/chemis_statisticssummary">
										<span class="title">Chemistry</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/geo_statisticssummary">
										<span class="title">Geography</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/geol_statisticssummary">
										<span class="title">Geology</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/math_statisticssummary">
										<span class="title">Mathematics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/phy_statisticssummary">
										<span class="title">Physics</span>
									</a>
								</li>
                                 <li>
									<a href="<?php echo base_url(); ?>admin/Spsdept_controller/stat_statisticssummary">
										<span class="title">Statistics</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SLS STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SLS Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/sls_statisticssummary">
										<span class="title">SLS - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/bio_statisticssummary">
										<span class="title">Biological Sciences</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/biochem_statisticssummary">
										<span class="title">Biochemistry</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Slsdept_controller/micro_statisticssummary">
										<span class="title">Microbiology</span>
									</a>
								</li>
							</ul>
						</li>
                        
                        <!--LINK FOR SSTE STATISTICS -->
                        <li>
							<a href="layout-page-transition-fade.html">
								<span class="title">SSTE Summary</span>
							</a>
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>admin/Statistics_controller/ste_statisticssummary">
										<span class="title">SSTE - Statistics</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/ite_statisticssummary">
										<span class="title">Industrial and Technology Education</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/sciedu_statisticssummary">
										<span class="title">Science Education</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/edutech_statisticssummary">
										<span class="title">Educational Technology</span>
									</a>
								</li>
                                <li>
									<a href="<?php echo base_url(); ?>admin/Stedept_controller/commedu_statisticssummary">
										<span class="title">Communication Education</span>
									</a>
								</li>
							</ul>
						</li>            
					</ul>
				</li>
                <!-- END SEMESTERIAL SUMMARY TATISTICS -->
				
				
				<!-- link for settings-->
				<li>
					<a href="layout-api.html">
						<i class="entypo-lock"></i>
						<span class="title">SETTINGS</span>
					</a>
					<ul>
						<li>
                            <a href="<?php echo base_url(); ?>admin/Settings/session_semester">
								<span class="title">SEMESTER/SESSION SETTINGS.</span>
							</a>
						</li>
					</ul>
				
				</li>
								
            </ul>
			
		</div> 

	</div><!--  Close Sidebar -->

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">

							 <!--To display Admin profile image using session variable -->
							<img src="<?php echo $this->Manageprofile_model->get_image_url($this->session->userdata('admin_id'));?>" alt="QAP" class="img-circle" width="80" />
							Welcome Admin
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
                                
                                
								<!-- <//?php echo anchor('admin/manageprofile/admin_profile/' . $this->session->userdata('admin_id') . '', 'Edit Profile', 'class="entypo-user"'); ?>-->
								<a href="<?php echo base_url();?>admin/Manageprofile/admin_profile" >
                        	     <i class="entypo-info"></i>
							     <span><?php echo 'edit_profile';?></span>
						          </a>
                                
							</li>	
                            
							<li>
                                <a href="<?php echo base_url();?>admin/Manageprofile/update_admin_password/" >
                        	     <i class="entypo-key"></i>
							     <span><?php echo 'change_password';?></span>
						          </a>
							</li>
		
						</ul>
					</li>
				</ul>
				
				
			
			</div><!--Close Profile Notification-- >
		
		
			<!-- Raw Links -->			
			<div class="col-md-6 col-sm-4 clearfix">
		
				<ul class="list-inline links-list pull-right">
		
					<li>
                        <a href="../fms">
					<h4><?php echo anchor('login/logout_admin', 'Log Out', 'title="logout"' ) ?>
					<i class="entypo-logout right"></i></h4>
					</a>
					</li>
					<?php echo "|" ; ?>
					<li>
                     <a href="../fms">
					<h4><?php echo anchor('admin', 'Home', 'title="home"' ) ?>
					<i class="entypo-home right"></i></h4>
					</a>
					</li>
				</ul>
		
			</div><!--Close Raw Links-->
		
		</div>
		 <br>
		<div class="row"> 
		<div class="col-md-2">
		<div  style="font-size:1em" class="label label-primary"><?php echo $this->session->userdata('email'); ?></div>
		</div>
		<div class="col-md-2">
		<?php echo $today = date("F j, Y, g:i a"); ?>
		</div>
		<div class="col-md-2">
		
		<?php echo $today = date("l"); ?>
		</div>
		
		<div class="col-md-4">
		<?php 
		
		$this->db->select('*');
		$this->db->from('session_table');
		$query = $this->db->get()->result();
        foreach($query as $row_session){
			
			echo '<button type="button" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="glyphicon glyphicon-info"></i></span>';
			echo $row_session->session_name . " " . "Academic Session" . " - " . $row_session->semester_name;
			echo '</button>';
		}
		
		
		
		?>
		</div>
		</div>
		
		<hr />
        
<!--
        <script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			// Sample Toastr Notification
			setTimeout(function()
			{			
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
					"toastClass": "green",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
		
				toastr.success("<//?php echo $this->session->userdata('email'); ?>", "You Have succesfully been Looged-In", opts);
			}, 3000);
			
			});
		
		
		function getRandomInt(min, max) 
		{
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		</script>
        
-->
        <!-- Display Login Success by System Admin -->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        

	
        <div class="alert alert-info" style="text-align:center; font-size:1.5em; font-style: oblique;font-weight: bold">
            QAP LECTURE EVALUATION AND MONITORING INFORMATION SYSTEM DASHBOARD
        </div>
	
		<hr />
		
		<br>
			<div>
				
				<!-- Load View into the template for admin-->
                
				<?php $this->load->view($main); ?>
				
				
				
				
				
								
		
		
		<!-- Footer -->
		<footer class="main" style="text-align:center">
			
			&copy; 2018 <strong>Futminna Quality Assurance and Productivity</strong> Designed by <a href="#">Johnson Obinna Innocent (2013/2/48900CI)</a>
		
		</footer>
	</div>
	
</div>


</div>


	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/zurb-responsive-tables/responsive-tables.css">

	<!-- Bottom scripts (common) -->
	<script src="<?php echo base_url(); ?>assets/js/gsap/main-gsap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/joinable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-api.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script> 
	<script src="<?php echo base_url(); ?>assets/js/datatables/TableTools.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="<?php echo base_url(); ?>assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-chat.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fileinput.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/lodash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/datatables/responsive/js/datatables.responsive.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/neon-chat.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/toastr.js"></script>
	


	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo base_url(); ?>assets/js/neon-custom.js"></script>
	


	<!-- Demo Settings -->
	<script src="<?php echo base_url(); ?>assets/js/neon-demo.js"></script>

</body>
</html>